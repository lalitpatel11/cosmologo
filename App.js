//import : react components
import React, {useEffect} from 'react';
import {
  View,
  SafeAreaView,
  StatusBar,
  Alert,
  Platform,
  Text,
  LogBox,
  Linking,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
//import : notification
import {NotificationAndroid} from './NotificationAndroid';
import {NotificationManagerIOS} from './NotificationManagerIOS';
//import : third parties
import messaging from '@react-native-firebase/messaging';
import {StripeProvider} from '@stripe/stripe-react-native';
import VersionCheck from 'react-native-version-check';
//import : globals
import {Colors} from 'global/Index';
//import : stack
import Drawer from 'navigation/Drawer/Drawer';
//import : redux
import {Provider} from 'react-redux';
import {store} from 'src/reduxToolkit/store/store';

const App = () => {
  //function
  async function requestUserPermission() {
    const authorizationStatus = await messaging().requestPermission({
      sound: false,
      announcement: true,
    });
  }
  const getToken = async () => {
    const token = await messaging().getToken();
    console.warn('token', token);
  };

  async function requestUserPermissionIos() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  }
  const getUrl = async () => {
    return await VersionCheck.getStoreUrl({});
  };
  //hook : useEffect
  useEffect(() => {
    if (Platform.OS === 'android') {
      var playStoreUrl = `https://play.google.com/store/apps/details?id=com.cosmologo`;
      var currentVersion = VersionCheck.getCurrentVersion();
      var url = getUrl();
      // VersionCheck.getLatestVersion({
      //   appID: '1658523173',
      //   provider: 'appStore',
      // }).then(res => {
      //   //App Store ID for iBooks.
      //   console.log('getStoreUrl', res);
      //   // this.setState({ storeUrl: res });
      // });
      VersionCheck.getLatestVersion({}).then(latestVersion => {
        VersionCheck.needUpdate({
          currentVersion: currentVersion,
          latestVersion: latestVersion,
        }).then(res => {
          console.log('url======', url._W);
          if (res.isNeeded) {
            Alert.alert(
              'App update available',
              'Cosmologo new version available please click on update to update app to latest version',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {
                  text: 'UPDATE',
                  onPress: () =>
                    Linking.openURL(Platform.OS === 'android' ? url._W : ''),
                },
              ],
            );
          }
        });
      });
    }

    NotificationAndroid.createChannel();
    NotificationAndroid.configure();
    try {
      if (Platform.OS == 'android') {
        requestUserPermission();
      } else {
        requestUserPermissionIos();
      }
      const unsubscribe = messaging().onMessage(async remoteMessage => {
        JSON.stringify(remoteMessage.data);
        const {messageId} = remoteMessage;
        const data = remoteMessage.notification;
        if (Platform.OS === 'android') {
          console.warn('data--->', data);
          NotificationAndroid.showNotification(
            data.title,
            data.body,
            data.subText,
            messageId,
            data,
          );
        } else {
          NotificationManagerIOS.showNotification(
            messageId,
            data.title,
            data.body,
            data,
            {},
          );
        }
      });
      return unsubscribe;
    } catch (error) {
      console.log(error.message);
    }
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      const {data, messageId} = remoteMessage;
      const {Title, notificationText, subText} = data;
      if (Platform.OS === 'android') {
        NotificationAndroid.showNotification(
          Title,
          notificationText,
          subText,
          messageId,
        );
      } else {
        NotificationManagerIOS.showNotification(
          messageId,
          Title,
          notificationText,
          data,
          {},
        );
      }
    });
  }, []);
  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);
  //UI
  return (
    <StripeProvider
      publishableKey="pk_test_51Ld79cKpF4IkCBflJphGtw8rfJtFCe5S57A0JDBTiVimAAts4RaFD0FmMeQJbrTo3unAlDweLax9RTOpvb33jZFl00o8VdWHdn"
      urlScheme="your-url-scheme" // required for 3D Secure and bank redirects
      merchantIdentifier="merchant.com.{{YOUR_APP_NAME}}" // required for Apple Pay
    >
      <Provider store={store}>
        <NavigationContainer>
          <SafeAreaView style={{flex: 1}}>
            <StatusBar backgroundColor={Colors.THEME_GREEN} />
            <Drawer />
          </SafeAreaView>
        </NavigationContainer>
      </Provider>
    </StripeProvider>
  );
};
export default App;
