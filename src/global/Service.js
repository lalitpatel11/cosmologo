//import : react components
import {Alert} from 'react-native';
//third parties
//import : axios
import axios from 'axios';
import Toast from 'react-native-simple-toast';
//endpoint : base_url
// export const BASE_URL = `https://nileprojects.in/cosmologo/public/api/`;
const isProduction = true;
export const BASE_URL = isProduction
  ? `https://cosmologo.com/dev/api/`
  : `https://cosmologo.com/staging/api/`;
//endpoint : endpoints user
export const USER_REGISTER = `user-register`;
export const USER_LOGIN = `user-login`;
export const USER_LOGOUT = `logout`;
export const FORGOT_PASSWORD = `forgot-password`;
export const FORGET_PASSWORD = `forget-password`;
export const VALIDATE_CODE = `validate-code`;
export const FORGET_PASSWORD_VERIFY = `forget-password-verify`;
export const RESET_PASSWORD = `reset-password`;
export const USER_APPOINTMENT_LIST = `user-appointment-list`;
export const USER_OLD_APPOINTMENT_LIST = `user-old-appointment-list`;
export const PRODUCT_LIST = `product-list`;
export const PRODUCT_CONCERN_AREA_LIST = `product-concern-area-list`;
export const BOOK_APPOINTMENT = `book-appointment`;
export const GET_APPOINTMENT_SERVICE_PROVIDER_LIST = `get-appointment-service-provider-list`;
export const CONFIRM_APPOINTMENT = `confirm-appointment`;
export const USER_UPCOMING_APPOINTMENT_LIST = `user-upcoming-appointment-list`;
export const GET_APPOINTMENT_DETAIL = `get-appointment-detail`;
export const USER_APPOINTMENT_HISTORY_LIST = `user-appointment-history-list`;
export const SLIDER_LIST = `slider-list`;
export const SEARCH_CATEGORY = `search-category`;
export const SERVICE_PROVIDER_LIST = `service-provider-list`;
export const CHECK_BOOK_APPOINTMENT = `check-book-appointment`;
export const DOWNLOAD_INVOICE = `invoice`;
export const USER_LINKS = 'user-policy';
export const USER_BOOKINGS = 'my-bookings';
export const USER_BOOKINGS_LIST = 'user-booking-list';
export const USER_NOTIFICATION = 'notify-list-mobile-user';
export const USER_READ_NOTIFICATION = 'notify-readu-all';
export const CANCEL_BOOKING_USERSIDE = 'cancel_booking_userside';
export const MAKE_PAYMENT = 'make-appointment-payment';
export const USER_COMPLETED_APPOINTMENT_COUNT =
  'user-completed-appointment-count';
export const USER_PAYMENT_DETAILS = 'payment-details-user';
export const USER_DELETE_ACCOUNT = `account-delete-user`;
//endpoint : endpoints service provider
export const SERVICE_PAYMENT_DETAILS = 'payment-details-service';
export const SERVICE_PROVIDER_NOTIFICATION = 'notify-list-mobile-service';
export const SERVICE_PROVIDER_READ_NOTIFICATION = 'notify-reads-all';
export const SERVICE_PROVIDER_REGISTER = `service-provider-register`;
export const SERVICE_PROVIDER_LOGIN = `service-provider-login`;
export const SELECT_SERVICE = `select-service`;
export const SERVICE_PROVIDER_DOCUMENT = `service-provider-document`;
export const SERVICE_PROVIDER_COMPLETED_APPOINTMENT_COUNT =
  'serviceprovider-completed-appointment-count';
export const TOTAL_PAYMENT_RECEIVED = `total-payment-received`;
export const TOTAL_COMPLETED_APPOINTMENT = 'total-completed-appointment';
export const COMPLETED_APPOINTMENT_LIST = 'completed-appointment-list';
export const UPCOMING_BOOKED_APPOINTMENT = 'upcoming-booked-appointment';
export const ONGOING_APPOINTMENT = 'service-provider-ongoing-appointment';
export const APPOINTMENT_STATUS_LIST = 'appointment-status-list';
export const CHECK_APPOINTMENT_STATUS = 'check-appointment-status';
export const CHANGE_APPOINTMENT_STATUS = 'change-appointment-status';
export const CANCEL_APPOINTMENT = 'cancel-appointment';
export const CANCEL_APPOINTMENT_USER_SERVICE =
  'cancel-appointment-user-service';
export const GET_AVAILABILITY_STATUS = `get-availability-status`;
export const CHANGE_AVAILABILITY_STATUS = `change-availability-status`;
export const SELECTED_SERVICES = `selected-services`;
export const VERIFY_APPOINTMENT = `varify-appointment`;
export const CHECK_USER_STATUS = `check-user-status`;
export const DELETE_SELECTED_SUBCATEGORY = `delete-selected-subcategory`;
export const DELETE_SELECTED_CATEGORY = `delete-selected-category`;
export const DELETE_SELECTED_PRODUCT = `delete-selected-product`;
export const DELETE_SELECTED_AREAOFCONCERN = `delete-selected-area-concern`;
export const ADD_MORE_SERVICES = `add-more-services`;
export const SERVICE_PROVIDER_ONGOING_APPOINTMENT = `service-provider-ongoing-appointment`;
export const SERVICE_PROVIDER_APPOINTMENT_DETAIL = `service-provider-appointment-detail`;
export const SERVICE_PROVIDER_CALENDAR = `service-provider-calendar`;
export const ADD_SERVICE_UNAVAILABLE = `add-service-unavailable`;
export const VERIFY_APPOINTMENT_OTP = `verify-appointment-otp`;
export const SERVICE_LINKS = 'service-policy';
export const SLOT_DELETE = 'slot-delete';
export const SLOT_EDIT = 'slot-edit';
export const SERVICE_PROVIDER_BOOKINGS_LIST = 'service-provider-bookinglist';
export const SERVICE_PROVIDER_TOTAL_PAYMENT = 'service-provider-totalpayment';
export const ADD_BUFFER_TIME = 'buffer-time';
export const SERVICE_UPDATE_PROFILE = `update-profile-service`;
export const SERVICE_APPROVED_STATUS = `approved-status`;
export const SERVICE_DELETE_ACCOUNT = `account-delete-service`;
//endpoint : endpoints other
export const UPDATE_PROFILE = `update-profile`;
export const SERVICE_SUB_CATEGORY = 'service-sub-category-list';
export const GET_SERVICE_CATEGORY = 'service-category-list';
export const UPDATE_LOCATION = 'update-location';
export const CHANGE_PASSWORD = 'change-password';
export const CHECK_TOKEN_EXPIRY = 'checktokeinexpiry';
export const CONTACT_US = 'contact-us';
//function :  get api
export const getApi = endPoint =>
  axios
    .get(`${BASE_URL}${endPoint}`)
    .then(res => {
      return res;
    })
    .catch(error => {
      if (error?.response?.status === 422) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
        console.log(error.response.headers);
      } else if (error?.response?.status === 404) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      } else if (error?.response?.status === 401) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      } else if (error?.response?.status === 500) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      } else {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      }
    });
//function :  get api with token
export const getApiWithToken = (token, endPoint) =>
  axios
    .get(`${BASE_URL}${endPoint}`, {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
    .then(res => {
      return res;
    })
    .catch(error => {
      if (error?.response?.status === 422) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
        console.log(error.response.headers);
      } else if (error?.response?.status === 404) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      } else if (error?.response?.status === 401) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      } else if (error?.response?.status === 500) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      } else {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      }
    });
//function :  post api
export const postApi = (endPoint, data) =>
  axios
    .post(`${BASE_URL}${endPoint}`, data, {
      headers: {
        'Content-Type': 'multipart/form-data',
        Accept: '*/*',
      },
    })
    .then(res => {
      return res;
    })
    .catch(error => {
      console.log('data', error.response.data);
      console.log('status', error.response.status);
      console.log('header', error.response.headers);
      if (error?.response?.status === 422) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('error status', error?.response?.status);
        console.log('error message', error.response.data.message);
      } else if (error?.response?.status === 404) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('error status', error?.response?.status);
        console.log('error message', error.response.data.message);
      } else if (error?.response?.status === 401) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('error status', error?.response?.status);
        console.log('error message', error.response.data.message);
      } else if (error?.response?.status === 500) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('error status', error?.response?.status);
        console.log('error message', error.response.data.message);
      } else if (error?.response?.status === 0) {
        // Alert.alert(
        //   '',
        //   `Internet connection appears to be offline. Please check your internet connection and try again.`,
        // );
        Toast.show(
          'Internet connection appears to be offline. Please check your internet connection and try again.',
          Toast.SHORT,
        );
      } else {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('error status', error?.response?.status);
        console.log('error message', error.response.data.message);
      }
    });

//function : post api with token
export const postApiWithToken = (token, endPoint, data) =>
  axios
    .post(`${BASE_URL}${endPoint}`, data, {
      headers:
        Object.keys(data).length > 0
          ? {
              'Content-Type': 'multipart/form-data',
              Accept: '*/*',
              Authorization: `Bearer ${token}`,
            }
          : {
              'Content-Type': 'application/json',
              Accept: 'application/json',
              Authorization: `Bearer ${token}`,
            },
    })
    .then(res => {
      return res;
    })
    .catch(error => {
      if (error?.response?.status === 422) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
        console.log(error.response.headers);
      } else if (error?.response?.status === 404) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('error status', error?.response?.status);
        console.log('error message', error.response.data.message);
      } else if (error?.response?.status === 401) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('error status', error?.response?.status);
        console.log('error message', error.response.data.message);
      } else if (error?.response?.status === 500) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('error status', error?.response?.status);
        console.log('error message', error.response.data.message);
      } else {
        // Alert.alert('', `${error}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('error status', error?.response?.status);
        console.log('error message', error.response.data.message);
      }
    });
//function : post api with json data
export const postJsonApiWithToken = (token, endPoint, data) =>
  axios
    .post(`${BASE_URL}${endPoint}`, data, {
      headers: {
        'Content-Type': 'application/json',
        Accept: '*/*',
        Authorization: `Bearer ${token}`,
      },
    })
    .then(res => {
      return res;
    })
    .catch(error => {
      if (error?.response?.status === 422) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
        console.log(error.response.headers);
      } else if (error?.response?.status === 404) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      } else if (error?.response?.status === 401) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      } else if (error?.response?.status === 500) {
        // Alert.alert('', `${error.response.data.message}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      } else {
        // Alert.alert('', `${error}`);
        Toast.show(error.response.data.message, Toast.SHORT);
        console.log('data', error.response.data);
        console.log('status', error.response.status);
      }
    });
