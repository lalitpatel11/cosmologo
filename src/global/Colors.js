export const BLACK = '#000000';

export const WHITE = '#FFFFFF';

export const THEME_GREEN = '#459743';

export const LITE_GREEN = '#50a44e';

export const BG_GREEN = '#def4df';

export const THEME_BLUE = '#447c95';

export const LITE_BLUE = '#549dbd';

export const RED = '#fe0000';

export const LITE_GREY = '#808080';
