const AppLogo = require('assets/images/logo.png');
const AppIcon = require('assets/images/logo.png');
const NavIcon = require('assets/images/nav.png');
const WelcomeBG = require('assets/images/welcomeBg.png');
const HomePaymentReceived = require('assets/images/home-payment-received.png');
const NoInternetImage = require('assets/images/no_internet.png');
const ProceduresIcon = {
  Banner: require('assets/images/banner.png'),
  Proc1: require('assets/images/Procedures/1.png'),
  Proc2: require('assets/images/Procedures/2.png'),
  Proc3: require('assets/images/Procedures/3.png'),
  service: require('assets/images/facial-surgery.png'),
  serviceImage: require('assets/images/facial.png'),
  availabilityImage: require('assets/images/availability.png'),
};
const drawerItemIcon = {
  infoIcon: require('assets/images/info.png'),
  termIcon: require('assets/images/terms-condition.png'),
  cancelPolicyIcon: require('assets/images/cancellation-policy.png'),
  disputePolicyIcon: require('assets/images/dispute-policy.png'),
  contactUsIcon: require('assets/images/contact-us.png'),
  bookingsIcon: require('assets/images/bookings-icon.png'),
  deleteAccountIcon: require('assets/images/delete-account.png'),
  paymentsIcon: require('assets/images/payments-icon.png'),
  rightArrow: require('assets/images/right-arrow.png'),
  consultIcon: require('assets/images/consulation.png'),
  consultBg: require('assets/images/consultation-bg.png'),
};
const ProfileIcon = {
  profileBg: require('assets/images/profile-bg.png'),
  logout: require('assets/images/profile-logout.png'),
  phoneIcon: require('assets/images/phone.png'),
  editIcon: require('assets/images/edit.png'),
  changePasswordIcon: require('assets/images/change-password.png'),
  paymentReceiveBg: require('assets/images/payment-received-card.png'),
  TotalAppntBg: require('assets/images/complete-appointments-card.png'),
  profileImage: require('assets/images/profile.png'),
  noImage: require('assets/images/no-image.png'),
};
const ContactUsImage = {
  loc1: require('assets/images/loc1.png'),
  loc2: require('assets/images/loc2.png'),
  loc3: require('assets/images/loc3.png'),
  loc4: require('assets/images/loc4.png'),
  headerImage: require('assets/images/contact-us-screen-image.png'),
  contactByEmail: require('assets/images/contact-by-email.png'),
  contactByPhone: require('assets/images/contact-by-phone.png'),
  contactByLocation: require('assets/images/contact-by-location.png'),
};
const Icons = {
  calendarCheck: require('assets/images/calenderCheck.png'),
  generateInvoice: require('assets/images/generate-invoice.png'),
  congratImage: require('assets/images/congrats.png'),
  totalPaymentIcon: require('assets/images/total-payment.png'),
  inactiveAccountIcon: require('assets/images/inactive-account.png'),
  walletIcon: require('assets/images/wallet.png'),
};
export {
  AppLogo,
  AppIcon,
  WelcomeBG,
  HomePaymentReceived,
  NoInternetImage,
  ProceduresIcon,
  drawerItemIcon,
  ProfileIcon,
  NavIcon,
  ContactUsImage,
  Icons,
};
