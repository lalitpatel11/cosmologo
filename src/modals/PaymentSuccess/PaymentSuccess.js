//import : react components
import React, {useState} from 'react';
import {View, Modal, TouchableOpacity} from 'react-native';
import {CommonActions, useNavigation, useRoute} from '@react-navigation/native';
//import : custom components
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//import : third parties
import moment from 'moment';
import Toast from 'react-native-simple-toast';
//import : global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//import : styles
import {styles} from './PaymentSuccessStyle';
//import : redux
import {useDispatch, useSelector} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {clearCart} from 'src/reduxToolkit/reducer/cart';

const PaymentSuccess = ({visible, setVisibility}) => {
  //variables
  const navigation = useNavigation();
  const route = useRoute();
  //variables : redux variables
  const dispatch = useDispatch();
  const cartItems = useSelector(state => state.cart.cartItems);
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [bookingId, setBookingId] = useState(route.params && typeof route.params.bookingId === 'string' && route.params.bookingId?.length > 0 ? route.params.bookingId : '');
  //function : navigation function
  // const resetIndexGoToBottomTab = CommonActions.reset({
  //   index: 1,
  //   routes: [{name: ScreenNames.USER_APPOINTMENT_DETAILS, params: {BookingId}}],
  // });

  const gotoAppointmentDetail = BookingId => {
    closeModal();
    navigation.replace(ScreenNames.USER_APPOINTMENT_DETAILS, {
      BookingId,
      hideTrack: true
    });
  };
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  //function : service function
  const confirmAppt = async () => {
    setShowLoader(true);
    try {
      const bookApptData = new FormData();
      bookApptData.append('user_id', userInfo.id);
      bookApptData.append(
        'product_concern_area_id',
        cartItems.data.concern_area_id,
      );
      bookApptData.append('appointment_date', cartItems.appointment_date);
      bookApptData.append(
        'appointment_time',
        moment(`${cartItems.appointment_time}`, 'h:mm A').format('HH:mm:ss'),
      );
      // bookApptData.append('service_provider_id', cartItems.service_provider.id);
      bookApptData.append(
        'amount',
        parseFloat(cartItems.data.price).toFixed(2),
      );
      console.log('bookApptData', bookApptData);
      console.log('userToken', userToken);
      console.warn(bookApptData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.BOOK_APPOINTMENT,
        bookApptData,
      );
      console.log('book apt resp', resp);
      if (resp.data.status) {
        // dispatch(showToast({text: resp.data.message}));
        Toast.show(resp.data.message, Toast.SHORT)
        dispatch(clearCart());
        gotoAppointmentDetail(resp.data.booking_id);
      } else {
        // dispatch(showToast({text: resp.data.message}));
        Toast.show(resp.data.message, Toast.SHORT)
      }
    } catch (error) {
      console.log('error in confirmAppt', error);
    }
    setShowLoader(false);
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyIcon.MaterialIcons
            name="check-circle-outline"
            size={40}
            color={Colors.THEME_GREEN}
            style={{alignSelf: 'center'}}
          />
          <MyText
            text={`Your payment of $ ${parseFloat(
              cartItems?.data?.price + cartItems?.data?.service_charges,
            ).toFixed(2)} is successfully completed`}
            fontFamily="bold"
            textAlign="center"
            textColor="theme_green"
            fontSize={16}
            marginVertical={10}
          />
          {/* <MyText
            text="Dolores rebum gubergren rebum lorem lorem sed consetetur. Lorem diam sit voluptua gubergren dolore, ipsum stet sed takimata sadipscing eirmod."
            lineHeight={25}
            fontFamily="light"
            textAlign="center"
          /> */}
          {/* <View style={styles.flexRowView}>
            <TouchableOpacity style={styles.downloadButton}>
              <MyIcon.Feather name="download" size={16} color={Colors.WHITE} />
            </TouchableOpacity>
            <MyText
              text="Download Invoice"
              fontFamily="medium"
              marginHorizontal={10}
            />
          </View> */}
          {/* <View style={{height: 20}} /> */}
          <MyButton
            Title="Review and Confirm Appointment"
            // onPress={gotoServiceProviders}
            // onPress={confirmAppt}
            onPress={()=>{dispatch(clearCart());gotoAppointmentDetail(bookingId);}}
          />
        </View>
        <CustomLoader showLoader={showLoader} />
      </View>
    </Modal>
  );
};

export default PaymentSuccess;
