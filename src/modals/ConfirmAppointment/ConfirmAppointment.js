//react components
import React from 'react';
import {View, Modal, TouchableOpacity, ActivityIndicator} from 'react-native';
import {useNavigation} from '@react-navigation/native';
//custom components
import MyText from 'components/MyText/MyText';
import DoctorCard from 'components/DoctorCard/DoctorCard';
import MyButton from 'components/MyButton/MyButton';
//global
import {Colors, MyIcon, ScreenNames} from 'global/Index';
//styles
import {styles} from './ConfirmAppointmentStyle';

const ConfirmAppointment = ({visible, setVisibility, Data}) => {
  //variables
  const navigation = useNavigation();
  //function : navigation function
  const gotoAppointmentDetail = () =>
    navigation.navigate(ScreenNames.USER_APPOINTMENT_DETAILS, {
      BookingId: Data?.appointment_detail?.booking_id,
    });
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  const gotoAppointmentDetails = () => {
    closeModal();
    gotoAppointmentDetail();
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} />
        <View style={styles.mainView}>
          {Object.keys(Data).length > 0 ? (
            <>
              <MyIcon.MaterialIcons
                name="check-circle-outline"
                size={60}
                color={Colors.THEME_GREEN}
                style={{alignSelf: 'center'}}
              />
              <MyText
                text={`Your appointment for ${Data?.appointment_detail?.subcategory} (${Data?.appointment_detail?.product},${Data?.appointment_detail?.concern_area_title}) Successfully booked`}
                fontFamily="medium"
                textAlign="center"
                textColor="theme_green"
                fontSize={16}
                marginVertical={10}
              />
              <View style={styles.flexRowView}>
                <MyText text="Booking ID : " />
                <MyText
                  text={Data?.appointment_detail?.booking_id}
                  textColor="theme_green"
                />
              </View>
              <DoctorCard
                ProfileImageUrl={Data?.service_provider_detail?.profile_image}
                Name={Data?.service_provider_detail?.fullname}
              />
              <View style={styles.flexRowView}>
                <MyText
                  text="Date & Time: "
                  fontFamily="bold"
                  textColor="lite_grey"
                />
                <MyText
                  text={Data?.appointment_detail?.date_time}
                  textColor="lite_grey"
                />
              </View>
              <TouchableOpacity style={styles.flexRowView}>
                <MyIcon.AntDesign
                  name="download"
                  color={Colors.THEME_GREEN}
                  size={20}
                />
                <MyText
                  text="Download Invoice"
                  textColor="theme_green"
                  fontFamily="bold"
                  marginHorizontal={5}
                />
              </TouchableOpacity>
              <MyButton
                Title="View Appointment"
                onPress={gotoAppointmentDetails}
              />
            </>
          ) : (
            <ActivityIndicator size="large" animating={true} />
          )}
        </View>
      </View>
    </Modal>
  );
};

export default ConfirmAppointment;
