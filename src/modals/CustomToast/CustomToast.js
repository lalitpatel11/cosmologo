//import : react components
import React from 'react';
import {View, Text, Animated, Modal, TouchableOpacity} from 'react-native';
//import : custom  components
import MyText from 'components/MyText/MyText';
//import : styles
import {styles} from './CustomToastStyle';
//import : redux
import {useDispatch, useSelector} from 'react-redux';
import {hideToast} from 'src/reduxToolkit/reducer/customToast';

const CustomToast = () => {
  //refs
  //variables : redux variables
  const dispatch = useDispatch();
  const visible = useSelector(state => state.customToast.visible);
  const text = useSelector(state => state.customToast.text);
  const duration = useSelector(state => state.customToast.duration);
  //function : modal function
  const closeModal = () => {
    dispatch(hideToast());
  };
  const closeAutomatic = () => {
    setTimeout(() => {
      dispatch(hideToast());
    }, duration);
  };
  //UI
  return (
    <Modal
      visible={visible}
      onShow={closeAutomatic}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyText
            text={text}
            fontFamily="bold"
            fontSize={16}
            marginBottom={20}
          />
          <TouchableOpacity onPress={closeModal} style={styles.buttonStyle}>
            <MyText text="OK" textColor="white" />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default CustomToast;
