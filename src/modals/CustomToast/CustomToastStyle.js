import {StyleSheet} from 'react-native';
import {Colors} from 'global/Index';

//global

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BLACK + '66',
  },
  blurView: {
    flex: 1,
  },
  mainView: {
    position: 'absolute',
    justifyContent: 'center',
    right: '5%',
    left: '5%',
    top: '40%',
    backgroundColor: Colors.WHITE,
    padding: 20,
    borderRadius: 20,
  },
  buttonStyle: {
    backgroundColor: Colors.THEME_GREEN,
    padding: 8,
    paddingHorizontal: 15,
    borderRadius: 100,
    alignSelf: 'flex-end',
  },
});
