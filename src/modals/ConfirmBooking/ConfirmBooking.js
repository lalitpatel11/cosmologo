//react components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import {Colors, Images, MyIcon} from 'global/Index';
import React from 'react';
import {View, Text, Modal, TouchableOpacity, Image} from 'react-native';
//styles
import {styles} from './ConfirmBookingStyle';

const ConfirmBooking = ({visible, setVisibility, selectedBookingId, gotoAppointmentDetail=()=>{}}) => {
  //function : modal funcion
  const closeModal = () => {
    setVisibility(false);
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          {/* <Image
            resizeMode="stretch"
            source={Images.Icons.congratImage}
            style={{height: 100, width: 150, alignSelf: 'center'}}
          />
          <MyText
            text="Your Payment for Add-on services $299.00 is Successfully Completed"
            textAlign="center"
            fontSize={16}
            marginVertical={10}
          />
          <MyText
            text="We know your time is valuable and so is that of our helpful consultants. A small deposit is required to ensure the time of your consultation that will be refunded or applied to your procedure after your consultation"
            textAlign="center"
            fontFamily="light"
            marginVertical={10}
          />
          <View style={styles.flexRowStyle}>
            <MyText text="Booking ID : " />
            <MyText
              text={selectedBookingId}
              fontFamily="medium"
              textColor="theme_green"
            />
          </View> */}
          <View style={styles.aptVerifiedView}>
            <MyIcon.AntDesign name="checkcircle" size={30} color={Colors.THEME_GREEN} />
            <MyText text="Appointment Verified" fontSize={16} marginTop={10} />
            <TouchableOpacity onPress={gotoAppointmentDetail}>
              <MyText text="Go to detail screen to update status" fontSize={14} marginTop={20} textColor="theme_green" isUnderLine />
            </TouchableOpacity>
          </View>
          {/* <View style={styles.flexRowStyle}>
            <MyIcon.AntDesign
              name="download"
              color={Colors.THEME_GREEN}
              size={20}
            />
            <MyText
              text="Generate Invoice Manually"
              textColor="theme_green"
              fontFamily="bold"
              marginHorizontal={5}
            />
          </View> */}
          {/* <View style={{height: 20}} /> */}
          {/* <MyButton Title="Validate Booking" /> */}
        </View>
      </View>
    </Modal>
  );
};

export default ConfirmBooking;
