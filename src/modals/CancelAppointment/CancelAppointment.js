//react components
import React from 'react';
import {View, Text, Modal, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
//custom components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './CancelAppointmentStyle';
import {connect} from 'react-redux';

const CancelAppointment = ({visible, setVisibility, onPress = () => {}, type = ''}) => {
  //variables
  const navigation = useNavigation();
  const gotoWebPage = async (endPoint, name) => {
    navigation.navigate(ScreenNames.SIDE_MENU_LINKS, {endPoint, name})
  }
  //function : navigation function
  const gotoCancellationPolicy = () =>
    // navigation.navigate(ScreenNames.CANCELLATION_POLICY);
    gotoWebPage('cancellation-policy', 'Cancellation Policy')
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  const viewCancelPolicy = () => {
    closeModal();
    gotoCancellationPolicy();
  };
  //function : service function
  const cancelAppointment = async () => {
    try {
      const cancelAppntData = new FormData();
      cancelAppntData.append('appointment_id');
      const resp = await Service.postApiWithToken(userToken, cancelAppntData);
    } catch (error) {
      console.log('error in cancelAppointment', error);
    }
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyText
            text={type === 'booking' ? "Are you sure you want to cancel the Booking?" : "Are you sure you want to cancel the Appointment?"}
            fontFamily="bold"
            textAlign="center"
            fontSize={16}
          />
          <MyText
            text="Consumer will get notified of cancellation Appointment and receive Payment of $299.00 as refund"
            textColor="lite_grey"
            textAlign="center"
            marginVertical={10}
          />
          <TouchableOpacity
            onPress={viewCancelPolicy}
            style={styles.cancelPolicyView}>
            <MyIcon.AntDesign name="eye" color={Colors.THEME_GREEN} size={20} />
            <MyText
              text="View Cancellation Policy"
              textColor="lite_green"
              fontFamily="bold"
              marginHorizontal={5}
            />
          </TouchableOpacity>
          <View style={styles.buttonView}>
            <MyButton
              Title="Close"
              width="35%"
              borderColor={Colors.THEME_GREEN}
              onPress={closeModal}
            />
            <MyButton
              Title={type === 'booking' ? "Cancel Booking" : "Cancel Appointment"}
              width="60%"
              backgroundColor={Colors.RED}
              onPress={onPress}
            />
          </View>

          <View style={{height: 10}} />
        </View>
      </View>
    </Modal>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(React.memo(CancelAppointment));
