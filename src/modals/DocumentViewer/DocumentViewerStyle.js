import {Colors} from 'global/Index';
import {StyleSheet, Dimensions} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BLACK + '66',
  },
  blurView: {
    flex: 1,
  },
  mainView: {
    padding: 20,
    margin: 20,
    backgroundColor: Colors.WHITE,
    borderRadius: 20,
  },
  flexRowView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
  otpTextInput: {
    backgroundColor: Colors.WHITE,
    padding: 10,
    textAlign: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    backgroundColor: Colors.WHITE,
    shadowRadius: 15,
    elevation: 2,
    height: 50,
    width: 50,
    borderRadius: 100,
    marginHorizontal: 10,
  },
  textInputStyle: {
    backgroundColor: Colors.WHITE,
    padding: 10,
    textAlign: 'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    backgroundColor: Colors.WHITE,
    shadowRadius: 15,
    elevation: 2,
    height: 50,
    width: 50,
    borderRadius: 100,
    marginHorizontal: 10,
  },
  timeView:{
    justifyContent:'center',
    alignItems:'center',
    marginRight:15,
    height:50,
    width:'auto',
    paddingHorizontal:10,
    borderRadius:10,
  },
  pdfContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
  },
  pdf: {
      flex:1,
      width:Dimensions.get('window').width,
      height:Dimensions.get('window').height,
  }
});
