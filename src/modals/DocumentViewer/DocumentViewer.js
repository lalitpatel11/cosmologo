//react components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
import React, {useRef, useState} from 'react';
import {View, Text, Modal, TouchableOpacity, TextInput, Keyboard, KeyboardAvoidingView, Platform, FlatList, Image} from 'react-native';
//styles
import {styles} from './DocumentViewerStyle';
// global
import {Colors} from 'global/Index';
//third parties
import Toast from 'react-native-simple-toast';
// import Pdf from 'react-native-pdf';

const DocumentViewer = ({visible, setVisibility, showImage, selectedDocument}) => {
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      // onShow={()=>{setDocument(selectedDocument); setIsImage(showImage)}}
      onShow={()=>{}}
      transparent>
        <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={[styles.mainView, {alignItems:'center', justifyContent:'center'}]}>
          {/* <MyText
            text="Add buffer time for this appointment"
            fontFamily="bold"
            textAlign="center"
            fontSize={16}
          /> */}
          {showImage ?
          <Image
            resizeMode="contain"
            // borderRadius={100}
            // source={require('assets/images/profile_pic.png')}
            source={{uri: selectedDocument}}
            style={{
              height: '90%',
              width: '90%',
            }}
          />
          :
          <MyText
            text="replace this with <pdf if pdf will be used in the future"
            fontFamily="bold"
            textAlign="center"
            fontSize={16}
          />
          // <View style={styles.pdfContainer}>
          //   <Pdf
          //       trustAllCerts={false}
          //       source={{ uri: selectedDocument, cache: true }}
          //       onLoadComplete={(numberOfPages,filePath) => {
          //           console.log(`Number of pages: ${numberOfPages}`);
          //       }}
          //       onPageChanged={(page,numberOfPages) => {
          //           console.log(`Current page: ${page}`);
          //       }}
          //       onError={(error) => {
          //           console.log(error);
          //       }}
          //       onPressLink={(uri) => {
          //           console.log(`Link pressed: ${uri}`);
          //       }}
          //       style={{width:'100%',height:'100%'}}/>
          //   </View>
            }
          {/* <MyButton Title="Close" width={'40%'} alignSelf={'center'} onPress={closeModal} marginTop={20}/> */}
          {/* <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', marginTop:20}}>
            <MyButton Title="Add buffer time" width={'40%'} onPress={()=>{}}/>
            <MyButton Title="No thanks" width={'40%'} backgroundColor={Colors.RED} onPress={()=>{}}/>
          </View> */}
        </View>
      </View>
      </KeyboardAvoidingView>
    </Modal>
  );
};

export default DocumentViewer;
