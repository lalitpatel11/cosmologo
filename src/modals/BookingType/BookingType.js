//react components
import React, {memo, useState} from 'react';
import {View, Text, Modal, TouchableOpacity} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
import RadioCard from 'components/RadioCard/RadioCard';
import MyButton from 'components/MyButton/MyButton';
//global
import {Constant} from 'global/Index';
//styles
import {styles} from './BookingTypeStyle';

const BookingType = ({visible, setvisibility}) => {
  //states
  const [selectedBookType, setSelectedBookType] = useState(0);
  //function : modal function
  const closeModal = () => {
    setvisibility(false);
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyText
            text="Select Your Booking Type"
            fontFamily="bold"
            fontSize={16}
            marginBottom={10}
          />
          {Constant?.BookingTypeData.map((item, index) => {
            return (
              <RadioCard
                key={item.id.toString()}
                index={index}
                selectedValue={selectedBookType}
                setSelectedValue={setSelectedBookType}
                Title={item.name}
              />
            );
          })}
          <View style={{height: 20}} />
          <MyButton Title="Next" />
        </View>
      </View>
    </Modal>
  );
};

export default memo(BookingType);
