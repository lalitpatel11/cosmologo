//react components
import React from 'react';
import {View, Text, Modal, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
//custom components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './UnselectServiceStyle';
import {connect} from 'react-redux';

const UnselectService = ({visible, setVisibility, text1, values, text2, next=()=>{}}) => {
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyText
            text={`${text1}`}
            fontFamily="bold"
            // textAlign="center"
            fontSize={16}
          />
          <Text>{`\n`}</Text>
          {values.map((el, index)=>{
            return (
              <MyText
                key={el}
                text={`${index+1}. ${el}`}
                fontFamily="bold"
                // textAlign="center"
                fontSize={16}
              />
            )
          })}
          <Text>{`\n`}</Text>
          <MyText
            text={`${text2}`}
            fontFamily="bold"
            // textAlign="center"
            fontSize={16}
          />
          <View style={styles.buttonView}>
            <MyButton
              Title="Don't select"
              width="35%"
              borderColor={Colors.RED}
              onPress={next}
            />
            <MyButton
              Title="Select"
              width="60%"
              backgroundColor={Colors.THEME_GREEN}
              onPress={closeModal}
            />
          </View>

          <View style={{height: 10}} />
        </View>
      </View>
    </Modal>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(React.memo(UnselectService));
