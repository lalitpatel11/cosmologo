//react components
import React from 'react';
import {View, Text, Modal, TouchableOpacity} from 'react-native';
//custom components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
//styles
import {styles} from './FreeConsultationStyle';
import {Colors, MyIcon} from 'global/Index';

const FreeConsultation = ({visible, setVisibility}) => {
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyIcon.MaterialIcons
            name="check-circle-outline"
            size={40}
            color={Colors.THEME_GREEN}
            style={{alignSelf: 'center'}}
          />
          <MyText
            text="We have received Your request, we will contact you within in 12 hrs"
            fontFamily="medium"
            fontSize={16}
            textAlign="center"
            marginVertical={10}
          />
          <View style={styles.flexRowStyle}>
            <MyText text="Request Id:" />
            <MyText text="#COS786978" textColor="theme_green" />
          </View>
          <View style={{height: 20}} />
          <MyButton Title="Close" />
        </View>
      </View>
    </Modal>
  );
};

export default FreeConsultation;
