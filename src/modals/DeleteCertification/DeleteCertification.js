//react components
import React from 'react';
import {View, Text, Modal, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
//custom components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './DeleteCertificationStyle';
import {connect} from 'react-redux';

const DeleteCertification = ({visible, setVisibility, deleteCertificate=()=>{}}) => {
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyText
            text="Are you sure you want to delete the certification?"
            fontFamily="bold"
            textAlign="center"
            fontSize={16}
          />
          <View style={styles.buttonView}>
            <MyButton
              Title="Close"
              width="35%"
              borderColor={Colors.THEME_GREEN}
              onPress={closeModal}
            />
            <MyButton
              Title="Delete certification"
              width="60%"
              backgroundColor={Colors.RED}
              onPress={deleteCertificate}
            />
          </View>

          <View style={{height: 10}} />
        </View>
      </View>
    </Modal>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(React.memo(DeleteCertification));
