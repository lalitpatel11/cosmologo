//react components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import {Colors, MyIcon} from 'global/Index';
import React from 'react';
import {View, Text, Modal, TouchableOpacity} from 'react-native';
//styles
import {styles} from './VerifyBookingStyle';

const VerifyBooking = ({
  visible,
  setVisibility,
  generateOtpPress = () => {},
  validateBookingPress = () => {},
  selectedBookingId
}) => {
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  //function : service function
  // const generateOtp = () => {
  //   closeModal();
  //   setShowOtherModal(true);
  // };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyText
            text="Verify Appointment Booking"
            fontFamily="bold"
            textAlign="center"
            fontSize={16}
          />
          <MyText
            text="Are you sure you want to verify the Appointment?"
            textColor="theme_green"
            fontFamily="bold"
            textAlign="center"
            marginVertical={10}
          />
          <MyTextInput placeholder={`Booking ID: ${selectedBookingId}`} editable={false}/>
          {/* <TouchableOpacity onPress={generateOtpPress}>
            <MyText
              text="**** Generate OTP"
              fontFamily="bold"
              textColor="theme_green"
              textAlign="center"
            />
          </TouchableOpacity> */}
          <TouchableOpacity style={styles.buttonView}>
            <MyButton
              Title="Close"
              width="35%"
              borderColor={Colors.THEME_GREEN}
              onPress={closeModal}
            />
            <MyButton Title="Validate OTP" width="60%" onPress={validateBookingPress}/>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default VerifyBooking;
