//react components
import React, {useState} from 'react';
import {View, Text, Modal, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
//custom components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './EditSlotStyle';
import {connect} from 'react-redux';
// third parties
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import DatePicker from 'react-native-date-picker';

const checkIfPastTime = (selDate, setTime) => {
  // console.log('check', new Date(`${selDate} ${setTime}`), moment(new Date(`${selDate} ${setTime}`)).isBefore(new Date()));
  return moment(new Date(`${selDate} ${setTime}`)).isBefore(new Date(), 'minute')
} 

const EditSlot = ({visible, setVisibility, userInfo, userToken, selectedItem}) => {
  //variables
  const navigation = useNavigation();
  // states
  const [openStartTime, setOpenStartTime] = useState(false);
  const [openEndTime, setOpenEndTime] = useState(false);
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [date, setDate] = useState(new Date());
  const [showLoader, setShowLoader] = useState(false);
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  const Validation = () => {
    if(!startTime){
      Toast.show('Please select Start Time', Toast.SHORT)
      return false
    } 
    if(!endTime){
      Toast.show('Please select End Time', Toast.SHORT)
      return false
    } 
    else {
      return true
    }
  }
  const editSlot = async () => {
    if (!Validation()) {
      return
    }
    setShowLoader(true)
    try {
      const myData = new FormData();
      myData.append('service_provider_id', userInfo.id);
      myData.append('service_unavailable_id', selectedItem.serviceUnavailableId);
      myData.append('start_time', moment(startTime).format('HH:mm'));
      myData.append('end_time', moment(endTime).format('HH:mm'));
      console.log('edit myData', myData);
      // setShowLoader(false);
      // return
      const resp = await Service.postApiWithToken(
        userToken, 
        Service.SLOT_EDIT, 
        myData
      );
      console.log('edit resp', resp);
      if (resp?.data?.status) {
        Toast.show(resp?.data?.message, Toast.SHORT)
        navigation.goBack()
      } else {
        Toast.show(resp?.data?.message, Toast.LONG)
      }
    } catch (error) {
      console.log('error in editSlot', error);
    }
    setShowLoader(false)
  }
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      onShow={()=>{
        if(Object.keys(selectedItem)?.length > 0){
          const dateParts = selectedItem?.date?.split('-')
          const startTimeParts = selectedItem?.startTime?.split(':')
          const endTimeParts = selectedItem?.endTime?.split(':')
          // const slotStartDate = new Date(dateParts[0], dateParts[1]-1,dateParts[2]-1, startTimeParts[0], startTimeParts[1],startTimeParts[2]).toString()
          const slotStartDate = moment(new Date(selectedItem?.date).setHours(startTimeParts[0], startTimeParts[1])).format('YYYY-MM-DD HH:mm')
          // const slotEndDate = new Date(dateParts[0], dateParts[1]-1,dateParts[2]-1, endTimeParts[0], endTimeParts[1],endTimeParts[2]).toString()
          const slotEndDate = moment(new Date(selectedItem?.date).setHours(endTimeParts[0], endTimeParts[1])).format('YYYY-MM-DD HH:mm')
          console.log('slotStartDate', slotStartDate);
          console.log('slotEndDate', slotEndDate);
          setStartTime(checkIfPastTime(selectedItem?.date, selectedItem?.startTime) ? '' : slotStartDate)
          setEndTime(checkIfPastTime(selectedItem?.date, selectedItem?.endTime) ? '' : slotEndDate)
        }
      }}
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
        <View style={styles.startEndDateView}>
          <MyText text="Select Start Time" fontFamily="medium" />
          <TouchableOpacity
            onPress={() => setOpenStartTime(true)}
            style={{
              marginVertical: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              shadowColor: '#000',
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.1,
              backgroundColor: Colors.WHITE,
              shadowRadius: 15,
              elevation: 2,
              padding: 10,
              borderRadius: 10,
            }}>
            <MyText
              text={
                startTime
                  ? moment(startTime).format('h:mm:ss A')
                  : 'Select Start Time'
              }
            />
            <MyIcon.AntDesign
              name="calendar"
              size={24}
              color={Colors.THEME_GREEN}
            />
          </TouchableOpacity>
          <MyText text="Select End Time" fontFamily="medium" />
          <TouchableOpacity
            onPress={() => setOpenEndTime(true)}
            style={{
              marginVertical: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              shadowColor: '#000',
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.1,
              backgroundColor: Colors.WHITE,
              shadowRadius: 15,
              elevation: 2,
              padding: 10,
              borderRadius: 10,
            }}>
            <MyText
              text={
                endTime
                  ? moment(endTime).format('h:mm:ss A')
                  : 'Select End Time'
              }
            />
            <MyIcon.AntDesign
              name="calendar"
              size={24}
              color={Colors.THEME_GREEN}
            />
          </TouchableOpacity>
          {/* <MyButton Title="Add Slot Time" onPress={()=>{}} /> */}
        </View>
          <View style={styles.buttonView}>
            <MyButton
              Title="Close"
              width="35%"
              borderColor={Colors.THEME_GREEN}
              onPress={closeModal}
              />
            <MyButton
              Title="Update Slot"
              width="60%"
              backgroundColor={Colors.THEME_GREEN}
              onPress={editSlot}
            />
          </View>

          <View style={{height: 10}} />
        </View>
      </View>
      <DatePicker
        modal
        // mode="date"
        mode="time"
        open={openStartTime}
        date={date}
        onConfirm={time => {
          setOpenStartTime(false);
          const diffDays = moment(moment(endTime).startOf('day')).diff(moment(time).startOf('day'),'days')
          // console.log("moment(new Date()).startOf('day')", moment(new Date()).startOf('day'));
          // console.log("moment(time).startOf('day')", moment(time).startOf('day'));
          const diffDays2 = moment(moment(selectedItem?.date).startOf('day')).diff(moment(time).startOf('day'),'days')
          // console.log('diffDays2', diffDays2);
          // console.log('compare new and old', moment(time).format('HH:mm'), moment(startTime).format('HH:mm'));
          const newStart = moment(time.setDate(time.getDate() + diffDays2)).format('YYYY-MM-DD HH:mm')
          // console.log('newStart', newStart);
          // console.log('endTime', endTime);
          const diffHours = moment(endTime).diff(moment(newStart), 'hours');
          // console.log('diffHours', diffHours);
          const endSmallerThanStart = moment(endTime).isBefore(moment(newStart))
          const currentDate = moment().format('YYYY-MM-DD HH:mm')
          // console.log('endSmallerThanStart', endSmallerThanStart);
          // may use later
          // if(startTime){
          //   const oldStart = moment(startTime).format('YYYY-MM-DD HH:mm')
          //   // console.log('new', newStart);
          //   // console.log('old', oldStart);
          //   // console.log('actual compare', moment(newStart).isSame(moment(oldStart),'minute'));
          //   if(moment(newStart).isSame(moment(oldStart),'minute')){
          //     Toast.show('New Start Time cannot be same as previous Start Time', Toast.SHORT)
          //   }
          // }
          // if(moment(new Date(time.setDate(time.getDate() + diffDays2))).isBefore(new Date(), 'minute')){
          if(moment(newStart).isBefore(moment(currentDate), 'minutes')){
            Toast.show('Past time not allowed', Toast.SHORT)
          // }else if(moment(new Date(endTime)).isBefore(moment(time.setDate(time.getDate() + 1)), 'minute')){
          // }else if(endTime < time.setDate(time.getDate() + diffDays)){
          }else if(endSmallerThanStart){
            // dispatch(showToast({text: 'End Time must be greater than Start Time',duration: 3000}));
            Toast.show('Start Time must be smaller than End Time', Toast.SHORT)
          }else if(diffHours < 1){
            Toast.show('End Time must be greater than Start Time by at least an hour', Toast.SHORT)
          }
          else{
            setStartTime(time);
          }
        }}
        onCancel={() => {
          setOpenStartTime(false);
        }}
      />
      <DatePicker
        modal
        // mode="date"
        mode="time"
        open={openEndTime}
        date={date}
        onConfirm={time => {
          setOpenEndTime(false);
          const diffDays = moment(moment(startTime).startOf('day')).diff(moment(time).startOf('day'),'days')
          const newEnd = moment(time.setDate(time.getDate() + diffDays)).format('YYYY-MM-DD HH:mm')
          const oldStart = moment(startTime).format('YYYY-MM-DD HH:mm')
          const diffHours = moment(newEnd).diff(moment(oldStart), 'hours')
          console.log('diffHours', diffHours);
          if(!startTime){
            Toast.show('Please select Start Time', Toast.SHORT)
          // }else if(moment(new Date(startTime).setSeconds(0,0)) >= moment(new Date(time).setSeconds(0,0))){
          }else if(startTime >= time.setDate(time.getDate() + diffDays)){
            // dispatch(showToast({text: 'End Time must be greater than Start Time',duration: 3000}));
            Toast.show('End Time must be greater than Start Time', Toast.SHORT)
          }else if(diffHours < 1){
            Toast.show('End Time must be greater than Start Time by at least an hour', Toast.SHORT)
          }
          else{
            setEndTime(time);
          }
        }}
        onCancel={() => {
          setOpenEndTime(false);
        }}
      />
      <CustomLoader showLoader={showLoader} />
    </Modal>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(React.memo(EditSlot));
