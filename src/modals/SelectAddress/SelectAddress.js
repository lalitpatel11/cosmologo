//react components
import MyText from 'components/MyText/MyText';
import {Colors, MyIcon} from 'global/Index';
import React from 'react';
import {View, Text, Modal, TouchableOpacity} from 'react-native';
//styles
import {styles} from './SelectAddressStyle';

const SelectAddress = ({visible, setVisibility, Addresses, setValue, setLatLng= ()=>{}}) => {
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyText
            text="Select Address"
            fontFamily="bold"
            textAlign="center"
            fontSize={20}
            marginBottom={20}
          />
          {Addresses.length > 0
            ? Addresses.map((item, index) => {
                return (
                  <TouchableOpacity
                    key={index.toString()}
                    onPress={() => {
                      setValue(item.formatted_address);
                      setLatLng(item.geometry.location);
                      closeModal();
                    }}
                    style={{
                      padding: 5,
                      backgroundColor: Colors.WHITE,
                      borderBottomWidth:
                        Addresses.length - 1 == index ? 0 : 0.5,
                      marginVertical: 2,
                      flexDirection: 'row',
                    }}>
                    <MyIcon.FontAwesome5 name="map-marker-alt" size={20} />
                    <MyText text={item.formatted_address} marginLeft={10} />
                  </TouchableOpacity>
                );
              })
            : null}
        </View>
      </View>
    </Modal>
  );
};

export default SelectAddress;
