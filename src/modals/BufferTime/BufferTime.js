//react components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
import React, {useRef, useState} from 'react';
import {View, Text, Modal, TouchableOpacity, TextInput, Keyboard, KeyboardAvoidingView, Platform, FlatList} from 'react-native';
//styles
import {styles} from './BufferTimeStyle';
// global
import {Colors} from 'global/Index';
//third parties
import Toast from 'react-native-simple-toast';

const getTime = (time) => {
  if(isNaN(Number(time))){
    return time
  }
  if(Number(time) < 60){
    return `${time} mins`
  }
  if(Number(time) == 60){
    return `1 hr`
  }
  const hours = Math.floor(Number(time) / 60)
  const minutes = Number(time) % 60
  const hrOrHrs = hours == 1 ? 'hr' : 'hrs'
  return minutes == 0 ? `${hours} ${hrOrHrs}` :`${hours} ${hrOrHrs} ${minutes} mins`
}

const VerifyApptOtp = ({visible, setVisibility, times=[], selectedTime ,setSelectedTime = () => {}, acceptAppointment = () => {}}) => {
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  // //ref
  // const firstCodeRef = useRef();
  // const secondCodeRef = useRef();
  // const thirdCodeRef = useRef();
  // const forthCodeRef = useRef();
  // // states
  // const [firstCode, setFirstCode] = useState('');
  // const [secondCode, setSecondCode] = useState('');
  // const [thirdCode, setThirdCode] = useState('');
  // const [forthCode, setForthCode] = useState('');
  const [availableTimes, setAvailableTimes] = useState([])
  const onAddBufferTime = () => {
    if(selectedTime === ''){
      Toast.show('Please select buffer time', Toast.SHORT)
      return
    }
    acceptAppointment(true) 
  }
  const onNoThanks = () => {
    setSelectedTime('');
    acceptAppointment(false)
  }
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      onShow={()=>setAvailableTimes(times.map((el, index)=>{return{id: String(index+1), time: el}}))}
      transparent>
        <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyText
            text="Add buffer time for this appointment"
            fontFamily="bold"
            textAlign="center"
            fontSize={16}
          />
          <FlatList
          data={availableTimes}
          horizontal={true}
          style={{marginTop:20}}
          keyExtractor={item=>item.id}
          renderItem={({item,index})=>{
            return (
              <TouchableOpacity onPress={()=>setSelectedTime(item.time)} style={[styles.timeView, {backgroundColor: selectedTime === item.time ? Colors.THEME_BLUE: 'white', borderColor: selectedTime === item.time ? null : Colors.LITE_GREY, borderWidth: selectedTime === item.time ? 0 : 1}]}>
                {/* <MyText text={item.time} textColor={selectedTime === item.time ? "white" : "black"} fontFamily="bold" /> */}
                <MyText text={getTime(item.time)} textColor={selectedTime === item.time ? "white" : "black"} fontFamily="bold" />
              </TouchableOpacity>
            )
          }}
          />
          <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', marginTop:20}}>
          <MyButton Title="Add buffer time" width={'40%'} onPress={onAddBufferTime}/>
          <MyButton Title="No thanks" width={'40%'} backgroundColor={Colors.RED} onPress={onNoThanks}/>

          </View>
        </View>
      </View>
      </KeyboardAvoidingView>
    </Modal>
  );
};

export default VerifyApptOtp;
