//react components
import React, { useState } from 'react';
import { View, Text, Modal, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
//custom components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
//third parties
import { Calendar } from 'react-native-calendars';
import moment from 'moment';
//global
import { Colors, MyIcon, ScreenNames, Service } from 'global/Index';
//styles
import { styles } from './AppointmentCalenderStyle';
import { connect, useDispatch } from 'react-redux';
import { showToast } from 'src/reduxToolkit/reducer/customToast';

const AppointmentCalender = ({ visible, setVisibility, userToken, startDay, endDay, markedDates, setStartDay, setEndDay, temporarySelectedDates, setTemporarySelectedDates, setMarkedDates, OKPressed = () => { }, clearSelection=()=>{} }) => {
    //redux variables
    const dispatch = useDispatch()
    //variables   
    const navigation = useNavigation();
    // state
    //function : navigation function
    const gotoCancellationPolicy = () =>
        navigation.navigate(ScreenNames.CANCELLATION_POLICY);
    //function : modal function
    const closeModal = () => {
        setVisibility(false);
    };
    //UI
    return (
        <Modal
            visible={visible}
            onRequestClose={closeModal}
            animationType="fade"
            transparent>
            <View style={styles.container}>
                <TouchableOpacity style={styles.blurView} onPress={closeModal} />
                <View style={styles.mainView}>
                    <Calendar
                        style={{
                            marginVertical: 10,
                            borderRadius: 10,
                        }}
                        onDayPress={day => {
                            if (startDay && !endDay) {
                                const selectedDates = [];
                                const date = {};
                                for (
                                    const d = moment(startDay);
                                    d.isSameOrBefore(day.dateString);
                                    d.add(1, 'days')
                                ) {
                                    date[d.format('YYYY-MM-DD')] = {
                                        marked: true,
                                        color: Colors.LITE_GREEN,
                                        textColor: 'white',
                                    };
                                    selectedDates.push(d.format('YYYY-MM-DD'));
                                    if (d.format('YYYY-MM-DD') === startDay)
                                        date[d.format('YYYY-MM-DD')].startingDay = true;
                                    if (d.format('YYYY-MM-DD') === day.dateString)
                                        date[d.format('YYYY-MM-DD')].endingDay = true;
                                }
                                setTemporarySelectedDates(selectedDates);
                                setMarkedDates({ ...date });
                                // console.log('first case', {...markedDates, ...date})
                                setEndDay(day.dateString);
                            } else {
                                setStartDay(day.dateString);
                                setEndDay(null);
                                setTemporarySelectedDates([day.dateString]);
                                setMarkedDates({
                                    [day.dateString]: {
                                        marked: true,
                                        color: Colors.THEME_GREEN,
                                        textColor: 'white',
                                        startingDay: true,
                                        endingDay: true,
                                    },
                                });
                            }
                        }}
                        // monthFormat={'yyyy MMM'}
                        // hideDayNames={false}
                        markingType={'period'}
                        markedDates={markedDates}
                        theme={{
                            selectedDayBackgroundColor: Colors.THEME_BLUE,
                            selectedDayTextColor: 'white',
                            monthTextColor: Colors.THEME_GREEN,
                            dayTextColor: 'black',
                            textMonthFontSize: 16,
                            textDayHeaderFontSize: 14,
                            arrowColor: Colors.THEME_GREEN,
                            dotColor: 'black',
                        }}
                    //   minDate={moment().format('YYYY-MM-DD')}
                    />
                    <View style={{flexDirection:'row', justifyContent:'center'}}>
                        <MyButton
                            Title="Clear Date Selection"
                            width="100%"
                            backgroundColor={Colors.RED}
                            onPress={clearSelection}
                        />
                    </View>
                    <View style={styles.buttonView}>
                        <MyButton
                            Title="Close"
                            width="40%"
                            borderColor={Colors.RED}
                            onPress={closeModal}
                        />
                        <MyButton
                            Title="OK"
                            width="40%"
                            backgroundColor={Colors.THEME_GREEN}
                            onPress={OKPressed}
                        />
                    </View>
                    <View style={{ height: 10 }} />
                </View>
            </View>
        </Modal>
    );
};
const mapStateToProps = state => ({
    userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(React.memo(AppointmentCalender));
