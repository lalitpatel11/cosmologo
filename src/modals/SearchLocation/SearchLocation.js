//react components
import React, {useState} from 'react';
import {View, Text, Modal, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
//custom components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
//third parties
import {Calendar} from 'react-native-calendars';
import moment from 'moment';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './SearchLocationStyle';
import {connect, useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const SearchLocation = ({
  visible,
  setVisibility,
  setAddress,
  setLatLng,
  OKPressed = () => {},
}) => {
  // state
  const closeModal = () => {
    setVisibility(false);
  };
  const GOOGLE_MAPS_APIKEY = 'AIzaSyACzgsZq8gI9VFkOw_fwLJdmezbc4iUxiM';
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <GooglePlacesAutocomplete
            placeholder="Search Location"
            textInputProps={{
              placeholderTextColor: '#c9c9c9',
              // placeholderTextColor: Colors.BLACK,
              returnKeyType: 'search',
              // onFocus: () => setShowPlacesList(true),
              // onBlur: () => setShowPlacesList(false),
              multiline:true
            }}
            enablePoweredByContainer={false}
            listViewDisplayed={'auto'}
            styles={styles.searchbar}
            onPress={(data, details = null) => {
              // 'details' is provided when fetchDetails = true
              // setShowPlacesList(false)
              setLatLng({
                lat: details.geometry.location.lat,
                lng: details.geometry.location.lng,
              });
              setAddress(data?.description);
            }}
            GooglePlacesDetailsQuery={{
              fields: 'geometry',
            }}
            fetchDetails={true}
            query={{
              key: GOOGLE_MAPS_APIKEY,
              language: 'en',
            }}
          />
          <View style={styles.buttonView}>
            {/* <MyButton
              Title="Close"
              width="40%"
              borderColor={Colors.RED}
              onPress={closeModal}
            /> */}
            <MyButton
              Title="OK"
              width="40%"
              backgroundColor={Colors.THEME_GREEN}
              onPress={closeModal}
            />
          </View>
          <View style={{height: 10}} />
        </View>
      </View>
    </Modal>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(React.memo(SearchLocation));
