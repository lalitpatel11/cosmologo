//react components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
import React, {useRef, useState} from 'react';
import {View, Text, Modal, TouchableOpacity, TextInput, Keyboard, KeyboardAvoidingView, Platform} from 'react-native';
//styles
import {styles} from './VerifyApptOtpStyle';
// global
import {Colors} from 'global/Index';

const VerifyApptOtp = ({visible, setVisibility, validateBookingPress = ()=>{}, firstCode, secondCode, thirdCode, forthCode, setFirstCode, setSecondCode, setThirdCode, setForthCode, firstCodeRef, secondCodeRef, thirdCodeRef, forthCodeRef, message, setMessage}) => {
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  // //ref
  // const firstCodeRef = useRef();
  // const secondCodeRef = useRef();
  // const thirdCodeRef = useRef();
  // const forthCodeRef = useRef();
  // // states
  // const [firstCode, setFirstCode] = useState('');
  // const [secondCode, setSecondCode] = useState('');
  // const [thirdCode, setThirdCode] = useState('');
  // const [forthCode, setForthCode] = useState('');
  //UI
  return (
    <Modal
      visible={visible}
      onRequestClose={closeModal}
      animationType="fade"
      transparent>
        <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.blurView} onPress={closeModal} />
        <View style={styles.mainView}>
          <MyText
            text="Verify Appointment Booking"
            fontFamily="bold"
            textAlign="center"
            fontSize={16}
          />
          <MyText
            text={`Please enter 4 digits Code received in consumer appointment`}
            textAlign="center"
            textColor="theme_green"
            fontFamily="medium"
            marginVertical={5}
          />
          <View style={styles.flexRowView}>
            <TextInput 
              placeholder=""
              ref={firstCodeRef}
              value={firstCode}
              onTouchStart={() => message ? setMessage('') : null} 
              maxLength={1} 
              keyboardType="number-pad"
              onChangeText={text => {
                setFirstCode(text);
                if (text.length == 1) {
                  secondCodeRef.current.focus();
                } else {
                  firstCodeRef.current.focus();
                }
              }}
              onSubmitEditing={() => secondCodeRef.current.focus()}
              style={{
                ...styles.textInputStyle,
                backgroundColor:
                  firstCode == '' ? Colors.WHITE : Colors.THEME_GREEN,
                color: firstCode == '' ? Colors.BLACK : Colors.WHITE,
              }}
            />
            <TextInput
              ref={secondCodeRef}
              value={secondCode}
              onTouchStart={() => message ? setMessage('') : null}
              placeholder={''}
              placeholderTextColor={
                secondCode == '' ? Colors.BLACK : Colors.WHITE
              }
              maxLength={1}
              keyboardType="number-pad"
              onChangeText={text => {
                setSecondCode(text);
                if (text.length == 1) {
                  thirdCodeRef.current.focus();
                } else {
                  firstCodeRef.current.focus();
                }
              }}
              onSubmitEditing={() => thirdCodeRef.current.focus()}
              style={{
                ...styles.textInputStyle,
                backgroundColor:
                  secondCode == '' ? Colors.WHITE : Colors.THEME_GREEN,
                color: secondCode == '' ? Colors.BLACK : Colors.WHITE,
              }}
            />
            <TextInput
              ref={thirdCodeRef}
              value={thirdCode}
              onTouchStart={() => message ? setMessage('') : null}
              placeholder={''}
              placeholderTextColor={thirdCode == '' ? Colors.BLACK : Colors.WHITE}
              maxLength={1}
              keyboardType="number-pad"
              onChangeText={text => {
                setThirdCode(text);
                if (text.length == 1) {
                  forthCodeRef.current.focus();
                } else {
                  secondCodeRef.current.focus();
                }
              }}
              onSubmitEditing={() => forthCodeRef.current.focus()}
              style={{
                ...styles.textInputStyle,
                backgroundColor:
                  thirdCode == '' ? Colors.WHITE : Colors.THEME_GREEN,
                color: thirdCode == '' ? Colors.BLACK : Colors.WHITE,
              }}
            />
            <TextInput
              ref={forthCodeRef}
              value={forthCode}
              placeholder=""
              onTouchStart={() => message ? setMessage('') : null}
              placeholderTextColor={forthCode == '' ? Colors.BLACK : Colors.WHITE}
              maxLength={1}
              keyboardType="number-pad"
              onChangeText={text => {
                setForthCode(text);
                if (text.length == 1) {
                  Keyboard.dismiss();
                } else {
                  thirdCodeRef.current.focus();
                }
              }}
              style={{
                ...styles.textInputStyle,
                backgroundColor:
                  forthCode == '' ? Colors.WHITE : Colors.THEME_GREEN,
                color: forthCode == '' ? Colors.BLACK : Colors.WHITE,
              }}
            />
          </View>
          {message ?
          <MyText
            text={message}
            fontFamily="bold"
            textAlign="center"
            fontSize={16}
            textColor={Colors.RED}
          />:null}    
          <View style={{height: 20}} />
          <MyButton Title="Validate Booking" onPress={validateBookingPress}/>
        </View>
      </View>
      </KeyboardAvoidingView>
    </Modal>
  );
};

export default VerifyApptOtp;
