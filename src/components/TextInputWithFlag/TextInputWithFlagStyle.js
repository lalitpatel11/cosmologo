import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  flagNumberView: {
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    backgroundColor: Colors.WHITE,
    shadowRadius: 15,
    elevation: 2,
    padding: 10,
    backgroundColor: Colors.WHITE,
    borderRadius: 5,
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  flexRowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
