import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {Colors, MyIcon} from 'global/Index';
import MyText from 'components/MyText/MyText';

const WelcomeCard = ({
  Title,
  backgroundColor = Colors.THEME_GREEN,
  Icon_bg_Color = Colors.LITE_GREEN,
  onPress = () => {},
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={onPress}
      style={{
        backgroundColor: backgroundColor,
        padding: 20,
        width: '48%',
        borderRadius: 20,
      }}>
      <View
        style={{
          backgroundColor: Icon_bg_Color,
          padding: 10,
          borderRadius: 100,
          alignSelf: 'flex-start',
        }}>
        <MyIcon.AntDesign name="user" size={24} color={Colors.WHITE} />
      </View>
      <MyText
        text={Title}
        textColor="white"
        fontFamily="medium"
        fontSize={12}
        marginVertical={10}
      />
      <View
        style={{
          alignSelf: 'flex-end',
        }}>
        <MyIcon.AntDesign name="rightcircle" size={35} color={Colors.WHITE} />
      </View>
    </TouchableOpacity>
  );
};

export default WelcomeCard;
