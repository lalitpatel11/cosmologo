import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    editButtonContainer:{
        backgroundColor: Colors.WHITE,
        borderRadius: 100,
        height:34,
        width:34,
        justifyContent:'center',
        alignItems:'center',
      },
});