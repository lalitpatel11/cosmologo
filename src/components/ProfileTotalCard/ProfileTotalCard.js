import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import MyText from 'components/MyText/MyText';
import {Colors, Images, MyIcon} from 'global/Index';
//styles
import {styles} from './ProfileTotalCardStyle';


const ProfileTotalCard = ({
  Icon,
  VectorIcon,
  Title,
  Amount,
  onPress = () => {},
  width,
  marginHorizontal,
  marginVertical = 10,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: width,
        height: 40,
        // backgroundColor: Colors.THEME_GREEN,
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'center',
        borderRadius: 10,
        padding: 5,
        marginHorizontal: marginHorizontal,
        marginVertical: marginVertical,
      }}>
        <MyText
        text={Title}
        fontSize={12}
        fontFamily="medium"
        textColor={Colors.THEME_GREEN}
        marginHorizontal={5}
        />
      <View
        style={{
            backgroundColor: Colors.WHITE,
            borderRadius: 100,
            padding: 5,
            shadowColor: '#000',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.1,
            backgroundColor: Colors.WHITE,
            shadowRadius: 15,
            elevation: 2,
            flexDirection:'row',
            justifyContent:'space-evenly',
            alignItems:'center'
        }}>
        <MyText
        text={Amount}
        fontSize={12}
        fontFamily="medium"
        textColor={Colors.THEME_GREEN}
        marginHorizontal={5}
        />
        <TouchableOpacity onPress={()=>{}} style={[styles.editButtonContainer, {marginRight:10}]}>
            <MyIcon.MaterialIcons
            name="delete-outline"
            size={20}
            color={Colors.RED}
            />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

export default ProfileTotalCard;
