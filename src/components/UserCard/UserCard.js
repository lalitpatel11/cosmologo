import {View, Text, Image} from 'react-native';
import React from 'react';
import {Colors, Service} from 'global/Index';
import MyText from 'components/MyText/MyText';
import { width } from 'global/Constant';

const UserCard = ({userImageUrl, UserName, status, service_time}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        borderRadius: 20,
        padding: 10,
        height: 120,
        borderWidth: 1,
        borderColor: Colors.BG_GREEN,
        marginVertical: 10,
      }}>
      {/* <View
        style={{
          borderTopRightRadius: 20,
          borderBottomLeftRadius: 20,
          position: 'absolute',
          backgroundColor: Colors.THEME_GREEN,
          paddingHorizontal: 30,
          paddingVertical: 2,
          right: 1,
      }}>
        <MyText text={status} fontSize={12} textColor="white"/>
      </View> */}
      <Image
        resizeMode="contain"
        source={ (userImageUrl === "" || userImageUrl === `${Service.BASE_URL.replace('api/','')}public`) ? require('assets/images/profile_pic.png') : {uri: userImageUrl}}
        style={{height: '100%', width: '30%', borderRadius: 20}}
      />
      {/* <View style={{marginLeft: 20, marginRight:20, width:'70%', marginTop: service_time ? 0:20}}> */}
      <View style={{marginLeft: 20, marginRight:20, width:'70%', marginTop: 20}}>
        <MyText text={UserName} fontSize={16} fontFamily="bold" />
        <Text style={{fontSize:12, marginTop:5, width:'90%'}}>Duration: {service_time}</Text>
        {/* <MyText text={`Location: ${user_address}`} fontSize={12} fontFamily="bold" textColor={'#B6C6D3'} marginTop={5} /> */}
      </View>
    </View>
  );
};

export default UserCard;
