//react components
import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//global
import {Colors, Images, MyIcon} from 'global/Index';

const DrawerItem = ({Icon, VectorIcon, Title, onPress = () => {}}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderWidth: 0.5,
        paddingHorizontal: 10,
        borderRadius: 10,
        marginVertical: 10,
        height: 55,
      }}>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        {VectorIcon ? (
          VectorIcon
        ) : (
          <Image
            resizeMode="contain"
            source={Icon}
            style={{height: 24, width: 24}}
          />
        )}

        <MyText text={Title} marginHorizontal={10} />
      </View>
      <Image
        resizeMode="contain"
        source={Images?.drawerItemIcon?.rightArrow}
        style={{height: 24, width: 24}}
      />
    </TouchableOpacity>
  );
};

export default DrawerItem;
