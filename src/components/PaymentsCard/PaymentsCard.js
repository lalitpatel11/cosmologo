//react components
import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';
import MyRoundedButton from 'components/MyRoundedButton/MyRoundedButton';
import DoctorCard from 'components/DoctorCard/DoctorCard';
import UserCard from 'components/UserCard/UserCard';
//global
import {Colors, MyIcon, Constant} from 'global/Index';
//styles
import {styles} from './PaymentsCardStyle';

const AppointmentCard = ({
  BookingID,
  ConcernArea = '',
  Product = '',
  SubCategory = '',
  ImageUrl,
  Amount = 0,
  DoctorName,
  DrProfileUrl,
  AppointmentTime,
  onPress = () => {},
  downloadInvoice = () => {},
  acceptAppointment = () => {},
  rejectAppointment = () => {},
  verifyAction = () => {},
  approvedStatus = '',
  isDoctor = true,
  isVerified,
  userImageUrl,
  UserName,
  SPcancelAppointment=()=>{},
  Role,
  Screen = '',
  duration = '',
  showRefund = false,
  refundAmount = null,
  userCancelAppointment=()=>{},
  showButtons = false,
  showVerifyButton = false,
  showStatus = false,
  generate = false,
  status = '',
  showOtp = '',
  otp = '',
  hideViewOtpButton = true,
  changeShowOtpValue = ()=>{},
  user_address = '',
  showUserCancelButton = false,
  showAppointmentDuration = false,
  service_time = '',
  showDoctorUserCard=true
}) => {
  //variables
  return (
    <View
      activeOpacity={0.8}
      // onPress={onPress}
      style={styles.container}>
      <View
        style={{
          borderTopRightRadius: 20,
          borderBottomLeftRadius: 20,
          position: 'absolute',
          backgroundColor: (status === 'cancelled') ? Colors.RED : Colors.THEME_GREEN,
          paddingHorizontal: 30,
          paddingVertical: 2,
          right: 1,
      }}>
        <MyText text={status} fontSize={12} textColor="white" style={{textTransform: 'capitalize'}}/>
      </View>

      <View style={styles.bookingIDView}>
        <MyText text={BookingID} textColor="theme_green" fontFamily="bold" />
      </View>  
      <View style={styles.flexRowView}>
        <Image
          source={{uri: ImageUrl}}
          style={{height: 60, width: 60, borderRadius: 10}}
        />
        <MyText
          text={Role === 'Service' ? `You received payment for ${SubCategory} (${Product}, ${ConcernArea})` : `You made payment for ${SubCategory} (${Product}, ${ConcernArea})`}
          marginHorizontal={10}
          width="70%"
        />
      </View>
      <View style={styles.calendarDateView}>
        <View style={{flexDirection:'row', alignItems:'center'}}>
          <MyIcon.AntDesign name="calendar" size={20} color={'#B1B1B1'} />
          <MyText
            // text={"Sat, 24 Oct 2021, 9:30 AM"}
            text={`Payment date:`}
            marginHorizontal={10}
            fontSize={12}
          />
        </View>
        <MyText
          // text={"Sat, 24 Oct 2021, 9:30 AM"}
          text={`${AppointmentTime}`}
          // marginHorizontal={10}
          fontSize={12}
        />
      </View>
      {(Role === 'User') && duration ?
      <View style={[styles.bookingIDView, {marginBottom: hideViewOtpButton ? 5 : 0}]}>
        <View style={styles.durationView}>
          <MyText text="Duration : " fontSize={12} />
          <MyText
            text={duration}
            fontSize={12}
            textColor="theme_green"
          />
        </View>
        {!hideViewOtpButton ?
          showOtp ?
          <View
            style={{
              backgroundColor: 'transparent' ,
              borderWidth: 1,
              borderColor: Colors.THEME_GREEN,
              justifyContent: 'center',
              alignItems: 'center',
              // alignSelf: alignSelf,
              flexDirection: 'row',
              height: 40,
              borderRadius: 5,
              width: "30%",
              marginVertical: 10,
            }}>
            <MyText text={otp} fontSize={12} />
          </View> 
          :
          <MyButton
            Title={"View Code"}
            width="30%"
            fontSize={12}
            backgroundColor={Colors.THEME_GREEN}
            // alignSelf='flex-end'
            onPress={changeShowOtpValue}
        />:null}
      </View>
        :null}
      <View style={styles.dottedLineStyle} />
      {showDoctorUserCard ?
        isDoctor ? (
          <DoctorCard
            disabled={true}
            Name={DoctorName}
            ProfileImageUrl={DrProfileUrl}
          />
        ) : (
          <UserCard userImageUrl={userImageUrl} UserName={UserName} status={status} service_time={service_time}/>
        )
      :null}
      {/* <MyText
        text={(Role === 'Service Provider')? `Payment paid of $ ${Amount}`:`Your payment of $ ${Amount} is successfully completed`}
        // marginVertical={10}
        textColor={'#B6C6D3'}
        textAlign='center'
      /> */}
      <View>
        <MyText
          text={Role === 'Service' ? `Payment paid of $ ${Amount}` : `Your payment of $ ${Amount} is successfully completed`}
          // marginVertical={10}
          textColor={"#808080"}
          textAlign='center'
          />
        <View style={{flexDirection:'row', justifyContent:'center', marginTop:10}}>
          <TouchableOpacity
            onPress={downloadInvoice}
            style={styles.buttonView}>
            <MyIcon.AntDesign
              name="download"
              size={20}
              color={Colors.THEME_GREEN}
            />
            <MyText
              text="Download Invoice"
              marginHorizontal={5}
              fontFamily="medium"
              textColor="theme_green"
            />
          </TouchableOpacity>
        </View>
          {showRefund && refundAmount !== null ?
          <MyText
            text={`Refund Amount: $ ${refundAmount}`}
            // marginVertical={10}
            // textColor={"#808080"}
            textAlign='center'
            marginTop={10}
          />:null}
      </View>
      {/* <View style={{flexDirection:'row', justifyContent:'center', marginTop:15, marginBottom:5,}}>
      { Role === 'User' && showUserCancelButton ?
      <>
        <MyRoundedButton
          Title={Screen === 'Bookings' ? "Cancel Booking" : "Cancel"}
          backgroundColor={(status === 'Completed') ? '#EBEBE4' : Colors.RED}
          fontSize={14}
          height={50}
          width="45%"
          marginVertical={0}
          onPress={userCancelAppointment}
          />
        </>
      : null}
      </View> */}
    </View>
  );
};

export default AppointmentCard;
