import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: Colors.WHITE,
    borderRadius: 20,
    marginVertical: 10,
    elevation: 3,
  },
  flexRowView: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  calendarDateView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    justifyContent:'space-between'
  },
  dottedLineStyle: {
    borderStyle: 'dotted',
    borderWidth: 1,
    borderRadius: 1,
    marginVertical: 5,
    borderColor: '#B6C6D3',
  },
  durationView:{
    flexDirection:'row',
    // marginBottom:10,
    alignItems:'center'
  },
  bookingIDView:{
    flexDirection:'row', 
    justifyContent:'space-between',
    alignItems:'center'
  },
  buttonView: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    alignSelf: 'flex-start',
    borderColor: Colors.THEME_GREEN,
    padding: 10,
    borderRadius: 5,
  },
});
