//react components
import React from 'react';
import {View, TouchableOpacity} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//globals
import {Colors, MyIcon} from 'global/Index';

const TitleViewAll = ({Title, isPlusButton, onPress = () => {}}) => {
  //UI
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 10,
      }}>
      <MyText text={Title} fontSize={16} fontFamily="bold" />
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        {isPlusButton ? (
          <>
            <TouchableOpacity
              onPress={onPress}
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <MyIcon.AntDesign
                name="plussquare"
                color={Colors.THEME_GREEN}
                size={24}
              />
              <MyText
                text="Add More Services"
                textColor="theme_green"
                marginHorizontal={5}
              />
            </TouchableOpacity>
          </>
        ) : (
          <>
            <TouchableOpacity
              onPress={onPress}
              style={{flexDirection: 'row', alignItems: 'center'}}>
              <MyText text="View All" marginHorizontal={10} />

              <MyIcon.AntDesign
                name="rightsquare"
                color={Colors.THEME_GREEN}
                size={24}
              />
            </TouchableOpacity>
          </>
        )}
      </View>
    </View>
  );
};

export default TitleViewAll;
