//import : react components
import React, {useCallback, useEffect} from 'react';
import {View, TouchableOpacity, Image, Keyboard, BackHandler} from 'react-native';
import {DrawerActions, useNavigation, useFocusEffect, CommonActions} from '@react-navigation/native';
//import : custom components
import MyText from 'components/MyText/MyText';
import WriteDataToAsync from 'components/WriteDataToAsync/WriteDataToAsync';
//import : global
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//import : styles
import {styles} from './MyHeaderWithoutLoginStyle';
//redux
import {useSelector, useDispatch} from 'react-redux';
import {logOutUser} from 'src/reduxToolkit/reducer/user';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import { width } from 'global/Constant';
const MyHeader = ({
  Title,
  hasDrawerButton = false,
  isBorderRadius = true,
  IsCartIcon = true,
  IsNotificationIcon = true,
  hideAddMore = true,
  backAction = () => {},
  isCustomBackAction = false,
  hasLeftButton = true,
  disableHardwareBackButton = false
}) => {
  //variables
  const navigation = useNavigation();
  //function : navigation function
  const openDrawer = () => navigation.dispatch(DrawerActions.openDrawer());
  const goBack = () =>{
    Keyboard.dismiss()
    if(isCustomBackAction){
      backAction()
    }else{
      navigation.canGoBack() ? navigation.goBack() : console.log("can't go back");
    }
  }
  const handleBackButton = () => {
    return true;
  }
  
  disableHardwareBackButton && useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackButton,
    );
    return () => backHandler.remove();
  }, []);
  //UI
  if(!hasLeftButton){
    return (
    <View
      style={{
        ...styles.container,
        borderBottomLeftRadius: isBorderRadius ? 30 : 0,
        borderBottomRightRadius: isBorderRadius ? 30 : 0,
        justifyContent:'center'
      }}>
      {/* <WriteDataToAsync /> */}

      {/* title section  */}
      <View style={{flexDirection: 'row', alignItems: 'center', }}>
        <MyText
          text={Title}
          fontFamily="black"
          fontSize={16}
          marginHorizontal={10}
          textColor="white"
        />
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
      </View>
    </View>
    )
  }
  return (
    <View
      style={{
        ...styles.container,
        borderBottomLeftRadius: isBorderRadius ? 30 : 0,
        borderBottomRightRadius: isBorderRadius ? 30 : 0,
      }}>
      {/* <WriteDataToAsync /> */}

      {/* section first drawer and back icon  */}
      <TouchableOpacity onPress={hasDrawerButton ? openDrawer : goBack}>
        {hasDrawerButton ? (
          <Image
            resizeMode="contain"
            source={Images.NavIcon}
            style={{
              height: 24,
              width: 24,
            }}
          />
        ) : (
          <MyIcon.Feather
            name={Title ? 'arrow-left' : 'menu'}
            size={24}
            color={Colors.WHITE}
          />
        )}
      </TouchableOpacity>
      {/* title section  */}
      <View style={{flexDirection: 'row', alignItems: 'center', marginRight:40,}}>
        <MyText
          text={Title}
          fontFamily="black"
          fontSize={16}
          marginHorizontal={10}
          textColor="white"
        />
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
      </View>
    </View>
  );
};

export default MyHeader;
