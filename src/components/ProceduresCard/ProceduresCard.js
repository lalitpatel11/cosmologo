//react components
import React from 'react';
import {TouchableOpacity, ImageBackground} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//third parties
import LinearGradient from 'react-native-linear-gradient';
//globals
import {Colors, Service} from 'global/Index';

const ProceduresCard = ({item, onPress = () => {}}) => {
  //UI
  return (
    <TouchableOpacity
      style={{
        marginVertical: 10,
        backgroundColor: Colors.WHITE,
        borderRadius: 20,
      }}
      onPress={onPress}>
      <ImageBackground
        borderRadius={20}
        resizeMode="cover"
        source={{
          uri: item.background_image,
        }}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          colors={['#469743' + 'dd', '#73a26d' + '11']}
          style={{
            padding: 20,
            borderRadius: 20,
          }}>
          <MyText
            text={item.title}
            fontSize={18}
            fontFamily="medium"
            textColor="white"
          />
          <MyText
            text={item.sub_title}
            fontSize={14}
            fontFamily="mediumItalic"
            textColor="white"
            marginVertical={5}
          />
          <MyText
            text={item.description}
            numberOfLines={3}
            fontSize={14}
            textColor="white"
            marginVertical={5}
          />
        </LinearGradient>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default ProceduresCard;
