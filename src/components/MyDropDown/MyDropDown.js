//react components
import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//global
import {Colors, MyIcon} from 'global/Index';

const MyDropDown = ({Data, value, setValue}) => {
  //states
  const [showDropDown, setShowDropDown] = useState(false);
  return (
    <View
      style={{
        backgroundColor: Colors.WHITE,
        borderRadius: 20,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.4,
        shadowRadius: 5,
        elevation: 2,
      }}>
      <TouchableOpacity
        onPress={() => setShowDropDown(!showDropDown)}
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          borderRadius: 20,
          shadowColor: '#000',
          backgroundColor: Colors.WHITE,
          shadowOffset: {width: 0, height: 2},
          shadowOpacity: 0.4,
          shadowRadius: 5,
          elevation: 2,
          padding: 5,
          paddingVertical: 10,
        }}>
        <MyText
          text={Object.keys(value).length > 0 ? value.title : Data[0].title}
        />
        <MyIcon.AntDesign name="down" size={20} />
      </TouchableOpacity>
      {showDropDown ? (
        <>
          {Data?.length > 0
            ? Data.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      setValue(item);
                      setShowDropDown(!showDropDown);
                    }}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      padding: 5,
                      paddingVertical: 10,
                      borderBottomWidth: Data.length - 1 == index ? 0 : 0.5,
                    }}>
                    <MyText text={item.title} />
                  </TouchableOpacity>
                );
              })
            : null}
        </>
      ) : null}
    </View>
  );
};

export default MyDropDown;
