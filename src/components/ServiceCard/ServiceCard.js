//react components
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Linking,
} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//third parties
import LinearGradient from 'react-native-linear-gradient';
//global
import {Colors, Constant, MyIcon} from 'global/Index';

const ServiceCard = ({
  Title,
  SubText,
  index,
  imageUrl,
  hasDeleteButton,
  selectedItem,
  onPress = () => {},
  CardPress = () => {},
  deletePress = () => {},
  height = 250,
  disabled = true,
  width = Constant.width / 2 - 30,
  item,
}) => {
  //UI
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={CardPress}
      style={{
        height: height,
        width: width,
        marginRight: 10,
        marginBottom: 10,
        borderWidth: selectedItem == Title ? 2 : 0,
        borderColor: Colors.THEME_GREEN,
        borderRadius: 20,
      }}>
      <ImageBackground
        resizeMode="cover"
        source={{uri: imageUrl}}
        borderRadius={19}
        style={{height: '100%', width: '100%', borderRadius: 20}}>
        <LinearGradient
          start={{x: 0, y: 1}}
          end={{x: 0, y: 0}}
          colors={['#469743' + 'dd', '#73a26d' + '11']}
          style={{
            height: '100%',
            width: '100%',
            borderRadius: 15,
          }}>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              paddingBottom: 10,
              paddingLeft: 10,
            }}>
            <MyText
              text={Title}
              textColor="white"
              fontSize={18}
              fontFamily="bold"
            />
            {SubText ? (
              <MyText
                text={SubText}
                textColor="white"
                fontSize={18}
                marginTop={10}
                fontFamily="bold"
              />
            ) : null}
            <TouchableOpacity onPress={() => Linking.openURL(item.link)}>
              <MyText
                text="More Info"
                isUnderLine
                fontFamily="bold"
                textColor="white"
              />
            </TouchableOpacity>

            {hasDeleteButton ? (
              <TouchableOpacity
                onPress={deletePress}
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 10,
                  backgroundColor: Colors.BG_GREEN,
                  borderRadius: 100,
                  padding: 5,
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginVertical: 10,
                }}>
                <MyIcon.MaterialIcons
                  name="delete"
                  color={Colors.RED}
                  size={24}
                />
              </TouchableOpacity>
            ) : null}
          </View>
        </LinearGradient>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default ServiceCard;
