import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import MyText from 'components/MyText/MyText';
import {Colors, Images, MyIcon} from 'global/Index';

const SelectedService = ({
  BookingId,
  ConcernArea = '',
  Product = '',
  SubCategory,
  ImageUrl,
  Amount = 0.0,
  downloadInvoicePress = () => {},
}) => {
  return (
    <View style={styles.serviceView}>
      <MyText
        text={BookingId}
        textColor="theme_green"
        fontFamily="bold"
        marginVertical={10}
      />
      <View style={styles.flexRowView}>
        <Image
          source={{uri: ImageUrl}}
          resizeMode="contain"
          borderRadius={10}
          style={{
            height: 70,
            width: 70,
          }}
        />
        <View style={styles.textView}>
          <MyText
            text={`${ConcernArea}(${Product})`}
            fontSize={16}
            fontFamily="medium"
          />
          <MyText text={SubCategory} textColor="theme_green" marginTop={10} />
        </View>
      </View>
      <View style={styles.paymentMsgView}>
        <MyText
          text={`Your payment of $${Amount} is successfully completed.`}
          textColor="theme_green"
          textAlign="center"
        />
      </View>
      <TouchableOpacity
        onPress={downloadInvoicePress}
        style={styles.buttonView}>
        <MyIcon.AntDesign
          name="download"
          size={20}
          color={Colors.THEME_GREEN}
        />
        <MyText
          text="Download Invoice"
          marginHorizontal={5}
          fontFamily="medium"
          textColor="theme_green"
        />
      </TouchableOpacity>
    </View>
  );
};

export default SelectedService;

const styles = StyleSheet.create({
  serviceView: {
    backgroundColor: Colors.WHITE,
    padding: 10,
    borderRadius: 10,
  },
  flexRowView: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  textView: {
    marginLeft: 20,
  },
  paymentMsgView: {
    backgroundColor: Colors.BG_GREEN,
    padding: 5,
    paddingVertical: 10,
    marginVertical: 10,
  },
  buttonView: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    alignSelf: 'flex-start',
    borderColor: Colors.THEME_GREEN,
    padding: 10,
    borderRadius: 5,
  },
});
