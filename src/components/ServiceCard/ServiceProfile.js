//react components
import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//global
import {Colors, Images, MyIcon} from 'global/Index';

const ServiceProfile = () => {
  return (
    <TouchableOpacity
      style={{
        height: 150,
        width: 150,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginHorizontal: 10,
      }}>
      <Image
        source={Images.ProceduresIcon.serviceImage}
        style={{
          width: '100%',
          height: '100%',
          borderRadius: 100,
        }}
      />
      <View style={{position: 'absolute', alignItems: 'center'}}>
        <MyIcon.MaterialIcons name="delete" color={Colors.WHITE} size={24} />
        <MyText
          text="Delete"
          textColor="white"
          fontSize={16}
          fontFamily="black"
        />
      </View>
    </TouchableOpacity>
  );
};

export default ServiceProfile;
