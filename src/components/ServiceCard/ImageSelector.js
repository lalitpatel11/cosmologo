//react components
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//globals
import {Colors, MyIcon} from 'global/Index';

const ImageSelector = () => {
  //UI
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
        backgroundColor: Colors.WHITE,
        borderRadius: 10,
        marginVertical: 10,
      }}>
      <MyText text="Service Profile Image" fontFamily="medium" />
      <TouchableOpacity
        style={{
          backgroundColor: Colors.BG_GREEN,
          alignSelf: 'flex-start',
          padding: 5,
          borderRadius: 5,
        }}>
        <MyIcon.AntDesign name="upload" size={24} color={Colors.THEME_GREEN} />
      </TouchableOpacity>
    </View>
  );
};

export default ImageSelector;
