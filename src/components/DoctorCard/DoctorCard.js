//react components
import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
//custom components
import MyText from 'components/MyText/MyText';
//global
import {Colors, Images, MyIcon, Service, ScreenNames} from 'global/Index';
//import : redux
import {useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const DoctorCard = ({
  Name,
  ProfileImageUrl,
  onPress = () => {},
  style,
  Icon = false,
  disabled = false,
}) => {
  //variables : redux variables
  const dispatch = useDispatch();
  const navigation = useNavigation();
  //UI
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={{
        ...style,
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        borderRadius: 20,
        padding: 10,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.1,
        backgroundColor: Colors.WHITE,
        elevation: 2,
        marginVertical: 10,
      }}>
      <Image
        resizeMode="contain"
        // source={ProfileImageUrl === "" ? Images.ProfileIcon.noImage : {uri: ProfileImageUrl}}
        source={(ProfileImageUrl === "" || ProfileImageUrl === `${Service.BASE_URL.replace('api/','')}public`) ? require('assets/images/profile_pic.png') : {uri: ProfileImageUrl}}
        style={{height: '100%', width: '30%', borderRadius: 20}}
      />
      <View style={{marginLeft: 20}}>
        <MyText text={Name} fontSize={16} fontFamily="bold" />
        {/* <MyText
          text="Service on demand"
          textColor={'#B6C6D3'}
          fontFamily="bold"
          marginVertical={10}
        /> */}
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: '90%',
            alignItems: 'center',
            marginVertical: 10
          }}>
          <TouchableOpacity style={{flexDirection: 'row'}} onPress={()=>{navigation.navigate(ScreenNames.CONTACT_US_SCREEN);}}>
            <MyIcon.AntDesign
              name="mail"
              color={Colors.THEME_GREEN}
              size={16}
            />
            <MyText
              text="Contact us"
              fontSize={13}
              textColor="theme_green"
              marginLeft={5}
            />
          </TouchableOpacity>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
            <MyIcon.Ionicons
              name="time-outline"
              color={Colors.THEME_GREEN}
              size={16}
            />
            <MyText
              text="09:00 PM - 5:00 PM"
              fontSize={13}
              marginLeft={5}
              textColor="#808080"
            />
          </View>
        </View>
      </View>
      {Icon ? (
        <MyIcon.AntDesign
          name="checkcircle"
          style={{
            position: 'absolute',
            right: 10,
            top: 10,
          }}
          size={24}
          color={Colors.THEME_GREEN}
        />
      ) : null}
    </TouchableOpacity>
  );
};

export default DoctorCard;
