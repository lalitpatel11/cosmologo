import {View, Text} from 'react-native';
import React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Colors} from 'global/Index';
import MyText from 'components/MyText/MyText';

const RadioCard = ({Title, index, selectedValue, setSelectedValue}) => {
  return (
    <TouchableOpacity
      onPress={() => setSelectedValue(index)}
      style={{
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.1,
        backgroundColor: Colors.WHITE,
        shadowRadius: 5,
        elevation: 2,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
        marginVertical: 5,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: index == selectedValue ? Colors.THEME_GREEN : Colors.WHITE,
      }}>
      <View
        style={{
          backgroundColor: Colors.WHITE,
          height: 20,
          width: 20,
          borderRadius: 100,
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: 0.5,
          borderColor: Colors.THEME_GREEN,
        }}>
        <View
          style={{
            height: 13,
            width: 13,
            borderRadius: 100,
            backgroundColor:
              index == selectedValue ? Colors.THEME_GREEN : Colors.WHITE,
          }}
        />
      </View>
      <MyText text={Title} marginHorizontal={10} />
    </TouchableOpacity>
  );
};

export default RadioCard;
