import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import MyText from 'components/MyText/MyText';
import {Colors, Images, MyIcon} from 'global/Index';

const IconButton = ({
  Icon,
  VectorIcon,
  Title,
  onPress = () => {},
  width,
  marginHorizontal,
  marginVertical = 10,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: width,
        height: 40,
        backgroundColor: Colors.THEME_GREEN,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        padding: 5,
        marginHorizontal: marginHorizontal,
        marginVertical: marginVertical,
      }}>
      <View
        style={{
          backgroundColor: Colors.WHITE,
          borderRadius: 100,
          padding: 5,
          shadowColor: '#000',
          shadowOffset: {width: 0, height: 2},
          shadowOpacity: 0.1,
          backgroundColor: Colors.WHITE,
          shadowRadius: 15,
          elevation: 2,
        }}>
        {VectorIcon ? (
          VectorIcon
        ) : (
          <Image
            resizeMode="contain"
            source={Icon}
            style={{height: 18, width: 18}}
          />
        )}
      </View>
      <MyText
        text={Title}
        fontSize={12}
        fontFamily="medium"
        textColor="white"
        marginHorizontal={5}
      />
    </TouchableOpacity>
  );
};

export default IconButton;
