//react components
import React from 'react';
import {View, TouchableOpacity, Keyboard} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//global
import {MyIcon} from 'global/Index';
//styles
import {styles} from './SimpleHeaderStyle';
import {useNavigation} from '@react-navigation/native';
const SimpleHeader = ({title, backButton = true}) => {
  //variables
  const navigation = useNavigation();
  //function : navigation function
  const goBack = () =>{
    Keyboard.dismiss()
    navigation.canGoBack() ? navigation.goBack() : console.log("can't go back");
  }
  return (
    <View style={styles.container}>
      {backButton ? (
        <TouchableOpacity onPress={goBack}>
          <MyIcon.AntDesign name="arrowleft" size={20} />
        </TouchableOpacity>
      ) : (
        <View style={{width: '5%'}} />
      )}

      <MyText text={title} fontSize={18} fontFamily="medium" />
      <View style={{width: '5%'}} />
    </View>
  );
};

export default SimpleHeader;
