import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {MyIcon} from 'global/Index';
import MyText from 'components/MyText/MyText';

const TermOfUseCheck = ({value, setValue, onPress = () => {}}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
      }}>
      <TouchableOpacity
        onPress={() => setValue(!value)}
        hitSlop={{top: 20, bottom: 20, left: 20, right: 20}}>
        {value ? (
          <MyIcon.Ionicons name="checkbox" size={20} />
        ) : (
          <MyIcon.Ionicons name="square-outline" size={20} />
        )}
      </TouchableOpacity>
      <MyText text="I have read and agree to the " marginLeft={10} />
      <TouchableOpacity onPress={onPress}>
        <MyText text="Terms of Use." textColor="theme_green" isUnderLine />
      </TouchableOpacity>
    </View>
  );
};

export default TermOfUseCheck;
