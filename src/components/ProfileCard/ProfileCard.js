//react components
import IconButton from 'components/IconButton/IconButton';
import ProfileTotalCard from 'components/ProfileTotalCard/ProfileTotalCard';
import {Colors, Images, MyIcon, Service} from 'global/Index';
import React from 'react';
import {View, Text, Image} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
//styles
import {styles} from './ProfileCardStyle';
import {EmailPhoneItem} from './ProfileComponents';

const ProfileCard = ({
  Email,
  Phone,
  ProfileImageUrl,
  changePasswordPress = () => {},
  editProfilePress = () => {},
  logout = () => {},
}) => {
  //varibales
  //UI
  return (
    <View style={styles.profileView}>
      <Image
        resizeMode="stretch"
        source={Images.ProfileIcon.profileBg}
        style={styles.backgroundImage}
      />
      <Image source={(ProfileImageUrl == "" || ProfileImageUrl === `${Service.BASE_URL.replace('api/','')}public`) ? require('assets/images/profile_pic.png') : {uri: ProfileImageUrl}} style={styles.profileImage} />
      <View style={{height: '13%'}} />
      <TouchableOpacity onPress={logout}>
        <Image source={Images.ProfileIcon.logout} style={styles.logoutIcon}/>
      </TouchableOpacity>
      <View style={{height: '2%'}} />
      <View style={{alignSelf: 'center'}}>
        <EmailPhoneItem
          title="Email"
          subText={Email}
          Icon={<MyIcon.Feather name="mail" color={Colors.WHITE} size={16} />}
        />
        <EmailPhoneItem
          title="Phone"
          subText={Phone}
          Icon={
            <MyIcon.Feather name="phone-call" color={Colors.WHITE} size={16} />
          }
        />
      </View>

      <View style={styles.flexRowStyle}>
        <IconButton
          Title="Change Password"
          Icon={Images.ProfileIcon.changePasswordIcon}
          onPress={changePasswordPress}
          width="48%"
        />
        <IconButton
          Title="Edit Profile"
          Icon={Images.ProfileIcon.editIcon}
          onPress={editProfilePress}
          width="48%"
        />
      </View>
      {false ?
      <View style={styles.flexRowStyle}>
        <ProfileTotalCard
          Title="Total Payment Received"
          Amount={34233.94}
          Icon={Images.ProfileIcon.changePasswordIcon}
          onPress={()=>{}}
          width="48%"
        />
        <ProfileTotalCard
          Title="Total Payment Completed"
          Amount={342}
          Icon={Images.ProfileIcon.editIcon}
          onPress={()=>{}}
          width="48%"
        />
      </View>
      :null}
    </View>
  );
};

export default ProfileCard;
