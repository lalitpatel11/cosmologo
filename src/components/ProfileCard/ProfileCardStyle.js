import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  profileView: {
    backgroundColor: Colors.WHITE,
    borderRadius: 30,
  },
  backgroundImage: {
    width: '100%',
    height: '50%',
    position: 'absolute',
    // borderRadius: 30,
  },
  profileImage: {
    height: 100,
    width: 100,
    marginTop: -50,
    alignSelf: 'center',
    borderRadius: 100,
    borderWidth: 3,
    borderColor: Colors.THEME_BLUE,
  },
  flexRowStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    marginVertical: 10,
  },
  logoutIcon: {
    height: 50,
    width: 50,
    alignSelf: 'center',
  },
});
