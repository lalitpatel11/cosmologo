import React from 'react';
import MyText from 'components/MyText/MyText';
import {Colors, Images} from 'global/Index';
import {Image, TouchableOpacity, View} from 'react-native';

export const EmailPhoneItem = ({title, subText, Icon}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        marginVertical: 5,
        alignItems: 'center',
      }}>
      <View
        style={{
          backgroundColor: Colors.THEME_GREEN,
          padding: 5,
          borderRadius: 100,
        }}>
        {Icon}
      </View>
      <View style={{marginLeft: 10}}>
        <MyText text={title} textColor="lite_grey" fontSize={10} />
        <MyText text={subText} fontSize={11} marginTop={5} />
      </View>
    </View>
  );
};

export const PaymentReceivedCard = ({
  onPress = () => {},
  Payment = 1234.99,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{height: 150, marginVertical: 10}}>
      <Image
        resizeMode="stretch"
        source={Images.ProfileIcon.paymentReceiveBg}
        style={{position: 'absolute', height: '100%', width: '100%'}}
      />
      <View style={{padding: 20, width: '50%'}}>
        <MyText text="Total Payments Received" fontSize={18} />
        <MyText
          text={`$ ${parseFloat(Payment).toFixed(2)}`}
          marginVertical={10}
        />
      </View>
    </TouchableOpacity>
  );
};
export const TotalCmpltAppointCard = ({
  onPress = () => {},
  Appointment = 123,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{height: 150, marginVertical: 10}}>
      <Image
        resizeMode="stretch"
        source={Images.ProfileIcon.TotalAppntBg}
        style={{position: 'absolute', height: '100%', width: '100%'}}
      />
      <View style={{padding: 20, width: '50%'}}>
        <MyText text="Total Completed Appointments" fontSize={18} />
        <MyText text={Appointment} marginVertical={10} />
      </View>
    </TouchableOpacity>
  );
};
