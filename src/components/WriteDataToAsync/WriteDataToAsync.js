//import : react components
import React, {useEffect, useRef} from 'react';
import {AppState, Platform} from 'react-native';
//import : third parties
import AsyncStorage from '@react-native-async-storage/async-storage';
//import : redux
import {connect} from 'react-redux';

const WriteDataToAsync = ({cartItems, wishList}) => {
  //hook : useRef
  const timeout = useRef(0);
  //function : imp function
  AppState.addEventListener('change', appState => {
    if (Platform.OS === 'android') {
      if (appState === 'background') {
        saveIntoAsync();
      }
    } else if (Platform.OS === 'ios') {
      if (appState === 'inactive') {
        saveIntoAsync();
      }
    }
  });
  const saveIntoAsync = async () => {
    AsyncStorage.setItem('cartItems', JSON.stringify(cartItems));
  };
  useEffect;
  useEffect(() => {
    if (timeout.current) clearTimeout(timeout.current);
    timeout.current = setTimeout(() => {}, 500);
    return () => {
      clearTimeout(timeout.current);
    };
  }, [cartItems]);
  return null;
};

const mapStateToProps = state => ({
  cartItems: state.cart.cartItems,
});
export default connect(mapStateToProps, null)(WriteDataToAsync);
