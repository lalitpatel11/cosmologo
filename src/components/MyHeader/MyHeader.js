//import : react components
import React, {useCallback} from 'react';
import {View, TouchableOpacity, Image, Keyboard} from 'react-native';
import {DrawerActions, useNavigation, useFocusEffect, CommonActions} from '@react-navigation/native';
//import : custom components
import MyText from 'components/MyText/MyText';
import WriteDataToAsync from 'components/WriteDataToAsync/WriteDataToAsync';
//import : global
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//import : styles
import {styles} from './MyHeaderStyle';
//redux
import {useSelector, useDispatch} from 'react-redux';
import {logOutUser} from 'src/reduxToolkit/reducer/user';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
const MyHeader = ({
  Title,
  hasDrawerButton = false,
  isBorderRadius = true,
  IsCartIcon = true,
  IsNotificationIcon = true,
  hideAddMore = true,
  backAction = () => {},
  isCustomBackAction = false,
  outsideApp = false,
}) => {
  //variables
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const cartItems = useSelector(state => state.cart.cartItems);
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  const userNotifications = useSelector(state => state.user.userNotifications);
  const serviceProviderNotifications = useSelector(state => state.user.serviceProviderNotifications);
  useFocusEffect(
    useCallback(() => {
      const checkTokenExpired = async () => {
        try {
          // console.log('header userToken', userToken);
          const resp = await Service.postApiWithToken(
            userToken,
            Service.CHECK_TOKEN_EXPIRY,
            {}
          );
          if (!resp?.data?.status) {
              Toast.show('Please login again', Toast.SHORT)
              await AsyncStorage.clear();
              dispatch(logOutUser());
              navigation.dispatch(resetIndexGoToWelcome);
            }
        } catch (error) {
          console.log('error in checkTokenExpired', error);
        }
      };
      checkTokenExpired();
      return () => {};
    }, []),
  );
  const resetIndexGoToWelcome = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.WELCOME}],
  });
  //function : navigation function
  const openDrawer = () => navigation.dispatch(DrawerActions.openDrawer());
  const goBack = () =>{
    Keyboard.dismiss()
    if(isCustomBackAction){
      backAction()
    }else{
      navigation.canGoBack() ? navigation.goBack() : console.log("can't go back");
    }
  }
  const gotoNotification = () => navigation.navigate(ScreenNames.NOTIFICATION);
  const gotoCart = () => navigation.navigate(ScreenNames.CART, {hideAddMore});
  //UI
  return (
    <View
      style={{
        ...styles.container,
        borderBottomLeftRadius: isBorderRadius ? 30 : 0,
        borderBottomRightRadius: isBorderRadius ? 30 : 0,
      }}>
      <WriteDataToAsync />

      {/* section first drawer and back icon  */}
      <TouchableOpacity onPress={hasDrawerButton ? openDrawer : goBack}>
        {hasDrawerButton ? (
          <Image
            resizeMode="contain"
            source={Images.NavIcon}
            style={{
              height: 24,
              width: 24,
            }}
          />
        ) : (
          <MyIcon.Feather
            name={Title ? 'arrow-left' : 'menu'}
            size={24}
            color={Colors.WHITE}
          />
        )}
      </TouchableOpacity>
      {/* title section  */}
      <View style={{flexDirection: 'row', alignItems: 'center', marginRight: (!IsCartIcon && !IsNotificationIcon) ? 40 : 0}}>
        <MyText
          text={Title}
          fontFamily="black"
          fontSize={16}
          marginHorizontal={10}
          textColor="white"
        />
      </View>
      {/* notification or cart icon  */}
      {(userInfo.user_type == '3' && IsNotificationIcon) ? (
        <TouchableOpacity style={{marginRight: 20}} onPress={gotoNotification}>
              {serviceProviderNotifications?.length > 0 ?
              <View
                style={{
                position: 'absolute',
                top: -10,
                right: -5,
                height: 15,
                width: 15,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.RED,
                borderRadius: 100,
              }}>
              <MyText text={serviceProviderNotifications?.length} fontSize={10} textColor="white" />
            </View>:null}
          {/* <MyText text={serviceProviderNotifications?.length} fontSize={16} textColor="white" style={{position:"absolute", bottom:24, right:5, fontWeight:'bold'}}/> */}
          <MyIcon.Feather name="bell" size={24} color={Colors.WHITE} />
        </TouchableOpacity>
      ) : (
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          {IsNotificationIcon ?  
          <TouchableOpacity
            onPress={gotoNotification} style={{marginRight:20}}>
              {userNotifications?.length > 0 ?
              <View
                style={{
                position: 'absolute',
                top: -10,
                right: -5,
                height: 15,
                width: 15,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: Colors.RED,
                borderRadius: 100,
              }}>
              <MyText text={userNotifications?.length} fontSize={10} textColor="white" />
            </View>:null}
            {/* <MyText text={userNotifications?.length} fontSize={16} textColor="white" style={{position:"absolute", bottom:24, right:5, fontWeight:'bold'}}/> */}
            <MyIcon.Feather name="bell" size={24} color={Colors.WHITE} />
          </TouchableOpacity>:null}
          {IsCartIcon ? (
            <TouchableOpacity onPress={gotoCart}>
              {Object.keys(typeof cartItems === 'object' ? cartItems: {}).length > 0 ? (
                <View
                  style={{
                    position: 'absolute',
                    top: -10,
                    right: -5,
                    height: 15,
                    width: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Colors.RED,
                    borderRadius: 100,
                  }}>
                  <MyText text={1} fontSize={10} textColor="white" />
                </View>
              ) : null}
              <MyIcon.Feather
                name="shopping-cart"
                size={24}
                color={Colors.WHITE}
              />
            </TouchableOpacity>
          ) : null}
        </View>
      )}
    </View>
  );
};

export default MyHeader;
