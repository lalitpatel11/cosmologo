//react components
import React, {useRef, useState, useEffect} from 'react';
import {View, TextInput, TouchableOpacity, Keyboard, Alert} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './OtpValidateStyle';
//third parties
import Toast from 'react-native-simple-toast';
import {useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {useNetInfo} from "@react-native-community/netinfo";

const OtpValidate = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //ref
  const firstCodeRef = useRef();
  const secondCodeRef = useRef();
  const thirdCodeRef = useRef();
  const forthCodeRef = useRef();

  //variables : route variables
  const phoneNumber = route.params.phoneNumber;
  const emailAddress = route.params.emailAddress;
  const userId = route.params.userId;
  const OTP = route.params.OTP;
  const dispatch = useDispatch();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [firstCode, setFirstCode] = useState('');
  const [secondCode, setSecondCode] = useState('');
  const [thirdCode, setThirdCode] = useState('');
  const [forthCode, setForthCode] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [resentOTP, setResentOTP] = useState('');
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //function : navigation function
  const gotoForgotPassword = userId => {
    const isServiceProvider =
      route.params &&
      route.params.role &&
      route.params.role === 'Service Provider'
        ? true
        : false;
    isServiceProvider
      ? navigation.navigate(ScreenNames.USER_FORGOT_PASSWORD, {
          userId: userId,
          role: 'Service Provider',
          OTP: resentOTP ? resentOTP : OTP,
          emailAddress: emailAddress,
        })
      : navigation.navigate(ScreenNames.USER_FORGOT_PASSWORD, {
          userId: userId,
          OTP: resentOTP ? resentOTP : OTP,
          emailAddress: emailAddress,
        });
  };
  //function : imp function
  const Validation = () => {
    if (
      firstCode == '' &&
      secondCode == '' &&
      thirdCode == '' &&
      forthCode == ''
    ) {
      // dispatch(showToast({text: 'Please enter valid OTP'}));
      Toast.show('Please enter valid OTP', Toast.SHORT)
      // Alert.alert('', 'Please enter valid OTP');
    } else if (resentOTP) {
      if (
        Number(firstCode + secondCode + thirdCode + forthCode) !==
        Number(resentOTP)
      ) {
        // dispatch(showToast({text: `OTP doesn't match`}));
        Toast.show(`OTP doesn't match`, Toast.SHORT)
        // Alert.alert('', `OTP doesn't match`);
      } else return true;
    } else if (
      Number(firstCode + secondCode + thirdCode + forthCode) !== Number(OTP)
    ) {
      // dispatch(showToast({text: `OTP doesn't match`}));
      Toast.show(`OTP doesn't match`, Toast.SHORT)
      // Alert.alert('', `OTP doesn't match`);
    } else return true;
  };
  //function : service function
  const ValidateOTP = async () => {
    if (Validation()) {
      gotoForgotPassword('');
      return;
    }
    return;
    if (Validation()) {
      setShowLoader(true);
      try {
        const OTP = firstCode + secondCode + thirdCode + forthCode;
        const validateOTPData = new FormData();
        validateOTPData.append('code', OTP);
        validateOTPData.append('email', emailAddress);
        validateOTPData.append('user_id', userId);
        const resp = await Service.postApi(
          // Service.VALIDATE_CODE,
          Service.FORGET_PASSWORD_VERIFY,
          validateOTPData,
        );
        if (resp.data.status) {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
          gotoForgotPassword(resp.data.user_id);
        } else {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
        }
      } catch (error) {
        console.log('error in ValidateOTP', error);
      }
      setShowLoader(false);
    }
  };
  const ResendOTP = async () => {
    setShowLoader(true);
    try {
      const isServiceProvider =
        route.params &&
        route.params.role &&
        route.params.role === 'Service Provider'
          ? true
          : false;
      const ResendOTPData = new FormData();
      // ResendOTPData.append('phone', phoneNumber);
      ResendOTPData.append('email', emailAddress);
      isServiceProvider
        ? ResendOTPData.append('roletype', 'S')
        : ResendOTPData.append('roletype', 'U');
      console.log('ResendOTPData', ResendOTPData);
      const resp = await Service.postApi(
        Service.FORGET_PASSWORD,
        ResendOTPData,
      );
      console.log('ResendOTP resp', resp);
      if (resp.data.status) {
        setResentOTP(resp.data.otp);
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in ResendOTP', error);
    }
    setShowLoader(false);
  };
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Forgot Password" />
      <View style={styles.mainView}>
        <View style={styles.confirmationView}>
          <MyIcon.Entypo name="check" size={16} color={Colors.THEME_GREEN} />
          <MyText
            text="CONFIRMATION"
            fontFamily="bold"
            textColor="theme_green"
            marginHorizontal={10}
          />
        </View>
        <View style={styles.flexRowView}>
          <MyIcon.Entypo name="mail" size={40} color={Colors.THEME_GREEN} />
          <View style={styles.numberView}>
            <MyText
              text="Please type the verification code sent to your registered email address"
              fontFamily="medium"
            />
            {/* <MyText
              text={`+91 ${phoneNumber}`}
              textColor="theme_green"
              fontFamily="medium"
            /> */}
          </View>
        </View>
        <View style={styles.otpView}>
          <TextInput
            ref={firstCodeRef}
            value={firstCode}
            placeholder={''}
            maxLength={1}
            keyboardType="number-pad"
            onChangeText={text => {
              setFirstCode(text);
              if (text.length == 1) {
                secondCodeRef.current.focus();
              } else {
                firstCodeRef.current.focus();
              }
            }}
            onSubmitEditing={() => secondCodeRef.current.focus()}
            style={{
              ...styles.textInputStyle,
              backgroundColor:
                firstCode == '' ? Colors.WHITE : Colors.THEME_GREEN,
              color: firstCode == '' ? Colors.BLACK : Colors.WHITE,
            }}
          />
          <TextInput
            ref={secondCodeRef}
            value={secondCode}
            placeholder={''}
            placeholderTextColor={
              secondCode == '' ? Colors.BLACK : Colors.WHITE
            }
            maxLength={1}
            keyboardType="number-pad"
            onChangeText={text => {
              setSecondCode(text);
              if (text.length == 1) {
                thirdCodeRef.current.focus();
              } else {
                firstCodeRef.current.focus();
              }
            }}
            onSubmitEditing={() => thirdCodeRef.current.focus()}
            style={{
              ...styles.textInputStyle,
              backgroundColor:
                secondCode == '' ? Colors.WHITE : Colors.THEME_GREEN,
              color: secondCode == '' ? Colors.BLACK : Colors.WHITE,
            }}
          />
          <TextInput
            ref={thirdCodeRef}
            value={thirdCode}
            placeholder={''}
            placeholderTextColor={thirdCode == '' ? Colors.BLACK : Colors.WHITE}
            maxLength={1}
            keyboardType="number-pad"
            onChangeText={text => {
              setThirdCode(text);
              if (text.length == 1) {
                forthCodeRef.current.focus();
              } else {
                secondCodeRef.current.focus();
              }
            }}
            onSubmitEditing={() => forthCodeRef.current.focus()}
            style={{
              ...styles.textInputStyle,
              backgroundColor:
                thirdCode == '' ? Colors.WHITE : Colors.THEME_GREEN,
              color: thirdCode == '' ? Colors.BLACK : Colors.WHITE,
            }}
          />
          <TextInput
            ref={forthCodeRef}
            value={forthCode}
            placeholder=""
            placeholderTextColor={forthCode == '' ? Colors.BLACK : Colors.WHITE}
            maxLength={1}
            keyboardType="number-pad"
            onChangeText={text => {
              setForthCode(text);
              if (text.length == 1) {
                Keyboard.dismiss();
              } else {
                thirdCodeRef.current.focus();
              }
            }}
            style={{
              ...styles.textInputStyle,
              backgroundColor:
                forthCode == '' ? Colors.WHITE : Colors.THEME_GREEN,
              color: forthCode == '' ? Colors.BLACK : Colors.WHITE,
            }}
          />
        </View>
        <MyButton Title="Validate Code" onPress={ValidateOTP} />
        <MyText text="Didn't you received any code?" />
        <TouchableOpacity onPress={ResendOTP} style={styles.resendOtpView}>
          <MyText text="RESEND CODE " fontSize={12} textColor="theme_green" />
          <MyIcon.AntDesign name="right" color={Colors.THEME_GREEN} />
        </TouchableOpacity>
      </View>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default OtpValidate;
