//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  PermissionsAndroid,
  BackHandler,
} from 'react-native';
import {CommonActions, StackActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import PaymentsCard from 'components/PaymentsCard/PaymentsCard';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './PaymentsMadeStyle';
//redux
import {connect, useSelector, useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
//import : third parties
import Toast from 'react-native-simple-toast';
import RNFetchBlob from 'rn-fetch-blob';
import {Dropdown} from 'react-native-element-dropdown';
import moment from 'moment';
import {useNetInfo} from "@react-native-community/netinfo";
//import : modals
import AppointmentCalender from 'modals/AppointmentCalender/AppointmentCalender';
import CancelAppointment from 'modals/CancelAppointment/CancelAppointment';

//data
const data = [
  {label: 'All', value: 'All'},
  {label: 'Completed', value: 'Completed'},
  {label: 'Cancelled', value: 'Cancelled'},
  {label: 'Processing', value: 'Processing'},
];
const PaymentsMade = ({navigation, userToken}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const dispatch = useDispatch();
  const userInfo = useSelector(state => state.user.userInfo);
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [PaymentsData, setPaymentsData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [isFocus, setIsFocus] = useState(false);
  const [aptStatus, setAptStatus] = useState(null);
  const [showCalendarModal, setShowCalendarModal] = useState(false);
  const [startDay, setStartDay] = useState(null);
  const [endDay, setEndDay] = useState(null);
  const [markedDates, setMarkedDates] = useState({});
  const [temporarySelectedDates, setTemporarySelectedDates] = useState([]);
  const [selectedItem, setSelectedItem] = useState({});
  const [showCancelAppt, setShowCancelAppt] = useState(false);
  //function : navigation function
  const gotoAppointmentDetail = (BookingId, duration) =>
    navigation.navigate(ScreenNames.USER_APPOINTMENT_DETAILS, {
      BookingId: BookingId,
      duration,
    });
  //function : service function
  const getPaymentsList = async (cancelledApt = false) => {
    setShowLoader(true);
    try {
      const paymentsListData = new FormData();
      userInfo.user_type == '3'
        ? paymentsListData.append('service_provider_id', userInfo.id)
        : paymentsListData.append('user_id', userInfo.id);
      console.log('userToken', userToken);
      const endPoint =
        userInfo.user_type == '3'
          ? Service.SERVICE_PAYMENT_DETAILS
          : Service.USER_PAYMENT_DETAILS;
      const resp = await Service.postApiWithToken(
        userToken,
        endPoint,
        paymentsListData,
      );
      console.log('getPaymentsList resp', resp);
      if (resp.data.status) {
        console.log('user payments data', resp.data.data);
        const updatedData = resp.data.data?.map(el => {
          return {
            ...el,
            payment_initiated_date_time: moment(
              el?.payment_initiated_date_time,
            ).format('ddd, D MMM YYYY, h:mm A'),
          };
        });
        console.log('updatedData', updatedData);
        setPaymentsData(updatedData);
        if (cancelledApt) {
          if (temporarySelectedDates?.length === 0) {
            console.log('not setting filtered directly');
            setFilteredData(updatedData);
            return;
          }
          // aptStatus && changeAptStatus(aptStatus, updatedData)
          temporarySelectedDates?.length > 0 && OKPressed(updatedData);
        } else {
          setFilteredData(updatedData);
        }
      }
    } catch (error) {
      console.log('error in getPaymentsList', error);
    }
    setShowLoader(false);
  };

  //function : get invoice url
  const getInvoiceUrl = async (appointmentId, bookingId) => {
    try {
      const endPoint = `${Service.DOWNLOAD_INVOICE}?appointment_id=${appointmentId}`;
      const res = await Service.postApiWithToken(userToken, endPoint, {});
      console.log('getInvoiceUrl res', res.data);
      if (res.data.status) {
        checkPermission(res.data.url, bookingId);
      }
    } catch (error) {
      console.log('error', error);
    }
  };
  //function : imp function
  const checkPermission = async (url, bookingId) => {
    if (Platform.OS === 'ios') {
      downloadFile(url, bookingId);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadFile(url, bookingId);
          console.log('Storage Permission Granted.');
        } else {
          dispatch(showToast({text: 'Storage Permission Not Granted'}));
          // Alert.alert('Error', 'Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('ERROR' + err);
      }
    }
  };
  //function : service function
  const downloadFile = async (url, bookingId) => {
    let pdfUrl = url;
    let DownloadDir =
      Platform.OS == 'ios'
        ? RNFetchBlob.fs.dirs.DocumentDir
        : RNFetchBlob.fs.dirs.DownloadDir;
    const {dirs} = RNFetchBlob.fs;
    const dirToSave =
      Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
    const configfb = {
      fileCache: true,
      useDownloadManager: true,
      notification: true,
      mediaScannable: true,
      title: 'Cosmologo',
      path: `${dirToSave}.pdf`,
    };
    const configOptions = Platform.select({
      ios: {
        fileCache: configfb.fileCache,
        title: configfb.title,
        path: configfb.path,
        appendExt: 'pdf',
      },
      android: configfb,
    });
    Platform.OS == 'android'
      ? RNFetchBlob.config({
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path: `${DownloadDir}/${bookingId}.pdf`,
            description: 'Cosmologo',
            title: `${bookingId}invoice.pdf`,
            mime: 'application/pdf',
            mediaScannable: true,
          },
        })
          .fetch('GET', `${pdfUrl}`)
          .catch(error => {
            console.warn(error.message);
          })
      : RNFetchBlob.config(configOptions)
          .fetch('GET', `${pdfUrl}`, {})
          .then(res => {
            if (Platform.OS === 'ios') {
              RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
              RNFetchBlob.ios.previewDocument(configfb.path);
            }
            console.log('The file saved to ', res);
          })
          .catch(e => {
            console.log('The file saved to ERROR', e.message);
          });
  };
  //function : render function
  const paymentRender = ({item, index}) => {
    return (
      <PaymentsCard
        BookingID={item.booking_id}
        SubCategory={item.sub_category_title}
        ConcernArea={item.concern_area}
        Product={item.product_title}
        ImageUrl={item.concern_area_image}
        Amount={parseFloat(item.amount).toFixed(2)}
        DoctorName={item.service_provider}
        DrProfileUrl={item.service_provider_profile_mage}
        AppointmentTime={item.payment_initiated_date_time}
        // onPress={() => gotoAppointmentDetail(item.booking_id, item.duration)}
        downloadInvoice={() =>
          checkPermission(item.invoice_url, item.booking_id)
        }
        showRefund={userInfo.user_type == '3' ? false : true}
        refundAmount={item.refund_amount}
        Role={userInfo.user_type == '3' ? 'Service' : 'User'}
        duration={item.duration}
        showButtons={true}
        userCancelAppointment={() => {
          setSelectedItem(item);
          setShowCancelAppt(true);
        }}
        status={item.payment_status}
        hideViewOtpButton={true}
        showUserCancelButton={
          item.is_verified == 1 ||
          item.cancelled == 3 ||
          item.cancelled == 4 ||
          item.status === 'Completed' ||
          moment(moment(item.date_time).format('YYYY-MM-DD')).isBefore(
            moment().format('YYYY-MM-DD'),
          )
            ? false
            : true
        }
        showDoctorUserCard={false}
      />
    );
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getPaymentsList();
    return () => {};
  }, []);

  const cancelAppointment = async (
    appointmentId,
    isVerified,
    appointmentDate,
    bookingId,
    isCancelled,
    aptStatus,
  ) => {
    if (isVerified == 1) {
      Toast.show('Verified appointment cannot be cancelled', Toast.SHORT);
      return;
    }
    if (aptStatus === 'Completed') {
      Toast.show('Completed appointment cannot be cancelled', Toast.SHORT);
      return;
    }
    if (isCancelled == 3) {
      Toast.show(
        'Appointment already cancelled by Service Provider',
        Toast.SHORT,
      );
      return;
    }
    if (
      moment(moment(appointmentDate).format('YYYY-MM-DD')).isBefore(
        moment().format('YYYY-MM-DD'),
      )
    ) {
      Toast.show('Booking date cannot be in past', Toast.SHORT);
      return;
    }
    setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('appointment_id', appointmentId);
      myData.append('booking_id', bookingId);
      console.log('myData', myData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.CANCEL_BOOKING_USERSIDE,
        myData,
      );
      console.log('resp', resp);
      if (resp.data.status) {
        Toast.show(resp.data.message, Toast.SHORT);
        // setFilteredData(filteredData.filter(el => el.id !== appointmentId))
        // setAppointmentData(AppointmentData.filter(el => el.id !== appointmentId))
        getPaymentsList(true);
        console.log('cancelAppointment', resp.data);
        console.warn('resp------>', resp.data);
      }
    } catch (error) {
      console.log('error in cancelAppointment', error);
    }
    setShowLoader(false);
    setShowCancelAppt(false);
  };
  const changeAptStatus = (value, data = []) => {
    // ! tell sanjay is 24 hrs date_time... then don't add am/pm
    setAptStatus(value);
    // All,Completed,Cancelled,Ongoing
    if (value === 'All') {
      console.log('setFilteredData', PaymentsData);
      setFilteredData(PaymentsData);
      return;
    }
    // else if(value === 'Ongoing'){
    //   const filteredByStatus = data.length > 0 ? data.filter(el => el.status === 'Processing') : AppointmentData.filter(el => el.status === 'Processing')
    //   console.log('setFilteredData', filteredByStatus);
    //   setFilteredData(filteredByStatus)
    //   return
    // }
    else {
      const filteredByStatus =
        data.length > 0
          ? data.filter(el => el.status === value)
          : PaymentsData.filter(el => el.status === value);
      console.log('setFilteredData', filteredByStatus);
      setFilteredData(filteredByStatus);
      return;
    }
  };
  const OKPressed = (data = []) => {
    if (temporarySelectedDates?.length === 0) {
      // dispatch(showToast({text:'Please select a date', duration:2000}))
      setShowCalendarModal(false);
      return;
    }
    const filteredByDays =
      data?.length > 0
        ? data.filter(el => {
            const newTime = el?.payment_initiated_date_time;
            if (
              temporarySelectedDates?.length === 1 &&
              moment(moment(newTime).format('YYYY-MM-DD')).isSame(
                moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
              )
            ) {
              return true;
            } else if (
              temporarySelectedDates?.length >= 2 &&
              moment(moment(newTime)).isBetween(
                moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
                moment(
                  moment(
                    temporarySelectedDates[temporarySelectedDates?.length - 1],
                  ).format('YYYY-MM-DD'),
                ).add(1, 'days'),
              )
            ) {
              return true;
            }
            return false;
          })
        : PaymentsData.filter(el => {
            const newTime = el?.payment_initiated_date_time;
            if (
              temporarySelectedDates?.length === 1 &&
              moment(moment(newTime).format('YYYY-MM-DD')).isSame(
                moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
              )
            ) {
              return true;
            } else if (
              temporarySelectedDates?.length >= 2 &&
              moment(moment(newTime)).isBetween(
                moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
                moment(
                  moment(
                    temporarySelectedDates[temporarySelectedDates?.length - 1],
                  ).format('YYYY-MM-DD'),
                ).add(1, 'days'),
              )
            ) {
              return true;
            }
            return false;
          });
    setFilteredData(filteredByDays);
    // setAptStatus(null)
    setShowCalendarModal(false);
  };
  const clearSelection = () => {
    setTemporarySelectedDates([]);
    setMarkedDates({});
    setStartDay(null);
    setEndDay(null);
    setFilteredData(PaymentsData);
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Payments" />
      <View style={styles.mainView}>
        <View
          style={{
            marginBottom: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <MyText
            text={`${filteredData.length} ${
              filteredData.length === 1 || filteredData.length === 0
                ? 'Payment found'
                : 'Payments found'
            }`}
            marginVertical={20}
            fontFamily="medium"
          />
          <TouchableOpacity
            onPress={() => {
              setShowCalendarModal(true);
            }}
            style={styles.subHeaderView}>
            <MyText
              text={`Select Date`}
              marginVertical={20}
              marginRight={10}
              fontFamily="medium"
            />
            {temporarySelectedDates?.length === 0 ? (
              <MyIcon.AntDesign
                name="calendar"
                size={24}
                color={Colors.THEME_GREEN}
              />
            ) : (
              <MyIcon.FontAwesome5
                name="calendar-check"
                size={24}
                color={Colors.THEME_GREEN}
              />
            )}
          </TouchableOpacity>
        </View>
        <FlatList
          data={filteredData}
          renderItem={paymentRender}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 150}}
        />
      </View>
      <CustomLoader showLoader={showLoader} />
      <AppointmentCalender
        visible={showCalendarModal}
        setVisibility={setShowCalendarModal}
        startDay={startDay}
        setStartDay={setStartDay}
        endDay={endDay}
        setEndDay={setEndDay}
        temporarySelectedDates={temporarySelectedDates}
        setTemporarySelectedDates={setTemporarySelectedDates}
        markedDates={markedDates}
        setMarkedDates={setMarkedDates}
        OKPressed={OKPressed}
        clearSelection={clearSelection}
      />
      <CancelAppointment
        visible={showCancelAppt}
        setVisibility={setShowCancelAppt}
        onPress={() =>
          cancelAppointment(
            selectedItem.id,
            selectedItem.is_verified,
            selectedItem.date_time,
            selectedItem.booking_id,
            selectedItem.cancelled,
            selectedItem.status,
          )
        }
        type={'booking'}
      />
    </View>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(PaymentsMade);
