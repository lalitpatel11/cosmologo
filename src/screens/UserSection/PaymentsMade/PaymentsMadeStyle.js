import {StyleSheet} from 'react-native';
import {Colors} from 'global/Index';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  dropdown: {
    height: 50,
    borderColor: Colors.LITE_GREY,
    borderWidth: 0.5,
    borderRadius: 8,
    paddingHorizontal: 8,
    flex:0.85,
    backgroundColor: Colors.WHITE
  },
  subHeaderView:{
    // backgroundColor: Colors.BG_GREEN,
    // borderRadius: 10,
    // paddingHorizontal: 20,
    // paddingVertical: 2,
    // flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection:'row'
  }
});