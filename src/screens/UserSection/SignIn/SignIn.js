//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyText from 'components/MyText/MyText';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyButton from 'components/MyButton/MyButton';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyPasswordInput from 'components/MyPasswordInput/MyPasswordInput';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//third parties
import {useNetInfo} from '@react-native-community/netinfo';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Geolocation from 'react-native-geolocation-service';
import Toast from 'react-native-simple-toast';
//global
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './SignInStyle';
import {connect} from 'react-redux';
import {setUser, setUserToken} from 'src/reduxToolkit/reducer/user';
import CustomToast from 'modals/CustomToast/CustomToast';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const SignIn = ({navigation, dispatch}) => {
  const {isConnected, isInternetReachable} = useNetInfo();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [emailAddress, setEmailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [fcmToken, setFcmToken] = useState('');

  //FUNCTION : navigation function
  const gotoSignUp = () => {
    navigation.navigate(ScreenNames.USER_SIGNUP);
  };
  const gotoForgotPassword = () =>
    navigation.navigate(ScreenNames.USER_MOBILE_OTP);
  const gotoHome = () => navigation.navigate(ScreenNames.USER_HOME);
  const gotoBottomTab = () => navigation.replace(ScreenNames.USER_BOTTOM_TAB);
  const resetIndexGoToBottomTab = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.USER_BOTTOM_TAB}],
  });
  //function : imp function
  const Validation = () => {
    if (emailAddress == '') {
      Toast.show('Please enter email address', Toast.LONG);
      // dispatch(
      //   showToast({text: 'Please enter email address', duration: 10000}),
      // );
      // Alert.alert('', 'Please enter email address');
    } else if (password == '') {
      Toast.show('Please enter password', Toast.LONG);
      // Alert.alert('', 'Please enter password');
    } else return true;
  };
  const requestLoactionPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Cosmologo App Location Permission',
            message: 'Cosmologo App needs access to your location ',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          return true;
        } else {
          console.warn('herer');
        }
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else {
      Geolocation.requestAuthorization('always' || 'whenInUse').then(
        getLocation(),
      );
    }
  };
  const getLocation = token => {
    try {
      Geolocation.getCurrentPosition(
        position => {
          console.log(position);
          const updateLocationData = new FormData();
          updateLocationData.append('lat', position.coords.latitude);
          updateLocationData.append('long', position.coords.longitude);
          console.warn('updateLocationData', updateLocationData);
          updateLocation(token, updateLocationData);
        },
        error => {
          // See error code charts below.
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (error) {
      console.log('error in updateLocation', error);
    }
  };
  //function : service function
  const signInUser = async () => {
    if (Validation()) {
      setShowLoader(true);
      try {
        const signInData = new FormData();
        signInData.append('email', emailAddress);
        signInData.append('password', password);
        signInData.append('fcmtoken', fcmToken);
        const resp = await Service.postApi(Service.USER_LOGIN, signInData);
        if (resp?.data?.status) {
          await AsyncStorage.setItem('userToken', resp.data.access_token);
          const jsonValue = JSON.stringify(resp.data.data);
          console.log('sign in jsonValue', jsonValue);
          await AsyncStorage.setItem('userInfo', jsonValue);
          // if (requestLoactionPermission()) {
          //   getLocation(resp.data.access_token);
          // } else {
          dispatch(setUserToken(resp.data.access_token));
          dispatch(setUser(resp.data.data));
          navigation.dispatch(resetIndexGoToBottomTab);
          // }
        } else {
          // Alert.alert('', `${resp.data.message}`);
          Toast.show(resp.data.message, Toast.SHORT);
        }
      } catch (error) {
        console.log('error in signInUser', error);
      }
      setShowLoader(false);
    }
  };
  const updateLocation = async (token, data) => {
    setShowLoader(true);
    try {
      const resp = await Service.postApiWithToken(
        token,
        Service.UPDATE_LOCATION,
        data,
      );
      console.warn('resp', resp.data);
    } catch (error) {
      console.log('error in updateLocation', error);
    }
    setShowLoader(false);
  };
  const checkToken = async () => {
    try {
      const token = await messaging().getToken();
      if (token) {
        setFcmToken(token);
      } else {
        console.log('could not get fcm token');
      }
    } catch (error) {
      console.log('error in getting fcm token', error);
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    checkToken();
  }, []);
  const gotoWebPage = async (endPoint, name) => {
    navigation.navigate(ScreenNames.SIDE_MENU_LINKS, {
      endPoint,
      name,
      role: 'User',
    });
  };
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Sign In" />
      <ScrollView style={styles.mainView}>
        <Image
          resizeMode="contain"
          source={Images.AppLogo}
          style={styles.imageStyle}
        />
        <MyTextInput
          Title="Email Address"
          placeholder="Enter email address"
          onChangeText={text => setEmailAddress(text)}
          keyboardType="email-address"
          // Icon={
          //   <MyIcon.AntDesign
          //     name="checkcircleo"
          //     size={24}
          //     color={Colors.THEME_GREEN}
          //   />
          // }
        />
        <MyPasswordInput
          Title="Password"
          placeholder="Enter password"
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
          showPassword
          ShowIcon={
            <MyIcon.FontAwesome5
              name="eye"
              size={20}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          HideIcon={
            <MyIcon.FontAwesome5
              name="eye-slash"
              size={20}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
        />
        <TouchableOpacity
          onPress={gotoForgotPassword}
          style={styles.forgotPassword}>
          <MyText text="Forgot password?" fontFamily="bold" />
        </TouchableOpacity>
        <MyButton Title="Sign In" onPress={signInUser} />
        <TouchableOpacity onPress={gotoSignUp} style={styles.bottomLineStyle}>
          <MyText text="Don't have an account yet? " fontFamily="bold" />
          <MyText text="Sign Up" fontFamily="bold" textColor="theme_green" />
        </TouchableOpacity>
        <View style={styles.termsAndPPView}>
          <MyText text="By logging in, you agree to our" />
          <TouchableOpacity
            onPress={() =>
              gotoWebPage('terms-condition', 'Terms And Conditions')
            }>
            <MyText
              text=" Terms and Conditions "
              textColor="theme_green"
              isUnderLine
            />
          </TouchableOpacity>
          <MyText text="and " />
          <TouchableOpacity
            onPress={() => gotoWebPage('privacy-policy', 'Privacy Policy')}>
            <MyText text="Privacy Policy" textColor="theme_green" isUnderLine />
          </TouchableOpacity>
        </View>
      </ScrollView>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

const mapDispatchToProps = dispatch => ({
  dispatch,
});
export default connect(null, mapDispatchToProps)(SignIn);
