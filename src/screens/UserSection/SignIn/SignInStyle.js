import {Constant} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  imageStyle: {
    width: Constant.width / 1.5,
    height: Constant.width / 2,
    alignSelf: 'center',
  },
  forgotPassword: {
    alignSelf: 'flex-end',
    marginVertical: 20,
    marginBottom: 40,
  },
  bottomLineStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    marginTop: 40,
  },
  termsAndPPView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    width: '80%',
  },
});
