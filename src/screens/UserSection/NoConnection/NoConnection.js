//import : react components
import React, {useState, useEffect} from 'react';
import {View, ScrollView, Image, TouchableOpacity, Text} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//import : custom components
import MyText from 'components/MyText/MyText';
import MyHeader from 'components/MyHeader/MyHeader';
import MyHeaderWithoutLogin from 'components/MyHeaderWithoutLogin/MyHeaderWithoutLogin';
import MyButton from 'components/MyButton/MyButton';
//import : third parties
import {useNetInfo} from '@react-native-community/netinfo';
import Toast from 'react-native-simple-toast';
//import : global
import {Colors, Images, ScreenNames, Service} from 'global/Index';
//import : styles
import {styles} from './NoInternetStyle';

const NoConnection = ({route, navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //UI
  return (
    <View style={{flex: 1}}>
      <MyHeaderWithoutLogin Title={'No Connection'} hasLeftButton={false} isBorderRadius={false} disableHardwareBackButton={true} />
      <ScrollView>
        <View style={{alignItems: 'center', padding: 20}}>
          <View style={{marginTop: 50}} />
          <Image
            resizeMode="cover"
            source={Images?.NoInternetImage}
            style={{height: 100}}
          />
          <MyText
            text="Oops, no internet connection"
            textColor="#3E5869"
            fontSize={21}
            textAlign="center"
            marginTop={20}
            fontFamily="bold"
          />
          <MyText
            text="Make sure your wifi or cellular data is turned on and then try again"
            textColor="#3E5869"
            fontSize={16}
            textAlign="center"
            marginTop={20}
          />
          <View style={{marginTop: 20}} />
          <MyButton
            Title={'Try Again'}
            width="30%"
            fontSize={12}
            marginTop={20}
            backgroundColor={Colors.THEME_GREEN}
            // alignSelf='flex-end'
            onPress={()=>{isInternetReachable ? navigation.navigate(ScreenNames.SPLASH) : Toast.show('Oops, no internet connection', Toast.SHORT)}}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default NoConnection;