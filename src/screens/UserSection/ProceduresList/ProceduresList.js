//react components
import React, {useState, useEffect} from 'react';
import {View, Text, FlatList} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import ProceduresCard from 'components/ProceduresCard/ProceduresCard';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//globals
import {ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ProceduresListStyle';
//redux
import {useSelector} from 'react-redux';
//third parties
import {useNetInfo} from "@react-native-community/netinfo";

const ProceduresList = ({navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const {userToken} = useSelector(state => state.user);
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [services, setServices] = useState([]);
  //function : navigation function
  const gotoSubCategoryService = item =>
    navigation.navigate(ScreenNames.USER_SUB_CATEGORY, {item: item});
  //function : service function
  const getServiceCategories = async () => {
    setShowLoader(true);
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.GET_SERVICE_CATEGORY,
      );
      if (resp.data.status) {
        setServices(resp.data.data);
      }
    } catch (error) {
      console.log('error in getServiceCategories', error);
    }
    setShowLoader(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getServiceCategories();

    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Our Procedures" />
      <View style={styles.mainView}>
        <FlatList
          contentContainerStyle={{paddingBottom: '30%'}}
          showsVerticalScrollIndicator={false}
          data={services}
          renderItem={({item, index}) => {
            return (
              <ProceduresCard
                key={index.toString()}
                item={item}
                onPress={() => gotoSubCategoryService(item)}
              />
            );
          }}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default ProceduresList;
