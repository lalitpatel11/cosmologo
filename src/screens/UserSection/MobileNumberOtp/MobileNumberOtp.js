//import : react components
import React, {useState, useEffect} from 'react';
import {View, Text, Keyboard, Alert, TouchableOpacity} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//import : custom components
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyButton from 'components/MyButton/MyButton';
import TextInputWithFlag from 'components/TextInputWithFlag/TextInputWithFlag';
import MyText from 'components/MyText/MyText';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import MyTextInput from 'components/MyTextInput/MyTextInput';
//import : third parties
import CountryPicker from 'react-native-country-codes-picker';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from "@react-native-community/netinfo";
//import : global
import {Constant, MyIcon, ScreenNames, Service, Colors} from 'global/Index';
//import : styles
import {styles} from './MobileNumberOtpStyle';

const MobileNumberOtp = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //hook : states
  const [showLoader, setShowLoader] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [OTP, setOTP] = useState('');
  const [show, setShow] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState({});
  const [emailAddress, setEmailAddress] = useState('');
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //function : navigation function
  const gotoCheckOtp = (userId, generatedOtp) =>{
    console.log('gotoCheckOtp OTP', generatedOtp);
    const isServiceProvider = route.params && route.params.role && route.params.role === 'Service Provider' ? true : false
    if (isServiceProvider) {
      navigation.navigate(ScreenNames.USER_OTP_VALIDATE, {
        OTP: generatedOtp,
        phoneNumber: phoneNumber,
        emailAddress: emailAddress,
        userId: userId,
        role: 'Service Provider' 
      });
    } else {
      navigation.navigate(ScreenNames.USER_OTP_VALIDATE, {
        OTP: generatedOtp,
        phoneNumber: phoneNumber,
        emailAddress: emailAddress,
        userId: userId,
      });
    }
  }
  //function : imp function
  const Validation = () => {
    // if (phoneNumber?.length < 10) {
    //   Alert.alert('', 'Phone number should be 10 digits long');
    // }
    if (emailAddress == '') {
      Toast.show('Please enter email address', Toast.SHORT)
    } 
    else return true;
  };  
  //function : service function
  const forgotPassword = async () => {
    if(Validation()){
      setShowLoader(true);
      try {
        const isServiceProvider = route.params && route.params.role && route.params.role === 'Service Provider' ? true : false
        const forgotPasswordData = new FormData();
        // forgotPasswordData.append('phone', phoneNumber);
        forgotPasswordData.append('email', emailAddress);
        isServiceProvider ? forgotPasswordData.append('roletype', 'S') : forgotPasswordData.append('roletype', 'U');
        console.log('forgotPasswordData', forgotPasswordData);
        const resp = await Service.postApi(
          Service.FORGET_PASSWORD,
          forgotPasswordData,
        );
        if (resp.data.status) {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT)
          // console.log('OTP', resp.data.token)
          // setOTP(resp.data.token);
          console.log('OTP', resp.data.otp)
          setOTP(resp.data.otp);
          gotoCheckOtp(resp.data.user_id, resp.data.otp);
        }else{
          Toast.show(resp.data.message, Toast.SHORT)
        }
      } catch (error) {
        console.log('error in forgotPassword', error);
      }
      setShowLoader(false);
    }
  };
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Forgot Password" />
      <View style={styles.mainView}>
        <MyText
          // text="Please enter your registered mobile number"
          text="Please enter your registered email address"
          fontSize={24}
          fontFamily="light"
          marginVertical={10}
        />
        {/* <TextInputWithFlag
          value={phoneNumber}
          Flag={selectedCountry.flag}
          CountryCode={selectedCountry.dial_code}
          onChangeText={text => setPhoneNumber(text)}
          onPress={() => setShow(true)}
          maxLength={10}
          placeholder="Enter Number"
          keyboardType="number-pad"
        /> */}
        <MyTextInput
          Title="Email Address"
          placeholder="Enter Email Address"
          keyboardType="email-address"
          onChangeText={text => setEmailAddress(text)}
          // onSubmitEditing={() => phoneNumberRef.current.focus()}
        />
        <View style={{height: 20}} />
        <MyButton
          Title="Next"
          onPress={() => {
            forgotPassword();
            Keyboard.dismiss();
          }}
        />
      </View>
      <CountryPicker
        show={show}
        disableBackdrop={false}
        style={styles.countrySilderStyle}
        // when picker button press you will get the country object with dial code
        pickerButtonOnPress={item => {
          // console.warn('item', item);
          // setCountryCode(item.dial_code);
          setSelectedCountry(item);
          setShow(false);
        }}
        placeholderTextColor={'#c9c9c9'}
        color={Colors.BLACK}
        onBackdropPress={() => setShow(false)}
      />
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default MobileNumberOtp;
