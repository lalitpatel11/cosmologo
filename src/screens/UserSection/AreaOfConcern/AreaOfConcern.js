//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Alert,
  Linking,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import IconButton from 'components/IconButton/IconButton';
//globals
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './AreaOfConcernStyle';
//redux
import {useDispatch} from 'react-redux';
import {addToCart} from 'src/reduxToolkit/reducer/cart';
import MyButton from 'components/MyButton/MyButton';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from '@react-native-community/netinfo';

const AreaOfConcern = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //data
  //variables : redux variables
  const dispatch = useDispatch();
  //variables : route variables
  const selectedProduct = route.params.selectedProduct;
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [ProductData, setProductData] = useState({});
  const [selectedAreaOfConcern, setSelectedAreaOfConcern] = useState({});
  //function : navigation function
  const gotoSelectDate = () => {
    // updating price to include service charge as well
    navigation.navigate(ScreenNames.USER_SELECT_DATE, {
      AOCData: {
        ...selectedAreaOfConcern,
        service_charges: Number(ProductData.service_charges),
        product_id: selectedProduct.id,
      },
    });
  };
  const gotoCart = () => navigation.replace(ScreenNames.CART);
  const gotoServiceProvides = () =>
    navigation.navigate(ScreenNames.USER_SERVICE_PROVIDERS, {
      sub_category_id: ProductData.product_detail.id,
    });
  //function : imp function
  const selectAOC = () => {
    if (Object.keys(selectedAreaOfConcern).length > 0) {
      gotoSelectDate();
    } else {
      // dispatch(showToast({text: 'Please select Area Of Concern'}));
      Toast.show('Please select Area Of Concern', Toast.SHORT);
      // Alert.alert('', 'Please select Area Of Concern');
    }
  };
  //function : service function
  const getAreaOfConcern = async () => {
    setShowLoader(true);
    try {
      const areaofConcernData = new FormData();
      areaofConcernData.append('product_id[0]', selectedProduct.id);
      const resp = await Service.postApi(
        Service.PRODUCT_CONCERN_AREA_LIST,
        areaofConcernData,
      );
      if (resp.data.status) {
        setProductData(resp.data.data);
        // console.warn('resp----->', resp.data.data);
      } else {
        Toast.show(resp?.data?.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in getAreaOfConcern', error);
    }
    setShowLoader(false);
  };
  const addAreOfConcernIntoCart = () => {
    if (Object.keys(selectedAreaOfConcern).length > 0) {
      // dispatch(addToCart(selectedAreaOfConcern));
      // gotoCart();
      gotoServiceProvides();
    } else {
      dispatch(showToast({text: 'Please select Area Of Concern'}));
      // Alert.alert('', 'Please select Area Of Concern');
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    }
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getAreaOfConcern();

    return () => {};
  }, []);
  //UI
  return (
    <View style={styles.container}>
      <MyHeader
        // Title={ProductData?.product_detail?.title?.toUpperCase()}
        Title={selectedProduct?.title?.toUpperCase()}
      />
      {Object.keys(ProductData)?.length === 0 ? (
        <View style={{alignItems: 'center'}}>
          <MyText
            text={'No result found'}
            marginVertical={20}
            fontFamily="medium"
          />
        </View>
      ) : null}
      {Object.keys(ProductData).length > 0 ? (
        <ScrollView
          style={styles.mainView}
          contentContainerStyle={{paddingBottom: 50}}>
          <Image
            resizeMode="cover"
            source={{
              uri: ProductData?.product_detail?.product_image,
            }}
            style={styles.imageStyle}
          />
          <MyText
            text={ProductData?.product_detail?.title?.toUpperCase()}
            fontSize={16}
            fontFamily="bold"
            marginTop={20}
            marginBottom={5}
          />
          <MyText
            text={ProductData?.product_detail?.sub_category}
            marginVertical={5}
          />
          <MyText
            text={ProductData?.product_detail?.description}
            fontFamily="medium"
            textColor="#808080"
            marginVertical={5}
          />
          <MyText
            text={ProductData?.product_detail?.sub_title}
            textColor="#808080"
            marginVertical={5}
          />
          <TouchableOpacity
            onPress={() => Linking.openURL(ProductData?.product_detail.link)}>
            <MyText
              text="More Info"
              fontFamily="bold"
              textColor="theme_green"
              isUnderLine
            />
          </TouchableOpacity>
          <MyText
            text="AREA OF CONCERN"
            fontSize={16}
            marginVertical={10}
            fontFamily="bold"
          />
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={ProductData?.concern_area_list}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  onPress={() => setSelectedAreaOfConcern(item)}
                  style={{
                    marginRight: 20,
                    // alignItems: 'center',
                  }}>
                  <Image
                    source={{uri: item.concern_area_image}}
                    style={{
                      height: 150,
                      width: '100%',
                      borderRadius: 20,
                      borderWidth:
                        selectedAreaOfConcern.concern_area_title ==
                        item.concern_area_title
                          ? 2
                          : 0,
                      borderColor: Colors.THEME_GREEN,
                    }}
                  />
                  {selectedAreaOfConcern.concern_area_title ==
                  item.concern_area_title ? (
                    <MyIcon.AntDesign
                      name="checkcircle"
                      style={
                        ProductData?.concern_area_list?.length === 1
                          ? {
                              position: 'absolute',
                              // right: 20,
                              top: 10,
                              right: 10,
                              // left: 80,
                            }
                          : {
                              position: 'absolute',
                              right: 20,
                              top: 10,
                            }
                      }
                      size={18}
                      color={Colors.THEME_GREEN}
                    />
                  ) : null}

                  <MyText
                    text={item.concern_area_title}
                    textAlign="center"
                    fontFamily="bold"
                    marginTop={10}
                  />
                  <MyText
                    text={`$ ${parseFloat(item.price).toFixed(2)} + $ ${
                      ProductData.service_charges
                    }`}
                    fontSize={12}
                    fontFamily="light"
                    textAlign="center"
                  />
                  <MyText
                    text="Service Charges Included"
                    textColor="theme_green"
                    fontSize={10}
                    textAlign="center"
                  />
                  <TouchableOpacity onPress={() => Linking.openURL(item.link)}>
                    <MyText
                      text="More Info"
                      fontSize={12}
                      fontFamily="bold"
                      textColor="theme_green"
                      isUnderLine
                      textAlign="center"
                    />
                  </TouchableOpacity>
                </TouchableOpacity>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
          />
          <View style={{height: 10}} />
          <MyButton Title="Choose Slot" onPress={selectAOC} />
        </ScrollView>
      ) : (
        <CustomLoader showLoader={showLoader} />
      )}
    </View>
  );
};

export default AreaOfConcern;
