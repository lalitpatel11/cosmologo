import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  imageStyle: {
    height: 200,
    width: '100%',
    borderRadius: 20,
  },
});
