import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  profileView: {
    backgroundColor: Colors.WHITE,
    borderRadius: 20,
    padding: 20,
  },
  serviceDemandview: {
    backgroundColor: Colors.THEME_GREEN,
    alignSelf: 'flex-start',
    padding: 3,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  contactUsView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
