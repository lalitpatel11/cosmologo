//react components
import React, {useState} from 'react';
import {View, Image, FlatList, TouchableOpacity} from 'react-native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import ServiceCard from 'components/ServiceCard/ServiceCard';
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';
import TitleViewAll from 'components/TitleViewAll/TitleViewAll';
//global
import {Colors, Images, MyIcon} from 'global/Index';
//styles
import {styles} from './AddOnStyle';
//import : redux
import {useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const AddOn = () => {
  //variables : redux variables
  const dispatch = useDispatch();
  //variables
  const ServiceArray = [1, 2, 3];
  //states
  const [selectedServices, setSelectedServices] = useState([]);
  //function : imp function
  const selectService = index => {
    selectedServices.push(index);
  };
  //function : render function
  const serviceRender = ({item, index}) => {
    return (
      <ServiceCard
        key={index.toString()}
        index={index}
        Title="Arm"
        SubText={'$200'}
        imageUrl={
          index == 0
            ? require('assets/images/upper-arms.png')
            : require('assets/images/abdominal-area.png')
        }
        onPress={() => selectService(index)}
      />
    );
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Add On" />
      <View style={styles.mainView}>
        <View style={styles.profileView}>
          <View style={styles.serviceDemandview}>
            <MyText text="Service on demand" fontSize={10} textColor="white" />
          </View>

          <Image
            source={Images.ProfileIcon.profileImage}
            style={{
              width: 100,
              height: 100,
              borderRadius: 100,
              alignSelf: 'center',
            }}
          />
          <MyText
            text="Dr. Keith Kelly"
            textAlign="center"
            fontSize={16}
            marginVertical={10}
            fontFamily="medium"
          />
          <TouchableOpacity style={styles.contactUsView} onPress={()=>{dispatch(showToast({text: 'Contact us clicked', duration: 1000}));}}>
            <MyIcon.Feather name="mail" color={Colors.THEME_GREEN} size={24} />
            <MyText
              text="Contact us"
              marginHorizontal={5}
              fontFamily="bold"
              textColor="theme_green"
            />
          </TouchableOpacity>
        </View>
        <TitleViewAll Title="6 Services Available" />
        <FlatList
          horizontal
          data={ServiceArray}
          renderItem={serviceRender}
          keyExtractor={(item, index) => index.toString()}
        />
        <MyButton Title="Make Payment" />
      </View>
    </View>
  );
};

export default AddOn;
