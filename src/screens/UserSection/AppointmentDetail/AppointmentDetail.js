//import : react components
import React, {useState, useEffect} from 'react';
import {PermissionsAndroid, Platform, ScrollView, View} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//import : custom components
import MyText from 'components/MyText/MyText';
import SelectedService from 'components/ServiceCard/SelectedService';
import MyHeader from 'components/MyHeader/MyHeader';
import DoctorCard from 'components/DoctorCard/DoctorCard';
import FAB_Button from 'components/FAB_Button/FAB_Button';
import MyButton from 'components/MyButton/MyButton';
import CustomLoader from 'components/CustomLoader/CustomLoader';

//import : globals
import {MyIcon, ScreenNames, Service, Colors} from 'global/Index';
//import : styles
import {styles} from './AppointmentDetailStyle';
//import : redux
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {useSelector, useDispatch} from 'react-redux';
//third parties
import {useNetInfo} from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';

const AppointmentDetail = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const BookingId = route.params.BookingId;
  const duration = route.params.duration;

  //variables : redux variables
  const userToken = useSelector(state => state.user.userToken);
  const dispatch = useDispatch();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [ApptDetailData, setApptDetailData] = useState({});
  const [hide, setHide] = useState((route.params && typeof route.params.hideTrack === 'boolean' && route.params.hideTrack === true) ? true: false);
  //function : navigation function
  const resetIndexGoToBottomTab = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.USER_BOTTOM_TAB}],
  });
  const gotoTrackService = () => {
    if(!ApptDetailData?.user_lat || (ApptDetailData?.user_lat === 'undefined') || !ApptDetailData?.user_long || (ApptDetailData?.user_long === 'undefined')) {
      // dispatch(showToast({text: 'Cannot track without User latitude and longitude'}));
      Toast.show('Cannot track without User latitude and longitude', Toast.SHORT)
      return
    }
    if(!ApptDetailData?.provider_lat || (ApptDetailData?.provider_lat === 'undefined') || !ApptDetailData?.provider_long || (ApptDetailData?.provider_long === 'undefined')) {
      // dispatch(showToast({text: 'Cannot track without Service Provider latitude and longitude'}));
      Toast.show('Cannot track without Service Provider latitude and longitude', Toast.SHORT)
      return
    }
    navigation.navigate(ScreenNames.USER_TRACK_SERVICE, {ApptDetailData});
  }
  const gotoBottomTab = () => {
    navigation.dispatch(resetIndexGoToBottomTab);
  };
  const gotoViewInvoice = (appointment_id, booking_id) =>
    navigation.navigate(ScreenNames.VIEW_INVOICE, {appointment_id, booking_id});
  //function : imp function

  //function : service function
  const getApptDetail = async () => {
    setShowLoader(true);
    try {
      const appDetailData = new FormData();
      appDetailData.append('booking_id', BookingId);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.GET_APPOINTMENT_DETAIL,
        appDetailData,
      );
      if (resp.data.status) {
        console.warn('data=======>', resp.data.data);
        setApptDetailData(resp.data.data);
        console.log('user setApptDetailData', resp.data.data);
        // console.warn(resp.data.data);
      }
    } catch (error) {
      console.log('error in getApptDetail', error);
    }
    setShowLoader(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getApptDetail();
    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title={ApptDetailData?.subcategory} />
      {Object.keys(ApptDetailData).length > 0 ? (
        <ScrollView
          contentContainerStyle={{paddingBottom: '20%'}}
          style={styles.mainView}>
          <SelectedService
            BookingId={BookingId}
            SubCategory={ApptDetailData?.subcategory}
            Product={ApptDetailData?.product_title}
            ConcernArea={ApptDetailData?.concern_area_title}
            ImageUrl={ApptDetailData?.concern_area_image}
            Amount={ApptDetailData?.amount}
            downloadInvoicePress={() => gotoViewInvoice(ApptDetailData.id, ApptDetailData.booking_id)}
          />
          <View style={styles.StatusSection}>
            <View style={styles.flexRowStyle}>
              <MyText text="Status : " fontFamily="medium" fontSize={16} />
              <MyText
                text={ApptDetailData?.status}
                fontFamily="medium"
                textColor="theme_green"
              />
            </View>
            <MyText
              text={`${ApptDetailData?.subcategory} Info`}
              fontFamily="medium"
              fontSize={16}
            />
            {duration ?
            <View style={styles.flexRowStyle}>
              <MyText text="Duration : " fontFamily="medium" fontSize={16} />
              <MyText
                text={duration}
                fontFamily="medium"
                textColor="theme_green"
              />
            </View>:null}
            <MyText
              text="Langoureux parce et comme et comme que soucieux. De l'amour vivre parfaite demain extase. Elle charme quel et tant pontife, surtout d'un ou faudra tes, mensonge divinement blaspheme sur nous, doué fin vois mon trésor soucieux, divinement fait musculeux grande pieds pleure etre florentines et. Magnifique charmer ô il tout.."
              fontFamily="light"
              fontSize={12}
              lineHeight={20}
              marginVertical={10}
            />
            <View style={styles.locationView}>
              <MyIcon.Feather name="map-pin" size={24} />
              <View style={{marginLeft: 10}}>
                <MyText text="Location" fontFamily="medium" />
                <MyText
                  text={ApptDetailData?.user_location}
                  textColor="lite_grey"
                />
              </View>
            </View>
          </View>
          {/* <DoctorCard
            Name={ApptDetailData?.service_provider}
            ProfileImageUrl={ApptDetailData?.service_provider_profile_mage}
          /> */}
          {!hide ?
            <MyButton Title="Track" onPress={gotoTrackService} />
          :null}
        </ScrollView>
      ) : (
        <CustomLoader showLoader={showLoader} />
      )}
      <FAB_Button 
        onPress={gotoBottomTab}
        icon={<MyIcon.Ionicons
                name={'home-outline'}
                color={Colors.WHITE}
                size={30}
              />}
       />
    </View>
  );
};

export default AppointmentDetail;
