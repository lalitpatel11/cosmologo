import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  locationView: {
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center',
  },
  StatusSection: {
    backgroundColor: Colors.WHITE,
    marginVertical: 20,
    borderRadius: 10,
    padding: 10,
  },
  flexRowStyle: {
    flexDirection: 'row',
    marginVertical: 10,
    alignItems:'center'
  },
});
