//import : react components
import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  ScrollView,
  FlatList,
  Alert,
  Linking,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//import : custom components
import MyText from 'components/MyText/MyText';
import MyHeader from 'components/MyHeader/MyHeader';
import MyButton from 'components/MyButton/MyButton';
import IconButton from 'components/IconButton/IconButton';
import ServiceCard from 'components/ServiceCard/ServiceCard';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//import : global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//import : styles
import {styles} from './ProcDetailStyle';
import {useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from '@react-native-community/netinfo';

const ProcDetail = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const subCategory = route.params.subCategory;
  const dispatch = useDispatch();
  //hook : states
  const [showLoader, setShowLoader] = useState(false);
  const [procDetailData, setProcDetailData] = useState({});
  const [selectedProduct, setSelectedProduct] = useState({});
  //function : navigation function
  const gotoAreaofconcern = () =>
    navigation.navigate(ScreenNames.USER_AREAOFCONCERN, {selectedProduct});
  const gotoSelectDate = () =>
    navigation.navigate(ScreenNames.USER_SELECT_DATE);
  //function : imp function
  const selectProduct = () => {
    if (Object.keys(selectedProduct).length > 0) {
      gotoAreaofconcern();
    } else {
      // dispatch(showToast({text: 'Please select product/treatment'}));
      Toast.show('Please select product/treatment', Toast.SHORT);
      // Alert.alert('', 'Please select product/treatment');
    }
  };
  const callNow = () => {
    // const phoneNumber = 8888888888;
    const phoneNumber = '+12122230716';
    if (Platform.OS === 'android') {
      Linking.openURL(`tel:${phoneNumber}`);
    } else {
      Linking.openURL(`telprompt:${phoneNumber}`);
    }
  };
  //function : service function
  const getProducts = async () => {
    setShowLoader(true);
    try {
      const getproductData = new FormData();
      getproductData.append('sub_category_id[0]', subCategory.id);
      const resp = await Service.postApi(Service.PRODUCT_LIST, getproductData);
      if (resp.data.status) {
        setProcDetailData(resp.data.data);
      } else {
        Toast.show(resp?.data?.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in getProducts', error);
    }
    setShowLoader(false);
  };

  //function : render function
  const ProductRender = ({item, index}) => {
    return (
      <ServiceCard
        Title={item.title}
        imageUrl={item.image}
        disabled={false}
        item={item}
        selectedItem={selectedProduct.title}
        CardPress={() => setSelectedProduct(item)}
      />
    );
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    }
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //
  //useEffect
  useEffect(() => {
    getProducts();
    return () => {};
  }, []);
  //UI
  return (
    <View style={styles.container}>
      <MyHeader
        // Title={procDetailData?.subcategory_detail?.title?.toUpperCase()}
        Title={subCategory?.title?.toUpperCase()}
      />
      {Object.keys(procDetailData)?.length === 0 ? (
        <View style={{alignItems: 'center'}}>
          <MyText
            text={'No result found'}
            marginVertical={20}
            fontFamily="medium"
          />
        </View>
      ) : null}
      {Object.keys(procDetailData).length > 0 ? (
        <ScrollView
          contentContainerStyle={{paddingBottom: 50}}
          showsVerticalScrollIndicator={false}
          style={styles.mainView}>
          <Image
            resizeMode="cover"
            source={{uri: procDetailData?.subcategory_detail?.background_image}}
            style={styles.imageStyle}
          />
          <MyText
            text={procDetailData?.subcategory_detail?.title.toUpperCase()}
            fontFamily="bold"
            marginTop={20}
            fontSize={16}
          />
          <MyText
            text={procDetailData?.subcategory_detail?.sub_title}
            lineHeight={20}
            fontFamily="medium"
            marginVertical={10}
          />
          <MyText
            text={procDetailData?.subcategory_detail?.description}
            textColor="#808080"
            lineHeight={20}
            marginBottom={10}
          />
          <TouchableOpacity
            onPress={() =>
              Linking.openURL(procDetailData?.subcategory_detail.link)
            }>
            <MyText
              text="More Info"
              textColor="theme_green"
              fontFamily="bold"
              isUnderLine
            />
          </TouchableOpacity>

          <View style={styles.flexRowStyle}>
            <IconButton
              Title="Call Now"
              VectorIcon={
                <MyIcon.Feather name="phone-call" color={Colors.THEME_GREEN} />
              }
              onPress={callNow}
              width="48%"
            />
            <IconButton
              Title="Book Free Consultation"
              VectorIcon={
                <MyIcon.FontAwesome5
                  name="headset"
                  color={Colors.THEME_GREEN}
                />
              }
              onPress={callNow}
              width="48%"
            />
          </View>
          <MyText
            text="PRODUCT / TREATMENT"
            fontSize={16}
            marginVertical={10}
            fontFamily="bold"
          />
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={procDetailData.product_list}
            renderItem={ProductRender}
            keyExtractor={(item, index) => index.toString()}
          />
          <MyButton Title="Next" onPress={selectProduct} />
        </ScrollView>
      ) : (
        <CustomLoader showLoader={showLoader} />
      )}
    </View>
  );
};

export default ProcDetail;
