//react components
import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Keyboard,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//import : third parties
import {
  CardField,
  CardFieldInput,
  useStripe,
  StripeContainer,
} from '@stripe/stripe-react-native';
import CreditCard from 'react-native-credit-card-form-ui';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
//global
import {Colors, Images, Service, ScreenNames} from 'global/Index';
//styles
import {styles} from './PaymentStyle';
//modal
import PaymentSuccess from 'modals/PaymentSuccess/PaymentSuccess';
//redux
import {useSelector, useDispatch} from 'react-redux';
import {clearCart} from 'src/reduxToolkit/reducer/cart';
//third parties
import {useNetInfo} from "@react-native-community/netinfo";

const Payment = ({route, navigation}) => {
  const {isInternetReachable} = useNetInfo();
  const creditCardRef = React.useRef();
  const dispatch = useDispatch();
  //variables : redux variables
  const cartItems = useSelector(state => state.cart.cartItems);
  const userToken = useSelector(state => state.user.userToken);
  const userInfo = useSelector(state => state.user.userInfo);
  //hook : states
  const [card, setCard] = useState(CardFieldInput.Details | null);
  const [madePayment, setMadePayment] = useState(false);
  const [bookingId, setBookingId] = useState(
    route.params &&
      typeof route.params.bookingId === 'string' &&
      route.params.bookingId?.length > 0
      ? route.params.bookingId
      : '',
  );
  const {confirmPayment, handleCardAction} = useStripe();
  const {initPaymentSheet, createToken, presentPaymentSheet} = useStripe();
  // const [appointmentId, setAppointmentId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showLoader, setShowLoader] = useState(false);
  //states
  const [showPaymentSuccess, setShowPaymentSuccess] = useState(false);
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  const gotoMyBookings = () => {
    CommonActions.reset({
      index: 1,
      routes: [{name: navigation.navigate(ScreenNames.BOOKINGS)}],
    });
    // navigation.navigate(ScreenNames.BOOKINGS)
  };
  //function : imp function
  // const handlePayClick = React.useCallback(() => {
  //   if (creditCardRef.current) {
  //     const {error, data} = creditCardRef.current.submit();
  //     console.log('ERROR: ', error);
  //     console.log('CARD DATA: ', data);
  //   }
  // }, []);
  const handlePayClick = async () => {
    if (madePayment) {
      Toast.show('You have already made payment', Toast.SHORT);
      return;
    }
    if (!(route.params && typeof route.params.appointmentId === 'number')) {
      Toast.show('Slot not available. Please choose another slot', Toast.SHORT);
      return;
    } else if (card === 0) {
      Toast.show('Please enter card details', Toast.SHORT);
      return;
    } else if (!card?.complete) {
      Toast.show('Please complete card details', Toast.SHORT);
      return;
    }
    setShowLoader(true);
    try {
      console.log('card', card);
      const res = await createToken({card, type: 'Card'});
      console.log('res', res);
      const myData = new FormData();
      myData.append('stripeToken', res.token.id);
      myData.append('appointment_id', route.params.appointmentId);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.MAKE_PAYMENT,
        myData,
      );
      if (resp.data.status) {
        setMadePayment(true);
        Toast.show(resp.data.message, Toast.SHORT);
        dispatch(clearCart());
        gotoMyBookings();
        // setShowPaymentSuccess(true)
      } else {
        Toast.show(resp.data.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in handlePayClick', error);
    }
    setShowLoader(false);
  };
  // placeholders: {
  //   number: '0000 0000 0000 0000',
  //   holder: 'TITULAR DO CARTÃO',
  //   expiration: 'MM/YYYY',
  //   cvv: '000',
  // }
  //function : service function
  const confirmAppt = async () => {
    console.log();
    setShowLoader(true);
    try {
      const bookApptData = new FormData();
      bookApptData.append('user_id', userInfo.id);
      bookApptData.append(
        'product_concern_area_id',
        cartItems.data.concern_area_id,
      );
      bookApptData.append('appointment_date', cartItems.appointment_date);
      bookApptData.append(
        'appointment_time',
        moment(`${cartItems.appointment_time}`, 'h:mm A').format('HH:mm:ss'),
      );
      // bookApptData.append('service_provider_id', cartItems.service_provider.id);
      bookApptData.append(
        'amount',
        parseFloat(cartItems.data.price).toFixed(2),
      );
      console.log('bookApptData', bookApptData);
      console.log('userToken', userToken);
      console.warn(bookApptData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.BOOK_APPOINTMENT,
        bookApptData,
      );
      console.log('book apt resp', resp);
      if (resp.data.status) {
        // dispatch(showToast({text: resp.data.message}));
        setAppointmentId(resp.data.appointment_id);
        Toast.show(resp.data.message, Toast.SHORT);
        // dispatch(clearCart());
        // gotoAppointmentDetail(resp.data.booking_id);
      } else {
        // dispatch(showToast({text: resp.data.message}));
        Toast.show(resp.data.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in confirmAppt', error);
    }
    setShowLoader(false);
  };
  // useEffect(()=>{
  //   confirmAppt()
  // },[])
  //UI
  return (
    <View style={styles.container}>
      <StripeContainer>
        <MyHeader Title="Payment " />
        <ScrollView keyboardShouldPersistTaps="never">
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            keyboardVerticalOffset={20}
            style={styles.container}>
            <View style={styles.mainView}>
              {/* <CreditCard
                  background={Colors.LITE_GREY}
                  ref={creditCardRef}
                  placeholderTextColor={Colors.BLACK}
                  placeholders={{
                    number: '0000 0000 0000 0000',
                    holder: 'Card Holder Name',
                    expiration: 'MM/YYYY',
                    cvv: '000',
                  }}
                  labels={{
                    number: '0000 0000 0000 0000',
                    holder: 'Card Holder Name',
                    expiration: 'EXP DATE',
                    cvv: 'CVV',
                  }}
                /> */}
              {!madePayment ? (
                <MyText
                  text="Add Card Details"
                  fontFamily="medium"
                  fontSize={16}
                />
              ) : null}
              <CardField
                accessible={true}
                postalCodeEnabled={false}
                placeholder={{
                  number: '4242 4242 4242 4242',
                }}
                cardStyle={{
                  borderRadius: 20,
                  // backgroundColor: '#a9bcd6',
                  // backgroundColor: '#7294c2',
                  backgroundColor: Colors.BG_GREEN,
                  borderColor: Colors.BLACK,
                  borderWidth: 1,
                  textColor: Colors.BLACK,
                  placeholderColor: '#c9c9c9',
                }}
                style={{
                  width: '100%',
                  height: 200,
                  marginTop: 20,
                  marginBottom: 30,
                }}
                onCardChange={cardDetails => {
                  setCard(cardDetails);
                  if (cardDetails?.complete) {
                    Keyboard.dismiss();
                  }
                }}
                onFocus={focusedField => {
                  console.log('focusField', focusedField);
                }}
              />
              <View style={styles.totalPaymentView}>
                <Image
                  source={Images.Icons.totalPaymentIcon}
                  style={styles.iconStyles}
                />
                <View style={styles.textView}>
                  <MyText
                    text="Total Payable Amount"
                    fontFamily="medium"
                    textColor="white"
                    fontSize={16}
                  />
                  <MyText
                    // text={`$ ${AOCData.price}`}
                    text={`$ ${parseFloat(
                      cartItems?.data?.price + cartItems?.data?.service_charges,
                    ).toFixed(2)}`}
                    marginTop={10}
                    textColor="white"
                    fontFamily="bold"
                    fontSize={20}
                  />
                </View>
              </View>
              {!madePayment ? (
                <>
                  <MyButton
                    // Title={`Payment $${cartItems?.data?.price+cartItems?.data?.service_charges}`}
                    Title={`Make Payment`}
                    onPress={handlePayClick}
                  />
                  <View style={styles.bookingNotConfirmed}>
                    <MyText
                      text="Note: Until you make payment, your booking will not be confirmed"
                      fontFamily="medium"
                      fontSize={16}
                    />
                  </View>
                </>
              ) : null}
            </View>
          </KeyboardAvoidingView>
          <PaymentSuccess
            visible={showPaymentSuccess}
            setVisibility={setShowPaymentSuccess}
            bookingId={bookingId}
          />
        </ScrollView>
        <CustomLoader showLoader={showLoader} />
      </StripeContainer>
    </View>
  );
};
export default Payment;
