import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';
const SIZE = 70;

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  totalPaymentView: {
    flexDirection: 'row',
    backgroundColor: Colors.THEME_GREEN,
    padding: 15,
    borderRadius: 10,
    marginVertical: 10,
  },
  bookingNotConfirmed: {
    flexDirection: 'row',
    // backgroundColor: Colors.THEME_GREEN,
    padding: 15,
    borderRadius: 10,
    marginVertical: 10,
    borderColor:Colors.RED,
    borderWidth:2,
    justifyContent:'center'
  },
  iconStyles: {
    height: SIZE,
    width: SIZE,
  },
  textView: {
    marginLeft: 20,
  },
});
