//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  FlatList,
  Image,
  TouchableOpacity,
  Alert,
  Linking,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import MyButton from 'components/MyButton/MyButton';
//third parties
import {SvgUri} from 'react-native-svg';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from '@react-native-community/netinfo';
//global
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './SubCategoryServiceStyle';
import MyRoundedButton from 'components/MyRoundedButton/MyRoundedButton';

const SubCategoryService = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const Category = route.params.item;
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [selectedSubCategory, setSelectedSubCategory] = useState({});
  const [subCategoriesData, setSubCategoriesData] = useState([]);
  //function : navigation function
  const gotoProcDetail = () =>
    navigation.navigate(ScreenNames.USER_PROC_DETAIL, {
      subCategory: selectedSubCategory,
    });
  //function : service function
  const getSubCategory = async () => {
    setShowLoader(true);
    try {
      const subCategoryData = new FormData();
      subCategoryData.append('service_category_id[0]', Category?.id);
      const resp = await Service.postApi(
        Service.SERVICE_SUB_CATEGORY,
        subCategoryData,
      );
      if (resp.data.status) {
        setSubCategoriesData(resp.data.data);
        console.log('setSubCategoriesData', resp.data.data);
      } else {
        Toast.show(resp?.data?.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in getSubCategory', error);
    }
    setShowLoader(false);
  };
  //function : service function
  const selectSubCtgry = () => {
    if (Object.keys(selectedSubCategory).length > 0) {
      console.log('selectedSubCategory', selectedSubCategory);
      gotoProcDetail();
      // gotoProcDetail()
    } else {
      // Alert.alert('', 'Please select any category');
      Toast.show('Please select any category', Toast.SHORT);
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    }
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getSubCategory();

    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title={Category?.title?.toUpperCase()} />
      <ScrollView
        contentContainerStyle={{paddingBottom: 50}}
        style={styles.mainView}>
        {subCategoriesData?.length === 0 ? (
          <View style={{alignItems: 'center'}}>
            <MyText
              text={'No result found'}
              marginVertical={20}
              fontFamily="medium"
            />
          </View>
        ) : null}
        {subCategoriesData.length > 0
          ? subCategoriesData.map((item, index) => {
              return (
                <TouchableOpacity
                  key={item.id.toString()}
                  onPress={() => setSelectedSubCategory(item)}
                  style={{
                    borderWidth:
                      selectedSubCategory.title == item.title ? 1 : 0,
                    borderColor: Colors.THEME_GREEN,
                    flexDirection: 'row',
                    alignItems: 'center',
                    padding: 10,
                    backgroundColor: Colors.WHITE,
                    borderRadius: 20,
                    marginVertical: 10,
                    // height: 150,
                  }}>
                  <View
                    style={{
                      height: 150,
                      width: '25%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      resizeMode="contain"
                      style={{
                        height: '100%',
                        width: '100%',
                        alignSelf: 'center',
                      }}
                      source={{
                        uri: item.icon_image,
                      }}
                    />
                    {/* <SvgUri
                      key={index.toString()}
                      width="100%"
                      height="100%"
                      uri={item?.icon_image}
                      style={{alignSelf: 'center'}}
                    /> */}
                  </View>
                  <View
                    style={{
                      marginLeft: 10,
                      width: '70%',
                    }}>
                    <MyText
                      text={item.title.toUpperCase()}
                      fontSize={16}
                      fontFamily="medium"
                    />
                    <MyText
                      text={`${item.sub_title}`}
                      numberOfLines={2}
                      marginVertical={10}
                    />
                    <MyText
                      text={item.description}
                      fontFamily="light"
                      textColor="#808080"
                      numberOfLines={3}
                      fontSize={12}
                    />
                    <TouchableOpacity
                      onPress={() => Linking.openURL(item.link)}>
                      <MyText
                        text="More info"
                        textColor="theme_green"
                        fontFamily="bold"
                        marginVertical={10}
                      />
                    </TouchableOpacity>
                  </View>

                  {selectedSubCategory.title == item.title ? (
                    <MyIcon.AntDesign
                      name="checkcircle"
                      style={{
                        position: 'absolute',
                        right: 10,
                        top: 10,
                      }}
                      size={24}
                      color={Colors.THEME_GREEN}
                    />
                  ) : null}
                </TouchableOpacity>
              );
            })
          : null}
      </ScrollView>
      <View style={styles.mainView}>
        <MyButton Title="Next" onPress={selectSubCtgry} />
      </View>

      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default SubCategoryService;
