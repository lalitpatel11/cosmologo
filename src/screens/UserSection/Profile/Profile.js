//react components
import React, {useState, useEffect} from 'react';
import {View, ScrollView} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import ProfileCard from 'components/ProfileCard/ProfileCard';
import TitleViewAll from 'components/TitleViewAll/TitleViewAll';
import DoctorCard from 'components/DoctorCard/DoctorCard';
import AppointmentCard from 'components/AppointmentCard/AppointmentCard';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import CustomLoaderLogout from 'components/CustomLoader/CustomLoaderLogout';
import {
  TotalCmpltAppointCard,
} from 'components/ProfileCard/ProfileComponents';
//global
import {ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ProfileStyle';
//redux
import {useSelector, useDispatch} from 'react-redux';
import {logOutUser} from 'src/reduxToolkit/reducer/user';
import {clearCart} from 'src/reduxToolkit/reducer/cart';
//third parties
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNetInfo} from "@react-native-community/netinfo";

const Profile = ({navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const dispatch = useDispatch();
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [ApptHistoryData, setApptHistoryData] = useState([]);
  const [showLoader, setShowLoader] = useState(false);
  const [showLoaderLogout, setShowLoaderLogout] = useState(false);
  const [completedAppointments, setCompletedAppointments] = useState('');
  //function : navigation function
  const gotoChangePassword = () =>
    navigation.navigate(ScreenNames.USER_CHANGE_PASSWORD);
  const gotoEditProfile = () => {
    navigation.navigate(ScreenNames.USER_EDIT_PROFILE);
  }
  //function : navigation function
  const gotoAppointmentDetail = BookingId =>
    navigation.navigate(ScreenNames.USER_APPOINTMENT_DETAILS, {
      BookingId: BookingId,
    });
  const gotoWelcome = () =>
  navigation.reset({
    index: 0,
    routes: [{name: ScreenNames.WELCOME}],
  });  
  //function : service function
  const getUpcomingBookAppnt = async () => {
    try {
      const upcomingBookAppntData = new FormData();
      upcomingBookAppntData.append('appointment_status', 'upcoming');
      const resp = await Service.postApiWithToken(
        userToken,
        Service.USER_APPOINTMENT_LIST,
        upcomingBookAppntData,
      );
    } catch (error) {
      console.log('error in getUpcomingBookAppnt', error);
    }
  };
  const getAppontsHistory = async () => {
    try {
      const apptHistoryData = new FormData();
      apptHistoryData.append('user_id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.USER_APPOINTMENT_HISTORY_LIST,
        apptHistoryData,
      );
      if (resp.data.status) {
        setApptHistoryData(resp.data.data);
        // console.warn('resp----->', resp.data.data);
      }
    } catch (error) {
      console.log('error in getAppontsHistory', error);
    }
  };
  const getCompletedAppointmentCount = async () => {
    setShowLoader(true)
    try {
      const apptHistoryData = new FormData();
      apptHistoryData.append('user_id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.USER_COMPLETED_APPOINTMENT_COUNT,
        apptHistoryData,
      );
      if (resp.data.status) {
        setCompletedAppointments(resp.data.count)
        // setApptHistoryData(resp.data.data);
        // console.warn('resp----->', resp.data.data);
      }
    } catch (error) {
      console.log('error in getAppontsHistory', error);
    }
    setShowLoader(false)
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // getAppontsHistory();
      // getUpcomingBookAppnt();
      getCompletedAppointmentCount();
    });
    return unsubscribe;
  }, [navigation]);
  const logoutDirectly = async () => {
    gotoWelcome();
    dispatch(logOutUser());
    // dispatch(clearCart());
    await AsyncStorage.clear();
  };
  const logout = async () => {
    setShowLoaderLogout(true);
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.USER_LOGOUT,
      );
      if (resp?.data?.status) {
        // don't need to show message after successfully logging out
        // dispatch(showToast({text: resp.data.message}));
        gotoWelcome();
        dispatch(logOutUser());
        dispatch(clearCart());
        await AsyncStorage.clear();
      }
    } catch (error) {
      console.log('error in logout', error);
    }
    setShowLoaderLogout(false);
  };
  const gotoUpcomingAppointment = () => {
    navigation.navigate(ScreenNames.USER_UPCOMING_APPOINTMENT_LIST)
  }
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Profile" hasDrawerButton={true} />
      <ScrollView
        // showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 100}}
        style={styles.mainView}>
        <View style={{height: 50}} />
        <ProfileCard
          Email={userInfo.email}
          // Phone={userInfo?.country_code ? `(${userInfo.country_code}) -${userInfo.phone}` : userInfo.phone}
          Phone={`${userInfo?.country_code} (${userInfo?.phone?.substring(0,3)}) ${userInfo?.phone?.substring(3,6)}-${userInfo?.phone?.substring(6)}`}
          ProfileImageUrl={userInfo.profile_image}
          changePasswordPress={gotoChangePassword}
          editProfilePress={gotoEditProfile}
          logout={logout}
        />
        <TotalCmpltAppointCard
          Appointment={completedAppointments}
          // onPress={gotoCompleteAppointListing}
        />
        <TitleViewAll Title="Upcoming Appointments" onPress={gotoUpcomingAppointment}/>
        {ApptHistoryData.length > 0
          ? ApptHistoryData.map((item, index) => {
              return (
                <AppointmentCard
                  key={index.toString()}
                  BookingID={item.booking_id}
                  SubCategory={item.subcategory}
                  ConcernArea={item.concern_area_title}
                  Product={item.product}
                  ImageUrl={item.concern_area_image}
                  Amount={item.amount}
                  AppointmentTime={item.date_time}
                  DrProfileUrl={item.service_provider_profile_mage}
                  DoctorName={item.service_provider}
                  onPress={() => gotoAppointmentDetail(item.booking_id)}
                  Role={'User'}
                />
              );
            })
          : null}

        {/* <TitleViewAll Title={`${completedAppointments} Appointments Completed`} /> */}
        {/* <AppointmentCard /> */}
      </ScrollView>
      <CustomLoader showLoader={showLoader}/>
      <CustomLoaderLogout showLoader={showLoaderLogout}/>
    </View>
  );
};

export default Profile;
