//react components
import React, {useState, useEffect} from 'react';
import {View, Text, Alert, Keyboard} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyText from 'components/MyText/MyText';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyPasswordInput from 'components/MyPasswordInput/MyPasswordInput';
import MyButton from 'components/MyButton/MyButton';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ForgotPasswordStyle';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//third parties
import Toast from 'react-native-simple-toast';
import {useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {useNetInfo} from "@react-native-community/netinfo";

const ForgotPassword = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const userId = route.params.userId;
  const OTP = route.params.OTP;
  const emailAddress = route.params.emailAddress;
  const dispatch = useDispatch();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //function : navigation function
  const gotoSignIn = () => {
    const isServiceProvider =
      route.params &&
      route.params.role &&
      route.params.role === 'Service Provider'
        ? true
        : false;
    isServiceProvider
      ? navigation.navigate(ScreenNames.SERVICE_SIGN_IN)
      : navigation.navigate(ScreenNames.USER_SIGNIN);
  };
  //function : imp function
  const Validation = () => {
    if (password == '') {
      // dispatch(showToast({text: 'Please enter password'}));
      Toast.show('Please enter password', Toast.SHORT)
      // Alert.alert('', 'Please enter password');
    } else if (confirmPassword == '') {
      // dispatch(showToast({text: 'Please enter confirm password'}));
      Toast.show('Please enter confirm password', Toast.SHORT)
      // Alert.alert('', 'Please enter confirm password');
    } else if (password != confirmPassword) {
      // dispatch(showToast({text: 'Password and confirm password not matching'}));
      Toast.show('Password and confirm password not matching', Toast.SHORT)
      // Alert.alert('', 'Password and confirm password not matching');
    } else return true;
  };
  //function : service function
  const resetPassword = async () => {
    if (Validation()) {
      Keyboard.dismiss();
      setShowLoader(true);
      try {
        const isServiceProvider =
          route.params &&
          route.params.role &&
          route.params.role === 'Service Provider'
            ? true
            : false;
        const resetPasswordData = new FormData();
        // resetPasswordData.append('user_id', userId);
        resetPasswordData.append('password', password);
        resetPasswordData.append('code', String(OTP));
        resetPasswordData.append('email', emailAddress);
        isServiceProvider
          ? resetPasswordData.append('roletype', 'S')
          : resetPasswordData.append('roletype', 'U');
        console.log('resetPasswordData', resetPasswordData);
        // resetPasswordData.append('password_confirmation', confirmPassword);
        const resp = await Service.postApi(
          // Service.RESET_PASSWORD,
          Service.FORGET_PASSWORD_VERIFY,
          resetPasswordData,
        );
        if (resp.data.status) {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
          gotoSignIn();
        } else {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
        }
      } catch (error) {
        console.log('error in resetPassword', error);
      }
      setShowLoader(false);
    }
  };
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Forgot Password" />
      <View style={styles.mainView}>
        <MyText
          text="Create New Password"
          fontFamily="bold"
          fontSize={26}
          textColor="theme_green"
        />
        <MyText
          text="Your new password must be different from previous used passwords"
          fontSize={16}
          marginVertical={10}
        />
        <MyPasswordInput
          Title="New Password"
          placeholder="Enter New Password"
          onChangeText={text => setPassword(text)}
          secureTextEntry
          showPassword
          ShowIcon={
            <MyIcon.FontAwesome5
              name="eye"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          HideIcon={
            <MyIcon.FontAwesome5
              name="eye-slash"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
        />
        <MyPasswordInput
          Title="Confirm Password"
          placeholder="Enter Confirm Password"
          onChangeText={text => setConfirmPassword(text)}
          secureTextEntry
          showPassword
          ShowIcon={
            <MyIcon.FontAwesome5
              name="eye"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          HideIcon={
            <MyIcon.FontAwesome5
              name="eye-slash"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
        />
        <View style={{height: 20}} />
        <MyButton Title="Save" onPress={resetPassword} />
      </View>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default ForgotPassword;
