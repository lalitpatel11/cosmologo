//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  FlatList,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import AppointmentCard from 'components/AppointmentCard/AppointmentCard';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './AppointmentStyle';
//redux
import {connect, useSelector, useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
//import : third parties
import Toast from 'react-native-simple-toast';
import RNFetchBlob from 'rn-fetch-blob';
import {Dropdown} from 'react-native-element-dropdown';
import moment from 'moment';
import {useNetInfo} from "@react-native-community/netinfo";
//import : modals
import AppointmentCalender from 'modals/AppointmentCalender/AppointmentCalender';
import CancelAppointment from 'modals/CancelAppointment/CancelAppointment';

//data
const data = [
  {label: 'All', value: 'All'},
  {label: 'Completed', value: 'Completed'},
  {label: 'Cancelled', value: 'Cancelled'},
  {label: 'Processing', value: 'Processing'},
];
const Appointment = ({navigation, userToken}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const dispatch = useDispatch();
  const userInfo = useSelector(state => state.user.userInfo);
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [AppointmentData, setAppointmentData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [isFocus, setIsFocus] = useState(false);
  const [aptStatus, setAptStatus] = useState(null);
  const [showCalendarModal, setShowCalendarModal] = useState(false);
  const [startDay, setStartDay] = useState(null);
  const [endDay, setEndDay] = useState(null);
  const [markedDates, setMarkedDates] = useState({});
  const [selectedItem, setSelectedItem] = useState({});
  const [showCancelAppt, setShowCancelAppt] = useState(false);
  const [temporarySelectedDates, setTemporarySelectedDates] = useState([]);
  //function : navigation function
  const gotoAppointmentDetail = (BookingId, duration, status) =>
    navigation.navigate(ScreenNames.USER_APPOINTMENT_DETAILS, {
      BookingId: BookingId,
      duration,
      hideTrack: status === 'Completed' || status === 'Cancelled' ? true : false
    });
  //function : service function
  const getAppointmentsList = async (cancelledApt = false) => {
    setShowLoader(true);
    try {
      const appntListData = new FormData();
      appntListData.append('user_id', userInfo.id);
      console.log('userToken', userToken);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.USER_UPCOMING_APPOINTMENT_LIST,
        appntListData,
      );
      if (resp.data.status) {
        console.log('user setAppointmentData', resp.data.data);
        const updatedData = resp.data.data.map(el => {
          return {...el, showOtp: false};
        });
        const updatedData2 = updatedData?.map(el => {
          const newTime = el?.date_time.replace('PM', '');
          const newTime2 = moment(
            moment(newTime).format('dddd,D MMMM YYYY,kk:mm'),
          ).format('ddd, D MMM YYYY, h:mm A');
          return {...el, date_time: newTime2};
        });
        setAppointmentData(updatedData2);
        if (cancelledApt) {
          if (!aptStatus && temporarySelectedDates?.length === 0) {
            console.log('not setting filtered directly');
            setFilteredData(updatedData2);
            return;
          }
          aptStatus && changeAptStatus(aptStatus, updatedData2);
          temporarySelectedDates?.length > 0 && OKPressed(updatedData2);
        } else {
          setFilteredData(updatedData2);
        }
      }
    } catch (error) {
      console.log('error in getAppointmentsList', error);
    }
    setShowLoader(false);
  };

  //function : get invoice url
  const getInvoiceUrl = async (appointmentId, bookingId) => {
    try {
      const endPoint = `${Service.DOWNLOAD_INVOICE}?appointment_id=${appointmentId}`;
      const res = await Service.postApiWithToken(userToken, endPoint, {});
      console.log('getInvoiceUrl res', res.data);
      if (res.data.status) {
        checkPermission(res.data.url, appointmentId, bookingId);
      }
    } catch (error) {
      console.log('error', error);
    }
  };
  //function : imp function
  const checkPermission = async (url, appointmentId, bookingId) => {
    if (Platform.OS === 'ios') {
      downloadFile(url, appointmentId, bookingId);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadFile(url, appointmentId, bookingId);
          console.log('Storage Permission Granted.');
        } else {
          // dispatch(showToast({text: 'Storage Permission Not Granted'}));
          Toast.show('Storage Permission Not Granted', Toast.SHORT)
          // Alert.alert('Error', 'Stora/ge Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('ERROR' + err);
      }
    }
  };
  //function : service function
  const downloadFile = async (url, appointmentId, bookingId) => {
    let pdfUrl = url;
    let DownloadDir =
      Platform.OS == 'ios'
        ? RNFetchBlob.fs.dirs.DocumentDir
        : RNFetchBlob.fs.dirs.DownloadDir;
    const {dirs} = RNFetchBlob.fs;
    const dirToSave =
      Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
    const configfb = {
      fileCache: true,
      useDownloadManager: true,
      notification: true,
      mediaScannable: true,
      title: 'Cosmologo',
      path: `${dirToSave}.pdf`,
    };
    const configOptions = Platform.select({
      ios: {
        fileCache: configfb.fileCache,
        title: configfb.title,
        path: configfb.path,
        appendExt: 'pdf',
      },
      android: configfb,
    });
    Platform.OS == 'android'
      ? RNFetchBlob.config({
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path: `${DownloadDir}/${bookingId}.pdf`,
            description: 'Cosmologo',
            title: `${bookingId}invoice.pdf`,
            mime: 'application/pdf',
            mediaScannable: true,
          },
        })
          .fetch('GET', `${pdfUrl}`)
          .catch(error => {
            console.warn(error.message);
          })
      : RNFetchBlob.config(configOptions)
          .fetch('GET', `${pdfUrl}`, {})
          .then(res => {
            if (Platform.OS === 'ios') {
              RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
              RNFetchBlob.ios.previewDocument(configfb.path);
            }
            console.log('The file saved to ', res);
          })
          .catch(e => {
            console.log('The file saved to ERROR', e.message);
          });
  };
  //function : render function
  const appointmentRender = ({item, index}) => {
    return (
      <AppointmentCard
        BookingID={item.booking_id}
        SubCategory={item.subcategory}
        ConcernArea={item.concern_area_title}
        Product={item.product}
        ImageUrl={item.concern_area_image}
        Amount={parseFloat(item.amount).toFixed(2)}
        DoctorName={item.service_provider}
        DrProfileUrl={item.service_provider_profile_mage}
        AppointmentTime={item.date_time}
        onPress={() => gotoAppointmentDetail(item.booking_id, item.duration, item.status)}
        downloadInvoice={() => getInvoiceUrl(item.id, item.booking_id)}
        Role={'User'}
        duration={item.duration}
        showButtons={true}
        userCancelAppointment={() => {
          setSelectedItem(item);
          setShowCancelAppt(true);
        }}
        status={item.status}
        showOtp={item.showOtp}
        otp={item.otp}
        hideViewOtpButton={item.is_verified == '1'}
        changeShowOtpValue={() => changeShowOtpValue(item.id)}
        showUserCancelButton={
          item.is_verified == 1 ||
          item.cancelled == 3 ||
          item.cancelled == 4 ||
          item.status === 'Completed'
            ? false
            : true
        }
      />
    );
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getAppointmentsList();
    return () => {};
  }, []);
  const cancelAppointment = async (
    appointmentId,
    isVerified,
    appointmentDate,
    bookingId,
    isCancelled,
    aptStatus,
  ) => {
    if (isVerified == 1) {
      Toast.show('Verified appointment cannot be cancelled', Toast.SHORT);
      return;
    }
    if (aptStatus === 'Completed') {
      Toast.show('Completed appointment cannot be cancelled', Toast.SHORT);
      return;
    }
    if (isCancelled == 3) {
      Toast.show(
        'Appointment already cancelled by Service Provider',
        Toast.SHORT,
      );
      return;
    }
    if (
      moment(moment(appointmentDate).format('YYYY-MM-DD')).isBefore(
        moment().format('YYYY-MM-DD'),
      )
    ) {
      Toast.show('Appointment date cannot be in past', Toast.SHORT);
      return;
    }
    setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('appointment_id', appointmentId);
      myData.append('roletype', 'U');
      myData.append('booking_id', bookingId);
      console.log('myData', myData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.CANCEL_APPOINTMENT_USER_SERVICE,
        myData,
      );
      console.log('resp', resp);
      if (resp.data.status) {
        Toast.show(resp.data.message, Toast.SHORT);
        // setFilteredData(filteredData.filter(el => el.id !== appointmentId))
        // setAppointmentData(AppointmentData.filter(el => el.id !== appointmentId))
        getAppointmentsList(true);
        console.log('cancelAppointment', resp.data);
        console.warn('resp------>', resp.data);
      }
    } catch (error) {
      console.log('error in cancelAppointment', error);
    }
    setShowLoader(false);
    setShowCancelAppt(false);
  };
  const changeAptStatus = (value, data = []) => {
    // ! tell sanjay is 24 hrs date_time... then don't add am/pm
    setAptStatus(value);
    // All,Completed,Cancelled,Ongoing
    if (value === 'All') {
      console.log('setFilteredData', AppointmentData);
      setFilteredData(AppointmentData);
      return;
    }
    // else if(value === 'Ongoing'){
    //   const filteredByStatus = data.length > 0 ? data.filter(el => el.status === 'Processing') : AppointmentData.filter(el => el.status === 'Processing')
    //   console.log('setFilteredData', filteredByStatus);
    //   setFilteredData(filteredByStatus)
    //   return
    // }
    else {
      const filteredByStatus =
        data.length > 0
          ? data.filter(el => el.status === value)
          : AppointmentData.filter(el => el.status === value);
      console.log('setFilteredData', filteredByStatus);
      setFilteredData(filteredByStatus);
      return;
    }
  };
  const OKPressed = (data = []) => {
    if (temporarySelectedDates?.length === 0) {
      // dispatch(showToast({text:'Please select a date', duration:2000}))
      setShowCalendarModal(false);
      return;
    }
    const filteredByDays =
      data?.length > 0
        ? data.filter(el => {
            const newTime = el?.date_time.replace('PM', '');
            if (
              temporarySelectedDates?.length === 1 &&
              moment(moment(newTime).format('YYYY-MM-DD')).isSame(
                moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
              )
            ) {
              return true;
            } else if (
              temporarySelectedDates?.length >= 2 &&
              moment(moment(newTime)).isBetween(
                moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
                moment(
                  moment(
                    temporarySelectedDates[temporarySelectedDates?.length - 1],
                  ).format('YYYY-MM-DD'),
                ).add(1, 'days'),
              )
            ) {
              return true;
            }
            return false;
          })
        : AppointmentData.filter(el => {
            const newTime = el?.date_time.replace('PM', '');
            if (
              temporarySelectedDates?.length === 1 &&
              moment(moment(newTime).format('YYYY-MM-DD')).isSame(
                moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
              )
            ) {
              return true;
            } else if (
              temporarySelectedDates?.length >= 2 &&
              moment(moment(newTime)).isBetween(
                moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
                moment(
                  moment(
                    temporarySelectedDates[temporarySelectedDates?.length - 1],
                  ).format('YYYY-MM-DD'),
                ).add(1, 'days'),
              )
            ) {
              return true;
            }
            return false;
          });
    setFilteredData(filteredByDays);
    // setAptStatus(null)
    setShowCalendarModal(false);
  };
  const clearSelection = () => {
    setTemporarySelectedDates([]);
    setMarkedDates({});
    setStartDay(null);
    setEndDay(null);
    setFilteredData(AppointmentData);
  };
  const changeShowOtpValue = appointmentId => {
    const data = [...filteredData];
    const updatedData = data.map(el=>el.id === appointmentId ? {...el, showOtp:!el.showOtp}: el)
    // console.log('updateddata showotp', updatedData);
    // const value = data.find(el => el.id === appointmentId);
    // value.showOtp = true;
    setFilteredData([...updatedData]);
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader hasDrawerButton={true} Title="Appointments" />
      <View style={styles.mainView}>
        <View
          style={{
            marginBottom: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Dropdown
            style={[
              styles.dropdown,
              isFocus && {borderColor: Colors.THEME_GREEN},
            ]}
            data={data}
            containerStyle={{backgroundColor: Colors.BG_GREEN}}
            activeColor={Colors.LITE_GREEN}
            maxHeight={300}
            labelField="label"
            valueField="value"
            placeholder={!isFocus ? 'Select Status' : 'Select Status'}
            value={aptStatus}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
            onChange={item => {
              changeAptStatus(item.value);
              setIsFocus(false);
            }}
            iconStyle={{width: 40, height: 40}}
            iconColor={Colors.BLACK}
            placeholderStyle={{marginLeft: 10}}
            selectedTextStyle={{marginLeft: 10}}
          />
          <TouchableOpacity
            onPress={() => {
              setShowCalendarModal(true);
            }}
            style={{
              backgroundColor: Colors.BG_GREEN,
              borderRadius: 10,
              padding: 10,
              flex: 0.1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {temporarySelectedDates?.length === 0 ? (
              <MyIcon.AntDesign
                name="calendar"
                size={24}
                color={Colors.THEME_GREEN}
              />
            ) : (
              <MyIcon.FontAwesome5
                name="calendar-check"
                size={24}
                color={Colors.THEME_GREEN}
              />
            )}
          </TouchableOpacity>
        </View>
        <MyText
          text={`${filteredData.length} ${
            filteredData.length === 1 || filteredData.length === 0
              ? 'Appointment booked'
              : 'Appointments booked'
          }`}
          marginVertical={20}
          fontFamily="medium"
        />
        <FlatList
          data={filteredData}
          renderItem={appointmentRender}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 150}}
          ListFooterComponent={<View />}
          ListFooterComponentStyle={{height: 100}}
        />
      </View>
      <CustomLoader showLoader={showLoader} />
      <AppointmentCalender
        visible={showCalendarModal}
        setVisibility={setShowCalendarModal}
        startDay={startDay}
        setStartDay={setStartDay}
        endDay={endDay}
        setEndDay={setEndDay}
        temporarySelectedDates={temporarySelectedDates}
        setTemporarySelectedDates={setTemporarySelectedDates}
        markedDates={markedDates}
        setMarkedDates={setMarkedDates}
        OKPressed={OKPressed}
        clearSelection={clearSelection}
      />
      <CancelAppointment
        visible={showCancelAppt}
        setVisibility={setShowCancelAppt}
        onPress={() =>
          cancelAppointment(
            selectedItem.id,
            selectedItem.is_verified,
            selectedItem.date_time,
            selectedItem.booking_id,
            selectedItem.cancelled,
            selectedItem.status,
          )
        }
      />
    </View>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(Appointment);
