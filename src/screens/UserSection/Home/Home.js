//react components
import React, {useEffect, useState} from 'react';
import {
  View,
  ImageBackground,
  TextInput,
  ScrollView,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//third parties
import {useNetInfo} from '@react-native-community/netinfo';
//custom components
import CustomLoader from 'components/CustomLoader/CustomLoader';
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import TitleViewAll from 'components/TitleViewAll/TitleViewAll';
import MyButton from 'components/MyButton/MyButton';
import DoctorCard from 'components/DoctorCard/DoctorCard';

//global
import {
  Colors,
  Constant,
  Images,
  MyIcon,
  ScreenNames,
  Service,
} from 'global/Index';
//styles
import {styles} from './HomeStyle';
import {connect, useDispatch} from 'react-redux';
import ProceduresCard from 'components/ProceduresCard/ProceduresCard';
import Carousel from 'components/Carousel/Carousel';
import {setUserNotifications} from 'src/reduxToolkit/reducer/user';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';

const Home = ({navigation, userToken, userInfo}) => {
  const {isInternetReachable} = useNetInfo();
  //variables
  const dispatch = useDispatch();
  //states
  const [services, setServices] = useState([]);
  const [SliderData, setSliderData] = useState([]);
  const [showLoader1, setShowLoader1] = useState(false);
  const [showLoader2, setShowLoader2] = useState(false);
  const [showLoader3, setShowLoader3] = useState(false);
  //function : navigation function
  const gotoSearch = () => navigation.navigate(ScreenNames.SEARCH);
  const gotoProcDetail = () =>
    navigation.navigate(ScreenNames.USER_PROC_DETAIL);
  const gotoSubCategoryService = item =>
    navigation.navigate(ScreenNames.USER_SUB_CATEGORY, {item});
  const gotoAddOn = () => navigation.navigate(ScreenNames.USER_ADD_ON);
  const gotoProceduresList = () =>
    navigation.navigate(ScreenNames.PROCEDURES_LIST);
  //function : service function
  const getServiceCategories = async () => {
    setShowLoader1(true)
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.GET_SERVICE_CATEGORY,
      );
      if (resp.data.status) {
        setServices(resp.data.data);
        // console.warn('resp---------->', resp.data.data);
      }
    } catch (error) {
      console.log('error in getServiceCategories', error);
    }
    setShowLoader1(false)
  };
  const getSliderList = async () => {
    setShowLoader2(true)
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.SLIDER_LIST,
      );
      if (resp.data.status) {
        setSliderData(resp.data.data);
      }
    } catch (error) {
      console.log('error in getSliderList', error);
    }
    setShowLoader2(false)
  };
  const getNotifications = async () => {
    setShowLoader3(true)
    const endPoint = Service.USER_NOTIFICATION
    try {
      const resp = await Service.postApiWithToken(
        userToken,
        endPoint,
        {},
      );
      if (resp.data.status) {
        console.warn('resp---->', resp.data);
        console.log('setNotificationData', resp.data.data);
        await AsyncStorage.setItem('userNotifications', JSON.stringify(resp.data.data));
        dispatch(setUserNotifications(resp.data.data));
        // setNotificationData(resp.data.data);
      }else{
        // Toast.show(resp.data.message, Toast.SHORT)
      }
    } catch (error) {
      console.log('error in getNotifications', error);
    }
    setShowLoader3(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getServiceCategories();
      getSliderList();
      getNotifications();
    });
    return unsubscribe;  
  }, [navigation]);

  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Home" hasDrawerButton={true} isBorderRadius={false} hideAddMore={false}/>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.nameAndTaglineView}>
          <MyText
            text={`Hi, ${userInfo.first_name} ${userInfo.last_name}`}
            fontSize={16}
            textColor="white"
          />
          <MyText
            text="Beauty at Home"
            fontSize={24}
            fontFamily="bold"
            textColor="white"
            marginVertical={10}
          />
        </View>
        <View style={styles.mainView}>
          <TouchableOpacity onPress={gotoSearch} style={styles.searchBox}>
            <MyIcon.AntDesign
              name="search1"
              color={Colors.THEME_GREEN}
              size={24}
              style={{marginRight: 10}}
            />
            <MyText text="Search by Category name" textColor="theme_green" />
          </TouchableOpacity>
          {/* scrollable banner */}
          {SliderData.length > 0 ? <Carousel data={SliderData} /> : null}

          {/* <View
            style={{
              marginVertical: 10,
            }}>
            <ImageBackground
              resizeMode="stretch"
              style={{
                height: 170,
                padding: 20,
              }}
              source={Images.ProceduresIcon.Banner}>
              <View
                style={{
                  width: '45%',
                }}>
                <MyText
                  text="Changing Lives For The Better, One Patient At A Time"
                  textColor="white"
                  fontSize={16}
                  fontFamily="medium"
                />
                <MyButton Title="OUR PROCEDURES" />
              </View>
            </ImageBackground>
          </View> */}
          <TitleViewAll Title="OUR PROCEDURES" onPress={gotoProceduresList} />
          {services.length > 0
            ? services.map((item, index) => {
                return (
                  <ProceduresCard
                    key={index.toString()}
                    item={item}
                    onPress={() => gotoSubCategoryService(item)}
                  />
                );
              })
            : null}
        </View>
      </ScrollView>
      <CustomLoader showLoader={showLoader1 || showLoader2 || showLoader3} />
    </View>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
  userInfo: state.user.userInfo,
});
export default connect(mapStateToProps, null)(Home);
