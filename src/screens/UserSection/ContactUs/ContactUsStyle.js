import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  contactUsComponent: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    backgroundColor: Colors.WHITE,
    shadowRadius: 15,
    elevation: 2,
    padding: 20,
  },
  iconStyle: {
    height: 40,
    width: 40,
    marginRight: 15,
  },
});
