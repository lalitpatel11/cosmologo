//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Platform,
  Linking
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//global
import {Images, Service, ScreenNames} from 'global/Index';
//styles
import {styles} from './ContactUsStyle';
//third parties
import {useNetInfo} from "@react-native-community/netinfo";

const ContactUsScreen = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  const [showLoader, setShowLoader] = useState(false);
  const [contactData, setContactData] = useState({});
  const callNow = (phoneNumber) => {
    if (Platform.OS === 'android') {
      Linking.openURL(`tel:${phoneNumber}`);
    } else {
      Linking.openURL(`telprompt:${phoneNumber}`);
    }
  };
  const getContactDetails = async () => {
    setShowLoader(true);
    try {
      const resp = await Service.postApi(
        Service.CONTACT_US,
      );
      if (resp.data.status) {
        setContactData(resp.data.data);
        console.log('setContactData', resp.data.data);
        console.warn('resp--->', resp.data);
      }
    } catch (error) {
      console.log('error in getContactDetails', error);
    }
    setShowLoader(false)
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  useEffect(()=>{
    getContactDetails();
    return () => {};
  },[])
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title={'Contact Us'} IsCartIcon={false} IsNotificationIcon={false} />
      <ScrollView
        contentContainerStyle={{paddingBottom: 50}}
        style={styles.mainView}>
        <Image source={Images.ContactUsImage.headerImage} style={{alignSelf:'center', width:'100%'}} resizeMode='cover'/>

        <TouchableOpacity style={styles.contactUsComponent} onPress={()=>callNow(`${contactData?.phone_code} ${contactData?.phone}`)}>
          <Image
            resizeMode="contain"
            source={Images.ContactUsImage.contactByPhone}
            style={styles.iconStyle}
          />
          <View>
            <MyText
              fontSize={14}
              textColor={'#3E5869'}
              text="Phone"
              fontFamily="bold"
              marginBottom={5}
            />
            {/* <TouchableOpacity onPress={()=>callNow('+91-9988737878')}> */}
              <MyText fontSize={14} textColor={'#3E5869'} text={`${contactData?.phone_code} ${contactData?.phone}`} />
            {/* </TouchableOpacity> */}
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.contactUsComponent} onPress={()=>{Linking.openURL(`mailto:${contactData?.email}`)}}>
          <Image
            resizeMode="contain"
            source={Images.ContactUsImage.contactByEmail}
            style={styles.iconStyle}
          />
          <View>
            <MyText
              fontSize={14}
              textColor={'#3E5869'}
              text="Email Address"
              fontFamily="bold"
              marginBottom={5}
            />
            {/* <TouchableOpacity onPress={()=>{Linking.openURL('mailto:support@example.com')}}> */}
              <MyText
                fontSize={14}
                textColor={'#3E5869'}
                text={contactData?.email}
              />
            {/* </TouchableOpacity> */}
          </View>
        </TouchableOpacity>
        <View style={styles.contactUsComponent}>
          <Image
            resizeMode="contain"
            source={Images.ContactUsImage.contactByLocation}
            style={styles.iconStyle}
          />
          <View>
            <MyText
              fontSize={14}
              textColor={'#3E5869'}
              text="Address"
              fontFamily="bold"
              marginBottom={5}
            />
            <MyText
              fontSize={14}
              textColor={'#3E5869'}
              text={contactData?.address}
              style={{paddingRight:50}}
            />
          </View>
        </View>
      </ScrollView>

      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default ContactUsScreen;
