//react components
import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  TextInput,
  TouchableOpacity,
  FlatList,
  Keyboard,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyText from 'components/MyText/MyText';
import ProceduresCard from 'components/ProceduresCard/ProceduresCard';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './SearchStyle';
//redux
import {useSelector} from 'react-redux';
//third parties
import {useNetInfo} from "@react-native-community/netinfo";

const Search = ({navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //ref
  const searchRef = useRef();
  //variables : redux variables
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [categoryData, setCategoryData] = useState([]);
  const [services, setServices] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [showLoader, setShowLoader] = useState(false);
  //function : navigation function
  const gotoSubCategoryService = item =>
    navigation.navigate(ScreenNames.USER_SUB_CATEGORY, {item: item});
  const goBack = () =>
    navigation.canGoBack() ? navigation.goBack() : console.log("can't go back");
  //function : imp function
  const cancelSearch = () => {};
  //function : service function
  const getServiceCategories = async () => {
    console.log('getServiceCategories fn called');
    setShowLoader(true);
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.GET_SERVICE_CATEGORY,
      );
      if (resp.data.status) {
        console.log('setServices', resp.data.data);
        setServices(resp.data.data);
        setFilteredData(resp.data.data);
      }
    } catch (error) {
      console.log('error in getServiceCategories', error);
    }
    setShowLoader(false);
  };
  const seachProcedures = async text => {
    try {
      const searchData = new FormData();
      searchData.append('title', text);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SEARCH_CATEGORY,
        searchData,
      );
      if (resp.data.status) {
        setCategoryData(resp.data.data);
      }
    } catch (error) {
      setCategoryData([]);
      console.log('error in seachProcedures', error);
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    searchRef.current.focus();
    return () => {
      Keyboard.dismiss();
    };
  }, []);
  //useEffect
  useEffect(() => {
    getServiceCategories();

    return () => {};
  }, []);
  const searchForText = (text) => {
    const myData = services?.filter(el=>{
      const titleLower = el?.title?.toLowerCase()
      const textLower = text?.trim()?.toLowerCase()
      const doesInclude = titleLower?.includes(textLower)
      if(doesInclude){
        return true
      }
      return false
    })
    setFilteredData(myData)
  }
  //UI
  return (
    <View style={styles.container}>
      <SearchHeader
        textInputRef={searchRef}
        BackPress={goBack}
        CancelPress={goBack}
        onChangeText={text => {searchForText(text)}}
      />
      <View style={styles.mainView}>
        {filteredData.length > 0 ? (
          <FlatList
            data={filteredData}
            renderItem={({item, index}) => {
              return (
                <ProceduresCard
                  item={item}
                  onPress={() => gotoSubCategoryService(item)}
                />
              );
            }}
            keyExtractor={item => item.id.toString()}
            ListFooterComponent={<View />}
            ListFooterComponentStyle={{height:100}}
          />
        ) : (
          <View>
            {/* // <ActivityIndicator animating={true} /> */}
            <MyText text={'No Categories found'} textAlign="center" />
          </View>
        )}
      </View>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default Search;

const SearchHeader = ({
  textInputRef,
  BackPress = () => {},
  CancelPress = () => {},
  onChangeText,
}) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.THEME_GREEN,
        padding: 10,
      }}>
      <TouchableOpacity onPress={BackPress}>
        <MyIcon.Feather name="arrow-left" size={24} color={Colors.WHITE} />
      </TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: Colors.LITE_GREEN,
          borderRadius: 10,
          width: '75%',
          paddingHorizontal: 10,
        }}>
        <MyIcon.Feather name="search" size={20} color={Colors.WHITE} />
        <TextInput
          ref={textInputRef}
          placeholder="Search by Category name"
          placeholderTextColor={Colors.WHITE}
          onChangeText={onChangeText}
          style={{
            color: Colors.WHITE,
            height: 40,
            width: '70%',
          }}
        />
      </View>
      <TouchableOpacity onPress={CancelPress}>
        <MyText text="Cancel" textColor="white" />
      </TouchableOpacity>
    </View>
  );
};
