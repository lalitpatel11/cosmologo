//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Platform,
  PermissionsAndroid,
  StyleSheet,
  Dimensions,
  Modal,
  TouchableOpacity,
  Linking
} from 'react-native';
import {CommonActions, useRoute } from '@react-navigation/native';
//custom components
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
//third parties
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import MapViewDirections from 'react-native-maps-directions';
import {useNetInfo} from "@react-native-community/netinfo";
//styles
import {styles} from './TrackServiceStyle';
import {Colors, MyIcon, ScreenNames} from 'global/Index';
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';

const TrackService = (navigation) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const route = useRoute();
  const ApptDetailData = route.params.ApptDetailData;
  //data
  const {width, height} = Dimensions.get('window');
  const ASPECT_RATIO = width / height;
  const LATITUDE = 37.771707;
  const LONGITUDE = -122.4053769;
  const LATITUDE_DELTA = 0.0922;
  const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
  // const GOOGLE_MAPS_APIKEY = 'AIzaSyBJqbxRoFBbpmwDrHOtVM26s9R1Fh5UWp0';
  const GOOGLE_MAPS_APIKEY = 'AIzaSyACzgsZq8gI9VFkOw_fwLJdmezbc4iUxiM';
  //hook :states
  const [showDetails, setShowDetails] = useState(false);
  const [totalDistance, setTotalDistance] = useState(0);
  const [totalDuration, setTotalDuration] = useState(0);
  const [coordinates] = useState([
    {
      latitude: Number(ApptDetailData.user_lat),
      longitude: Number(ApptDetailData.user_long),
    },
    {
      latitude: Number(ApptDetailData.provider_lat),
      longitude: Number(ApptDetailData.provider_long),
      // latitude: 30.743340,
      // longitude: 76.811119,
    },
  ]);
  const [currentLocation, setCurrentLocation] = useState({latitude: Number(ApptDetailData.user_lat), longitude: Number(ApptDetailData.user_long), latitudeDelta: 0.9, longitudeDelta: 0.9,});
  //function : imp function
  const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message:
            'This App needs access to your location ' +
            'so we can know where you are.',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use locations ');
        myposition();
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.log(err);
    }
  };
  const myposition = () => {
    Geolocation.getCurrentPosition(
      position => {
        const locData = {
          latitude: position?.coords?.latitude,
          longitude: position?.coords?.longitude,
          latitudeDelta: 0.9,
          longitudeDelta: 0.9,
        };
        setCurrentLocation(locData);
      },
      Error => {
        console.error(Error);
      },
      {
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 10000,
      },
    );
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    Geolocation.watchPosition(
      position => {
        // const { coordinate, routeCoordinates, distanceTravelled } =   this.state;
        const {latitude, longitude} = position.coords;
        console.warn('new updated position', position);
        const newCoordinate = {
          latitude,
          longitude,
        };
        // if (Platform.OS === "android") {
        //   if (this.marker) {
        //     this.marker._component.animateMarkerToCoordinate(
        //       newCoordinate,
        //       500
        //     );
        //    }
        //  } else {
        //    coordinate.timing(newCoordinate).start();
        //  }
        //  this.setState({
        //    latitude,
        //    longitude,
        //    routeCoordinates: routeCoordinates.concat([newCoordinate]),
        //    distanceTravelled:
        //    distanceTravelled + this.calcDistance(newCoordinate),
        //    prevLatLng: newCoordinate
        //  });
      },
      error => console.log(error),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );

    return () => {};
  }, []);

  // useEffect(() => {
  //   if (Platform.OS === 'android') {
  //     requestLocationPermission();
  //   } else {
  //     Geolocation.requestAuthorization('always').then(res => {
  //       myposition();
  //     });
  //   }
  //   return () => {};
  // }, []);
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Track Service" />
      {Object.keys(currentLocation).length > 0 ? (
        <>
          <MapView
            showsBuildings={true}
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            initialRegion={currentLocation}
            style={styles.container}>
            <Marker
              coordinate={coordinates[0]}
              // image={{
              //   height: 5,
              //   width: 5,
              //   uri: 'https://cdn3.iconfinder.com/data/icons/search-optimization/512/pin_map_development_marker_optimization_flat_icon-512.png',
              // }}
              pinColor="red"
            />
            <Marker coordinate={coordinates[1]} pinColor="blue">
              <MyIcon.FontAwesome5
                size={50}
                name="car-alt"
                color={Colors.BLACK}
              />
            </Marker>
            <MapViewDirections
              origin={coordinates[0]}
              destination={coordinates[1]}
              waypoints={
                coordinates.length > 2 ? coordinates.slice(1, -1) : undefined
              }
              splitWaypoints={true}
              precision="high"
              timePrecision="now"
              mode="DRIVING"
              apikey={GOOGLE_MAPS_APIKEY} // insert your API Key here
              strokeWidth={6}
              strokeColor={Colors.THEME_GREEN}
              onStart={params => {
                console.log(
                  `Started routing between "${params.origin}" and "${params.destination}"`,
                );
              }}
              onReady={result => {
                setTotalDistance(result.distance);
                setTotalDuration(result.duration);
                console.log(`Distance: ${result.distance} km`);
                console.log(`Duration: ${result.duration} min.`);
              }}
              optimizeWaypoints={true}
            />
          </MapView>
        </>
      ) : null}
      <View
        style={{
          position: 'absolute',
          width: '100%',
          alignItems: 'center',
          bottom: 10,
        }}>
        <MyButton
          onPress={() => setShowDetails(true)}
          Title={'Show Detail'}
          width="50%"
        />
      </View>
      <TrackDetailModal
        visible={showDetails}
        setVisibility={setShowDetails}
        totalDistance={totalDistance}
        totalDuration={totalDuration}
        serviceProviderLocation={coordinates[1]}
        userLocation={coordinates[0]}
      />
    </View>
  );
};

export default TrackService;

const TrackDetailModal = ({
  visible,
  setVisibility,
  totalDistance,
  totalDuration,
  serviceProviderLocation,
  userLocation
}) => {
  const url = `https://www.google.com/maps/dir/?api=1&origin=${serviceProviderLocation?.latitude},${serviceProviderLocation?.longitude}&destination=${userLocation?.latitude},${userLocation?.longitude}&dir_action=navigate`;
  //function : modal function
  const closeModal = () => {
    setVisibility(false);
  };
  return (
    <Modal
      visible={visible}
      animationType="slide"
      onRequestClose={closeModal}
      transparent={true}>
      <View style={{flex: 1, justifyContent: 'flex-end', padding: 20}}>
        <TouchableOpacity style={{flex: 1}} onPress={closeModal} />

        <View
          style={{
            backgroundColor: Colors.WHITE,
            padding: 20,
            borderRadius: 20,
          }}>
          <MyText
            text={`Distance :-${parseFloat(totalDistance).toFixed(2)} km`}
          />
          <MyText
            text={`Duration :-${Math.floor(
              totalDuration / 60,
            )} hours and ${parseInt(totalDuration % 60)} minutes`}
          />
          <MyButton Title="Direction" onPress={() => Linking.openURL(url)} />
        </View>
      </View>
    </Modal>
  );
};
