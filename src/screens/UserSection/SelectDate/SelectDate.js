//react components
import React, {useState, useEffect} from 'react';
import {View, ScrollView, TouchableOpacity, Alert} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';
import MyHeader from 'components/MyHeader/MyHeader';
import IconButton from 'components/IconButton/IconButton';
//third parties
import {Calendar} from 'react-native-calendars';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
//global
import {Colors, ScreenNames, MyIcon} from 'global/Index';
//styles
import {styles} from './SelectDateStyle';
//import : redux
import {useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {addToCart} from 'src/reduxToolkit/reducer/cart';
import {useNetInfo} from "@react-native-community/netinfo";

const SelectDate = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const dispatch = useDispatch();
  //variables : route variables
  const AOCData = route.params.AOCData;
  //data
  const SlotData = [
    {
      id: 1,
      time: '10:00 AM',
      hours: 10,
    },
    {
      id: 2,
      time: '11:00 AM',
      hours: 11,
    },
    {
      id: 3,
      time: '12:00 PM',
      hours: 12,
    },
    {
      id: 4,
      time: '01:00 PM',
      hours: 13,
    },
    {
      id: 5,
      time: '02:00 PM',
      hours: 14,
    },
    {
      id: 6,
      time: '03:00 PM',
      hours: 15,
    },
    {
      id: 7,
      time: '04:00 PM',
      hours: 16,
    },
    {
      id: 8,
      time: '05:00 PM',
      hours: 17,
    },
    {
      id: 9,
      time: '06:00 PM',
      hours: 18,
    },
    {
      id: 10,
      time: '07:00 PM',
      hours: 19,
    },
  ];

  //variables
  const _format = 'YYYY-MM-DD';
  //variables : route variables
  // const AOCData = route.params.AOCData;
  //states
  const [_markedDates, set_markedDates] = useState({});
  const [withoutServiceProvider, setWithoutServiceProvider] = useState(true);
  const [ApptDate, setApptDate] = useState('');
  const [ApptTime, setApptTime] = useState('');
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //function : imp function
  const onDaySelect = day => {
    const _selectedDay = moment(day.dateString).format(_format);
    if (moment(_selectedDay).isBefore(moment(), 'day')) {
      // dispatch(
      //   showToast({
      //     text: `The Date must be Bigger or Equal to today's date`,
      //     duration: 1000,
      //   }),
      // );
      Toast.show(`The Date must be Bigger or Equal to today's date`, Toast.SHORT)
      return;
    }
    const updatedMarkedDates = {
      [_selectedDay]: {
        customStyles: {
          container: {
            backgroundColor: Colors.THEME_GREEN,
          },
        },
        marked: true,
        selected: true,
      },
    };
    if (ApptTime) {
      setApptDate(_selectedDay);
      const hours = SlotData.find(el => el.time === ApptTime)?.hours;
      // if changing to today's date and selected time is not available (is the past), remove selected time
      if (
        moment(_selectedDay).isSame(new Date(), 'day') &&
        moment(moment().add(1, 'hours')).format('H:mm') >
          moment(new Date().setHours(hours, 0, 0)).format('H:mm')
      ) {
        setApptTime('');
      }
    } else {
      setApptDate(_selectedDay);
    }
    set_markedDates(updatedMarkedDates);
  };

  //function : navigation function
  const gotoServiceProviders = () =>
    navigation.navigate(ScreenNames.USER_SERVICE_PROVIDERS, {
      AOCData,
      ApptDate,
      ApptTime,
    });
  //function : navigation function
  const gotoCart = () =>
    navigation.navigate(ScreenNames.CART, {hideAddMore: true});
  //function : imp function
  const selectAppointmentDate = () => {
    if (ApptDate == '') {
      // dispatch(showToast({text: 'Please select Date for appointment'}));
      Toast.show('Please select Date for appointment', Toast.SHORT)
      // Alert.alert('', 'Please select Date for appointment');
    } else if (ApptTime == '') {
      // dispatch(showToast({text: 'Please select Time for appointment'}));
      Toast.show('Please select Time for appointment', Toast.SHORT)
      // Alert.alert('', 'Please select Time for appointment');
    } else {
      gotoServiceProviders();
    }
  };
  const addItemToCart = () => {
    if (ApptDate == '') {
      // dispatch(showToast({text: 'Please select Date for appointment'}));
      Toast.show('Please select Date for appointment', Toast.SHORT)
      // Alert.alert('', 'Please select Date for appointment');
      return;
    } else if (ApptTime == '') {
      // dispatch(showToast({text: 'Please select Time for appointment'}));
      Toast.show('Please select Time for appointment', Toast.SHORT)
      // Alert.alert('', 'Please select Time for appointment');
      return;
    }
    const addToCartData = {
      data: AOCData,
      appointment_date: ApptDate,
      appointment_time: ApptTime,
      // service_provider: selectedProvider,
    };
    dispatch(addToCart(addToCartData));
    // dispatch(showToast({text: 'Item added successfully'}));
    gotoCart();
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Book Appointment" />
      <ScrollView
        contentContainerStyle={{paddingBottom: '20%'}}
        showsVerticalScrollIndicator={false}
        style={styles.mainView}>
        <Calendar
          style={styles.calendarStyle}
          onDayPress={day => {
            onDaySelect(day);
          }}
          enableSwipeMonths={true}
          markingType="custom"
          markedDates={_markedDates}
          minDate={moment().format('YYYY-MM-DD')}
        />
        <MyText
          text="Choose Slot"
          fontFamily="bold"
          fontSize={16}
          marginVertical={10}
        />
        <View style={styles.slotView}>
          {SlotData.map((item, index) => {
            return (
              <TouchableOpacity
                onPress={() => {
                  // const newDate = new Date(moment().format('YYYY-MM-DD')+' '+item.time)
                  // console.log('moment', moment(newDate).format('hh:mm A'));
                  // console.log('disabled', moment().format('H:mm') > moment(newDate).format('H:mm'));
                  if (!ApptDate) {
                    Toast.show(
                      'Please select Date for appointment',
                      Toast.SHORT,
                    );
                  } else {
                    setApptTime(item.time);
                  }
                }}
                disabled={
                  ApptDate &&
                  moment(ApptDate).isSame(new Date(), 'day') &&
                  moment(moment().add(1, 'hours')).format('H:mm') >
                    moment(new Date().setHours(item.hours, 0, 0)).format('H:mm')
                    ? true
                    : false
                }
                style={{
                  ...styles.slotItemStyle,
                  backgroundColor:
                    ApptTime == item.time
                      ? Colors.THEME_GREEN
                      : ApptDate &&
                        moment(ApptDate).isSame(new Date(), 'day') &&
                        moment(moment().add(1, 'hours')).format('H:mm') >
                          moment(new Date().setHours(item.hours, 0, 0)).format(
                            'H:mm',
                          )
                      ? '#ECECEC'
                      : Colors.WHITE,
                }}
                key={item.id.toString()}>
                <MyText
                  text={item.time}
                  textColor={ApptTime == item.time ? 'white' : 'black'}
                />
              </TouchableOpacity>
            );
          })}
        </View>
        {withoutServiceProvider ? (
          <View
            style={{
              padding: 20,
            }}>
            <IconButton
              onPress={addItemToCart}
              VectorIcon={
                <MyIcon.AntDesign
                  name="shoppingcart"
                  size={18}
                  color={Colors.THEME_GREEN}
                />
              }
              Title="Add To Cart"
            />
          </View>
        ) : (
          <MyButton
            Title="Select Service Providers"
            onPress={selectAppointmentDate}
          />
        )}
      </ScrollView>
    </View>
  );
};

export default SelectDate;
