//react components
import React, {useState, useEffect} from 'react';
import {View, Text, Alert} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyButton from 'components/MyButton/MyButton';
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyPasswordInput from 'components/MyPasswordInput/MyPasswordInput';
//global
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ChangePasswordStyle';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import {connect, useDispatch} from 'react-redux';
//third parties
import Toast from 'react-native-simple-toast';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {useNetInfo} from "@react-native-community/netinfo";

const ChangePassword = ({navigation, userToken}) => {
  const {isInternetReachable} = useNetInfo();
  //states
  const dispatch = useDispatch();
  const [showLoader, setShowLoader] = useState(false);
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //function : navigation function
  const gotoProfile = () => navigation.navigate(ScreenNames.USER_PROFILE);
  //function : imp function
  const Validation = () => {
    if (oldPassword == '') {
      // dispatch(showToast({text: 'Please enter old password'}));
      Toast.show('Please enter old password', Toast.SHORT)
      // Alert.alert('', 'Please enter old password');
    } else if (newPassword == '') {
      // dispatch(showToast({text: 'Please enter new password'}));
      Toast.show('Please enter new password', Toast.SHORT)
      // Alert.alert('', 'Please enter new password');
    } else if (confirmPassword == '') {
      // dispatch(showToast({text: 'Please enter confirm password'}));
      Toast.show('Please enter confirm password', Toast.SHORT)
      // Alert.alert('', 'Please enter confirm password');
    } else if (newPassword != confirmPassword) {
      // dispatch(
      //   showToast({text: 'Please enter same password and confirm password'}),
      // );
      Toast.show('Please enter same password and confirm password', Toast.SHORT)
      // Alert.alert('', 'Please enter same password and confirm password');
    } else return true;
  };
  //function : service function
  const changePassword = async () => {
    if (Validation()) {
      setShowLoader(true);
      try {
        const changePasswordData = new FormData();
        changePasswordData.append('old_password', oldPassword);
        changePasswordData.append('password', newPassword);
        changePasswordData.append('password_confirmation', confirmPassword);
        const resp = await Service.postApiWithToken(
          userToken,
          Service.CHANGE_PASSWORD,
          changePasswordData,
        );
        if (resp.data.status) {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
          gotoProfile();
        } else {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
        }
      } catch (error) {
        console.log('error in changePassword', error);
      }
      setShowLoader(false);
    }
  };

  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Change Password" />
      <View style={styles.mainView}>
        <MyText text="Change Password" fontFamily="bold" fontSize={26} />
        <MyText
          text="Your new password must be different from previous used passwords"
          textColor="lite_grey"
          fontSize={16}
          marginVertical={10}
        />
        <MyPasswordInput
          Title="Old Password"
          placeholder="Enter Old Password"
          onChangeText={text => setOldPassword(text)}
          showPassword
          ShowIcon={
            <MyIcon.FontAwesome5
              name="eye"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          HideIcon={
            <MyIcon.FontAwesome5
              name="eye-slash"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          secureTextEntry={true}
        />
        <MyPasswordInput
          Title="New Password"
          placeholder="Enter New Password"
          onChangeText={text => setNewPassword(text)}
          showPassword
          ShowIcon={
            <MyIcon.FontAwesome5
              name="eye"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          HideIcon={
            <MyIcon.FontAwesome5
              name="eye-slash"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          secureTextEntry={true}
        />
        <MyPasswordInput
          Title="Confirm Password"
          placeholder="Enter Confirm Password"
          onChangeText={text => setConfirmPassword(text)}
          showPassword
          ShowIcon={
            <MyIcon.FontAwesome5
              name="eye"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          HideIcon={
            <MyIcon.FontAwesome5
              name="eye-slash"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          secureTextEntry={true}
        />
        <View style={{height: 20}} />
        <MyButton Title="Save" onPress={changePassword} />
      </View>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(ChangePassword);
