//import : react components
import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  TouchableOpacity,
  Alert,
  Keyboard,
  Image,
  PermissionsAndroid,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//import : custom components
import SearchLocation from 'modals/SearchLocation/SearchLocation';
import SelectImageSource from 'modals/SelectImageSource/SelectImageSource';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyText from 'components/MyText/MyText';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyPasswordInput from 'components/MyPasswordInput/MyPasswordInput';
import MyButton from 'components/MyButton/MyButton';
import TextInputWithFlag from 'components/TextInputWithFlag/TextInputWithFlag';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import DateSelector from 'components/DateSelector/DateSelector';
import TermOfUseCheck from 'components/TermOfUseCheck/TermOfUseCheck';
import SelectAddress from 'modals/SelectAddress/SelectAddress';
//import : third parties
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import messaging from '@react-native-firebase/messaging';
import Toast from 'react-native-simple-toast';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import CountryPicker from 'react-native-country-codes-picker';
import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';
import {useNetInfo} from "@react-native-community/netinfo";
//import : global
import {Colors, Constant, MyIcon, ScreenNames, Service} from 'global/Index';
//import : styles
import {styles} from './SignUpStyle';
//import : redux
import {useDispatch, useSelector} from 'react-redux';
import {setUser, setUserToken} from 'src/reduxToolkit/reducer/user';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const SignUp = ({navigation}) => {
  const {isInternetReachable} = useNetInfo();
  Geocoder.init('AIzaSyBJqbxRoFBbpmwDrHOtVM26s9R1Fh5UWp0');
  const GOOGLE_MAPS_APIKEY = 'AIzaSyACzgsZq8gI9VFkOw_fwLJdmezbc4iUxiM';
  //hook : ref
  const firstNameRef = useRef();
  const lastNameRef = useRef();
  const emailAddressRef = useRef();
  const phoneNumberRef = useRef();
  const passwordRef = useRef();
  const confirmPasswordRef = useRef();
  const addressRef = useRef();
  //variables
  const dispatch = useDispatch();
  //hook : states
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [emailAddress, setEmailAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [filePath, setFilePath] = useState('');
  const [gender, setGender] = useState(1);
  const [date, setDate] = useState(new Date());
  const [address, setAddress] = useState('');
  const [latLng, setLatLng] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [showLoader, setShowLoader] = useState(false);
  const [isAcceptedTC, setIsAcceptedTC] = useState(false);
  const [showSelectAddress, setShowSelectAddress] = useState(false);
  const [allCurrentLocations, setAllCurrentLocations] = useState([]);
  const [open, setOpen] = useState(false);
  const [show, setShow] = useState(false);
  const [fcmToken, setFcmToken] = useState('');
  const [selectedCountry, setSelectedCountry] = useState({
    code: 'US',
    dial_code: '+1',
    flag: '🇺🇸',
    name: {
      by: '',
      cz: 'Spoj  ené státy',
      en: 'United States',
      es: 'Estados Unidos',
      pl: 'Stany Zjednoczone',
      pt: 'Estados Unidos',
      ru: 'Соединенные Штаты',
      ua: 'Сполучені Штати',
    },
  });
  const [showImageSourceModal, setShowImageSourceModal] = useState(false);
  const [showLocationModal, setShowLocationModal] = useState(false);
  //function : navigation function
  const gotoSignIn = () => navigation.navigate(ScreenNames.USER_SIGNIN);
  const resetIndexGoToBottomTab = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.USER_BOTTOM_TAB}],
  });
  //function : imp function
  const openLibrary = () => {
    let options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose Photo from Custom Option',
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, response => {
      if (response.didCancel) {
        // Alert.alert('User cancelled camera picker');
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'User cancelled image picker'}));
        Toast.show('User cancelled image picker', Toast.SHORT)
        // Alert.alert('User cancelled image picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Camera not available on device'}));
        Toast.show('Camera not available on device', Toast.SHORT)
        // Alert.alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Permission not satisfied'}));
        Toast.show('Permission not satisfied', Toast.SHORT)
        // Alert.alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: response.errorMessage}));
        Toast.show(response.errorMessage, Toast.SHORT)
        // Alert.alert(response.errorMessage);
        return;
      } else {
        setFilePath(response.assets[0]);
        setShowImageSourceModal(false);
      }
      setShowImageSourceModal(false);
    });
  };
  const Validation = () => {
    if (firstName == '') {
      // dispatch(showToast({text: 'Please enter first name', duration: 1000}));
      Toast.show('Please enter first name', Toast.SHORT)
    } else if (lastName == '') {
      // dispatch(showToast({text: 'Please enter last name', duration: 1000}));
      Toast.show('Please enter last name', Toast.SHORT)
    } else if (emailAddress == '') {
      // dispatch(showToast({text: 'Please enter email address', duration: 1000}));
      Toast.show('Please enter email address', Toast.SHORT)
    } else if (phoneNumber == '') {
      // dispatch(showToast({text: 'Please enter phone number', duration: 1000}));
      Toast.show('Please enter phone number', Toast.SHORT)
    }
    // else if (filePath == '') {
    //   dispatch(
    //     showToast({text: 'Please upload profile picture', duration: 1000}),
    //   );
    // }
    else if (
      moment(date).format('YYYY-MM-DD') >=
      moment(new Date()).format('YYYY-MM-DD')
    ) {
      // dispatch(
      //   showToast({text: 'Please select valid date of birth', duration: 1000}),
      // );
      Toast.show('Please select valid date of birth', Toast.SHORT)
    } else if (address == '') {
      // dispatch(showToast({text: 'Please add location', duration: 1000}));
      Toast.show('Please add location', Toast.SHORT)
    } else if (password == '') {
      // dispatch(showToast({text: 'Please enter password', duration: 1000}));
      Toast.show('Please enter password', Toast.SHORT)
    } else if (confirmPassword == '') {
      // dispatch(
      //   showToast({text: 'Please enter confirm password', duration: 1000}),
      // );
      Toast.show('Please enter confirm password', Toast.SHORT)
    } else if (password != confirmPassword) {
      // dispatch(
      //   showToast({
      //     text: 'Password and confirm password not match',
      //     duration: 1000,
      //   }),
      // );
      Toast.show('Password and confirm password not match', Toast.SHORT)
    } else if (isAcceptedTC == false) {
      // dispatch(showToast({text: 'Please accept Terms of Use', duration: 1000}));
      Toast.show('Please accept Terms of Use', Toast.SHORT)
    } else return true;
  };
  //function : service function
  const signUpUser = async () => {
    if (Validation()) {
      setShowLoader(true);
      const signUpData = new FormData();
      signUpData.append('first_name', firstName);
      signUpData.append('last_name', lastName);
      signUpData.append('email', emailAddress);
      signUpData.append('phone', phoneNumber);
      signUpData.append('country_code', selectedCountry.dial_code);
      signUpData.append('country_flag', selectedCountry.flag);
      if (filePath != '') {
        const imageName = filePath.uri.slice(
          filePath.uri.lastIndexOf('/'),
          filePath.uri.length,
        );
        signUpData.append('profile_image', {
          name: imageName,
          type: filePath.type,
          uri: filePath.uri,
        });
      }
      signUpData.append('gender', gender);
      signUpData.append('dob', moment(date).format('YYYY-MM-DD'));
      signUpData.append('latitude', latLng.lat);
      signUpData.append('longtitude', latLng.lng);
      if (filePath != '') {
        const imageName = filePath.uri.slice(
          filePath.uri.lastIndexOf('/'),
          filePath.uri.length,
        );
        signUpData.append('profile_image', {
          name: imageName,
          type: filePath.type,
          uri: filePath.uri,
        });
      }
      signUpData.append('address', address);
      signUpData.append('password', password);
      signUpData.append('password_confirmation', confirmPassword);
      signUpData.append('fcmtoken', fcmToken);
      console.log('user signUpData', signUpData);
      try {
        const resp = await Service.postApi(Service.USER_REGISTER, signUpData);
        if (resp?.data?.status) {
          // Alert.alert('', `${resp.data.message}`);
          Toast.show(resp.data.message, Toast.SHORT);
          await AsyncStorage.setItem('userToken', resp.data.access_token);
          const jsonValue = JSON.stringify(resp.data.data);
          console.log('user signup jsonValue', jsonValue);
          await AsyncStorage.setItem('userInfo', jsonValue);
          dispatch(setUserToken(resp.data.access_token));
          dispatch(setUser(resp.data.data));
          navigation.dispatch(resetIndexGoToBottomTab);
        } else {
          // dispatch(showToast({text: resp.data.message}));
          Toast.show(resp.data.message, Toast.SHORT);
        }
      } catch (error) {
        console.log('error in signUpUser', error.response.data);
      }
      setShowLoader(false);
    }
  };
  const requestLocationPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Access Required',
            message: 'This App needs to Access your location',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          getCurrentLocation();
        } else {
          // dispatch(showToast({text: 'Location permission denied'}));
          Toast.show('Location permission denied', Toast.SHORT)
          // Alert.alert('', 'Location permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };
  const getCurrentLocation = () => {
    setShowLoader(true);
    try {
      Geolocation.getCurrentPosition(
        position => {
          Geocoder.from(position.coords.latitude, position.coords.longitude)
            .then(json => {
              if (json.status == 'OK') {
                // setAllCurrentLocations(json.results);
                var addressComponent = json.results[0].formatted_address;
                setAddress(json.results[0].formatted_address);
                setLatLng(json.results[0].geometry.location);
                setShowLocationModal(true);
                console.log(addressComponent);
                setShowLoader(false);
                // setShowSelectAddress(true);
              }
            })
            .catch(error => console.warn(error));
        },
        error => {
          setShowLoader(false);
          // See error code charts below.
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (error) {
      setShowLoader(false);
      console.log('error in updateLocation', error);
    }
  };
  //function : imp function
  const openCamera = () => {
    const options = {
      width: 1080,
      height: 1080,
      cropping: true,
      mediaType: 'photo',
      compressImageQuality: 1,
      compressImageMaxHeight: 1080 / 2,
      compressImageMaxWidth: 1080 / 2,
    };
    launchCamera(options, response => {
      if (response.didCancel) {
        // Alert.alert('User cancelled camera picker');
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'User cancelled picking image'}));
        Toast.show('User cancelled picking image', Toast.SHORT)
        // Alert.alert('User cancelled picking image');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Camera not available on device'}));
        Toast.show('Camera not available on device', Toast.SHORT)
        // Alert.alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Permission not satisfied'}));
        Toast.show('Permission not satisfied', Toast.SHORT)
        // Alert.alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: response.errorMessage}));
        Toast.show('response.errorMessage', Toast.SHORT)
        // Alert.alert(response.errorMessage);
        return;
      }
      console.log('Response = ', response.assets[0]);
      setFilePath(response.assets[0]);
      setShowImageSourceModal(false);
    });
  };
  //function : imp function
  const checkCameraPermission = async () => {
    if (Platform.OS === 'ios') {
      openCamera();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to access camera',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          openCamera();
          console.log('Storage Permission Granted.');
        } else {
          // dispatch(showToast({text: 'Storage Permission Not Granted'}));
          Toast.show('Storage Permission Not Granted', Toast.SHORT)
          // Alert.alert('Error', 'Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('ERROR' + err);
      }
    }
  };
  const checkToken = async () => {
    try {
      const token = await messaging().getToken();
      if (token) {
        setFcmToken(token);
      } else {
        console.log('could not get fcm token');
      }
    } catch (error) {
      console.log('error in getting fcm token', error);
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    checkToken();
  }, []);
  const gotoWebPage = async (endPoint, name) => {
    navigation.navigate(ScreenNames.SIDE_MENU_LINKS, {
      endPoint,
      name,
      role: 'User',
    });
  };
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Sign Up" />
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={{paddingBottom: 50}}
          style={styles.mainView}>
          <MyText
            text="Let's get started"
            fontFamily="bold"
            fontSize={32}
            textColor="theme_green"
          />
          <MyText
            text="Enter your details to create account"
            fontSize={18}
            marginBottom={20}
          />
          {filePath == '' ? (
            // <TouchableOpacity onPress={chooseFile} style={styles.uploadProfile}>
            //   <MyText
            //     text={
            //       filePath == '' ? 'Upload Profile Picture' : filePath.fileName
            //     }
            //     style={{
            //       width: '85%',
            //     }}
            //   />
            //   <MyIcon.AntDesign
            //     name="upload"
            //     size={20}
            //     color={Colors.THEME_GREEN}
            //   />
            // </TouchableOpacity>
            <View style={styles.imageViewStyle}>
              <Image
                resizeMode="contain"
                borderRadius={100}
                source={require('assets/images/profile_pic.png')}
                style={{
                  height: '100%',
                  width: '100%',
                }}
              />
              <TouchableOpacity
                onPress={() => {
                  setShowImageSourceModal(true);
                }}
                style={styles.addButtonStyle}>
                <MyIcon.AntDesign
                  name="plus"
                  color={Colors.THEME_GREEN}
                  size={24}
                />
              </TouchableOpacity>
            </View>
          ) : (
            <View style={styles.imageViewStyle}>
              <Image
                resizeMode="cover"
                borderRadius={100}
                source={{uri: filePath.uri}}
                style={{height: '100%', width: '100%'}}
              />
              <TouchableOpacity
                onPress={() => setFilePath('')}
                style={styles.deleteButtonStyle}>
                <MyIcon.MaterialIcons
                  name="delete"
                  color={Colors.RED}
                  size={24}
                />
              </TouchableOpacity>
            </View>
          )}
          <MyTextInput
            inputRef={firstNameRef}
            Title="First Name"
            placeholder="Enter First Name"
            onChangeText={text => setFirstName(text)}
            onSubmitEditing={() => lastNameRef.current.focus()}
          />
          <MyTextInput
            inputRef={lastNameRef}
            Title="Last Name"
            placeholder="Enter Last Name"
            onChangeText={text => setLastName(text)}
            onSubmitEditing={() => emailAddressRef.current.focus()}
          />
          <MyTextInput
            inputRef={emailAddressRef}
            Title="Email Address"
            placeholder="Enter Email Address"
            keyboardType="email-address"
            onChangeText={text => setEmailAddress(text)}
            onSubmitEditing={() => phoneNumberRef.current.focus()}
            // Icon={
            //   <MyIcon.AntDesign
            //     name="checkcircleo"
            //     size={24}
            //     color={Colors.THEME_GREEN}
            //   />
            // }
          />
          <TextInputWithFlag
            inputRef={phoneNumberRef}
            value={phoneNumber}
            Flag={selectedCountry.flag}
            CountryCode={selectedCountry.dial_code}
            placeholder="Enter Phone Number"
            keyboardType="number-pad"
            maxLength={10}
            onPress={() => setShow(true)}
            onChangeText={text => setPhoneNumber(text)}
            onSubmitEditing={() => Keyboard.dismiss()}
          />

          <MyText text="Gender" fontFamily="bold" />
          <View style={styles.genderView}>
            {Constant.Gender.map((item, index) => {
              return (
                <TouchableOpacity
                  onPress={() => setGender(index + 1)}
                  key={item.id.toString()}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <MyIcon.Ionicons
                    name={
                      gender == index + 1
                        ? 'radio-button-on'
                        : 'radio-button-off'
                    }
                    color={Colors.THEME_GREEN}
                    size={24}
                  />
                  <MyText text={item.name} />
                </TouchableOpacity>
              );
            })}
          </View>
          <MyText text="Birthdate" fontFamily="bold" />
          <DateSelector
            Title={
              moment(date).format('YYYY-MM-DD') ==
              moment(new Date()).format('YYYY-MM-DD')
                ? 'Select Date'
                : // : moment(date).format('MMMM Do YYYY')
                  moment(date).format('DD-MM-YYYY')
            }
            onPress={() => setOpen(true)}
            dateViewStyle={{borderWidth:0}}
          />
          {/* <TouchableOpacity
            onPress={requestLocationPermission}
            style={{
              backgroundColor: Colors.WHITE,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              shadowColor: '#000',
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.1,
              elevation: 2,
              padding: 10,
              borderRadius: 10,
          }}>
          <MyIcon.FontAwesome5 name="map-marker-alt" size={20} />
          <MyText
            text="Add Location"
            marginHorizontal={10}
            fontFamily="bold"
          />
          </TouchableOpacity>
          <MyTextInput
            inputRef={addressRef}
            value={address}
            placeholder="Enter Address"
            multiline={true}
            editable={false}
            onChangeText={text => setAddress(text)}
            onSubmitEditing={() => passwordRef.current.focus()}
          /> */}
          <GooglePlacesAutocomplete
            placeholder="Add Location"
            textInputProps={{
              placeholderTextColor: '#c9c9c9',
              // placeholderTextColor: Colors.BLACK,
              returnKeyType: 'search',
              // onFocus: () => setShowPlacesList(true),
              // onBlur: () => setShowPlacesList(false),
              multiline: true,
              // onTouchStart: ()=>{downButtonHandler()}
              height: 55,
            }}
            enablePoweredByContainer={false}
            listViewDisplayed={'auto'}
            styles={styles.searchbar}
            onPress={(data, details = null) => {
              // 'details' is provided when fetchDetails = true
              // setShowPlacesList(false)
              setLatLng({
                lat: details.geometry.location.lat,
                lng: details.geometry.location.lng,
              });
              setAddress(data?.description);
            }}
            GooglePlacesDetailsQuery={{
              fields: 'geometry',
            }}
            fetchDetails={true}
            query={{
              key: GOOGLE_MAPS_APIKEY,
              language: 'en',
            }}
          />
          <MyPasswordInput
            inputRef={passwordRef}
            Title="Password"
            placeholder="Enter Password"
            onChangeText={text => setPassword(text)}
            onSubmitEditing={() => confirmPasswordRef.current.focus()}
            secureTextEntry={true}
            showPassword
            ShowIcon={
              <MyIcon.FontAwesome5
                name="eye"
                size={20}
                style={{
                  transform: [
                    {
                      scaleX: -1,
                    },
                  ],
                }}
                color={Colors.LITE_GREY}
              />
            }
            HideIcon={
              <MyIcon.FontAwesome5
                name="eye-slash"
                size={20}
                style={{
                  transform: [
                    {
                      scaleX: -1,
                    },
                  ],
                }}
                color={Colors.LITE_GREY}
              />
            }
          />
          <MyPasswordInput
            inputRef={confirmPasswordRef}
            Title="Confirm Password"
            placeholder="Enter Confirm Password"
            onChangeText={text => setConfirmPassword(text)}
            onSubmitEditing={() => Keyboard.dismiss()}
            textInputWidth="85%"
            secureTextEntry={true}
            showPassword
            ShowIcon={
              <MyIcon.FontAwesome5
                name="eye"
                size={20}
                style={{
                  transform: [
                    {
                      scaleX: -1,
                    },
                  ],
                }}
                color={Colors.LITE_GREY}
              />
            }
            HideIcon={
              <MyIcon.FontAwesome5
                name="eye-slash"
                size={20}
                style={{
                  transform: [
                    {
                      scaleX: -1,
                    },
                  ],
                }}
                color={Colors.LITE_GREY}
              />
            }
          />
          <TermOfUseCheck
            value={isAcceptedTC}
            setValue={setIsAcceptedTC}
            onPress={() =>
              gotoWebPage('terms-condition', 'Terms And Conditions')
            }
          />
          <View style={{height: 20}} />
          <MyButton Title="Sign Up" onPress={signUpUser} />
          <TouchableOpacity onPress={gotoSignIn} style={styles.bottomLineStyle}>
            <MyText text="Already have an account? " fontFamily="bold" />
            <MyText text="Sign In" fontFamily="bold" textColor="theme_green" />
          </TouchableOpacity>
          <View style={styles.termsAndPPView}>
            <MyText text="By signing up, you agree to our" />
            <TouchableOpacity
              onPress={() =>
                gotoWebPage('terms-condition', 'Terms And Conditions')
              }>
              <MyText
                text=" Terms and Conditions "
                textColor="theme_green"
                isUnderLine
              />
            </TouchableOpacity>
            <MyText text="and " />
            <TouchableOpacity
              onPress={() => gotoWebPage('privacy-policy', 'Privacy Policy')}>
              <MyText
                text="Privacy Policy"
                textColor="theme_green"
                isUnderLine
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <CustomLoader showLoader={showLoader} />
      <DatePicker
        modal
        mode="date"
        open={open}
        date={date}
        onConfirm={date => {
          if (
            moment(date).format('YYYY-MM-DD') >=
            moment(new Date()).format('YYYY-MM-DD')
          ) {
            setOpen(false);
            // dispatch(
            //   showToast({
            //     text: 'Please select valid date of birth',
            //     duration: 1000,
            //   }),
            // );
            Toast.show('Please select valid date of birth', Toast.SHORT)
          } else if (
            // moment(new Date()).format('YYYY') - moment(date).format('YYYY') <
            moment().diff(date, 'years', true) < 18
          ) {
            setOpen(false);
            // dispatch(
            //   showToast({
            //     text: 'Your age must be at least 18 years',
            //     duration: 1000,
            //   }),
            // );
            Toast.show('Your age must be at least 18 years', Toast.SHORT)
          } else {
            setOpen(false);
            setDate(date);
          }
        }}
        onCancel={() => {
          setOpen(false);
        }}
      />
      <CountryPicker
        show={show}
        disableBackdrop={false}
        style={styles.countrySilderStyle}
        // when picker button press you will get the country object with dial code
        pickerButtonOnPress={item => {
          // console.warn('item', item);
          // setCountryCode(item.dial_code);
          setSelectedCountry(item);
          setShow(false);
        }}
        placeholderTextColor={'#c9c9c9'}
        color={Colors.BLACK}
        onBackdropPress={() => setShow(false)}
      />
      <SelectAddress
        visible={showSelectAddress}
        setVisibility={setShowSelectAddress}
        Addresses={allCurrentLocations}
        setValue={setAddress}
        setLatLng={setLatLng}
      />
      <SelectImageSource
        visible={showImageSourceModal}
        setVisibility={setShowImageSourceModal}
        openLibrary={openLibrary}
        openCamera={checkCameraPermission}
      />
      <SearchLocation
        visible={showLocationModal}
        setVisibility={setShowLocationModal}
        setAddress={setAddress}
        setLatLng={setLatLng}
      />
    </View>
  );
};

export default SignUp;
