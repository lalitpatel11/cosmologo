import {Colors, Fonts} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  birthDateView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  bottomLineStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
    marginTop: 40,
  },
  uploadProfile: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.WHITE,
    padding: 10,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: Colors.THEME_GREEN,
    borderStyle: 'dashed',
    marginVertical: 10,
  },
  imageViewStyle: {
    height: 150,
    width: 150,
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    // borderWidth: 2,
    // borderColor: Colors.THEME_GREEN,
    marginVertical: 10,
    borderRadius: 150 / 2,
  },
  deleteButtonStyle: {
    position: 'absolute',
    backgroundColor: Colors.WHITE,
    borderRadius: 100,
    padding: 5,
    right: 5,
    top: 5,
  },
  addButtonStyle: {
    position: 'absolute',
    backgroundColor: Colors.WHITE,
    borderRadius: 100,
    padding: 5,
    right: 5,
    bottom: 5,
  },
  genderView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: 10,
  },
  searchbar: {
    description: {
      fontWeight: 'bold',
    },
    predefinedPlacesDescription: {
      color: '#1faadb',
    },
    textInputContainer: {
      backgroundColor: 'rgba(0,0,0,0)',
      // top: 50,
      // width: width - 10,
      borderWidth: 0,
      marginTop:5,
    },
    textInput: {
      marginLeft: 0,
      marginRight: 0,
      height: 100,
      color: Colors.BLACK,
      fontSize: 14,
      borderWidth: 0,
      elevation: 2,
      fontFamily: Fonts.BOLD,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
    },
    listView: {
      // backgroundColor: 'rgba(192,192,192,0.9)',
      // top: 23,
    },
  },
  termsAndPPView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    width: '80%',
  },
});
