//react components
import React, {useState, useEffect} from 'react';
import {View, Image, ScrollView, TouchableOpacity, Alert} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyText from 'components/MyText/MyText';
import MyHeader from 'components/MyHeader/MyHeader';
import DoctorCard from 'components/DoctorCard/DoctorCard';
import MyButton from 'components/MyButton/MyButton';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//global
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ServiceProvidersStyle';
//modal
import ConfirmAppointment from 'modals/ConfirmAppointment/ConfirmAppointment';
import SelectedService from 'components/ServiceCard/SelectedService';
//redux
import {useDispatch, useSelector} from 'react-redux';
import {addToCart} from 'src/reduxToolkit/reducer/cart';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import IconButton from 'components/IconButton/IconButton';
//third parties
import {useNetInfo} from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';

const ServiceProviders = ({route, navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const AOCData = route.params.AOCData;
  const ApptDate = route.params.ApptDate;
  const ApptTime = route.params.ApptTime;

  //variables : redux variables
  const dispatch = useDispatch();
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [SPData, setSPData] = useState({});
  const [serviceProvidersData, setServiceProvidersData] = useState([]);
  const [selectedProvider, setSelectedProvider] = useState('');
  //states : modal states
  const [showConfirmAppnt, setShowConfirmAppnt] = useState(false);
  const [modalData, setModalData] = useState({});
  //function : navigation function
  const gotoHome = () => navigation.navigate(ScreenNames.USER_BOTTOM_TAB);
  const gotoPayment = () =>
    navigation.navigate(ScreenNames.USER_PAYMENT, {
      ApptDate,
      AOCData,
      ApptTime,
    });
  // const gotoCart = () => navigation.navigate(ScreenNames.CART);
  const gotoCart = () =>
    navigation.navigate(ScreenNames.CART, {hideAddMore: true});
  const gotoSelectDate = () =>
    navigation.navigate(ScreenNames.USER_SELECT_DATE);
  //function : service function
  const getServiceProviders = async () => {
    setShowLoader(true);
    try {
      const servicePostData = new FormData();
      servicePostData.append('area_of_concern_id', AOCData.concern_area_id);
      servicePostData.append('date', ApptDate);
      servicePostData.append('time', ApptTime);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SERVICE_PROVIDER_LIST,
        servicePostData,
      );
      if (resp.data.status) {
        // console.warn('resp====>', resp.data);
        if (resp.data.data.length > 0) {
          setSPData(resp.data.data);
          setServiceProvidersData(resp.data.data);
        } else {
          // dispatch(
          //   showToast({
          //     text: 'No service provider available at the moment!',
          //     duration: 2000,
          //   }),
          // );
          Toast.show('No service provider available at the moment!', Toast.SHORT)
        }
      }
    } catch (error) {
      console.log('error in getServiceProviders', error);
    }
    setShowLoader(false);
  };
  const confirmAppointment = async () => {
    if (Object.keys(selectedProvider).length > 0) {
      setShowLoader(true);
      // try {
      //   const confirmAptData = new FormData();
      //   confirmAptData.append('booking_id', bookingId);
      //   confirmAptData.append('service_provider_id', selectedProvider.id);
      //   const resp = await Service.postApiWithToken(
      //     userToken,
      //     Service.CONFIRM_APPOINTMENT,
      //     confirmAptData,
      //   );
      //   if (resp.data.status) {
      //     setModalData(resp.data.data);
      //     setShowConfirmAppnt(true);
      //   }
      // } catch (error) {
      //   console.log('error in confirmAppointment', error);
      // }
      setShowLoader(false);
    } else {
      // dispatch(showToast({text: 'Please Select Service Provider'}));
      Toast.show('Please Select Service Provider', Toast.SHORT)
      // Alert.alert('', 'Please Select Service Provider');
    }
  };
  const addItemToCart = () => {
    if (Object.keys(selectedProvider).length > 0) {
      const addToCartData = {
        data: AOCData,
        appointment_date: ApptDate,
        appointment_time: ApptTime,
        service_provider: selectedProvider,
      };
      dispatch(addToCart(addToCartData));
      // dispatch(showToast({text: 'Item added successfully'}));
      Toast.show('Item added successfully', Toast.SHORT)
      gotoCart();
    } else {
      // dispatch(
      //   showToast({text: 'Please Select Service Provider', duration: 500}),
      // );
      Toast.show('Please Select Service Provider', Toast.SHORT)
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getServiceProviders();
    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Service Providers" />
      {serviceProvidersData.length > 0 ? (
        <ScrollView
          contentContainerStyle={{paddingBottom: 30}}
          style={styles.mainView}>
          {/* <SelectedService
            BookingId={'#555'}
            ImageUrl={SPData?.appointment_detail?.concern_area_image}
            ConcernArea={SPData?.appointment_detail?.concern_area_title}
            Product={SPData?.appointment_detail?.product}
            SubCategory={SPData?.appointment_detail?.subcategory}
            Amount={parseFloat(SPData?.appointment_detail?.amount).toFixed(2)}
          /> */}
          {/* <MyText
            text={`${SPData?.service_provider_list?.length} Service provider available  in context to the service being booking`}
            fontSize={16}
            fontFamily="medium"
            marginVertical={10}
          /> */}
          {serviceProvidersData?.length > 0
            ? serviceProvidersData?.map((item, index) => {
                return (
                  <TouchableOpacity
                    key={index.toString()}
                    onPress={() => setSelectedProvider(item)}
                    style={{
                      height: 100,
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      shadowColor: '#000',
                      shadowOffset: {width: 0, height: 2},
                      shadowOpacity: 0.1,
                      backgroundColor: Colors.WHITE,
                      shadowRadius: 15,
                      elevation: 2,
                      borderRadius: 10,
                      borderWidth: selectedProvider.id == item.id ? 2 : 0,
                      borderColor: Colors.THEME_GREEN,
                      padding: 10,
                      marginVertical: 5,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        height: '100%',
                      }}>
                      <Image
                        resizeMode="contain"
                        borderRadius={100}
                        source={
                          item.profile_image
                            ? {uri: item.profile_image}
                            : require('assets/images/profile_pic.png')
                        }
                        style={{
                          height: '50%',
                          width: '20%',
                        }}
                      />
                      <View style={{marginLeft: 10}}>
                        <MyText
                          text={`${item.first_name} ${item.last_name}`}
                          fontFamily="medium"
                        />
                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          <MyIcon.Entypo
                            name="mail"
                            color={Colors.THEME_GREEN}
                          />
                          <MyText text={item.email} marginLeft={5} />
                        </View>

                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          <MyIcon.MaterialCommunityIcons
                            name="phone-in-talk"
                            color={Colors.THEME_GREEN}
                          />
                          <MyText text={item.phone} marginLeft={5} />
                        </View>
                      </View>
                    </View>

                    <MyIcon.MaterialCommunityIcons
                      name="arrow-right-circle"
                      color={Colors.THEME_GREEN}
                      size={30}
                    />
                  </TouchableOpacity>
                );
              })
            : null}
        </ScrollView>
      ) : null}
      {serviceProvidersData?.length > 0 ? (
        <View
          style={{
            padding: 20,
          }}>
          <IconButton
            onPress={addItemToCart}
            VectorIcon={
              <MyIcon.AntDesign
                name="shoppingcart"
                size={18}
                color={Colors.THEME_GREEN}
              />
            }
            Title="Add To Cart"
          />
        </View>
      ) : null}
      <CustomLoader showLoader={showLoader} />
      <ConfirmAppointment
        visible={showConfirmAppnt}
        setVisibility={setShowConfirmAppnt}
        Data={modalData}
      />
    </View>
  );
};

export default ServiceProviders;
