//import : react components
import React, {useEffect, useState} from 'react';
import {Alert, FlatList, Image, TouchableOpacity, View} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//import : custom components
import MyText from 'components/MyText/MyText';
import MyHeader from 'components/MyHeader/MyHeader';
import MyButton from 'components/MyButton/MyButton';
import DoctorCard from 'components/DoctorCard/DoctorCard';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//import : globals
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//import : styles
import {styles} from './CartStyle';
//import : redux
import {useDispatch, useSelector} from 'react-redux';
import {removeFromCart} from 'src/reduxToolkit/reducer/cart';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
//third parties
import Toast from 'react-native-simple-toast';
import {useNetInfo} from '@react-native-community/netinfo';

const Cart = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const dispatch = useDispatch();
  const userInfo = useSelector(state => state.user.userInfo);
  const cartItems = useSelector(state => state.cart.cartItems);
  const userToken = useSelector(state => state.user.userToken);
  console.warn(cartItems);
  //hook : states
  const [showLoader, setShowLoader] = useState(false);
  const [mainViewFlex, setMainViewFlex] = useState(
    Object.keys(cartItems).length > 0 ? 0.7 : 0.8,
  );
  const [hide, setHide] = useState(
    route.params &&
      typeof route.params.hideAddMore === 'boolean' &&
      route.params.hideAddMore === true
      ? true
      : false,
  );
  const [withoutServiceProvider, setWithoutServiceProvider] = useState(true);
  //function : navigation function
  const gotoBottomTab = () => navigation.navigate(ScreenNames.USER_BOTTOM_TAB);
  const gotoPayment = (appointmentId, bookingId) => {
    navigation.navigate(ScreenNames.USER_PAYMENT, {appointmentId, bookingId});
  };
  // function: other
  const addMoreServices = () => {
    if (Object.keys(cartItems).length > 0) {
      Alert.alert(
        '',
        `Only one service can be availed at a time. On continuing, previous service will be delete from cart.`,
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => gotoBottomTab()},
        ],
      );
    } else {
      gotoBottomTab();
    }
  };
  //function : cart function
  const removeItemFromCart = id => {
    // dispatch(showToast({text: 'this service has been deleted successfully'}));
    Toast.show('this service has been deleted successfully', Toast.SHORT);
    dispatch(removeFromCart());
    setMainViewFlex(0.8);
  };
  const ProceedTOCheckOut = () =>
    navigation.navigate(ScreenNames.USER_SELECT_DATE);
  //function : service function
  const showDisclaimer = () => {
    Alert.alert(
      'Disclaimer',
      'Please consult your doctor to address your concerns before using the Cosmologo App and before making any medical decisions.',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Proceed', onPress: () => checkAppointmentAvailable()},
      ],
    );
  };
  const checkAppointmentAvailable = async () => {
    if (Object.keys(cartItems)?.length === 0) {
      Toast.show('Your cart is empty', Toast.SHORT);
      return;
    }
    setShowLoader(true);
    try {
      const formData = new FormData();
      formData.append('user_id', userInfo.id);
      formData.append(
        'product_concern_area_id',
        cartItems.data.concern_area_id,
      );
      formData.append('product_id', cartItems?.data?.product_id);
      formData.append('appointment_date', cartItems.appointment_date);
      formData.append('appointment_time', cartItems.appointment_time);
      formData.append('amount', parseFloat(cartItems.data.price).toFixed(2));
      // formData.append('service_provider_id', cartItems.service_provider.id);
      console.log('checkAppointmentAvailable', formData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.CHECK_BOOK_APPOINTMENT,
        formData,
      );
      console.log('checkAppointmentAvailable resp', resp);
      if (resp.data.status) {
        gotoPayment(
          resp.data?.appointment?.id,
          resp.data?.appointment?.booking_id,
        );
        // dispatch(showToast({text: resp.data.message}));
        Toast.show(resp.data.message, Toast.SHORT);
        console.warn('resp', resp.data);
      } else {
        // dispatch(showToast({text: resp.data.message}));
        Toast.show(resp.data.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in checkAppointmentAvailable', error);
    }
    setShowLoader(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    }
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  // useEffect(() => {
  //   let total = 0;
  //   cartItems.map((e, index) => (total += e.data.price));
  //   setTotal(total);
  //   return () => {};
  // }, [cartItems]);
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Cart" IsCartIcon={false} />
      <View style={[styles.mainView, {flex: mainViewFlex}]}>
        {Object.keys(cartItems).length > 0 ? (
          <View
            style={{
              padding: 10,
              shadowColor: '#000',
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.1,
              backgroundColor: Colors.WHITE,
              shadowRadius: 15,
              elevation: 2,
              borderRadius: 10,
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginVertical: 5,
              }}>
              <Image
                source={{uri: cartItems.data.concern_area_image}}
                style={{height: '100%', width: '20%'}}
              />
              <View
                style={{
                  width: '70%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <MyText
                  text={cartItems.data.concern_area_title}
                  fontFamily="bold"
                />
                <MyText
                  text={`$ ${parseFloat(
                    cartItems.data.price + cartItems?.data?.service_charges,
                  ).toFixed(2)}`}
                />
              </View>
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  right: 10,
                  top: 10,
                }}
                onPress={() => removeItemFromCart()}>
                <MyIcon.MaterialIcons
                  name="delete"
                  color={Colors.RED}
                  size={24}
                />
              </TouchableOpacity>
            </View>

            {/* <DoctorCard
              Name={`${cartItems.service_provider.first_name} ${cartItems.service_provider.last_name}`}
              ProfileImageUrl={cartItems.service_provider.profile_image}
            /> */}
          </View>
        ) : (
          <MyText
            text="Your cart is empty!!!"
            textAlign="center"
            fontSize={16}
            fontFamily="medium"
            marginVertical={10}
          />
        )}
      </View>
      <View
        style={{
          flex: 1 - mainViewFlex,
          padding: 20,
          borderTopLeftRadius: 50,
          borderTopRightRadius: 50,
          backgroundColor: Colors.WHITE,
        }}>
        <MyText text="Cart Summary " fontFamily="bold" />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 10,
          }}>
          <MyText text="Total Payable Amount : " fontFamily="medium" />
          <MyText
            text={
              Object.keys(cartItems).length > 0
                ? `$${parseFloat(
                    cartItems.data.price + cartItems?.data?.service_charges,
                  ).toFixed(2)}`
                : `$0.00`
            }
          />
        </View>
        {Object.keys(cartItems).length > 0 ? (
          <MyButton Title="Proceed to pay" onPress={showDisclaimer} />
        ) : null}
        {hide ? (
          <MyButton
            Title="Go to Home"
            backgroundColor={Colors.THEME_BLUE}
            onPress={gotoBottomTab}
          />
        ) : (
          <MyButton
            Title="Add more services"
            backgroundColor={Colors.THEME_BLUE}
            onPress={addMoreServices}
          />
        )}
      </View>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default Cart;
