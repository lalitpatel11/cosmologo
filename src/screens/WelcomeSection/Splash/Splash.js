//react components
import React, {useEffect} from 'react';
import {View, Image} from 'react-native';
//third parties
import AsyncStorage from '@react-native-async-storage/async-storage';
//global
import {Images, ScreenNames} from 'global/Index';
//styles
import {styles} from './SplashStyle';
import {CommonActions} from '@react-navigation/native';
//redux
import {useDispatch} from 'react-redux';
import {setUser, setUserToken, setTotalPaymentAmount, setUserNotifications, setServiceProviderNotifications} from 'src/reduxToolkit/reducer/user';
import {setToCart} from 'src/reduxToolkit/reducer/cart';

const Splash = ({navigation}) => {
  //variables : redux variables
  const dispatch = useDispatch();
  //function : navigation function
  const resetIndexGoToUserBottomTab = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.USER_BOTTOM_TAB}],
  });
  const resetIndexGoToServiceBottomTab = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.SERVICE_BOTTOM_TAB}],
  });
  const resetIndexGoToWelcome = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.WELCOME}],
  });
  const gotoWelcome = () => navigation.replace(ScreenNames.WELCOME);
  //useEffect
  useEffect(() => {
    setTimeout(async () => {
      const userInfo = await AsyncStorage.getItem('userInfo');
      const userToken = await AsyncStorage.getItem('userToken');
      const cartItems = await AsyncStorage.getItem('cartItems');
      const totalPaymentAmount = await AsyncStorage.getItem('totalPaymentAmount');
      if(totalPaymentAmount){
        const amount = JSON.parse(totalPaymentAmount)
        dispatch(setTotalPaymentAmount(amount));
      }
      if(userInfo && typeof userInfo.user_type === 'number'){
        if(userInfo.user_type == '3'){
          const notifications = await AsyncStorage.getItem('serviceProviderNotifications');
          console.log('SP notifications', notifications);
          if(notifications){
            dispatch(setServiceProviderNotifications(JSON.parse(notifications)));
          }
        }else {
          const notifications = await AsyncStorage.getItem('userNotifications');
          console.log('User notifications', notifications);
          if(notifications){
            dispatch(setUserNotifications(JSON.parse(notifications)));
          }
        }
      }
      const userData = JSON.parse(userInfo);
      const cartData = JSON.parse(cartItems);
      console.warn(userData);
      if (userData) {
        dispatch(setUserToken(userToken));
        dispatch(setUser(userData));
        if (cartData != null) {
          dispatch(setToCart(cartData));
        } else {
          dispatch(setToCart({}));
        }
        // dispatch(setToCart(cartData));
        if (userData.user_type == 2) {
          navigation.dispatch(resetIndexGoToUserBottomTab);
        } else {
          navigation.dispatch(resetIndexGoToServiceBottomTab);
        }
      } else {
        navigation.dispatch(resetIndexGoToWelcome);
      }
    }, 2000);
    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <View style={styles.mainView}>
        <Image
          resizeMode="contain"
          source={Images.AppLogo}
          style={{width: '70%', height: 200}}
        />
      </View>
    </View>
  );
};

export default Splash;
