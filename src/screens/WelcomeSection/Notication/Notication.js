//react components
import React, {useState, useEffect} from 'react';
import {View, FlatList, TouchableOpacity, ScrollView} from 'react-native';
import {useNavigation, CommonActions} from '@react-navigation/native';
//custom components
import MyText from 'components/MyText/MyText';
import MyHeader from 'components/MyHeader/MyHeader';
import CustomLoader from 'components/CustomLoader/CustomLoader';
// third parties
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from "@react-native-community/netinfo";
//global
import {Colors, Service, ScreenNames} from 'global/Index';
//styles
import {styles} from './NoticationStyle';
//redux
import {useSelector, useDispatch} from 'react-redux';
import {
  setServiceProviderNotifications,
  setUserNotifications,
} from 'src/reduxToolkit/reducer/user';

const Notication = () => {
  const {isInternetReachable} = useNetInfo();
  //variables
  // const NotificationData = [
  //   {
  //     id: 1,
  //     title: 'Appointment Booked for BLEPHAROPLASTY',
  //     status: 'Processing',
  //     check: 'unread',
  //     time: '21 min ago',
  //   },
  //   {
  //     id: 2,
  //     title: 'Appointment Booked for BLEPHAROPLASTY',
  //     status: 'Processing',
  //     check: 'read',
  //     time: '21 min ago',
  //   },
  //   {
  //     id: 3,
  //     title: 'Appointment Booked for BLEPHAROPLASTY',
  //     status: 'Processing',
  //     check: 'unread',
  //     time: '21 min ago',
  //   },
  //   {
  //     id: 4,
  //     title: 'Appointment Booked for BLEPHAROPLASTY',
  //     status: 'Processing',
  //     check: 'read',
  //     time: '21 min ago',
  //   },
  //   {
  //     id: 5,
  //     title: 'Appointment Booked for BLEPHAROPLASTY',
  //     status: 'Out for the Service',
  //     check: 'unread',
  //     time: '21 min ago',
  //   },
  // ];
  // variables
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  // states
  const [showLoader, setShowLoader] = useState(false);
  const [NotificationData, setNotificationData] = useState([]);
  //function : service function
  const getNotifications = async () => {
    setShowLoader(true);
    const endPoint =
      userInfo.user_type == '3'
        ? Service.SERVICE_PROVIDER_NOTIFICATION
        : Service.USER_NOTIFICATION;
    try {
      const resp = await Service.postApiWithToken(userToken, endPoint, {});
      if (resp.data.status) {
        console.warn('resp---->', resp.data);
        console.log('setNotificationData', resp.data.data);
        setNotificationData(resp.data.data);
      } else {
        Toast.show(resp.data.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in getNotifications', error);
    }
    setShowLoader(false);
  };
  const readNotifications = async () => {
    setShowLoader(true);
    const endPoint =
      userInfo.user_type == '3'
        ? Service.SERVICE_PROVIDER_READ_NOTIFICATION
        : Service.USER_READ_NOTIFICATION;
    console.log('endPoint', endPoint);
    try {
      const resp = await Service.postApiWithToken(userToken, endPoint, {});
      if (resp.data.status) {
        Toast.show(resp.data.message, Toast.SHORT);
        console.warn('resp---->', resp.data);
        setNotificationData([]);
        userInfo.user_type == '3'
          ? dispatch(setServiceProviderNotifications([]))
          : dispatch(setUserNotifications([]));
      } else {
        Toast.show(resp.data.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in getNotifications', error);
    }
    setShowLoader(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getNotifications();
    });
    return unsubscribe;
  }, [navigation]);
  //function : render function
  const notiListRender = ({item, index}) => {
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          borderBottomWidth: 0.5,
          paddingVertical: 10,
        }}>
        <View
          style={{
            marginTop: 5,
            height: 7,
            width: 7,
            marginRight: 10,
            borderRadius: 100,
            backgroundColor:
              item.status?.toLowerCase() == 'Cancelled'.toLowerCase() ||
              item.status?.toLowerCase() == 'Rejected'.toLowerCase()
                ? Colors.RED
                : Colors.THEME_GREEN,
            // : null || item.read_status?.toLowerCase() == 'unread'
            // ? Colors.THEME_GREEN
            // : 'transparent',
          }}
        />
        <View
          style={{
            width: '95%',
          }}>
          <MyText
            text={item.message}
            textColor={'lite_grey'}
            // textColor={
            //   (item.status?.toLowerCase() == 'Cancelled'.toLowerCase() || item.status?.toLowerCase() == 'Rejected'.toLowerCase())
            //     ? 'red'
            //     : Colors.THEME_GREEN
            //     // : null || item.read_status?.toLowerCase() == 'unread'
            //     // ? 'black'
            //     // : 'lite_grey'
            // }
            marginBottom={5}
          />
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <MyText
              text="Status: "
              fontSize={12}
              textColor={'lite_grey'}
              // textColor={(item.status?.toLowerCase() == 'Cancelled'.toLowerCase() || item.status?.toLowerCase() == 'Rejected'.toLowerCase()) ? 'red' : 'black'}
              fontFamily={
                item.read_status?.toLowerCase() == 'unread' ? 'medium' : 'light'
              }
            />
            <MyText
              text={item.status}
              fontSize={12}
              textColor={
                item.status?.toLowerCase() == 'Cancelled'.toLowerCase() ||
                item.status?.toLowerCase() == 'Rejected'.toLowerCase()
                  ? 'red'
                  : 'theme_green'
              }
              fontFamily={
                item.read_status?.toLowerCase() == 'unread' ? 'medium' : 'light'
              }
            />
          </View>
        </View>
        <View
          style={{
            justifyContent: 'flex-end',
            // right:130,
            marginLeft: 'auto',
          }}>
          <MyText
            // text={item.time}
            // text={moment.duration(moment().diff(item.created_at)).asHours()}
            text={moment(item.created_at).format('ddd, D MMM YYYY, h:mm A')}
            textColor="lite_grey"
            fontSize={9}
            textAlign="right"
          />
        </View>
      </TouchableOpacity>
    );
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Notification" />
      <View style={styles.mainView}>
        {NotificationData?.length > 0 ? (
          <View style={styles.notificationCountView}>
            <MyText
              text={`${NotificationData?.length} ${
                NotificationData.length === 1
                  ? 'unread notification'
                  : 'unread notifications'
              }`}
              marginVertical={20}
              fontFamily="medium"
            />
            <TouchableOpacity onPress={readNotifications}>
              <MyText
                // text={'Mark all as read'}
                text={'Clear All'}
                fontSize={16}
                textColor={Colors.THEME_GREEN}
                fontFamily="medium"
                // textColor="white"
                textAlign="right"
                marginVertical={10}
              />
            </TouchableOpacity>
          </View>
        ) : null}
        {NotificationData?.length > 0 ? (
          <FlatList
            data={NotificationData}
            renderItem={notiListRender}
            keyExtractor={item => item.id.toString()}
            ListFooterComponent={<View />}
            ListFooterComponentStyle={{height: 200}}
          />
        ) : (
          <MyText
            text={'No Notifications found'}
            fontSize={16}
            fontFamily="medium"
            // textColor="white"
            textAlign="center"
            marginVertical={10}
          />
        )}
      </View>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default Notication;
