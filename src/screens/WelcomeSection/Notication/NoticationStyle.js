import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  notificationCountView:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
  }
});
