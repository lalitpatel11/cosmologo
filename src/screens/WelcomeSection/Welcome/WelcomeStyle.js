import {Constant} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    flex: 1,
    padding: 20,
  },
  serviceView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 50,
  },
  // termsAndPPView: {
  //   flexDirection: 'row',
  //   flexWrap: 'wrap',
  //   alignItems: 'center',
  //   alignSelf: 'center',
  //   justifyContent: 'center',
  //   width: '70%',
  // },
  bottomView: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  imageStyle: {
    width: Constant.width / 1.5,
    height: Constant.width,
    alignSelf: 'center',
  },
});
