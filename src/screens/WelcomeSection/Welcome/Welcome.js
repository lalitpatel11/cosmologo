//react components
import React, {useCallback} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  Alert,
  Linking,
} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
import WelcomeCard from 'components/WelcomeCard/WelcomeCard';
//global
import {Colors, Images, ScreenNames} from 'global/Index';
//styles
import {styles} from './WelcomeStyle';

const Welcome = ({navigation}) => {
  //function : navigation function
  const gotoUserSection = () => navigation.navigate(ScreenNames.USER_SIGNIN);
  const gotoServiceSection = () =>
    navigation.navigate(ScreenNames.SERVICE_SIGN_IN);
  //function : imp function
  const userSection = () => {
    gotoUserSection();
  };
  const serviceSection = () => {
    gotoServiceSection();
  };
  const gotoTermsOfUse = async () => {
    const url = `https://lexingtonplasticsurgeons.com/terms-and-conditions-of-use/`;
    await Linking.openURL(url);
  };
  //UI
  return (
    <View style={styles.container}>
      <Image
        style={StyleSheet.absoluteFill}
        width="100%"
        height="100%"
        source={Images.WelcomeBG}
      />
      <View style={styles.mainView}>
        <Image
          resizeMode="contain"
          source={Images.AppLogo}
          style={styles.imageStyle}
        />
        <View style={styles.bottomView}>
          <MyText
            text="Beauty at Home"
            fontFamily="black"
            fontSize={32}
            textColor="theme_green"
          />
          <View style={styles.serviceView}>
            <WelcomeCard Title="USER" onPress={userSection} />
            <WelcomeCard
              Title="SERVICE PROVIDER"
              backgroundColor={Colors.THEME_BLUE}
              Icon_bg_Color={Colors.LITE_BLUE}
              onPress={serviceSection}
            />
          </View>
          <MyText
            text="Disclaimer : Please consult your doctor to address your concerns before using the Cosmologo App and before making any medical decisions."
            textAlign="center"
            textColor="theme_green"
            marginVertical={20}
          />
          {/* <View style={styles.termsAndPPView}>
            <MyText text="By Proceeding you agree with the" />
            <TouchableOpacity onPress={gotoTermsOfUse}>
              <MyText
                text=" Terms of Use "
                textColor="theme_green"
                isUnderLine
              />
            </TouchableOpacity>
            <MyText text="and " />
            <TouchableOpacity onPress={gotoTermsOfUse}>
              <MyText
                text="Privacy Policy"
                textColor="theme_green"
                isUnderLine
              />
            </TouchableOpacity>
          </View> */}
        </View>
      </View>
    </View>
  );
};

export default Welcome;
