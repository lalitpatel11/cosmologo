//react components
import React from 'react';
import {View, Text, FlatList} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
import MyHeader from 'components/MyHeader/MyHeader';
//styles
import {styles} from './ContactUsStyle';
import ServiceCard from 'components/ServiceCard/ServiceCard';

const ContactUs = () => {
  const locationData = [1, 2, 3, 4];
  //function : render function
  const locationRender = ({item, index}) => {
    return (
      <ServiceCard
        Title="New York"
        imageUrl={
          index == 0
            ? require('assets/images/loc1.png')
            : null || index == 1
            ? require('assets/images/loc2.png')
            : null || index == 2
            ? require('assets/images/loc3.png')
            : null || index == 3
            ? require('assets/images/loc4.png')
            : null
        }
      />
    );
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Conatct Us" />
      <View style={styles.mainView}>
        <MyText text="Contact Us" fontFamily="bold" fontSize={18} />
        <FlatList
          key={'#'}
          numColumns={2}
          data={locationData}
          renderItem={locationRender}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </View>
  );
};

export default ContactUs;
