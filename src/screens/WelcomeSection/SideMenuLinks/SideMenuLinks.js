//import : react components
import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
//import : custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyHeaderWithoutLogin from 'components/MyHeaderWithoutLogin/MyHeaderWithoutLogin';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//import : third parties
import {WebView} from 'react-native-webview';
import {useNetInfo} from "@react-native-community/netinfo";
// import : globals
import {Service, ScreenNames} from 'global/Index';
//import : styles
import {styles} from './SideMenuLinksStyle';
//import : redux
import {useSelector} from 'react-redux';

const SideMenuLinks = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const userInfo = role ? {} : useSelector(state => state.user.userInfo);
  //variables : route variables
  const name = route.params.name;
  const endPoint = route.params.endPoint;
  const role = route.params?.role || '';
  const [link, setLink]= useState('')
  const [showLoader, setShowLoader] = useState(false);
  const [showLoader2, setShowLoader2] = useState(false);

  const getPageLink = async () => {
    setShowLoader(true)
    try {
      const myData = new FormData();
      myData.append('name', endPoint);
      let specificEndPoint = ''
      if(role){
        specificEndPoint = role === 'Service' ? Service.SERVICE_LINKS : Service.USER_LINKS
      }
      else{
        specificEndPoint = Number(userInfo?.user_type) === 3 ? Service.SERVICE_LINKS : Service.USER_LINKS
      }
      console.log('specificEndPoint', specificEndPoint);
      const resp = await Service.postApi(
        specificEndPoint,
        myData
      );
      console.log(`${endPoint} resp`, resp);
      if (resp.data.status === true) {
        setLink(resp.data.link)
      }
    } catch (error) {
      console.log(`error in ${endPoint}`, error);
    }
    setShowLoader(false)
  }
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.navigate(ScreenNames.NO_CONNECTION);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  useEffect(()=>{
    getPageLink()
  },[])
  //UI
  return (
    <View style={styles.container}>
      {role ? 
        <MyHeaderWithoutLogin Title={name} />
        :
        <MyHeader Title={name} IsCartIcon={false} IsNotificationIcon={false}/>
      }
      <WebView
        source={{uri: link}}
        contentMode="mobile"
        style={styles.webViewStyle}
      />
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default SideMenuLinks;
