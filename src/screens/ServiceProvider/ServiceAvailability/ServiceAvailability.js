//react components
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Image,
  Alert,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import NoInternet from 'components/NoInternet/NoInternet';
//import : modals
import EditSlot from 'modals/EditSlot/EditSlot';
import DeleteSlot from 'modals/DeleteSlot/DeleteSlot';
import MyButton from 'components/MyButton/MyButton';
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//third parties
import {Dropdown} from 'react-native-element-dropdown';
import {Calendar, Agenda} from 'react-native-calendars';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from "@react-native-community/netinfo";
//global
import {
  Colors,
  Constant,
  Images,
  MyIcon,
  ScreenNames,
  Service,
} from 'global/Index';
//styles
import {styles} from './ServiceAvailabilityStyle';
//redux
import {useDispatch, useSelector} from 'react-redux';
import FAB_Button from 'components/FAB_Button/FAB_Button';
import moment from 'moment';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const ServiceAvailability = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //data
  const [items, setItems] = useState({});
  //variables : route variables
  const AgendaList = route.params.AgendaList;
  //variables : redux variables
  const dispatch = useDispatch();
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [AgendaData, setAgendaData] = useState([]);
  const [showLoader, setShowLoader] = useState(false);
  const [value, setValue] = useState(null);
  const [isEmptyAgenda, setIsEmptyAgenda] = useState(false);
  const [isFocus, setIsFocus] = useState(false);
  //states : calendar states
  const [showAgenda, setShowAgenda] = useState(false);
  const [showEditSlotModal, setShowEditSlotModal] = useState(false);
  const [selectedItem, setSelecteditem] = useState({});
  const [showDeleteSlotModal, setShowDeleteSlotModal] = useState(false);
  const [isOnline, setIsOnline] = useState(true);
  //function : navigation function
  const gotoManageAvailability = () =>
    navigation.navigate(ScreenNames.SERVICE_MANAGE_AVAILABILITY, {
      AgendaList: AgendaList,
    });
  const gotoHome = () => navigation.navigate(ScreenNames.SERVICE_HOME);
  //function : imp function
  //function : service function
  const getAgendaData = async () => {
    setShowLoader(true);
    try {
      const requiredData = new FormData();
      requiredData.append('service_provider_id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SERVICE_PROVIDER_CALENDAR,
        requiredData,
      );
      if (resp.data.status) {
        setIsEmptyAgenda(true);
        setAgendaData(resp.data.data);
        console.warn('resp---->', resp.data);
      }
    } catch (error) {
      console.log('error in getAgendaData', error);
    }
    setShowLoader(false);
  };
  const getAvailabilityStatus = async () => {
    setShowLoader(true);
    try {
      const statusData = new FormData();
      statusData.append('service_provider_id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.GET_AVAILABILITY_STATUS,
        statusData,
      );
      if (resp.data.status) {
        setValue(resp.data.available_status);
        // console.warn('resp----->', resp.data);
      }
    } catch (error) {
      console.log('error in getAvailabilityStatus', error);
    }
    setShowLoader(false);
  };
  const changeAvailabilityStatus = async () => {
    if (value == null) {
      dispatch(
        showToast({text: 'Please select status for manage availability'}),
      );
      // Alert.alert('', 'Please select status for manage availability');
    } else {
      setShowLoader(true);
      try {
        const statusData = new FormData();
        statusData.append('available_status', value);
        statusData.append('service_provider_id', userInfo.id);
        const resp = await Service.postApiWithToken(
          userToken,
          Service.CHANGE_AVAILABILITY_STATUS,
          statusData,
        );
        if (resp.data.status) {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
          gotoHome();
        }
      } catch (error) {
        console.log('error in changeAvailabilityStatus', error);
      }
      setShowLoader(false);
    }
  };
  //function : render function
  const renderItem = item => {
    // show buttons is true if date is not in past and no appointment is booked on that date (type B is not there)
    // const showButtons = !moment(item.date).isBefore(moment(), 'day') && item?.type === 'A' && items[item.date]?.filter(el=>el?.type === 'B')?.length === 0
    const showButtons =
      !moment(item.date).isBefore(moment(), 'day') && item.isEdit == 0;
    return (
      <View
        style={{
          ...styles.item,
          backgroundColor:
            // item.isAvailable == 1 ? Colors.THEME_GREEN : Colors.RED,
            item.type === 'A'
              ? Colors.THEME_BLUE
              : item.type === 'B'
              ? Colors.THEME_GREEN
              : Colors.RED,
        }}>
        <MyText
          text={item.name}
          fontFamily="bold"
          fontSize={16}
          textColor="white"
        />
        {item.type === 'A' ? (
          <View>
            <View style={styles.genericView}>
              <View
                style={{flexDirection: 'row', marginTop: showButtons ? 0 : 5}}>
                <MyIcon.AntDesign
                  name="clockcircleo"
                  size={20}
                  color={Colors.WHITE}
                  style={{marginRight: 5}}
                />
                <MyText
                  text={`Start Time : ${moment(
                    item.startTime,
                    'HH:mm:ss',
                  ).format('h:mm A')}`}
                  textColor="white"
                />
              </View>
              {showButtons ? (
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity
                    onPress={() => {
                      setSelecteditem(item);
                      setShowEditSlotModal(true);
                    }}
                    style={[styles.editButtonContainer, {marginRight: 20}]}>
                    <MyIcon.MaterialCommunityIcons
                      name="pencil-outline"
                      size={20}
                      color={Colors.THEME_GREEN}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      setSelecteditem(item);
                      setShowDeleteSlotModal(true);
                    }}
                    style={[styles.editButtonContainer, {marginRight: 10}]}>
                    <MyIcon.MaterialIcons
                      name="delete-outline"
                      size={20}
                      color={Colors.RED}
                    />
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>
            <View
              style={{flexDirection: 'row', marginTop: showButtons ? 0 : 5}}>
              <MyIcon.AntDesign
                name="clockcircleo"
                size={20}
                color={Colors.WHITE}
                style={{marginRight: 5}}
              />
              <MyText
                text={`End Time : ${moment(item.endTime, 'HH:mm:ss').format(
                  'h:mm A',
                )}`}
                textColor="white"
              />
            </View>
          </View>
        ) : null}
        {item.type === 'B' ? (
          <>
            <View style={{flexDirection: 'row'}}>
              <MyIcon.AntDesign
                name="clockcircleo"
                size={20}
                color={Colors.WHITE}
                style={{marginRight: 5}}
              />
              <MyText
                text={`Start Time : ${moment(item.startTime, 'HH:mm:ss').format(
                  'h:mm A',
                )}`}
                textColor="white"
              />
            </View>
          </>
        ) : null}
      </View>
    );
  };
  const rowHasChanged = (r1, r2) => {
    return r1.name !== r2.name;
  };
  const timeToString = time => {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  };
  const loadItems = day => {
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = timeToString(time);
        if (!items[strTime] && items[strTime] == undefined) {
          items[strTime] = [];
          for (let i = 0; i < AgendaList?.length; i++) {
            if (AgendaList[i]?.date == strTime) {
              items[strTime].push({
                name: AgendaList[i].eventName,
                date: AgendaList[i].date,
                startTime: AgendaList[i].start_time,
                endTime: AgendaList[i].end_time,
                isAvailable: AgendaList[i].is_available,
                type: AgendaList[i].type,
                serviceUnavailableId: AgendaList[i].service_unavailable_id,
                isEdit: AgendaList[i]?.is_edit,
              });
            }
          }
        }
      }
      //console.log(this.state.items);
      const newItems = {};
      Object.keys(items).forEach(key => {
        newItems[key] = items[key];
      });
      setItems(newItems);
      // this.setState({
      //   items: newItems,
      // });
    }, 0);
    // console.log(`Load Items for ${day.year}-${day.month}`);
  };

  const renderEmptyDate = () => {
    return (
      <View style={styles.emptyDate}>
        <Text>No Appointments!</Text>
      </View>
    );
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  // useEffect(() => {
  //   getAgendaData();
  //   return () => {};
  // }, []);
  const deleteSlot = async serviceUnavailableId => {
    setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('service_provider_id', userInfo.id);
      myData.append('service_unavailable_id', serviceUnavailableId);
      console.log('deleteSlot myData', myData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SLOT_DELETE,
        myData,
      );
      console.log('deleteSlot resp', resp);
      if (resp.data.status) {
        Toast.show(resp.data.message, Toast.SHORT);
        setShowDeleteSlotModal(false);
        navigation.navigate(ScreenNames.SERVICE_HOME);
      } else {
        Toast.show(resp.data.message, Toast.SHORT);
        setShowDeleteSlotModal(false);
      }
    } catch (error) {
      setShowDeleteSlotModal(false);
      console.log('error in deleteSlot', error);
    }
    setShowLoader(false);
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Manage Availability" />
      <Agenda
        items={items}
        // showOnlySelectedDayItems
        selected={moment(new Date()).format('YYYY-MM-DD')}
        renderItem={renderItem}
        loadItemsForMonth={day => loadItems(day)}
        renderEmptyDate={renderEmptyDate}
        onDayPress={day => {
          console.log('day pressed');
        }}
        rowHasChanged={rowHasChanged}
      />

      <FAB_Button onPress={gotoManageAvailability} />
      <CustomLoader showLoader={showLoader} />
      {Object.keys(selectedItem)?.length > 0 ? (
        <EditSlot
          visible={showEditSlotModal}
          setVisibility={setShowEditSlotModal}
          userInfo={userInfo}
          userToken={userToken}
          selectedItem={selectedItem}
        />
      ) : null}
      <DeleteSlot
        visible={showDeleteSlotModal}
        setVisibility={setShowDeleteSlotModal}
        deleteSlot={() => deleteSlot(selectedItem?.serviceUnavailableId)}
      />
    </View>
  );
};

export default ServiceAvailability;
