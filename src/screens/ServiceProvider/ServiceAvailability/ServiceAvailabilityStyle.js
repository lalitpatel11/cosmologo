import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  calendarStyle: {
    borderRadius: 10,
    padding: 5,
  },
  item: {
    backgroundColor: '#8FBC8F',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },
  upperView: {
    backgroundColor: Colors.WHITE,
    padding: 10,
    borderRadius: 10,
  },
  middleView: {
    backgroundColor: Colors.WHITE,
    padding: 10,
    borderRadius: 10,
    marginVertical: 20,
  },
  dropdown: {
    height: 50,
    borderColor: Colors.LITE_GREY,
    borderWidth: 0.5,
    borderRadius: 8,
    paddingHorizontal: 8,
  },
  editButtonContainer:{
    backgroundColor: Colors.WHITE,
    borderRadius: 100,
    height:34,
    width:34,
    justifyContent:'center',
    alignItems:'center',
  },
  genericView:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
  }
});