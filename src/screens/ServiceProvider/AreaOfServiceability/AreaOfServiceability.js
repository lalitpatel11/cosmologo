//react components
import React, {useState} from 'react';
import {
  View,
  ScrollView,
  Image,
  KeyboardAvoidingView,
  Platform,
  Alert,
} from 'react-native';
//custom components
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyButton from 'components/MyButton/MyButton';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//global
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './AreaOfServiceabilityStyle';
//import : third parties
import Toast from 'react-native-simple-toast';

const AreaOfServiceability = ({navigation, route}) => {
  //variables : route variables
  const selectedAreaOfConcern = route.params.selectedAreaOfConcern;
  const userId = route.params.userId;
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [code, setCode] = useState('');
  const [state, setState] = useState('');
  const [city, setCity] = useState('');
  //function : navigation function
  const gotoUploadDocumet = () =>
    navigation.navigate(ScreenNames.SERVICE_UPLOAD_DOCUMENTS, {userId: userId});
  const gotoSignIn = () => navigation.navigate(ScreenNames.SERVICE_SIGN_IN);
  //function : imp function
  const Validation = () => {
    if (code == '') {
      Toast.show('Please enter ZIP / PIn code', Toast.LONG);
      // Alert.alert('', 'Please enter ZIP / PIn code');
    } else if (state == '') {
      Toast.show('Please enter state', Toast.LONG);
      // Alert.alert('', 'Please enter state');
    } else if (city == '') {
      Toast.show('Please enter city', Toast.LONG);
      // Alert.alert('', 'Please enter city');
    } else return true;
  };
  //function : service function
  const selectService = async () => {
    if (Validation()) {
      setShowLoader(true);
      try {
        const selectServiceData = new FormData();
        selectServiceData.append('service_provider_id', userId);
        selectedAreaOfConcern.map((e, i) => {
          selectServiceData.append(`area_concern_id[${i}]`, e);
        });
        selectServiceData.append('pin', code);
        selectServiceData.append('city', city);
        selectServiceData.append('state', state);
        console.warn(selectServiceData);
        const resp = await Service.postApi(
          Service.SELECT_SERVICE,
          selectServiceData,
        );
        if (resp.data.status) {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
          gotoSignIn();
        } else {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
        }
      } catch (error) {
        console.log('error in selectService', error);
      }
      setShowLoader(false);
    }
  };
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Area of Serviceability" />
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <ScrollView style={styles.mainView}>
          <Image
            resizeMode="contain"
            source={Images.AppIcon}
            style={styles.AppIcon}
          />
          <MyTextInput
            Title="Enter ZIP / PIN Code"
            placeholder="Enter ZIP / PIN Code"
            onChangeText={text => setCode(text)}
            keyboardType="number-pad"
            maxLength={6}
          />
          <MyTextInput
            placeholder="Enter State"
            onChangeText={text => setState(text)}
            Icon={
              <MyIcon.AntDesign
                name="downcircleo"
                size={24}
                color={Colors.LITE_GREY}
              />
            }
          />
          <MyTextInput
            placeholder="Enter City"
            onChangeText={text => setCity(text)}
            Icon={
              <MyIcon.AntDesign
                name="downcircleo"
                size={24}
                color={Colors.LITE_GREY}
              />
            }
          />
        </ScrollView>
        <View style={styles.bottomSection}>
          <MyButton Title="Next" onPress={selectService} />
        </View>
      </KeyboardAvoidingView>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default AreaOfServiceability;
