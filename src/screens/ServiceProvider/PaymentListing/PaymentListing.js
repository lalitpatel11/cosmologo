//react components
import React from 'react';
import {View, Image, TouchableOpacity, FlatList} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
import MyHeader from 'components/MyHeader/MyHeader';
import MyButton from 'components/MyButton/MyButton';
//global
import {Colors, Images, MyIcon} from 'global/Index';
//styles
import {styles} from './PaymentListingStyle';
import DateSelector from 'components/DateSelector/DateSelector';

const PaymentListing = () => {
  //data
  const paymentData = [1, 2, 3, 4, 5];
  //function : render function
  const paymentListRender = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
          borderWidth: 0.5,
          marginVertical: 10,
          borderRadius: 20,
          padding: 10,
        }}>
        <View
          style={{
            borderTopRightRadius: 20,
            borderBottomLeftRadius: 20,
            position: 'absolute',
            backgroundColor: Colors.THEME_GREEN,
            paddingHorizontal: 10,
            right: 1,
          }}>
          <MyText text="Received Successfully" textColor="white" />
        </View>

        <Image
          source={Images.ProfileIcon.profileImage}
          style={{height: 100, width: 100}}
        />
        <View
          style={{
            width: '70%',
            marginLeft: 10,
            justifyContent: 'center',
          }}>
          <MyText
            text="Payment from John Smith of $298.00 for BLEPHAROPLASTY"
            fontFamily="bold"
            marginTop={15}
          />
          <MyButton
            Title="Initiate Refund"
            width="60%"
            height={30}
            borderColor={Colors.THEME_GREEN}
          />
        </View>
      </View>
    );
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Payments" />
      <View style={styles.mainView}>
        <View style={styles.totalPaymentView}>
          <Image
            source={Images.Icons.totalPaymentIcon}
            style={styles.iconStyles}
          />
          <View style={styles.textView}>
            <MyText
              text="Total Payable Amount"
              fontFamily="medium"
              fontSize={16}
            />
            <MyText
              text="$299.00"
              marginTop={10}
              fontFamily="bold"
              fontSize={20}
            />
          </View>
        </View>
        <DateSelector />
        <FlatList
          data={paymentData}
          renderItem={paymentListRender}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{paddingBottom: 200}}
          showsVerticalScrollIndicator={false}
          style={{marginVertical: 10}}
        />
      </View>
    </View>
  );
};

export default PaymentListing;
