import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

const SIZE = 70;
const HEIGHT = 40;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  totalPaymentView: {
    flexDirection: 'row',
    backgroundColor: Colors.BG_GREEN,
    padding: 10,
    borderRadius: 10,
    marginVertical: 10,
  },
  iconStyles: {
    height: SIZE,
    width: SIZE,
  },
  textView: {
    marginLeft: 20,
  },
});
