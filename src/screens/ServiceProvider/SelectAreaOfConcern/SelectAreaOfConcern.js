//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Alert,
  FlatList
} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
//globals
import {
  Colors,
  Constant,
  Images,
  MyIcon,
  ScreenNames,
  Service,
} from 'global/Index';
//modals
import UnselectService from 'modals/UnselectService/UnselectService'; 
//styles
import {styles} from './SelectAreaOfConcernStyle';
import MyButton from 'components/MyButton/MyButton';
import {useDispatch, useSelector} from 'react-redux';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  setUser,
  setUserToken,
  setTotalPaymentAmount,
} from 'src/reduxToolkit/reducer/user';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from "@react-native-community/netinfo";

const SelectAreaOfConcern = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const selectedProduct = route.params.selectedProduct;
  const selectedSubCategory = route.params.selectedSubCategory;
  const selectedCategory = route.params.selectedCategory;
  const productList = route.params.productList;
  const userId = route.params.userId;
  //variables : redux variables
  const dispatch = useDispatch();
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [areaOfConcernList, setAreaOfConcernList] = useState([]);
  const [selectedAreaOfConcern, setSelectedAreaOfConcern] = useState([]);
  const [selected, setSelected] = useState([]);
  const [openUnselectServiceModal, setOpenUnselectServiceModal] = useState(false);
  const [unselectMessage, setUnselectMessage] = useState('');
  const [text1, setText1] = useState('');
  const [values, setValues] = useState([]);
  const [text2, setText2] = useState('');
  const [AOCByProduct, setAOCByProduct] = useState([]);
  //function : imp function
  const selectItem = item => {
    const index = selectedAreaOfConcern.findIndex(e => e == item.id);
    // const index = selectedAreaOfConcern.findIndex(e => e == item.concern_area_id);
    if (index > -1) {
      const filterArray = selectedAreaOfConcern.filter(e => e != item.id);
      // const filterArray = selectedAreaOfConcern.filter(e => e != item.concern_area_id);
      setSelectedAreaOfConcern(filterArray);
      const temp = [] 
      AOCByProduct.map((el, index)=>{
        let count = 0
        el?.map(el2=>{
          if(filterArray.includes(el2.id)){
            count++
          }
        })
        temp[index] = count
      })
      setSelected(temp)
      console.log('setSelected', temp);
    } else {
      setSelectedAreaOfConcern([...selectedAreaOfConcern, item.id]);
      // setSelectedAreaOfConcern([...selectedAreaOfConcern, item.concern_area_id]);
      const temp = []
      AOCByProduct.map((el, index)=>{
        let count = 0
        el?.map(el2=>{
          if([...selectedAreaOfConcern, item.id].includes(el2.id)){
            count++
          }
        })
        temp[index] = count
      })
      setSelected(temp)
      console.log('setSelected', temp);
    }
  };
  //function : navigation function
  const gotoHome = () => navigation.navigate(ScreenNames.SERVICE_HOME);
  const resetIndexGoToBottomTab = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.SERVICE_BOTTOM_TAB}],
  });
  const gotoServiceability = () =>
    navigation.navigate(ScreenNames.AREA_OF_SERVICEABILITY, {
      selectedAreaOfConcern,
      userId,
    });
  //function : service function
  const getAreaOfConcern = async () => {
    setShowLoader(true);
    try {
      const areaofConcernData = new FormData();
      selectedProduct.map((e, i) => {
        areaofConcernData.append(`product_id[${i}]`, e);
      });
      // areaofConcernData.append('product_id', selectedProduct.id);
      const resp = await Service.postApi(
        Service.PRODUCT_CONCERN_AREA_LIST,
        areaofConcernData,
      );
      if (resp.data.status) {
        let serIndex = 0
        const data = resp.data.data.concern_area_list?.map(el=>{
          serIndex++
          return {...el, serIndex: String(serIndex) }
        })
        const unique = [...new Set(data?.map(item => item.product_id))]
        console.log('unique', unique);
        const allAOCs = []
        unique.map((un, index)=>{
          const aocs = []
          data?.map(el=>{
            if(el.product_id === un){
              // final[un].push(el)
              // console.log('final[index]', final[index]);
              aocs.push(el)
            }
          })
          var obj = {};
          obj[un] = aocs;
          // allAOCs.push(obj);
          allAOCs.push(aocs);
        })
        setAOCByProduct(allAOCs)
        // console.log('setAOCByProduct', allAOCs);
        setSelected(new Array(allAOCs?.length).fill(0))
        // console.log('new Array(allAOCs?.length).fill(0)', new Array(allAOCs?.length).fill(0));
        setAreaOfConcernList(data);
        console.log('setAreaOfConcernList', data);
        // console.warn('resp--->', resp.data);
      }
    } catch (error) {
      console.log('error in getAreaOfConcern', error);
    }
    setShowLoader(false);
  };
  const addMoreCategory = async () => {
    const notSelectedIndexes = []
    for(let i = 0; i < selected.length; i++){
      // console.log('selected[i]', selected[i]);
      if(selected[i] == 0){
        notSelectedIndexes.push(i)
        // categoryiesData?.find(el=>el.id == subcatByCat[0][0].service_category_id)?.title
      }
    }
    // console.log('prod selected', selected);
    // console.log('prod notSelectedIndexes', notSelectedIndexes);
    const unselectedProductIds = []
    AOCByProduct.map((sb, index)=>{
      if(notSelectedIndexes.includes(index)){
        unselectedProductIds.push(sb[0]?.product_id)
      }
    })
    // console.log('unselectedCategoryIds', unselectedProductIds);
    const unselectedProducts = []
    unselectedProductIds.map(el=>{
      const prodName = productList?.find(el2=>el2?.id == el)?.title
      unselectedProducts.push(prodName)
    })
    console.log('unselectedsubcategories', unselectedProducts);
    if(selected.map(el=>el).reduce((a,b)=>a+b, 0) === 0){
      Toast.show('Please select at least one area of concern', Toast.SHORT)
      return
    }
    // if(unselectedProducts?.length > 0){
    //   setText1('Please select at least one area of concern for')
    //   setValues(unselectedProducts)
    //   setText2('Else above products will be removed')
    //   setOpenUnselectServiceModal(true)
    //   return  
    // }
    if (selectedAreaOfConcern.length === 0) {
      Toast.show('Please select Area Of Concern', Toast.LONG);
      // Alert.alert('', `Please select Area Of Concern`);
      return;
    }
    console.log('selectedAreaOfConcern', selectedAreaOfConcern);
    const finalData = areaOfConcernList.filter(el=>selectedAreaOfConcern.includes(el.id))
    console.log('finalData', finalData);
    setShowLoader(true)
    try {
      const requiredData = new FormData();
      requiredData.append('service_provider_id', userId);
      const data  = finalData.map(el=>{
        const {category_id, subcategory_id, product_id, id, concern_area_id} = el
        return {category_id, subcategory_id, product_id, id, concern_area_id}
      })
      console.log('aoc data', data);
      data.map((e, i) => {
        // requiredData.append(`concern_area_id[${i}]`, e.concern_area_id);
        requiredData.append(`area_concern_id[${i}][concern_area_id]`, e.concern_area_id);
        requiredData.append(`area_concern_id[${i}][category_id]`, e.category_id);
        requiredData.append(`area_concern_id[${i}][id]`, e.id);
        requiredData.append(`area_concern_id[${i}][product_id]`, e.product_id);
        requiredData.append(`area_concern_id[${i}][subcategory_id]`, e.subcategory_id);
      });
      // requiredData.append('area_concern_id', data);
      // selectedProduct.map((e, i) => {
      //   requiredData.append(`product_id[${i}]`, e);
      // });
      // selectedSubCategory.map((e, i) => {
      //   requiredData.append(`subcategory_id[${i}]`, e);
      // });
      // selectedCategory.map((e, i) => {
      //   requiredData.append(`category_id[${i}]`, e);
      // });
      console.log('addmoreser requiredData', requiredData);
      // setShowLoader(false)
      // return
      const resp = await Service.postApiWithToken(
        userToken,
        Service.ADD_MORE_SERVICES,
        requiredData,
      );
      console.log('addmoreser resp', resp);
      if (resp.data.status) {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT);
        navigation.dispatch(resetIndexGoToBottomTab);
        // gotoHome();
      }
    } catch (error) {
      console.log('error in addMoreCategory', error);
    }
    setShowLoader(false);
  };
  const nextSelectService = async () => {
    const finalData = areaOfConcernList.filter(el=>selectedAreaOfConcern.includes(el.id))
    console.log('finalData', finalData);
    setShowLoader(true);
    try {
      const selectServiceData = new FormData();
      selectServiceData.append('service_provider_id', userId);
      // selectedAreaOfConcern.map((e, i) => {
      //   selectServiceData.append(`area_concern_id[${i}]`, e);
      // });
      const data  = finalData.map(el=>{
        const {category_id, subcategory_id, product_id, id, concern_area_id} = el
        return {category_id, subcategory_id, product_id, id, concern_area_id}
      })
      // data.map((e, i) => {
      //   selectServiceData.append(`area_concern_id[${i}]`, e);
      // });
      data.map((e, i) => {
        // requiredData.append(`concern_area_id[${i}]`, e.concern_area_id);
        selectServiceData.append(`area_concern_id[${i}][concern_area_id]`, e.concern_area_id);
        selectServiceData.append(`area_concern_id[${i}][category_id]`, e.category_id);
        selectServiceData.append(`area_concern_id[${i}][id]`, e.id);
        selectServiceData.append(`area_concern_id[${i}][product_id]`, e.product_id);
        selectServiceData.append(`area_concern_id[${i}][subcategory_id]`, e.subcategory_id);
      });
      // selectedProduct.map((e, i) => {
      //   selectServiceData.append(`product_id[${i}]`, e);
      // });
      // selectedSubCategory.map((e, i) => {
      //   selectServiceData.append(`subcategory_id[${i}]`, e);
      // });
      // selectedCategory.map((e, i) => {
      //   selectServiceData.append(`category_id[${i}]`, e);
      // });
      console.log('selectServiceData', selectServiceData);
      const resp = await Service.postApi(
        Service.SELECT_SERVICE,
        selectServiceData,
      );
      if (resp.data.status) {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT);
        await AsyncStorage.setItem('userToken', resp.data.access_token);
        dispatch(setUserToken(resp.data.access_token));
        const jsonValue = JSON.stringify(resp.data.data);
        console.log('serv signUp jsonValue', jsonValue);
        await AsyncStorage.setItem('userInfo', jsonValue);
        dispatch(setUser(resp.data.data));
        if (typeof resp.data.data?.total_payment === 'number') {
          await AsyncStorage.setItem(
            'totalPaymentAmount',
            JSON.stringify(resp.data.data?.total_payment),
          );
          dispatch(setTotalPaymentAmount(resp.data.data?.total_payment));
        }
        // setOpenUnselectServiceModal(false)
        navigation.dispatch(resetIndexGoToBottomTab);
      } else {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in selectService', error);
      // setOpenUnselectServiceModal(false)
    }
    setShowLoader(false);
  }
  const nextAddMore = async () => {
    const finalData = areaOfConcernList.filter(el=>selectedAreaOfConcern.includes(el.id))
    console.log('finalData', finalData);
    setShowLoader(true);
    try {
      const requiredData = new FormData();
      requiredData.append('service_provider_id', userId);
      const data  = finalData.map(el=>{
        const {category_id, subcategory_id, product_id, id, concern_area_id} = el
        return {category_id, subcategory_id, product_id, id, concern_area_id}
      })
      console.log('aoc data', data);
      data.map((e, i) => {
        // requiredData.append(`concern_area_id[${i}]`, e.concern_area_id);
        requiredData.append(`area_concern_id[${i}][concern_area_id]`, e.concern_area_id);
        requiredData.append(`area_concern_id[${i}][category_id]`, e.category_id);
        requiredData.append(`area_concern_id[${i}][id]`, e.id);
        requiredData.append(`area_concern_id[${i}][product_id]`, e.product_id);
        requiredData.append(`area_concern_id[${i}][subcategory_id]`, e.subcategory_id);
      });
      // requiredData.append('area_concern_id', data);
      // selectedProduct.map((e, i) => {
      //   requiredData.append(`product_id[${i}]`, e);
      // });
      // selectedSubCategory.map((e, i) => {
      //   requiredData.append(`subcategory_id[${i}]`, e);
      // });
      // selectedCategory.map((e, i) => {
      //   requiredData.append(`category_id[${i}]`, e);
      // });
      console.log('addmoreser requiredData', requiredData);
      // setShowLoader(false)
      // return
      const resp = await Service.postApiWithToken(
        userToken,
        Service.ADD_MORE_SERVICES,
        requiredData,
      );
      console.log('addmoreser resp', resp);
      if (resp.data.status) {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT);
        // setOpenUnselectServiceModal(false)
        navigation.dispatch(resetIndexGoToBottomTab);
        // gotoHome();
      }
    } catch (error) {
      console.log('error in addMoreCategory', error);
      // setOpenUnselectServiceModal(false)
    }
    setShowLoader(false);
  }
  const selectService = async () => {
    const notSelectedIndexes = []
    for(let i = 0; i < selected.length; i++){
      console.log('selected[i]', selected[i]);
      if(selected[i] == 0){
        notSelectedIndexes.push(i)
        // categoryiesData?.find(el=>el.id == subcatByCat[0][0].service_category_id)?.title
      }
    }
    console.log('prod selected', selected);
    console.log('prod notSelectedIndexes', notSelectedIndexes);
    const unselectedProductIds = []
    AOCByProduct.map((sb, index)=>{
      if(notSelectedIndexes.includes(index)){
        unselectedProductIds.push(sb[0]?.product_id)
      }
    })
    console.log('unselectedCategoryIds', unselectedProductIds);
    const unselectedProducts = []
    unselectedProductIds.map(el=>{
      const prodName = productList?.find(el2=>el2?.id == el)?.title
      unselectedProducts.push(prodName)
    })
    console.log('unselectedsubcategories', unselectedProducts);
    if(selected.map(el=>el).reduce((a,b)=>a+b, 0) === 0){
      Toast.show('Please select at least one area of concern', Toast.SHORT)
      return
    }
    // if(unselectedProducts?.length > 0){
    //   // setUnselectMessage(`Please select at least one area of concern for ${unselectedProducts.join(', ')}. Else ${unselectedProducts.join(', ')} will be removed`)  
    //   setText1('Please select at least one area of concern for')
    //   setValues(unselectedProducts)
    //   setText2('Else above products will be removed')
    //   setOpenUnselectServiceModal(true)
    //   return  
    // }
    console.log('selectedAreaOfConcern', selectedAreaOfConcern);
    if (selectedAreaOfConcern.length === 0) {
      Toast.show('Please select Area Of Concern', Toast.LONG);
      // Alert.alert('', `Please select Area Of Concern`);
      return;
    }
    const finalData = areaOfConcernList.filter(el=>selectedAreaOfConcern.includes(el.id))
    console.log('finalData', finalData);
    setShowLoader(true);
    try {
      const selectServiceData = new FormData();
      selectServiceData.append('service_provider_id', userId);
      // selectedAreaOfConcern.map((e, i) => {
      //   selectServiceData.append(`area_concern_id[${i}]`, e);
      // });
      const data  = finalData.map(el=>{
        const {category_id, subcategory_id, product_id, id, concern_area_id} = el
        return {category_id, subcategory_id, product_id, id, concern_area_id}
      })
      // data.map((e, i) => {
      //   selectServiceData.append(`area_concern_id[${i}]`, e);
      // });
      data.map((e, i) => {
        // requiredData.append(`concern_area_id[${i}]`, e.concern_area_id);
        selectServiceData.append(`area_concern_id[${i}][concern_area_id]`, e.concern_area_id);
        selectServiceData.append(`area_concern_id[${i}][category_id]`, e.category_id);
        selectServiceData.append(`area_concern_id[${i}][id]`, e.id);
        selectServiceData.append(`area_concern_id[${i}][product_id]`, e.product_id);
        selectServiceData.append(`area_concern_id[${i}][subcategory_id]`, e.subcategory_id);
      });
      // selectedProduct.map((e, i) => {
      //   selectServiceData.append(`product_id[${i}]`, e);
      // });
      // selectedSubCategory.map((e, i) => {
      //   selectServiceData.append(`subcategory_id[${i}]`, e);
      // });
      // selectedCategory.map((e, i) => {
      //   selectServiceData.append(`category_id[${i}]`, e);
      // });
      console.log('selectServiceData', selectServiceData);
      const resp = await Service.postApi(
        Service.SELECT_SERVICE,
        selectServiceData,
      );
      if (resp.data.status) {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT);
        await AsyncStorage.setItem('userToken', resp.data.access_token);
        dispatch(setUserToken(resp.data.access_token));
        const jsonValue = JSON.stringify(resp.data.data);
        console.log('serv signUp jsonValue', jsonValue);
        await AsyncStorage.setItem('userInfo', jsonValue);
        dispatch(setUser(resp.data.data));
        if (typeof resp.data.data?.total_payment === 'number') {
          await AsyncStorage.setItem(
            'totalPaymentAmount',
            JSON.stringify(resp.data.data?.total_payment),
          );
          dispatch(setTotalPaymentAmount(resp.data.data?.total_payment));
        }
        navigation.dispatch(resetIndexGoToBottomTab);
      } else {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in selectService', error);
    }
    setShowLoader(false);
  };
  
  const selectAreaOfConcern = () => {
    if (selectedAreaOfConcern.length > 0) {
      selectService();
    } else {
      Toast.show('Please select Area Of Concern', Toast.LONG);
      // Alert.alert('', `Please select Area Of Concern`);
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getAreaOfConcern();
    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Area Of Concern" />
      <ScrollView
        contentContainerStyle={{paddingBottom: '20%'}}
        style={styles.mainView}>
        {/* <MyText
          text={`Choose Area Of Concern`}
          fontFamily="bold"
          textAlign="center"
          marginBottom={20}
        />
        <Image
          resizeMode="contain"
          source={Images.AppIcon}
          style={styles.AppIcon}
        /> */}
        <View style={styles.productView}>
          {AOCByProduct.map((prodlist, index) => {
            return (
              <View>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center',marginBottom:10, marginTop:20}}>
              <View style={{flexDirection:'row', alignItems:'center', width:'60%'}}>
              <MyText
                text={`${productList?.find(el=>el.id == prodlist[0].product_id)?.title}`}
                // text={`category 1`}
                fontFamily="bold"
                numberOfLines={2}
                fontSize={18}
                textColor='#3E5869'
                // width={'80%'}
                marginLeft={10}
              />
              <MyText
                text={`(${selected[index]})`}
                // text={`category 1`}
                fontFamily="medium"
                textColor="theme_green"
                numberOfLines={2}
                fontSize={18}
                // width={'80%'}
                marginLeft={5}
              />
              </View>
              {prodlist?.length > 2 ?
              <View style={{flexDirection:'row', alignItems:'center', alignSelf:'flex-end', marginRight:10}}>
                <MyText
                  text={'More'}
                  // text={`category 1`}
                  fontFamily="medium"
                  textColor="theme_green"
                  numberOfLines={2}
                  fontSize={14}
                  // width={'80%'}
                  marginLeft={5}
                />
                <Image source={require('assets/images/caret-right.png')} resizeMode='cover' style={{height:25, width:25}}/>
              </View>
              :null}
              </View>
              <FlatList
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  data={prodlist}
                  // keyExtractor={(_, index) => index.toString()}
                  keyExtractor={item => item.serIndex}
                  renderItem={({item, index}) => (
              <TouchableOpacity
                key={item.serIndex}
                onPress={() => selectItem(item)}
                style={{
                  width: Constant.width / 2 - 40,
                  height: Constant.width / 2 - 50,
                  // height: 200,
                  margin: 10,
                  backgroundColor: Colors.WHITE,
                  borderRadius: 20,
                  borderWidth:
                    selectedAreaOfConcern.indexOf(item.id) > -1 ? 2 : 0,
                  borderColor: Colors.THEME_GREEN,
                  alignItems:'center',
                  // justifyContent:'center',
                  shadowColor: '#459743',
                  shadowOffset: {width: 0, height: 5},
                  shadowOpacity: 0.8,
                  shadowRadius: 15,
                  elevation: 5,
                }}>
                <Image
                  source={{uri: item.concern_area_image}}
                  style={{
                    width: 70,
                    height: 70,
                    borderRadius: 35,
                    marginTop:15
                  }}
                />
                <MyText
                  text={item.concern_area_title}
                  fontFamily="medium"
                  textColor='#3E5869'
                  fontSize={16}
                  numberOfLines={2}
                  width={'80%'}
                  marginTop={20}
                  textAlign='center'
                />
                {selectedAreaOfConcern.indexOf(item.id) > -1 ? 
                <Image source={require('assets/images/check-circle-service.png')} resizeMode='cover' style={{height:25, width:25,position:'absolute', right:10, top:10}}/>
                :null}
              </TouchableOpacity>
              )}
              />  
      
          </View>
            );
          })}
        </View>
      </ScrollView>
      <View
        style={{
          backgroundColor: Colors.WHITE,
          padding: 10,
          paddingHorizontal: 20,
        }}>
        <MyButton
          Title="Next"
          onPress={userToken == '' ? selectAreaOfConcern : addMoreCategory}
        />
      </View>
      <CustomLoader showLoader={showLoader} />
      <UnselectService
        visible={openUnselectServiceModal}
        setVisibility={setOpenUnselectServiceModal}
        text1={text1}
        values={values}
        text2={text2}
        next={userToken == '' ? nextSelectService : nextAddMore}
        // deleteSlot={() => deleteSlot(selectedItem?.serviceUnavailableId)}
      />
    </View>
  );
};

export default SelectAreaOfConcern;
