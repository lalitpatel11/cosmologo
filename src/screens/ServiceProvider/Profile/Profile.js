//react components
import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, Image, StyleSheet} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import ProfileCard from 'components/ProfileCard/ProfileCard';
import TitleViewAll from 'components/TitleViewAll/TitleViewAll';
import AppointmentCard from 'components/AppointmentCard/AppointmentCard';
import {
  PaymentReceivedCard,
  TotalCmpltAppointCard,
} from 'components/ProfileCard/ProfileComponents';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import CustomLoaderLogout from 'components/CustomLoader/CustomLoaderLogout';
//globals
import {Images, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ProfileStyle';
//redux
import {connect, useDispatch} from 'react-redux';
import {logOutUser} from 'src/reduxToolkit/reducer/user';
import {clearCart} from 'src/reduxToolkit/reducer/cart';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
//third parties
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useNetInfo} from "@react-native-community/netinfo";
import Toast from 'react-native-simple-toast';

const Profile = ({navigation, userToken, userInfo}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const dispatch = useDispatch();
  //states
  const [TotalPayment, setTotalPayment] = useState(0);
  const [TotalAppointment, setTotalAppointment] = useState(0);
  const [upcomingAppointData, setUpcomingAppointData] = useState([]);
  const [showLoader, setShowLoader] = useState(false);
  const [showLoaderLogout, setShowLoaderLogout] = useState(false);
  const [completedAppointments, setCompletedAppointments] = useState('');
  //function : navigation function
  const gotoPaymentListing = () =>
    navigation.navigate(ScreenNames.SERVICE_PAYMENT_LISTING);
  const gotoCompleteAppointListing = () =>
    navigation.navigate(ScreenNames.SERVICE_CMPLETE_APPOINT_LISTING);
  const gotoEditProfile = () => {
    navigation.navigate(ScreenNames.SERVICE_EDIT_PROFILE);
  }
  const gotoChangePassword = () =>
    navigation.navigate(ScreenNames.SERVICE_CHANGE_PASSWORD);
  const gotoWelcome = () =>
  navigation.reset({
    index: 0,
    routes: [{name: ScreenNames.WELCOME}],
  });
  //function : service function
  const getTotalPaymentReceived = async () => {
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.TOTAL_PAYMENT_RECEIVED,
      );
      if (resp.data.status) {
        setTotalPayment(resp.data.total_payment);
      }
    } catch (error) {
      console.log('error in getTotalPaymentReceived', error);
    }
  };
  const getTotalAppointment = async () => {
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.TOTAL_COMPLETED_APPOINTMENT,
      );
      if (resp.data.status) {
        setTotalAppointment(resp.data.total_completed_appointment);
      }
    } catch (error) {
      console.log('error in getTotalAppointment', error);
    }
  };
  const getUpcomingAppointment = async () => {
    try {
      const resp = await Service.postApiWithToken(
        userToken,
        Service.UPCOMING_BOOKED_APPOINTMENT,
        {},
      );
      if (resp.data.status) {
        setUpcomingAppointData(resp.data.data);
      }
    } catch (error) {
      console.log('error in getUpcomingAppointment', error);
    }
  };
  const getCompletedAppointmentCount = async () => {
    setShowLoader(true)
    try {
      const apptHistoryData = new FormData();
      apptHistoryData.append('serviceproviderid', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SERVICE_PROVIDER_COMPLETED_APPOINTMENT_COUNT,
        apptHistoryData,
      );
      if (resp.data.status) {
        setCompletedAppointments(resp.data.count)
        // setApptHistoryData(resp.data.data);
        // console.warn('resp----->', resp.data.data);
      }
    } catch (error) {
      console.log('error in getAppontsHistory', error);
    }
    setShowLoader(false)
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      // getTotalPaymentReceived();
      // getTotalAppointment();
      // getUpcomingAppointment();
      getCompletedAppointmentCount();
    });
    return unsubscribe;
  }, [navigation]);
  //function : service function
  const logoutDirectly = async () => {
    gotoWelcome();
    dispatch(logOutUser());
    // dispatch(clearCart());
    await AsyncStorage.clear();
  };
  //function : service function
  const logout = async () => {
    setShowLoaderLogout(true);
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.USER_LOGOUT,
      );
      if (resp?.data?.status) {
        // don't need to show message after successfully logging out
        // dispatch(showToast({text: resp.data.message}));
        gotoWelcome();
        dispatch(logOutUser());
        dispatch(clearCart());
        await AsyncStorage.clear();
      }
    } catch (error) {
      console.log('error in logout', error);
    }
    setShowLoaderLogout(false);
  };
  const gotoUpcomingAppointment = () => {
    if(userInfo?.is_active == 0){
      // dispatch(showToast({text: 'Your account is not approved by administrator.', duration: 1000}));
      Toast.show('Your account is not approved by administrator.', Toast.SHORT)
      return
    }
    navigation.navigate(ScreenNames.SERVICE_UPCOMING_APPOINTMENT_LIST)
  }
  //UI
  return (
    <View style={styles.container}>
      <MyHeader hasDrawerButton={true} Title="Profile" />
      <ScrollView
        contentContainerStyle={{paddingBottom: 100}}
        style={styles.mainView}>
        <View style={{height: 50}} />
        <ProfileCard
          Email={userInfo.email}
          // Phone={userInfo?.country_code ? `(${userInfo.country_code}) -${userInfo.phone}` : userInfo.phone}
          Phone={`${userInfo?.country_code} (${userInfo?.phone?.substring(0,3)}) ${userInfo?.phone?.substring(3,6)}-${userInfo?.phone?.substring(6)}`}
          ProfileImageUrl={userInfo?.profile_image}
          editProfilePress={gotoEditProfile}
          changePasswordPress={gotoChangePassword}
          logout={logout}
        />
        {/* <PaymentReceivedCard
          Payment={TotalPayment}
          onPress={gotoPaymentListing}
        /> */}
        <TotalCmpltAppointCard
          Appointment={completedAppointments}
          // onPress={gotoCompleteAppointListing}
        />
        <TitleViewAll Title="Upcoming Appointments" onPress={gotoUpcomingAppointment}/>
        {upcomingAppointData.length > 0
          ? upcomingAppointData.map((item, index) => {
              return <AppointmentCard />;
            })
          : null}
        {/* <TitleViewAll Title={`${completedAppointments} Appointments Completed`} />  */}
      </ScrollView>
      <CustomLoader showLoader={showLoader}/>
      <CustomLoaderLogout showLoader={showLoaderLogout}/>
    </View>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
  userInfo: state.user.userInfo,
});
export default connect(mapStateToProps, null)(Profile);
