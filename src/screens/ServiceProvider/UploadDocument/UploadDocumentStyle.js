import {Constant, Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  AppIcon: {
    width: Constant.width / 1.5,
    height: Constant.width / 2,
    alignSelf: 'center',
  },
  bottomSection: {
    padding: 20,
  },
  docImageStyle: {
    height: 100,
    width: 100,
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    // borderWidth: 2,
    // borderColor: Colors.THEME_GREEN,
    marginVertical: 10,
    // borderRadius: 150 / 2,
  },
  docAddButtonStyle: {
    // position: 'absolute',
    backgroundColor: Colors.WHITE,
    borderRadius: 25,
    padding: 5,
    height:40,
    width:40,
    justifyContent:'center',
    alignItems:'center',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    backgroundColor: Colors.WHITE,
    shadowRadius: 15,
    elevation: 2,
    borderRadius: 20,
  },
});
