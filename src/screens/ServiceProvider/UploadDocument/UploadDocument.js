//react components
import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, Image, Alert, KeyboardAvoidingView, Platform, TouchableOpacity, PermissionsAndroid} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyText from 'components/MyText/MyText';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyButton from 'components/MyButton/MyButton';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import DateSelector from 'components/DateSelector/DateSelector';
import SelectImageSource from 'modals/SelectImageSource/SelectImageSource';
import DocumentViewer from 'modals/DocumentViewer/DocumentViewer';
//third parties
import moment from 'moment';
import DatePicker from 'react-native-date-picker';
import DocumentPicker from 'react-native-document-picker';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from "@react-native-community/netinfo";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
//global
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './UploadDocumentStyle';
import DeleteCertification from 'modals/DeleteCertification/DeleteCertification';

const UploadDocument = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const userId = route.params.userId;
  //states
  const [document1, setDocument1] = useState('');
  const [document2, setDocument2] = useState('');
  const [document3, setDocument3] = useState('');
  const [insurance, setInsurance] = useState('');
  const [taxID, setTaxID] = useState('');
  const [expiryDate1, setExpiryDate1] = useState(new Date());
  const [openExpiryDate1, setOpenExpiryDate1] = useState(false);
  const [expiryDate2, setExpiryDate2] = useState(new Date());
  const [openExpiryDate2, setOpenExpiryDate2] = useState(false);
  const [expiryDate3, setExpiryDate3] = useState(new Date());
  const [openExpiryDate3, setOpenExpiryDate3] = useState(false);
  const [date, setDate] = useState(new Date());
  const [issueDate1, setIssueDate1] = useState(new Date());
  const [openIssueDate1, setOpenIssueDate1] = useState(false);
  const [issueDate2, setIssueDate2] = useState(new Date());
  const [openIssueDate2, setOpenIssueDate2] = useState(false);
  const [issueDate3, setIssueDate3] = useState(new Date());
  const [openIssueDate3, setOpenIssueDate3] = useState(false);
  const [whichIndex, setWhichIndex] = useState(null);
  const [openDeleteCertModal, setOpenDeleteCertModal] = useState(false);
  //states : modal states
  const [showLoader, setShowLoader] = useState(false);
  const [showImageSourceModal, setShowImageSourceModal] = useState(false);
  const [showDocumenteModal, setShowDocumenteModal] = useState(false);
  const [showImage, setShowImage] = useState(false);
  const [selectedDocument, setSelectedDocument] = useState('');
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //function : navigation function
  const gotoWelcome = () => navigation.navigate(ScreenNames.WELCOME);
  const gotoSelectService = () =>
    navigation.replace(ScreenNames.SERVICE_SELECT_SERVICE, {userId});
  const gotoSignServiceProvider = () =>
    navigation.navigate(ScreenNames.SERVICE_SIGN_IN);
  //function : imp function
  const openDocument = async setValue => {
    try {
      const resp = await DocumentPicker.pickSingle({
        // type: [DocumentPicker.types.allFiles]
        type: [DocumentPicker.types.images],
      });
      setValue(resp);
    } catch (error) {
      console.log('error in openDocument', error);
    }
  };
  const Validation = () => {
    if (document1 == '') {
      // Alert.alert('', 'Please upload certification 1');
      Toast.show('Please upload certification 1', Toast.SHORT)
    } 
    else if(document1 !== '' && moment(issueDate1).format('YYYY-MM-DD') == moment(new Date()).format('YYYY-MM-DD')){
      Toast.show('Please select Issue Date for certification 1', Toast.SHORT)
    } 
    else if(document1 !== '' && moment(expiryDate1).format('YYYY-MM-DD') == moment(new Date()).format('YYYY-MM-DD')){
      Toast.show('Please select Expiry Date for certification 1', Toast.SHORT)
    } 
    else if(document2 !== '' && moment(issueDate2).format('YYYY-MM-DD') == moment(new Date()).format('YYYY-MM-DD')){
      Toast.show('Please select Issue Date for certification 2', Toast.SHORT)
    } 
    else if(document2 !== '' && moment(expiryDate2).format('YYYY-MM-DD') == moment(new Date()).format('YYYY-MM-DD')){
      Toast.show('Please select Expiry Date for certification 2', Toast.SHORT)
    } 
    else if(document3 !== '' && moment(issueDate3).format('YYYY-MM-DD') == moment(new Date()).format('YYYY-MM-DD')){
      Toast.show('Please select Issue Date for certification 3', Toast.SHORT)
    } 
    else if(document3 !== '' && moment(expiryDate3).format('YYYY-MM-DD') == moment(new Date()).format('YYYY-MM-DD')){
      Toast.show('Please select Expiry Date for certification 3', Toast.SHORT)
    } 
    else if (insurance == '') {
      // Alert.alert('', 'Please enter insurance');
      Toast.show('Please enter insurance', Toast.SHORT)
    } else if (taxID == '') {
      // Alert.alert('', 'Please enter tax id');
      Toast.show('Please enter tax id', Toast.SHORT)
    } else return true;
  };
  //function : service function
  const uploadDocument = async () => {
    if (Validation()) {
      setShowLoader(true);
      try {
        const DocumentData = new FormData();
        DocumentData.append('service_provider_id', userId);
        const imageName1 = document1.uri.slice(
          document1.uri.lastIndexOf('/'),
          document1.uri.length,
        );
        DocumentData.append('certification1', {
          name: imageName1,
          type: document1.type,
          uri: document1.uri,
          // expiryDate: moment(expiryDate1).format('YYYY-MM-DD'),
          // issueDate: moment(issueDate1).format('YYYY-MM-DD')
        });
        if (document2 != '') {
          const imageName = document2.uri.slice(
            document2.uri.lastIndexOf('/'),
            document2.uri.length,
          );
          DocumentData.append('certification2', {
            name: imageName,
            type: document2.type,
            uri: document2.uri,
            // expiryDate: moment(expiryDate2).format('YYYY-MM-DD'),
            // issueDate: moment(issueDate2).format('YYYY-MM-DD')
          });
        }
        if (document3 != '') {
          const imageName = document3.uri.slice(
            document3.uri.lastIndexOf('/'),
            document3.uri.length,
          );
          DocumentData.append('certification3', {
            name: imageName,
            type: document3.type,
            uri: document3.uri,
            // expiryDate: moment(expiryDate3).format('YYYY-MM-DD'),
            // issueDate: moment(issueDate3).format('YYYY-MM-DD')
          });
        }
        if (document1 != '') {
          DocumentData.append('issueDate1', moment(issueDate1).format('YYYY-MM-DD'));
          DocumentData.append('expiryDate1', moment(expiryDate1).format('YYYY-MM-DD'));
        }
        if (document2 != '') {
          DocumentData.append('issueDate2', moment(issueDate2).format('YYYY-MM-DD'));
          DocumentData.append('expiryDate2', moment(expiryDate2).format('YYYY-MM-DD'));
        }
        if (document3 != '') {
          DocumentData.append('issueDate3', moment(issueDate3).format('YYYY-MM-DD'));
          DocumentData.append('expiryDate3', moment(expiryDate3).format('YYYY-MM-DD'));
        }

        DocumentData.append('insurance', insurance);
        DocumentData.append('tax_id', taxID);

        console.log('DocumentData', DocumentData);
        const resp = await Service.postApi(
          Service.SERVICE_PROVIDER_DOCUMENT,
          DocumentData,
        );
        console.log('DocumentData resp', resp);
        if (resp.data.status) {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT)
          gotoSelectService();
        } else {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT)
        }
      } catch (error) {
        console.log('error in uploadDocument', error);
      }
      setShowLoader(false);
    }
  };
  //function : imp function
  const openLibrary = () => {
    let options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose Photo from Custom Option',
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, response => {
      if (response.didCancel) {
        // Alert.alert('User cancelled camera picker');
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'User cancelled picking image'}));
        Toast.show('User cancelled picking image', Toast.SHORT)
        // Alert.alert('User cancelled picking image');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Camera not available on device'}));
        Toast.show('Camera not available on device', Toast.SHORT)
        // Alert.alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Permission not satisfied'}));
        Toast.show('Permission not satisfied', Toast.SHORT)
        // Alert.alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: response.errorMessage}));
        Toast.show(response.errorMessage, Toast.SHORT)
        // Alert.alert(response.errorMessage);
        return;
      }
      console.log('Response = ', response.assets[0]);
      if(whichIndex === 0){
        setDocument1(response.assets[0])
      }else if(whichIndex === 1){
        setDocument2(response.assets[0])
      }else if(whichIndex === 2){
        setDocument3(response.assets[0])
      }
      setShowImageSourceModal(false);
    });
  };
  //function : imp function
  const openCamera = () => {
    const options = {
      width: 1080,
      height: 1080,
      cropping: true,
      mediaType: 'photo',
      compressImageQuality: 1,
      compressImageMaxHeight: 1080 / 2,
      compressImageMaxWidth: 1080 / 2,
    };
    launchCamera(options, response => {
      if (response.didCancel) {
        // Alert.alert('User cancelled camera picker');
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'User cancelled picking image'}));
        Toast.show('User cancelled picking image', Toast.SHORT)
        // Alert.alert('User cancelled picking image');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Camera not available on device'}));
        Toast.show('Camera not available on device', Toast.SHORT)
        // Alert.alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Permission not satisfied'}));
        Toast.show('Permission not satisfied', Toast.SHORT)
        // Alert.alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: response.errorMessage}));
        Toast.show('response.errorMessage', Toast.SHORT)
        // Alert.alert(response.errorMessage);
        return;
      }
      console.log('Response = ', response.assets[0]);
      if(whichIndex === 0){
        setDocument1(response.assets[0])
      }else if(whichIndex === 1){
        setDocument2(response.assets[0])
      }else if(whichIndex === 2){
        setDocument3(response.assets[0])
      }
      // setFilePath(response.assets[0]);
      setShowImageSourceModal(false);
    });
  };
  //function : imp function
  const checkCameraPermission = async () => {
    if (Platform.OS === 'ios') {
      openCamera();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission Required',
            message:
              'Application needs access to your camera to click your profile image',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          openCamera();
          console.log('Camera Permission Granted.');
        } else {
          // dispatch(showToast({Text: 'Camera Permission Not Granted'}));
          Toast.show('Camera Permission Not Granted', Toast.SHORT)
          // Alert.alert('Error', 'Camera Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('ERROR' + err);
      }
    }
  };
  const deleteCertificate = () => {
    if(whichIndex === 0){
      setDocument1('')
    }else if(whichIndex === 1){
      setDocument2('')
    }else if(whichIndex === 2){
      setDocument3('')
    }
    setOpenDeleteCertModal(false)
  }
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Upload Documents" />
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        >
        <ScrollView style={styles.mainView} contentContainerStyle={{paddingBottom: 50}}>
          <Image
            resizeMode="contain"
            source={Images.AppIcon}
            style={styles.AppIcon}
          />
          {document1 != '' ?
          <View>
          <MyText text="Certification 1" fontFamily="bold" /> 
          <View style={{flexDirection:'row', justifyContent:'space-between', backgroundColor:'#fff', borderRadius:10, marginTop:10, paddingHorizontal:20, paddingVertical:10}}>
            <View style={styles.docImageStyle}>
            <Image
              resizeMode="contain"
              source={{uri: document1.uri}}
              style={{
                height: '100%',
                width: '100%',
                borderRadius: 100 / 2,
              }}
            />
            </View>
            <View style={{flexDirection:'row', alignItems:'center',}}>
            <TouchableOpacity
              // onPress={chooseFile}
              onPress={()=>{setShowImage(true);setSelectedDocument(document1?.uri);setShowDocumenteModal(true)}}
              style={[styles.docAddButtonStyle, {marginRight:20,}]}>
              <MyIcon.Entypo
                name="eye"
                // color={expiryStatuses[index2] || approvedStatuses[index2] == 0 ? Colors.THEME_GREEN : Colors.LITE_GREY}
                color={Colors.THEME_GREEN}
                size={24}
                />
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={chooseFile}
              onPress={() => {setWhichIndex(0);setShowImageSourceModal(true);}}
              style={[styles.docAddButtonStyle, {marginRight:20}]}>
              <MyIcon.MaterialCommunityIcons
                name="pencil-outline"
                color={Colors.THEME_GREEN}
                size={24}
                />
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={chooseFile}
              onPress={() => {setWhichIndex(0);setOpenDeleteCertModal(true);}}
              style={[styles.docAddButtonStyle, {}]}>
              <MyIcon.MaterialIcons
                name="delete-outline"
                color={Colors.RED}
                size={24}
              />
            </TouchableOpacity>
            </View>
          </View>
          </View>
          :
          <MyTextInput
          placeholder={
              document1 == '' ? 'Upload Certification 1' : document1.name
            }
            // onPress={() => openDocument(setDocument1)}
            onPress={() => {setWhichIndex(0);setShowImageSourceModal(true);}}
            disabled={false}
            editable={false}
            Icon={
              <MyIcon.AntDesign
                name="upload"
                size={24}
                color={Colors.THEME_GREEN}
              />
            }
            />
          }
          <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
            <DateSelector
              Title={
                moment(issueDate1).format('YYYY-MM-DD') ==
                moment(new Date()).format('YYYY-MM-DD')
                  ? 'Issue Date'
                  : // : moment(date).format('MMMM Do YYYY')
                    moment(issueDate1).format('DD-MM-YYYY')
              }
              onPress={() => setOpenIssueDate1(true)}
              calenderViewStyle={{width:'48%'}}
              dateViewStyle={{borderWidth:0}}
            />
            <DateSelector
              Title={
                moment(expiryDate1).format('YYYY-MM-DD') ==
                moment(new Date()).format('YYYY-MM-DD')
                  ? 'Expiry Date'
                  : // : moment(date).format('MMMM Do YYYY')
                    moment(expiryDate1).format('DD-MM-YYYY')
              }
              onPress={() => setOpenExpiryDate1(true)}
              calenderViewStyle={{width:'48%'}}
              dateViewStyle={{borderWidth:0}}
            />
            </View>
            {document2 != '' ?
          <View>
          <MyText text="Certification 2" fontFamily="bold" marginTop={10}/> 
          <View style={{flexDirection:'row', justifyContent:'space-between', backgroundColor:'#fff', borderRadius:10, marginTop:10, paddingHorizontal:20, paddingVertical:10}}>
            <View style={styles.docImageStyle}>
            <Image
              resizeMode="contain"
              source={{uri: document2.uri}}
              style={{
                height: '100%',
                width: '100%',
                borderRadius: 100 / 2
              }}
            />
            </View>
            <View style={{flexDirection:'row', alignItems:'center',}}>
            <TouchableOpacity
              // onPress={chooseFile}
              onPress={()=>{setShowImage(true);setSelectedDocument(document2?.uri);setShowDocumenteModal(true)}}
              style={[styles.docAddButtonStyle, {marginRight:20,}]}>
              <MyIcon.Entypo
                name="eye"
                // color={expiryStatuses[index2] || approvedStatuses[index2] == 0 ? Colors.THEME_GREEN : Colors.LITE_GREY}
                color={Colors.THEME_GREEN}
                size={24}
                />
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={chooseFile}
              onPress={() => {setWhichIndex(1);setShowImageSourceModal(true);}}
              style={[styles.docAddButtonStyle, {marginRight:20}]}>
              <MyIcon.MaterialCommunityIcons
                name="pencil-outline"
                color={Colors.THEME_GREEN}
                size={24}
                />
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={chooseFile}
              onPress={() => {setWhichIndex(1);setOpenDeleteCertModal(true);}}
              style={[styles.docAddButtonStyle, {}]}>
              <MyIcon.MaterialIcons
                name="delete-outline"
                color={Colors.RED}
                size={24}
              />
            </TouchableOpacity>
            </View>
          </View>
          </View>
          :
          <MyTextInput
            placeholder={
              document2 == '' ? 'Upload Certification 2' : document2.name
            }
            // onPress={() => openDocument(setDocument2)}
            onPress={() => {setWhichIndex(1);setShowImageSourceModal(true);}}
            disabled={false}
            editable={false}
            Icon={
              <MyIcon.AntDesign
                name="upload"
                size={24}
                color={Colors.THEME_GREEN}
              />
            }
          />
          }
          <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
            <DateSelector
              Title={
                moment(issueDate2).format('YYYY-MM-DD') ==
                moment(new Date()).format('YYYY-MM-DD')
                  ? 'Issue Date'
                  : // : moment(date).format('MMMM Do YYYY')
                    moment(issueDate2).format('DD-MM-YYYY')
              }
              onPress={() => setOpenIssueDate2(true)}
              calenderViewStyle={{width:'48%'}}
              dateViewStyle={{borderWidth:0}}
            />
            <DateSelector
              Title={
                moment(expiryDate2).format('YYYY-MM-DD') ==
                moment(new Date()).format('YYYY-MM-DD')
                  ? 'Expiry Date'
                  : // : moment(date).format('MMMM Do YYYY')
                    moment(expiryDate2).format('DD-MM-YYYY')
              }
              onPress={() => setOpenExpiryDate2(true)}
              calenderViewStyle={{width:'48%'}}
              dateViewStyle={{borderWidth:0}}
            />
            </View>
            {document3 != '' ?
          <View>
          <MyText text="Certification 3" fontFamily="bold" marginTop={10}/> 
          <View style={{flexDirection:'row', justifyContent:'space-between', backgroundColor:'#fff', borderRadius:10, marginTop:10, paddingHorizontal:20, paddingVertical:10}}>
            <View style={styles.docImageStyle}>
            <Image
              resizeMode="contain"
              source={{uri: document3.uri}}
              style={{
                height: '100%',
                width: '100%',
                borderRadius: 100 / 2
              }}
            />
            </View>
            <View style={{flexDirection:'row', alignItems:'center',}}>
            <TouchableOpacity
              // onPress={chooseFile}
              onPress={()=>{setShowImage(true);setSelectedDocument(document3?.uri);setShowDocumenteModal(true)}}
              style={[styles.docAddButtonStyle, {marginRight:20,}]}>
              <MyIcon.Entypo
                name="eye"
                // color={expiryStatuses[index2] || approvedStatuses[index2] == 0 ? Colors.THEME_GREEN : Colors.LITE_GREY}
                color={Colors.THEME_GREEN}
                size={24}
                />
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={chooseFile}
              onPress={() => {setWhichIndex(2);setShowImageSourceModal(true);}}
              style={[styles.docAddButtonStyle, {marginRight:20}]}>
              <MyIcon.MaterialCommunityIcons
                name="pencil-outline"
                color={Colors.THEME_GREEN}
                size={24}
                />
            </TouchableOpacity>
            <TouchableOpacity
              // onPress={chooseFile}
              onPress={() => {setWhichIndex(2);setOpenDeleteCertModal(true);}}
              style={[styles.docAddButtonStyle, {}]}>
              <MyIcon.MaterialIcons
                name="delete-outline"
                color={Colors.RED}
                size={24}
              />
            </TouchableOpacity>
            </View>
          </View>
          </View>
          :  
          <MyTextInput
            placeholder={
              document3 == '' ? 'Upload Certification 3' : document3.name
            }
            // onPress={() => openDocument(setDocument3)}
            onPress={() => {setWhichIndex(2);setShowImageSourceModal(true);}}
            disabled={false}
            editable={false}
            Icon={
              <MyIcon.AntDesign
                name="upload"
                size={24}
                color={Colors.THEME_GREEN}
              />
            }
          />
          }
          <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
            <DateSelector
              Title={
                moment(issueDate3).format('YYYY-MM-DD') ==
                moment(new Date()).format('YYYY-MM-DD')
                ? 'Issue Date'
                : // : moment(date).format('MMMM Do YYYY')
                moment(issueDate3).format('DD-MM-YYYY')
              }
              onPress={() => setOpenIssueDate3(true)}
              calenderViewStyle={{width:'48%'}}
              dateViewStyle={{borderWidth:0}}
            />
            <DateSelector
              Title={
                moment(expiryDate3).format('YYYY-MM-DD') ==
                moment(new Date()).format('YYYY-MM-DD')
                  ? 'Expiry Date'
                  : // : moment(date).format('MMMM Do YYYY')
                    moment(expiryDate3).format('DD-MM-YYYY')
              }
              onPress={() => setOpenExpiryDate3(true)}
              calenderViewStyle={{width:'48%'}}
              dateViewStyle={{borderWidth:0}}
            />
            </View>
          <MyTextInput
            placeholder="Enter Insurance"
            onChangeText={text => setInsurance(text)}
          />
          <MyTextInput
            placeholder="Enter Tax ID"
            onChangeText={text => setTaxID(text)}
          />
        </ScrollView>
      </KeyboardAvoidingView>
      {/* {Platform.OS === 'ios' ?  */}
      <View style={{height: '5%'}} />
      {/* :null} */}
      <View style={styles.bottomSection}>
        <MyButton Title="Submit" onPress={uploadDocument} />
      </View>
      <DatePicker
        modal
        mode="date"
        // mode="time"
        open={openExpiryDate1}
        date={expiryDate1}
        onConfirm={time => {
          setOpenExpiryDate1(false);
          setExpiryDate1(time);
        }}
        onCancel={() => {
          setOpenExpiryDate1(false);
        }}
        minimumDate={new Date()}
      />
      <DatePicker
        modal
        mode="date"
        // mode="time"
        open={openExpiryDate2}
        date={expiryDate2}
        onConfirm={time => {
          setOpenExpiryDate2(false);
          setExpiryDate2(time);
        }}
        onCancel={() => {
          setOpenExpiryDate2(false);
        }}
        minimumDate={new Date()}
      />
      <DatePicker
        modal
        mode="date"
        // mode="time"
        open={openExpiryDate3}
        date={expiryDate3}
        onConfirm={time => {
          setOpenExpiryDate3(false);
          setExpiryDate3(time);
        }}
        onCancel={() => {
          setOpenExpiryDate3(false);
        }}
        minimumDate={new Date()}
      />
      <DatePicker
        modal
        mode="date"
        open={openIssueDate1}
        date={issueDate1}
        onConfirm={date => {
          setOpenIssueDate1(false);
          setIssueDate1(date);
        }}
        onCancel={() => {
          setOpenIssueDate1(false);
        }}
        maximumDate={new Date()}
      />
      <DatePicker
        modal
        mode="date"
        open={openIssueDate2}
        date={issueDate2}
        onConfirm={date => {
          setOpenIssueDate2(false);
          setIssueDate2(date);
        }}
        onCancel={() => {
          setOpenIssueDate2(false);
        }}
        maximumDate={new Date()}
      />
      <DatePicker
        modal
        mode="date"
        open={openIssueDate3}
        date={issueDate3}
        onConfirm={date => {
          setOpenIssueDate3(false);
          setIssueDate3(date);
        }}
        onCancel={() => {
          setOpenIssueDate3(false);
        }}
        maximumDate={new Date()}
      />
      <CustomLoader showLoader={showLoader} />
      <SelectImageSource
        visible={showImageSourceModal}
        setVisibility={setShowImageSourceModal}
        openLibrary={openLibrary}
        openCamera={checkCameraPermission}
      />
      <DeleteCertification
        visible={openDeleteCertModal}
        setVisibility={setOpenDeleteCertModal}
        deleteCertificate={deleteCertificate}
        // deleteSlot={() => deleteSlot(selectedItem?.serviceUnavailableId)}
      />
      <DocumentViewer
        visible={showDocumenteModal}
        setVisibility={setShowDocumenteModal}
        showImage={showImage}
        selectedDocument={selectedDocument}
      />
    </View>
  );
};

export default UploadDocument;
