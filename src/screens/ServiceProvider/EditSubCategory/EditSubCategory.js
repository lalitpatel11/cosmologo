//react components
import React from 'react';
import {FlatList, ScrollView, TouchableOpacity, View} from 'react-native';
//custom components
import ServiceProfile from 'components/ServiceCard/ServiceProfile';
import ImageSelector from 'components/ServiceCard/ImageSelector';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyText from 'components/MyText/MyText';
import MyHeader from 'components/MyHeader/MyHeader';
//globals
import {Colors, MyIcon} from 'global/Index';
//styles
import {styles} from './EditSubCategoryStyle';
import MyButton from 'components/MyButton/MyButton';

const EditSubCategory = () => {
  //data
  const data = [1, 2, 3, 4];
  //function : render function
  const imageRender = ({item, index}) => {
    return <ServiceProfile />;
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Edit Sub Category" />
      <ScrollView
        contentContainerStyle={{paddingBottom: 50}}
        style={styles.mainView}>
        <ServiceProfile />
        <ImageSelector />
        <FlatList
          horizontal
          data={data}
          renderItem={imageRender}
          keyExtractor={(item, index) => index.toString()}
          showsHorizontalScrollIndicator={false}
        />
        <ImageSelector />
        <MyText text="Service Cost" fontSize={16} fontFamily="bold" />
        <View style={styles.serviceCostView}>
          <MyText text="$ 299.78" />
          <TouchableOpacity style={styles.changeIconView}>
            <MyIcon.Feather name="edit" color={Colors.THEME_GREEN} />
            <MyText
              text="Change Cost"
              textColor="theme_green"
              fontSize={12}
              marginHorizontal={5}
              fontFamily="medium"
            />
          </TouchableOpacity>
        </View>
        <MyText text="Procedure Descrition" fontSize={16} fontFamily="bold" />
        <MyTextInput
          placeholder="Lorem ipsum kjsfdu kgfds kldjfg kldfhg kljdfhg ilfdg lgf gjdfjbgljkdg gebgiug gbrijgberiug er irfdklgrg rkhoiwefgb f kusdfyg uksfy jsadfrgsf ksghdiar yuierwt lonvci saiot afhuk aewrb jxcbv"
          multiline={true}
        />
        <View style={styles.flexRowView}>
          <MyButton
            Title="Cancel"
            width="48%"
            borderColor={Colors.THEME_GREEN}
          />
          <MyButton Title="Save Details" width="48%" />
        </View>
      </ScrollView>
    </View>
  );
};

export default EditSubCategory;
