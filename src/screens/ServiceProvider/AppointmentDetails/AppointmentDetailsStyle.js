import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  serviceView: {
    backgroundColor: Colors.WHITE,
    padding: 10,
    paddingVertical: 20,
    borderRadius: 10,
  },
  imageStyle: {
    height: 70,
    width: 50,
    borderRadius: 10,
  },
  flexRowStyle: {
    flexDirection: 'row',
  },
  textView: {
    marginLeft: 10,
  },
  paymentSuccssTextView: {
    backgroundColor: Colors.BG_GREEN,
    padding: 5,
    paddingVertical: 10,
    marginVertical: 10,
  },
  generateInvoiceBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: Colors.THEME_GREEN,
    alignSelf: 'flex-start',
    paddingHorizontal: 10,
    padding: 5,
    borderRadius: 5,
  },
  manageStatusView: {
    backgroundColor: Colors.WHITE,
    padding: 10,
    marginVertical: 20,
    borderRadius: 10,
  },
  locationView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  dropdown: {
    // flex:0.85,
    height: 50,
    borderColor: Colors.LITE_GREY,
    borderWidth: 0.5,
    borderRadius: 8,
    paddingHorizontal: 8,
    width:'75%'
  },
});
