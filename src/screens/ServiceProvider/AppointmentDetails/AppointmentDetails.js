//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  PermissionsAndroid,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyText from 'components/MyText/MyText';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyButton from 'components/MyButton/MyButton';
import DoctorCard from 'components/DoctorCard/DoctorCard';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import {IconListCard} from '../Home/HomeComponents';
//third parties
import Toast from 'react-native-simple-toast';
import {Dropdown} from 'react-native-element-dropdown';
import RNFetchBlob from 'rn-fetch-blob';
import moment from 'moment';
import {useNetInfo} from "@react-native-community/netinfo";
//globals
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './AppointmentDetailsStyle';
//redux
import {useDispatch, useSelector} from 'react-redux';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import UserCard from 'components/UserCard/UserCard';
import CancelAppointment from 'modals/CancelAppointment/CancelAppointment';
import VerifyApptOtp from 'modals/VerifyApptOtp/VerifyApptOtp';
import ConfirmBooking from 'modals/ConfirmBooking/ConfirmBooking';
import VerifyBooking from 'modals/VerifyBooking/VerifyBooking';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const AppointmentDetails = ({route, navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const appointment_id = route.params.appointment_id;
  const status = route.params.status;
  const isVerified = route.params.isVerified;
  const service_time = route.params.service_time;
  const approvedStatus = route.params?.approvedStatus || '';
  const disableManageStatus = route.params?.disableManageStatus || '';
  //variables : redux variables
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [value, setValue] = useState(null);
  const [isFocus, setIsFocus] = useState(false);
  const [AppmtStatusData, setAppmtStatusData] = useState([]);
  const [appointmentStatus, setAppointmentStatus] = useState('');
  const [detailData, setDetailData] = useState({});
  //states : modal states
  const [showVerifyAppt, setShowVerifyAppt] = useState(false);
  const [showCancelAppt, setShowCancelAppt] = useState(false);
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //function : service function
  const getAppointmentStatusList = async () => {
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.APPOINTMENT_STATUS_LIST,
      );
      if (resp.data.status) {
        // console.warn('resp---->', resp.data);
        // for unverfied appointment, don't show completed status
        // const updateData = isVerified == 1 ? resp.data.data : resp.data.data?.filter(el=>el?.id != 4)
        console.log('setAppmtStatusData', resp.data.data);
        const updateData = !status
          ? resp.data.data?.filter(el => el?.id == 4)
          : resp.data.data?.filter(el => el?.id != 4);
        setAppmtStatusData(updateData);
        setValue(updateData.find(el => el?.title === status)?.id);
        setAppointmentStatus(status);
        // getAppointmentStatus(resp.data.data)
      }
    } catch (error) {
      console.log('error in getAppointmentStatusList', error);
    }
    setShowLoader(false);
  };
  //function : service function
  const getAppointmentStatus = async statusData => {
    try {
      const myData = new FormData();
      myData.append('appointment_id', appointment_id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.CHECK_APPOINTMENT_STATUS,
        myData,
      );
      if (resp.data.status) {
        console.warn('resp---->', resp);
        setValue(
          statusData.find(el => el?.title === resp?.data?.data?.title)?.id,
        );
      }
    } catch (error) {
      console.log('error in getAppointmentStatus', error);
    }
    setShowLoader(false);
  };
  const getAppointmentDeatails = async () => {
    setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('appointment_id', appointment_id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SERVICE_PROVIDER_APPOINTMENT_DETAIL,
        myData,
      );
      if (resp.data.status) {
        const detailTime = resp.data.data.date_time.replace('PM', '');
        const detailTime2 = moment(
          moment(detailTime).format('dddd,D MMMM YYYY,kk:mm'),
        ).format('ddd, D MMMM YYYY, h:mm A');
        setDetailData({...resp.data.data, date_time: detailTime2});
        console.log('setDetailData', resp.data.data);
        console.warn('resp--->', resp.data);
      }
    } catch (error) {
      console.log('error in getAppointmentDeatails', error);
    }
  };
  const cancelAppointment = async (
    appointmentId,
    appointmentDate,
    bookingId,
    isCancelled,
  ) => {
    // if(isVerified == 1){
    //   Toast.show('Verified appointment cannot be cancelled', Toast.SHORT)
    //   return
    // }
    if (isCancelled == 4) {
      Toast.show('Appointment already cancelled by User', Toast.SHORT);
      return;
    }
    if (
      moment(moment(appointmentDate).format('YYYY-MM-DD')).isBefore(
        moment().format('YYYY-MM-DD'),
      )
    ) {
      Toast.show('Appointment date cannot be in past', Toast.SHORT);
      return;
    }
    !showLoader && setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('appointment_id', appointmentId);
      myData.append('roletype', 'S');
      myData.append('booking_id', bookingId);
      console.log('myData', myData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.CANCEL_APPOINTMENT_USER_SERVICE,
        myData,
      );
      console.log('resp', resp);
      if (resp.data.status) {
        Toast.show(resp.data.message, Toast.SHORT);
        // setFilteredData(filteredData.filter(el => el.id !== appointmentId))
        // setAppointmentData(AppointmentData.filter(el => el.id !== appointmentId))
        // getOngoingAppnt()
        console.log('cancelAppointment', resp.data);
        console.warn('resp------>', resp.data);
      }
    } catch (error) {
      console.log('error in cancelAppointment', error);
    }
    setShowLoader(false);
  };
  const resetIndexGoToBottomTab = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.SERVICE_BOTTOM_TAB}],
  });
  const changeAppointmentStatus = async () => {
    if (typeof value !== 'number') {
      Toast.show('Please select status', Toast.SHORT);
      return;
    }
    setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('service_provider_id', userInfo.id);
      myData.append('appointment_id', appointment_id);
      myData.append('status_id', value);
      console.log('myData', myData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.CHANGE_APPOINTMENT_STATUS,
        myData,
      );
      if (resp.data.status) {
        setAppointmentStatus(
          AppmtStatusData.find(el => el?.id === value)?.title,
        );
        // if status changed to completed
        if (value == 4) {
          // navigation.navigate(ScreenNames.SERVICE_HOME)
          navigation.dispatch(resetIndexGoToBottomTab)
        }
        if (value == 5) {
          cancelAppointment(
            appointment_id,
            detailData.date_time,
            detailData.booking_id,
            detailData.cancelled,
          );
        }
        console.warn('resp---->', resp.data);
      }
    } catch (error) {
      console.log('error in changeAppointmentStatus', error);
    }
    setShowLoader(false);
  };
  //useEffect
  useEffect(() => {
    getAppointmentDeatails();
    getAppointmentStatusList();
    showLoader && setShowLoader(false);
    return () => {};
  }, []);

  //function : get invoice url
  const getInvoiceUrl = async bookingId => {
    try {
      const endPoint = `${Service.DOWNLOAD_INVOICE}?appointment_id=${appointment_id}`;
      const res = await Service.postApiWithToken(userToken, endPoint, {});
      console.log('getInvoiceUrl res', res.data);
      if (res.data.status) {
        checkPermission(res.data.url, bookingId);
      }
    } catch (error) {
      console.log('error', error);
    }
  };
  //function : imp function
  const checkPermission = async (url, bookingId) => {
    if (Platform.OS === 'ios') {
      downloadFile(url, bookingId);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadFile(url, bookingId);
          console.log('Storage Permission Granted.');
        } else {
          Toast.show('Storage Permission Not Granted', Toast.LONG);
          // Alert.alert('Error', 'Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('ERROR' + err);
      }
    }
  };
  //function : service function
  const downloadFile = async (url, bookingId) => {
    let pdfUrl = url;
    let DownloadDir =
      Platform.OS == 'ios'
        ? RNFetchBlob.fs.dirs.DocumentDir
        : RNFetchBlob.fs.dirs.DownloadDir;
    const {dirs} = RNFetchBlob.fs;
    const dirToSave =
      Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
    const configfb = {
      fileCache: true,
      useDownloadManager: true,
      notification: true,
      mediaScannable: true,
      title: 'Cosmologo',
      path: `${dirToSave}.pdf`,
    };
    const configOptions = Platform.select({
      ios: {
        fileCache: configfb.fileCache,
        title: configfb.title,
        path: configfb.path,
        appendExt: 'pdf',
      },
      android: configfb,
    });
    Platform.OS == 'android'
      ? RNFetchBlob.config({
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path: `${DownloadDir}/.pdf`,
            description: 'Cosmologo',
            title: `${bookingId}invoice.pdf`,
            mime: 'application/pdf',
            mediaScannable: true,
          },
        })
          .fetch('GET', `${pdfUrl}`)
          .catch(error => {
            console.warn(error.message);
          })
      : RNFetchBlob.config(configOptions)
          .fetch('GET', `${pdfUrl}`, {})
          .then(res => {
            if (Platform.OS === 'ios') {
              RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
              RNFetchBlob.ios.previewDocument(configfb.path);
            }
            console.log('The file saved to ', res);
          })
          .catch(e => {
            console.log('The file saved to ERROR', e.message);
          });
  };

  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title={detailData?.subcategory} />
      {Object.keys(detailData).length > 0 ? (
        <ScrollView
          contentContainerStyle={{paddingBottom: '20%'}}
          style={styles.mainView}>
          <View style={styles.serviceView}>
            {disableManageStatus === 'yes' ? (
              <View
                style={{
                  borderTopRightRadius: 20,
                  borderBottomLeftRadius: 20,
                  position: 'absolute',
                  // backgroundColor: (status === 'Cancelled') ? Colors.RED : Colors.THEME_GREEN,
                  backgroundColor: Colors.THEME_GREEN,
                  paddingHorizontal: 30,
                  paddingVertical: 2,
                  right: 1,
                }}>
                <MyText text={approvedStatus} fontSize={12} textColor="white" />
              </View>
            ) : null}
            <MyText
              text={detailData?.booking_id}
              fontFamily="bold"
              textColor="theme_green"
              marginVertical={10}
            />
            <View style={styles.flexRowStyle}>
              <Image
                source={{uri: detailData?.concern_area_image}}
                style={styles.imageStyle}
              />
              <View style={styles.textView}>
                <MyText
                  text={detailData?.concern_area_title}
                  fontFamily="bold"
                  marginVertical={10}
                />
                <MyText
                  text={detailData?.product}
                  textColor="lite_grey"
                  fontFamily="medium"
                />
              </View>
            </View>
            <View style={{height: 10}}></View>
            {/* <View style={styles.paymentSuccssTextView}>
              <MyText
                text={`Your payment of $${detailData?.amount} is successfully completed`}
                textColor="theme_green"
              />
            </View> */}
            <MyText
              text={`Appointment date: ${detailData?.date_time}`}
              marginBottom={10}
            />
            {/* <TouchableOpacity style={styles.generateInvoiceBtn} onPress={()=>{getInvoiceUrl(detailData?.booking_id)}}>
              <MyIcon.AntDesign
                name="download"
                color={Colors.THEME_GREEN}
                size={16}
              />
              <MyText
                text="Generate invoice Manually"
                marginHorizontal={5}
                fontSize={12}
                fontFamily="bold"
                textColor="theme_green"
              />
            </TouchableOpacity> */}
          </View>
          <View style={styles.manageStatusView}>
            <MyText text="User detail" fontFamily="bold" marginVertical={10} />
            <UserCard
              UserName={detailData?.user_name}
              userImageUrl={detailData?.user_profile_image}
              // status={AppmtStatusData.find(el=>el?.id === value)?.title}
              status={appointmentStatus}
              service_time={service_time}
            />
            {disableManageStatus !== 'yes' ? (
              <MyText
                text="Manage Status"
                fontFamily="bold"
                marginVertical={10}
              />
            ) : null}
            {AppmtStatusData.length > 0 && disableManageStatus !== 'yes' ? (
              <View
                style={{
                  // marginBottom: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  width: '100%',
                }}>
                <Dropdown
                  style={[
                    styles.dropdown,
                    isFocus && {borderColor: Colors.THEME_GREEN},
                  ]}
                  data={AppmtStatusData}
                  containerStyle={{backgroundColor: Colors.BG_GREEN}}
                  activeColor={Colors.LITE_GREEN}
                  maxHeight={300}
                  labelField="title"
                  valueField="id"
                  placeholder={!isFocus ? 'Select Status' : 'Select Status'}
                  value={value}
                  onFocus={() => setIsFocus(true)}
                  onBlur={() => setIsFocus(false)}
                  onChange={item => {
                    setValue(item.id);
                    setIsFocus(false);
                  }}
                />
                <TouchableOpacity
                  onPress={changeAppointmentStatus}
                  style={{
                    // backgroundColor: Colors.BG_GREEN,
                    borderRadius: 10,
                    // padding: 10,
                    // flex: 0.1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: '15%',
                  }}>
                  <MyIcon.AntDesign
                    name="checkcircle"
                    size={30}
                    color={Colors.THEME_GREEN}
                  />
                </TouchableOpacity>
              </View>
            ) : null}

            {/* {AppointmentStatus.length > 0 ? (
            <MyDropDown
              Data={AppointmentStatus}
              value={selectedStatus}
              setValue={setSelectedStatus}
            />
          ) : null} */}

            <MyText
              text="Procedures Details"
              fontFamily="bold"
              marginVertical={10}
            />
            {/* <MyText text="Lorem ipsum kjsfdu kgfds kldjfg kldfhg kljdfhg ilfdg lgf gjdfjbgljkdg gebgiug gbrijgberiug er irfdklgrg rkhoiwefgb f kusdfyg uksfy jsadfrgsf ksghdiar yuierwt lonvci saiot afhuk aewrb jxcbv" /> */}
            <MyText text={detailData?.description} />
            <View style={styles.locationView}>
              <MyIcon.Feather name="map-pin" size={24} />
              <View style={{marginLeft: 10}}>
                <MyText text="Location" fontFamily="medium" />
                <MyText
                  // text="ghanchiwara,pindwara,sirohi-raj"
                  text={detailData?.user_address}
                  textColor="lite_grey"
                  style={{paddingRight: 10}}
                />
              </View>
            </View>
          </View>
          {/* <MyButton Title="Save" onPress={changeAppointmentStatus} /> */}
          {/* <MyButton
            Title="Verify Appointment Booking"
            onPress={() => setShowVerifyAppt(true)}
          />
          <MyButton
            Title="Request Cancellation Appointment"
            backgroundColor={Colors.RED}
            onPress={() => setShowCancelAppt(true)}
          /> */}
        </ScrollView>
      ) : null}
      <VerifyBooking
        visible={showVerifyAppt}
        setVisibility={setShowVerifyAppt}
      />
      <CancelAppointment
        visible={showCancelAppt}
        setVisibility={setShowCancelAppt}
      />
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default AppointmentDetails;
