//import : react components
import React, {useState, useEffect} from 'react';
import {View, ScrollView, TouchableOpacity, Alert} from 'react-native';
import {CommonActions} from '@react-navigation/native';
// import :custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';
//import : third parties
import {Calendar} from 'react-native-calendars';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from "@react-native-community/netinfo";
//import : globals
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//import : styles
import {styles} from './AddAvailableSlotStyle';
//import : redux
import {useSelector} from 'react-redux';
import CustomLoader from 'components/CustomLoader/CustomLoader';

const AddAvailableSlot = ({navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //variables
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  //hook : states
  const [showLoader, setShowLoader] = useState(false);
  const [_markedDates, set_markedDates] = useState({});
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [slots, setSlots] = useState([]);
  const [date, setDate] = useState(new Date());
  const [openStartTime, setOpenStartTime] = useState(false);
  const [openEndTime, setOpenEndTime] = useState(false);
  const [selectedDates, setSelectedDates] = useState([]);
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //function : navigation function
  const gotoHome = () => navigation.navigate(ScreenNames.SERVICE_BOTTOM_TAB);
  //function : imp function
  function getDatesInRange(startDate, endDate) {
    const _format = 'YYYY-MM-DD';
    const date = new Date(startDate.getTime());
    const dates = [];
    while (date <= endDate) {
      dates.push(moment(new Date(date)).format(_format));
      date.setDate(date.getDate() + 1);
    }
    return dates;
  }
  const onDaySelect = day => {
    const _format = 'YYYY-MM-DD';
    const _selectedDay = moment(day.dateString).format(_format);
    const d1 = new Date('2022-01-18');
    const d2 = new Date('2022-01-24');
    const updatedMarkedDates =
      Object.keys(_markedDates).length > 0
        ? {
            [_selectedDay]: {
              endingDay: true,
              color: 'green',
              // customStyles: {
              //   container: {
              //     backgroundColor: Colors.THEME_GREEN,
              //   },
              // },
              marked: true,
              selected: true,
            },
          }
        : {
            [_selectedDay]: {
              startingDay: true,
              color: 'green',
              // customStyles: {
              //   container: {
              //     backgroundColor: Colors.THEME_GREEN,
              //   },
              // },
              marked: true,
              selected: true,
            },
          };
    // setUnAvailableDate(_selectedDay);
    setSelectedDates([...selectedDates, _selectedDay]);
    set_markedDates(dates => ({
      ...dates,
      ...updatedMarkedDates,
    }));
  };
  const addSlotTime = () => {
    const data = {
      start_time: moment(startTime).format('h:mm:ss'),
      end_time: moment(endTime).format('h:mm:ss'),
    };
    setSlots([...slots, data]);
    setStartTime('');
    setEndTime('');
  };
  const removeSlot = item => {
    const filterArray = slots.filter(
      e => JSON.stringify(e) != JSON.stringify(item),
    );
    setSlots(filterArray);
  };
  const Validation = () => {
    if (selectedDates.length <= 0) {
      Toast.show('Please select date', Toast.LONG);
      // Alert.alert('', 'Please select date');
    } else return true;
  };
  //function : service function
  const addSlot = async () => {
    if (Validation()) {
      setShowLoader(true);
      try {
        // const requiredData = new FormData();
        // requiredData.append('service_provider_id', userInfo.id);
        // requiredData.append('type', 'A');
        // requiredData.append('event_name', 'hello this is testing');
        // requiredData.append('date', selectedDates);
        // requiredData.append('slot', slots);
        // console.warn('requiredData', requiredData);
        const postData = {
          service_provider_id: userInfo.id,
          type: 'A',
          event_name: 'testing',
          date: selectedDates,
          slot: slots,
        };
        const resp = await Service.postJsonApiWithToken(
          userToken,
          Service.ADD_SERVICE_UNAVAILABLE,
          postData,
        );
        if (resp.data.status) {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
          gotoHome();
        }
      } catch (error) {
        console.log('error in addSlot', error);
      }
      setShowLoader(false);
    }
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Add Slot" />
      <ScrollView
        contentContainerStyle={{paddingBottom: '20%'}}
        style={styles.mainView}>
        <Calendar
          markingType={'period'}
          style={styles.calendarStyle}
          onDayPress={day => onDaySelect(day)}
          enableSwipeMonths={true}
          //   markingType="custom"
          markedDates={_markedDates}
        />
        <View style={styles.startEndDateView}>
          <MyText text="Select Start Time" fontFamily="medium" />
          <TouchableOpacity
            onPress={() => setOpenStartTime(true)}
            style={{
              marginVertical: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              shadowColor: '#000',
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.1,
              backgroundColor: Colors.WHITE,
              shadowRadius: 15,
              elevation: 2,
              padding: 10,
              borderRadius: 10,
            }}>
            <MyText
              text={
                startTime
                  ? moment(startTime).format('h:mm:ss A')
                  : 'Select Start Time'
              }
            />
            <MyIcon.AntDesign
              name="calendar"
              size={24}
              color={Colors.THEME_GREEN}
            />
          </TouchableOpacity>
          <MyText text="Select End Time" fontFamily="medium" />
          <TouchableOpacity
            onPress={() => setOpenEndTime(true)}
            style={{
              marginVertical: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
              shadowColor: '#000',
              shadowOffset: {width: 0, height: 2},
              shadowOpacity: 0.1,
              backgroundColor: Colors.WHITE,
              shadowRadius: 15,
              elevation: 2,
              padding: 10,
              borderRadius: 10,
            }}>
            <MyText
              text={
                endTime
                  ? moment(endTime).format('h:mm:ss A')
                  : 'Select End Time'
              }
            />
            <MyIcon.AntDesign
              name="calendar"
              size={24}
              color={Colors.THEME_GREEN}
            />
          </TouchableOpacity>
          <MyButton Title="Add Slot Time" onPress={addSlotTime} />
        </View>
        {slots.length > 0 ? (
          <>
            <MyText text="Slots" fontFamily="medium" />
            {slots.map((item, index) => {
              return (
                <View style={styles.slotView}>
                  <View>
                    <MyText text={`Start Time : ${item.start_time}`} />
                    <MyText text={`End Time : ${item.end_time}`} />
                  </View>
                  <TouchableOpacity onPress={() => removeSlot(item)}>
                    <MyIcon.AntDesign
                      name="minuscircleo"
                      color={Colors.RED}
                      size={20}
                    />
                  </TouchableOpacity>
                </View>
              );
            })}
          </>
        ) : null}
        <MyButton Title="Add Slot" onPress={addSlot} />
      </ScrollView>
      <DatePicker
        modal
        // mode="date"
        mode="time"
        open={openStartTime}
        date={date}
        onConfirm={time => {
          setOpenStartTime(false);
          setStartTime(time);
        }}
        onCancel={() => {
          setOpenStartTime(false);
        }}
      />
      <DatePicker
        modal
        // mode="date"
        mode="time"
        open={openEndTime}
        date={date}
        onConfirm={time => {
          setOpenEndTime(false);
          setEndTime(time);
        }}
        onCancel={() => {
          setOpenEndTime(false);
        }}
      />
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default AddAvailableSlot;
