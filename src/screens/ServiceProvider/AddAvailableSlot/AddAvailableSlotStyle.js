import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  calendarStyle: {
    padding: 5,
    borderRadius: 10,
    marginVertical: 10,
  },
  startEndDateView: {
    backgroundColor: Colors.WHITE,
    padding: 10,
    borderRadius: 10,
    marginVertical: 10,
  },
  slotView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.WHITE,
    padding: 10,
    marginVertical: 5,
    borderRadius: 10,
  },
});
