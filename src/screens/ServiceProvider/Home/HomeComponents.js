import MyText from 'components/MyText/MyText';
import {Colors, Constant, Images, Fonts} from 'global/Index';
import React from 'react';
import {Image, TouchableOpacity, View, StyleSheet, Text, ImageBackground} from 'react-native';
import {styles} from './HomeStyle';

export const ServiceCard = ({Title, Icon, onPress = () => {}}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        backgroundColor: Colors.WHITE,
        alignSelf: 'flex-start',
        width: Constant.width / 2 - 30,
        height: 130,
        padding: 10,
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.1,
        backgroundColor: Colors.WHITE,
        shadowRadius: 15,
        elevation: 2,
        marginRight: 20,
      }}>
      <Image
        source={Images.ProceduresIcon.service}
        style={{height: 50, width: 50}}
      />
      <MyText text={Title} marginTop={10} />
      <Image
        source={Images.drawerItemIcon.rightArrow}
        style={{
          height: 20,
          width: 20,
          position: 'absolute',
          right: -10,
          top: '50%',
        }}
      />
    </TouchableOpacity>
  );
};

export const IconListCard = ({Title, Icon, onPress = () => {}}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.WHITE,
        padding: 10,
        borderRadius: 10,
        marginVertical: 10,
      }}>
      <Image source={Icon} style={{height: 50, width: 50}} />
      <MyText text={Title} marginHorizontal={20} />
    </TouchableOpacity>
  );
};

//props
export const AccountNotActivatedText = ({
  style={}, type=''
}) => {
  //UI
  return (
    type === 'review' ?
    <Text style={style}>
      Your account is under{`\n`}<Text style={[style, {color: Colors.THEME_GREEN}]}>review</Text> by {`\n`}administrator.
    </Text>
    :
    <Text style={style}>
      Your account is{`\n`}<Text style={[style, {color: Colors.RED}]}>suspended</Text> by {`\n`}administrator.
    </Text>
  );
};
export default AccountNotActivatedText;

export const TotalPaymentReceived = ({
  amount
}) => {
  //UI
  return (
    <ImageBackground
        resizeMode="cover"
        source={Images.HomePaymentReceived}
        borderRadius={19}
        style={{borderRadius: 20, marginTop:20, height:170, justifyContent:'center'}}
      >
      <View style={{flexDirection:'row', alignItems:'center', padding:20}}>
        <View style={{flex:1}}>
          <MyText text={'Total Payments Received'} textColor={'#446F42'} fontSize={18} marginBottom={15}/>
          <MyText text={`$ ${amount === undefined ? '': amount}`} textColor={'#294527'} fontSize={18}/>
          {/* <MyText text={`$ 0`} textColor={'#294527'} fontSize={18}/> */}
        </View>
        <Image
          resizeMode="contain"
          source={Images?.Icons?.walletIcon}
          style={{height: 130, width: 130, flex:1}}
          />
      </View>
      </ImageBackground>
  );
};
