import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  nameAndTaglineView: {
    padding: 20,
    backgroundColor: Colors.THEME_GREEN,
    paddingBottom: 40,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  newAppointmentView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.WHITE,
    height: 50,
    paddingHorizontal: 20,
    borderRadius: 10,
    marginTop: -40,
  },
  inactiveAccountView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.WHITE,
    height: 120,
    paddingHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
  },
  totalPaymentView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.WHITE,
    height: 120,
    paddingHorizontal: 20,
    borderRadius: 10,
    marginTop: 20,
  },
  flexRowStyle: {
    marginVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
