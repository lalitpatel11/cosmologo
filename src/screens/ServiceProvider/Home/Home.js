//import : react components
import React, {useState, useEffect} from 'react';
import {View, ScrollView, Image, TouchableOpacity, Text} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//import : custom components
import CustomLoader from 'components/CustomLoader/CustomLoader';
import MyText from 'components/MyText/MyText';
import MyHeader from 'components/MyHeader/MyHeader';
import {PaymentReceivedCard} from 'components/ProfileCard/ProfileComponents';
import {IconListCard, ServiceCard, AccountNotActivatedText, TotalPaymentReceived} from './HomeComponents';
import MyButton from 'components/MyButton/MyButton';
import NoInternet from 'components/NoInternet/NoInternet';
//import : global
import {Colors, Images, ScreenNames, Service} from 'global/Index';
//import : styles
import {styles} from './HomeStyle';
//import : modals
import VerifyBooking from 'modals/VerifyBooking/VerifyBooking';
import VerifyApptOtp from 'modals/VerifyApptOtp/VerifyApptOtp';
import ConfirmBooking from 'modals/ConfirmBooking/ConfirmBooking';
//import : redux
import {connect, useSelector, useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {setServiceProviderNotifications, setTotalPaymentAmount, setUser} from 'src/reduxToolkit/reducer/user';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import NetInfo, {useNetInfo} from "@react-native-community/netinfo";

const checkApproved = (userInfo) =>{
  // is_reupload, 0 (not reuploaded), 1 (reuploaded), 
  // console.log('checkApproved called variable', userInfo);
  let approved = true
  // console.log('userInfo?.certificate1', userInfo?.certificate1);
  if(userInfo?.is_reupload1 == 1 && userInfo?.certificate1 !== null){
    if(userInfo?.approved_status1 != 1){
      approved = false
      // console.log('approved1 changed to false', approved);
    }  
  }
  // console.log('userInfo?.certificate2', userInfo?.certificate2);
  if(userInfo?.is_reupload2 == 1 && userInfo?.certificate2 !== null){
    if(userInfo?.approved_status2 != 1){
      approved = false
      // console.log('approved2 changed to false', approved);
    }  
  }
  // console.log('userInfo?.certificate3', userInfo?.certificate3);
  if(userInfo?.is_reupload3 == 1 && userInfo?.certificate3 !== null){
    if(userInfo?.approved_status3 != 1){
      approved = false
      // console.log('approved3 changed to false', approved);
    }  
  }
  // console.log('approved at end', approved);
  return approved
}
const checkExpired = (userInfo) =>{
  // is_reupload, 0 (not reuploaded), 1 (reuploaded), 
  // console.log('checkExpired called variable', userInfo);
  let expired = false
  // console.log('userInfo?.certificate1', userInfo?.certificate1);
  if(userInfo?.certificate1 !== null){
    if(userInfo?.expiry_status1){
      expired = true
      // console.log('expired1 changed to false', expired);
    }  
  }
  // console.log('userInfo?.certificate2', userInfo?.certificate2);
  if(userInfo?.certificate2 !== null){
    if(userInfo?.expiry_status2){
      expired = true
      // console.log('expired2 changed to false', expired);
    }  
  }
  // console.log('userInfo?.certificate3', userInfo?.certificate3);
  if(userInfo?.certificate3 !== null){
    if(userInfo?.expiry_status3){
      expired = true
      // console.log('expired3 changed to false', expired);
    }  
  }
  // console.log('expired at end', expired);
  return expired
}

const Home = ({navigation, userToken}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const userInfo = useSelector(state => state.user.userInfo);
  const totalPaymentAmount = useSelector(state => state.user.totalPaymentAmount);
  const dispatch = useDispatch();
  //hook : states
  const [TotalPayment, setTotalPayment] = useState(0);
  //hook : states : modal states
  const [showVerifyBooking, setShowVerifyBooking] = useState(false);
  const [generateOtp, setGenerateOtp] = useState(false);
  const [showConfirmBooking, setShowConfirmBooking] = useState(false);
  const [AgendaList, setAgendaList] = useState([]);
  const [showLoader, setShowLoader] = useState(false);
  const [showLoader1, setShowLoader1] = useState(false);
  const [showLoader2, setShowLoader2] = useState(false);
  const [showLoader3, setShowLoader3] = useState(false);
  const [showLoader4, setShowLoader4] = useState(false);
  const [isOnline, setIsOnline] = useState(true);
  const [newDocStatusApproved, setNewDocStatusApproved] = useState(checkApproved(userInfo));
  const [newDocStatusExpired, setNewDocStatusExpired] = useState(checkExpired(userInfo));
  //function : navigation function
  const gotoManageService = () =>
    navigation.navigate(ScreenNames.SERVICE_MANAGE);
  const gotoServiceAvailability = () =>{
    if(userInfo?.is_active == 0){
      // dispatch(showToast({text: 'Your account is not approved by administrator.', duration: 1000}));
      Toast.show('Your account is not approved by administrator.', Toast.SHORT)
      return
    }
    console.log('AgendaList', AgendaList);
    navigation.navigate(ScreenNames.SERVICE_AVAILABILITY, {AgendaList});
  }
  const gotoAppntListing = () =>{
    if(userInfo?.is_active == 0){
      // dispatch(showToast({text: 'Your account is not approved by administrator.', duration: 1000}));
      Toast.show('Your account is not approved by administrator.', Toast.SHORT)
      return
    }
    navigation.navigate(ScreenNames.SERVICE_APPOINTMENT_LIST);
  }
  const gotoVerifiedAppntListing = () =>{
    if(userInfo?.is_active == 0){
      // dispatch(showToast({text: 'Your account is not approved by administrator.', duration: 1000}));
      Toast.show('Your account is not approved by administrator.', Toast.SHORT)
      return
    }
    navigation.navigate(ScreenNames.SERVICE_VERIFIED_APPOINTMENT_LIST);
  }
  const gotoAddSlot = () => navigation.navigate(ScreenNames.ADD_AVAILABLE_SLOT);
  //function : service function
  const getTotalPaymentReceived = async () => {
    setShowLoader1(true)
    try {
      const myData = new FormData();
      myData.append('serviceprovider_id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        // Service.TOTAL_PAYMENT_RECEIVED,
        Service.SERVICE_PROVIDER_TOTAL_PAYMENT,
        myData
      );
      if (resp.data.status) {
        // if(typeof resp.data?.totalamount === 'number'){
        //   await AsyncStorage.setItem('totalPaymentAmount', JSON.stringify(resp.data?.totalamount));
        //   dispatch(setTotalPaymentAmount(resp.data?.totalamount));
        // }
        setTotalPayment(resp.data?.totalamount);
      }
    } catch (error) {
      console.log('error in getTotalPaymentReceived', error);
    }
    setShowLoader1(false)
  };
  const getDocApprovalStatus = async () => {
    setShowLoader4(true)
    try {
      const myData = new FormData();
      myData.append('id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        // Service.TOTAL_PAYMENT_RECEIVED,  
        Service.SERVICE_APPROVED_STATUS,
        myData
      );
      console.log('getDocApprovalStatus resp', resp.data.data);
      if (resp.data.status) {
        const updatedUserInfo = {...userInfo, approved_status1: resp.data.data?.approved_status1, approved_status2: resp.data.data?.approved_status2, approved_status3: resp.data.data?.approved_status3, 
          expiry_status1: resp.data.data?.expiry_status1, expiry_status2: resp.data.data?.expiry_status2, expiry_status3: resp.data.data?.expiry_status3,
          expiry_date1: resp.data.data?.expiry_date1, expiry_date2: resp.data.data?.expiry_date2, expiry_date3: resp.data.data?.expiry_date3,
          is_approved: resp.data.data?.is_approved, 
          is_active: resp.data.data?.is_active,
        }
        if(checkApproved(resp.data.data)){
          setNewDocStatusApproved(true)
        }else{
          setNewDocStatusApproved(false)
        }

        if(checkExpired(resp.data.data)){
          setNewDocStatusExpired(true)
        }else{
          setNewDocStatusExpired(false)
        }
        // if(resp.data.data?.approved_status1 != 1 || resp.data.data?.approved_status2 != 1 ||resp.data.data?.approved_status3 != 1){
        //   setNewDocStatusNotApproved(true)
        // }else{
        //   setNewDocStatusNotApproved(false)
        // }
        console.log('updatedUserInfo', updatedUserInfo);
        await AsyncStorage.setItem('userInfo', JSON.stringify(updatedUserInfo));
        dispatch(setUser(updatedUserInfo));
        // {approved_status1: '1', approved_status2: '0', approved_status3: '0'}
        // if(typeof resp.data?.totalamount === 'number'){
        //   await AsyncStorage.setItem('totalPaymentAmount', JSON.stringify(resp.data?.totalamount));
        //   dispatch(setTotalPaymentAmount(resp.data?.totalamount));
        // }
        // setTotalPayment(resp.data?.totalamount);
      }
    } catch (error) {
      console.log('error in getDocApprovalStatus', error);
    }
    setShowLoader4(false)
  };
  const getAgendaData = async () => {
    setShowLoader2(true)
    try {
      const requiredData = new FormData();
      requiredData.append('service_provider_id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SERVICE_PROVIDER_CALENDAR,
        requiredData,
      );
      if (resp.data.status) {
        setAgendaList(resp.data.data);
        console.log('setAgendaList resp---->', resp.data);
        console.warn('resp---->', resp.data);
      }
    } catch (error) {
      console.log('error in getAgendaData', error);
    }
    setShowLoader2(false)
  };
  const getNotifications = async () => {
    setShowLoader3(true);
    const endPoint = Service.SERVICE_PROVIDER_NOTIFICATION
    try {
      const resp = await Service.postApiWithToken(
        userToken,
        endPoint,
        {},
      );
      if (resp.data.status) {
        console.warn('resp---->', resp.data);
        console.log('setNotificationData', resp.data.data);
        await AsyncStorage.setItem('serviceProviderNotifications', JSON.stringify(resp.data.data));
        dispatch(setServiceProviderNotifications(resp.data.data));
        // setNotificationData(resp.data.data);
      }else{
        // Toast.show(resp.data.message, Toast.SHORT)
      }
    } catch (error) {
      console.log('error in getNotifications', error);
    }
    setShowLoader3(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //hook : useEffect
  // useEffect(() => {
  //   const unsubscribe = navigation.addListener('focus', () => {
  //     if(areDocumentsNotApproved()){
  //       setNewDocStatusNotApproved(true)
  //     }else{
  //       setNewDocStatusNotApproved(false)
  //     }
  //   });
  //   return unsubscribe;
  // }, [navigation]);
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      console.log('current status',userInfo?.approved_status1 != 1 || userInfo?.approved_status2 != 1 ||userInfo?.approved_status3 != 1);
      console.log('userinfo', userInfo);
      getTotalPaymentReceived();
      getAgendaData();
      getDocApprovalStatus()
      getNotifications();
    });
    return unsubscribe;
  }, [navigation, isOnline]);
  const areDocumentsNotApproved = () => {
    return userInfo?.approved_status1 != 1 || userInfo?.approved_status2 != 1 ||userInfo?.approved_status3 != 1
  }

  const areDocumentsExpired = () => {
    return userInfo?.expiry_status1 || userInfo?.expiry_status2 || userInfo?.expiry_status3
  }
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Home" hasDrawerButton={true} isBorderRadius={false} />
      <ScrollView>
        <View style={styles.nameAndTaglineView}>
          <MyText text="Good Morning" fontSize={16} textColor="white" />
          <MyText
            text={`${userInfo.first_name} ${userInfo.last_name}`}
            fontSize={24}
            fontFamily="bold"
            textColor="white"
            marginVertical={10}
          />
        </View>
        <View style={styles.mainView}>
          <TouchableOpacity
            onPress={gotoAppntListing}
            style={styles.newAppointmentView}>
            <MyText text="New Appointment Received" />
            <Image
              resizeMode="contain"
              source={Images?.drawerItemIcon?.rightArrow}
              style={{height: 24, width: 24}}
            />
          </TouchableOpacity>
          {/* <PaymentReceivedCard Payment={TotalPayment} /> */}
          {/* <View style={styles.flexRowStyle}>
            <ServiceCard
              Title="Manage Service Availability"
              onPress={gotoServiceAvailability}
            /> */}
            {/* <ServiceCard
              Title="Verify Appointment Booking"
              onPress={() => setShowVerifyBooking(true)}
            /> */}
            {/* <ServiceCard Title="Manage Service" onPress={gotoManageService} />
          </View> */}
          {/* <IconListCard
            Icon={Images.Icons.calendarCheck}
            Title="Verify Appointment Booking"
            onPress={() => setShowVerifyBooking(true)}
          /> */}
          {/* <IconListCard
            Icon={Images.Icons.calendarCheck}
            Title="Add Available Slot"
            onPress={() => gotoAddSlot()}
          /> */}
          {/* <IconListCard
            Icon={Images.Icons.generateInvoice}
            Title="Generate Invoice Manually"
            onPress={() => setShowVerifyBooking(true)}
          /> */}
          {!newDocStatusApproved ?
          <View style={styles.inactiveAccountView}>
              <Text style={{flex:1, fontSize:18}}>
                Documents under <Text style={{color: Colors.THEME_GREEN}}>review</Text> by administrator.
              </Text>
              <Image
                resizeMode="contain"
                source={require('../../../assets/images/doc_under_review_image.png')}
                style={{height: 80, width: 80}}
              />
          </View>
          :
          // areDocumentsExpired() ?
          newDocStatusExpired ?
          <View style={styles.inactiveAccountView}>
            <Text style={{flex:1, fontSize:18}}>
              Document <Text style={{color: Colors.RED}}>expired!</Text>{`\n`}Upload from Profile.
            </Text>
            <Image
                resizeMode="contain"
                source={require('../../../assets/images/doc_expired_image.png')}
                style={{height: 80, width: 80}}
              />
          </View>
          :
          (userInfo?.is_active == 0 || userInfo?.is_approved == 2) ? 
          <View
            style={styles.inactiveAccountView}>
            <AccountNotActivatedText type={userInfo?.is_approved == 2 ? 'rejected' : 'review'} style={{flex:1, fontSize:18}}/>
              <Image
                resizeMode="contain"
                source={Images?.Icons?.inactiveAccountIcon}
                style={{height: 80, width: 80}}
              />
          </View>
          :
          <TotalPaymentReceived amount={TotalPayment}/>
          }
          <View style={styles.flexRowStyle}>
            <ServiceCard
              Title="Manage Service Availability"
              onPress={gotoServiceAvailability}
            />
            {/* <ServiceCard
              Title="Verify Appointment Booking"
              onPress={() => setShowVerifyBooking(true)}
            /> */}
            <ServiceCard Title="Manage Service" onPress={gotoManageService} />
          </View>
          <IconListCard
            Icon={Images.Icons.calendarCheck}
            Title="Verify Appointment Booking"
            onPress={() => gotoVerifiedAppntListing()}
          />
        </View>
      </ScrollView>
      <VerifyBooking
        visible={showVerifyBooking}
        setVisibility={setShowVerifyBooking}
        generateOtpPress={() => {
          setShowVerifyBooking(false);
          setGenerateOtp(true);
        }}
        validateBookingPress={() => {
          setShowVerifyBooking(false);
          setShowConfirmBooking(true);
        }}
      />

      <VerifyApptOtp visible={generateOtp} setVisibility={setGenerateOtp} />
      <ConfirmBooking
        visible={showConfirmBooking}
        setVisibility={setShowConfirmBooking}
      />
      <CustomLoader showLoader={showLoader1 || showLoader2 || showLoader3 || showLoader4} />
    </View>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});

export default connect(mapStateToProps, null)(Home);
