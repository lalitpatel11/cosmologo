//react components
import React, {useState, useEffect} from 'react';
import {View, ScrollView, Image, TouchableOpacity, Alert} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyButton from 'components/MyButton/MyButton';
import MyText from 'components/MyText/MyText';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyPasswordInput from 'components/MyPasswordInput/MyPasswordInput';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import NoInternet from 'components/NoInternet/NoInternet';
//third parties
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import NetInfo, {useNetInfo} from "@react-native-community/netinfo";
//global
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ServiceSignInStyle';
//redux
import {connect} from 'react-redux';
import {
  setUser,
  setUserToken,
  setTotalPaymentAmount,
} from 'src/reduxToolkit/reducer/user';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const ServiceSignIn = ({navigation, dispatch}) => {
  const {isInternetReachable} = useNetInfo();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [emailAddress, setEmailAddress] = useState('');
  const [password, setPassword] = useState('');
  const [fcmToken, setFcmToken] = useState('');
  const [isOnline, setIsOnline] = useState(true);
  const [retrying, setRetrying] = useState(false);
  //function : navigation function
  const gotoBottomTab = () =>
    navigation.navigate(ScreenNames.SERVICE_BOTTOM_TAB);
  const resetIndexGoToBottomTab = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.SERVICE_BOTTOM_TAB}],
  });
  const gotoSignUp = () => {
    navigation.navigate(ScreenNames.SERVICE_SIGN_UP, {flag: 0});
  };

  const gotoSelectService = userId =>
    navigation.replace(ScreenNames.SERVICE_SELECT_SERVICE, {userId: userId});
  const gotoUploadDocumet = userId =>
    navigation.navigate(ScreenNames.SERVICE_UPLOAD_DOCUMENTS, {userId: userId});
  const gotoForgotPassword = () =>
    navigation.navigate(ScreenNames.USER_MOBILE_OTP, {
      role: 'Service Provider',
    });
  //function : imp function
  const Validation = () => {
    if (emailAddress == '') {
      // dispatch(showToast({text: 'Please enter email address'}));
      Toast.show('Please enter email address', Toast.SHORT)
      // Alert.alert('', 'Please enter email address');
    } else if (password == '') {
      // dispatch(showToast({text: 'Please enter password'}));
      Toast.show('Please enter password', Toast.SHORT)
      // Alert.alert('', 'Please enter password');
    } else return true;
  };
  //function : service function
  const SignInServiceProvider = async () => {
    if (Validation()) {
      setShowLoader(true);
      try {
        const signInData = new FormData();
        signInData.append('email', emailAddress);
        signInData.append('password', password);
        signInData.append('fcmtoken', fcmToken);
        console.log('sp signInData', signInData);
        const resp = await Service.postApi(
          Service.SERVICE_PROVIDER_LOGIN,
          signInData,
        );
        if (resp.data.status) {
          await AsyncStorage.setItem('userToken', resp.data.access_token);
          dispatch(setUserToken(resp.data.access_token));
          const jsonValue = JSON.stringify(resp.data.data);
          console.log('sp signin jsonValue', jsonValue);
          await AsyncStorage.setItem('userInfo', jsonValue);
          dispatch(setUser(resp.data.data));
          // await AsyncStorage.setItem('totalPaymentAmount', JSON.stringify(resp.data.data?.total_payment));
          // dispatch(setTotalPaymentAmount(resp.data.data?.total_payment));
          navigation.dispatch(resetIndexGoToBottomTab);
        } else {
          // Alert.alert('', resp.data.message);
          console.log('resp.data', resp.data);
          Toast.show(resp.data.message, Toast.SHORT);
          console.warn('resp---->', resp.data);
          if (resp.data.current_step == 2) {
            gotoUploadDocumet(resp.data.user_id);
          } else if (resp.data.current_step == 3) {
            gotoSelectService(resp.data.user_id);
          }
        }
      } catch (error) {
        console.log('error in ServiceSignIn', error);
      }
      setShowLoader(false);
    }
  };
  const checkToken = async () => {
    try {
      const token = await messaging().getToken();
      if (token) {
        setFcmToken(token);
      } else {
        console.log('could not get fcm token');
      }
    } catch (error) {
      console.log('error in getting fcm token', error);
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });

  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    checkToken();
  }, []);
  const gotoWebPage = async (endPoint, name) => {
    navigation.navigate(ScreenNames.SIDE_MENU_LINKS, {
      endPoint,
      name,
      role: 'Service',
    });
  };
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Sign In" />
      <ScrollView style={styles.mainView}>
        <Image
          resizeMode="contain"
          source={Images.AppLogo}
          style={styles.imageStyle}
        />
        <MyTextInput
          Title="Email Address"
          placeholder="Enter Email Address"
          onChangeText={text => setEmailAddress(text)}
          keyboardType="email-address"
          // Icon={
          //   <MyIcon.AntDesign
          //     name="checkcircleo"
          //     size={24}
          //     color={Colors.THEME_GREEN}
          //   />
          // }
        />
        <MyPasswordInput
          Title="Password"
          placeholder="Enter Password"
          onChangeText={text => setPassword(text)}
          secureTextEntry={true}
          showPassword
          ShowIcon={
            <MyIcon.FontAwesome5
              name="eye"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          HideIcon={
            <MyIcon.FontAwesome5
              name="eye-slash"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
        />
        <TouchableOpacity
          onPress={gotoForgotPassword}
          style={styles.forgotPassword}>
          <MyText text="Forgot password?" fontFamily="bold" />
        </TouchableOpacity>
        <MyButton Title="Sign In" onPress={SignInServiceProvider} />
        <TouchableOpacity onPress={true ? gotoSignUp : () => navigation.navigate(ScreenNames.SERVICE_UPLOAD_DOCUMENTS, {userId:''})} style={styles.bottomLineStyle}>
          <MyText text="Don't have an account yet? " fontFamily="bold" />
          <MyText text="Sign Up" fontFamily="bold" textColor="theme_green" />
        </TouchableOpacity>
        <View style={styles.termsAndPPView}>
          <MyText text="By logging in, you agree to our" />
          <TouchableOpacity
            onPress={() =>
              gotoWebPage('terms-condition', 'Terms And Conditions')
            }>
            <MyText
              text=" Terms and Conditions "
              textColor="theme_green"
              isUnderLine
            />
          </TouchableOpacity>
          <MyText text="and " />
          <TouchableOpacity
            onPress={() => gotoWebPage('privacy-policy', 'Privacy Policy')}>
            <MyText text="Privacy Policy" textColor="theme_green" isUnderLine />
          </TouchableOpacity>
        </View>
      </ScrollView>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};
const mapDispatchToProps = dispatch => ({dispatch});

export default connect(null, mapDispatchToProps)(ServiceSignIn);
