//react components
import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Alert,
  Image,
  Keyboard,
  Platform,
  PermissionsAndroid,
  KeyboardAvoidingView,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import SelectImageSource from 'modals/SelectImageSource/SelectImageSource';
import SearchLocation from 'modals/SearchLocation/SearchLocation';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyText from 'components/MyText/MyText';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyPasswordInput from 'components/MyPasswordInput/MyPasswordInput';
import MyButton from 'components/MyButton/MyButton';
import TextInputWithFlag from 'components/TextInputWithFlag/TextInputWithFlag';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import DateSelector from 'components/DateSelector/DateSelector';
import TermOfUseCheck from 'components/TermOfUseCheck/TermOfUseCheck';
//import : third parties
import {useNetInfo} from "@react-native-community/netinfo";
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import messaging from '@react-native-firebase/messaging';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import DatePicker from 'react-native-date-picker';
import Geolocation from 'react-native-geolocation-service';
import moment from 'moment';
import Geocoder from 'react-native-geocoding';
import CountryPicker from 'react-native-country-codes-picker';
import Toast from 'react-native-simple-toast';
//global
import {Colors, Constant, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ServiceSignUpStyle';
import SelectAddress from 'modals/SelectAddress/SelectAddress';
//import : redux
import {connect, useDispatch, useSelector} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const ServiceSignUp = ({navigation}) => {
  Geocoder.init('AIzaSyBJqbxRoFBbpmwDrHOtVM26s9R1Fh5UWp0');
  const GOOGLE_MAPS_APIKEY = 'AIzaSyACzgsZq8gI9VFkOw_fwLJdmezbc4iUxiM';
  const {isInternetReachable} = useNetInfo();
  //ref
  let listViewRef = useRef();
  const firstNameRef = useRef();
  const lastNameRef = useRef();
  const emailAddressRef = useRef();
  const phoneNumberRef = useRef();
  const socialSecurityNumberRef = useRef();
  const addressRef = useRef();
  const passwordRef = useRef();
  const confirmPasswordRef = useRef();
  //states
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [emailAddress, setEmailAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [filePath, setFilePath] = useState('');
  const [gender, setGender] = useState(1);
  const [date, setDate] = useState(new Date());
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [socialSecurityNumber, setSocialSecurityNumber] = useState('');
  const [address, setAddress] = useState('');
  const [latLng, setLatLng] = useState('');
  const [serviceRange, setServiceRange] = useState('');
  const [allCurrentLocations, setAllCurrentLocations] = useState([]);
  const [fcmToken, setFcmToken] = useState('');
  const dispatch = useDispatch();
  //hooks : modal states
  const [showLoader, setShowLoader] = useState(false);
  const [isAcceptedTC, setIsAcceptedTC] = useState(false);
  const [open, setOpen] = useState(false);
  const [showSelectAddress, setShowSelectAddress] = useState(false);
  const [show, setShow] = useState(false);
  const [selectedCountry, setSelectedCountry] = useState({
    code: 'US',
    dial_code: '+1',
    flag: '🇺🇸',
    name: {
      by: '',
      cz: 'Spoj  ené státy',
      en: 'United States',
      es: 'Estados Unidos',
      pl: 'Stany Zjednoczone',
      pt: 'Estados Unidos',
      ru: 'Соединенные Штаты',
      ua: 'Сполучені Штати',
    },
  });
  const [showImageSourceModal, setShowImageSourceModal] = useState(false);
  const [showLocationModal, setShowLocationModal] = useState(false);
  const [scrollY, setScrollY] = useState(null);
  //function : navigation function
  const gotoSignIn = () => navigation.navigate(ScreenNames.SERVICE_SIGN_IN);
  const gotoSelectService = userId =>
    navigation.replace(ScreenNames.SERVICE_SELECT_SERVICE, {userId: userId});
  const gotoUploadDocumet = userId =>
    navigation.navigate(ScreenNames.SERVICE_UPLOAD_DOCUMENTS, {userId});
  const gotoProfilePage = () =>
    navigation.navigate(ScreenNames.SERVICE_PROFILE);
  //function : imp function
  const openLibrary = () => {
    let options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose Photo from Custom Option',
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    launchImageLibrary(options, response => {
      if (response.didCancel) {
        // Alert.alert('User cancelled camera picker');
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'User cancelled image picker'}));
        Toast.show('User cancelled image picker', Toast.SHORT)
        // Alert.alert('User cancelled image picker');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Camera not available on device'}));
        Toast.show('Camera not available on device', Toast.SHORT)
        // Alert.alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Permission not satisfied'}));
        Toast.show('Permission not satisfied', Toast.SHORT)
        // Alert.alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: response.errorMessage}));
        Toast.show(response.errorMessage, Toast.SHORT)
        // Alert.alert(response.errorMessage);
        return;
      }
      console.log('Response = ', response.assets[0]);
      setFilePath(response.assets[0]);
      setShowImageSourceModal(false);
    });
  };
  const Validation = () => {
    if (isAcceptedTC == false) {
      // dispatch(showToast({text: 'Please accept Terms of Use'}));
      Toast.show('Please accept Terms of Use', Toast.SHORT)
      // Alert.alert('', 'Please accept Terms of Use');
    } else if (firstName == '') {
      // dispatch(showToast({text: 'Please enter first name'}));
      Toast.show('Please enter first name', Toast.SHORT)
      // Alert.alert('', 'Please enter first name');
    } else if (lastName == '') {
      // dispatch(showToast({text: 'Please enter last name'}));
      Toast.show('Please enter last name', Toast.SHORT)
      // Alert.alert('', 'Please enter last name');
    } else if (emailAddress == '') {
      // dispatch(showToast({text: 'Please enter email address'}));
      Toast.show('Please enter email address', Toast.SHORT)
      // Alert.alert('', 'Please enter email address');
    } else if (phoneNumber == '') {
      // dispatch(showToast({text: 'Please enter phone number'}));
      Toast.show('Please enter phone number', Toast.SHORT)
      // Alert.alert('', 'Please enter phone number');
    }
    // else if (filePath == '') {
    //   Alert.alert('', 'Please upload profile picture');
    // }
    else if (
      moment(date).format('YYYY-MM-DD') >=
      moment(new Date()).format('YYYY-MM-DD')
    ) {
      // dispatch(showToast({text: 'Please select valid date of birth'}));
      Toast.show('Please select valid date of birth', Toast.SHORT)
      // Alert.alert('', 'Please select valid date of birth');
    } else if (socialSecurityNumber == '') {
      // dispatch(showToast({text: 'Please enter social security number'}));
      Toast.show('Please enter social security number', Toast.SHORT)
      // Alert.alert('', 'Please enter social security number');
    } else if (address == '') {
      // dispatch(showToast({text: 'Please add location'}));
      Toast.show('Please add location', Toast.SHORT)
      // Alert.alert('', 'Please add location');
    } else if (serviceRange == '') {
      // dispatch(showToast({text: 'Please enter range of service'}));
      Toast.show('Please enter range of service', Toast.SHORT)
      // Alert.alert('', 'Please enter range of service');
    } else if (password == '') {
      // dispatch(showToast({text: 'Please enter password'}));
      Toast.show('Please enter password', Toast.SHORT)
      // Alert.alert('', 'Please enter password');
    } else if (confirmPassword == '') {
      // dispatch(showToast({text: 'Please enter confirm password'}));
      Toast.show('Please enter confirm password', Toast.SHORT)
      // Alert.alert('', 'Please enter confirm password');
    } else if (password != confirmPassword) {
      // dispatch(showToast({text: 'Password and confirm password not match'}));
      Toast.show('Password and confirm password not match', Toast.SHORT)
      // Alert.alert('', 'Password and confirm password not match');
    } else return true;
  };
  const requestLocationPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Access Required',
            message: 'This App needs to Access your location',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          getCurrentLocation();
        } else {
          Alert.alert('', 'Location permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };
  const getCurrentLocation = () => {
    setShowLoader(true);
    try {
      Geolocation.getCurrentPosition(
        position => {
          Geocoder.from(position.coords.latitude, position.coords.longitude)
            .then(json => {
              if (json.status == 'OK') {
                // setAllCurrentLocations(json.results);
                var addressComponent = json.results[0].formatted_address;
                setAddress(json.results[0].formatted_address);
                setLatLng(json.results[0].geometry.location);
                setShowLocationModal(true);
                // navigation.navigate(ScreenNames.SELECT_LOCATION, {latLng: json.results[0].geometry.location, user_type: 3})
                console.log(addressComponent);
                setShowLoader(false);
                // setShowSelectAddress(true);
              }
            })
            .catch(error => console.warn(error));
        },
        error => {
          setShowLoader(false);
          // See error code charts below.
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } catch (error) {
      setShowLoader(false);
      console.log('error in updateLocation', error);
    }
  };
  //function : service function
  const ServSignUp = async () => {
    if (Validation()) {
      setShowLoader(true);
      try {
        const servSignUpData = new FormData();
        servSignUpData.append('first_name', firstName);
        servSignUpData.append('last_name', lastName);
        servSignUpData.append('email', emailAddress);
        servSignUpData.append('phone', phoneNumber);
        servSignUpData.append('country_code', selectedCountry.dial_code);
        servSignUpData.append('country_flag', selectedCountry.flag);
        // servSignUpData.append('gender', gender);
        servSignUpData.append('dob', moment(date).format('YYYY-MM-DD'));
        servSignUpData.append('social_security_number', socialSecurityNumber);
        servSignUpData.append('latitude', latLng.lat);
        servSignUpData.append('longtitude', latLng.lng);
        if (filePath != '') {
          const imageName = filePath.uri.slice(
            filePath.uri.lastIndexOf('/'),
            filePath.uri.length,
          );
          servSignUpData.append('profile_image', {
            name: imageName,
            type: filePath.type,
            uri: filePath.uri,
          });
        }
        servSignUpData.append('address', address);
        servSignUpData.append('range', serviceRange);
        if (filePath != '') {
          const imageName = filePath.uri.slice(
            filePath.uri.lastIndexOf('/'),
            filePath.uri.length,
          );
          servSignUpData.append('profile_image', {
            name: imageName,
            type: filePath.type,
            uri: filePath.uri,
          });
        }
        servSignUpData.append('password', password);
        servSignUpData.append('password_confirmation', confirmPassword);
        servSignUpData.append('fcmtoken', fcmToken);
        console.log('servSignUpData', servSignUpData);
        const resp = await Service.postApi(
          Service.SERVICE_PROVIDER_REGISTER,
          servSignUpData,
        );
        console.log('sp signup resp', resp);
        if (resp.data.status) {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
          gotoUploadDocumet(resp.data.user_id);
        } else {
          // Alert.alert('', resp.data.message);
          Toast.show(resp.data.message, Toast.SHORT);
        }
      } catch (error) {
        console.log('error in ServSignUp', error);
      }
      setShowLoader(false);
    }
  };
  //function : imp function
  const openCamera = () => {
    const options = {
      width: 1080,
      height: 1080,
      cropping: true,
      mediaType: 'photo',
      compressImageQuality: 1,
      compressImageMaxHeight: 1080 / 2,
      compressImageMaxWidth: 1080 / 2,
    };
    launchCamera(options, response => {
      if (response.didCancel) {
        // Alert.alert('User cancelled camera picker');
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'User cancelled picking image'}));
        Toast.show('User cancelled picking image', Toast.SHORT)
        // Alert.alert('User cancelled picking image');
        return;
      } else if (response.errorCode == 'camera_unavailable') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Camera not available on device'}));
        Toast.show('Camera not available on device', Toast.SHORT)
        // Alert.alert('Camera not available on device');
        return;
      } else if (response.errorCode == 'permission') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: 'Permission not satisfied'}));
        Toast.show('Permission not satisfied', Toast.SHORT)
        // Alert.alert('Permission not satisfied');
        return;
      } else if (response.errorCode == 'others') {
        setShowImageSourceModal(false);
        // dispatch(showToast({text: response.errorMessage}));
        Toast.show('response.errorMessage', Toast.SHORT)
        // Alert.alert(response.errorMessage);
        return;
      }
      console.log('Response = ', response.assets[0]);
      setFilePath(response.assets[0]);
      setShowImageSourceModal(false);
    });
  };
  //function : imp function
  const checkCameraPermission = async () => {
    if (Platform.OS === 'ios') {
      openCamera();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to access camera',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          openCamera();
          console.log('Storage Permission Granted.');
        } else {
          // dispatch(showToast({text: 'Storage Permission Not Granted'}));
          Toast.show('Storage Permission Not Granted', Toast.SHORT)
          // Alert.alert('Error', 'Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('ERROR' + err);
      }
    }
  };
  const checkToken = async () => {
    try {
      const token = await messaging().getToken();
      if (token) {
        setFcmToken(token);
      } else {
        console.log('could not get fcm token');
      }
    } catch (error) {
      console.log('error in getting fcm token', error);
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    checkToken();
  }, []);
  const downButtonHandler = () => {
    //OnCLick of down button we scrolled the list to bottom
    // listViewRef.current.scrollToEnd({ animated: true });
    listViewRef.current.scrollTo({y: scrollY + 400});
  };
  const gotoWebPage = async (endPoint, name) => {
    navigation.navigate(ScreenNames.SIDE_MENU_LINKS, {
      endPoint,
      name,
      role: 'Service',
    });
  };
  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title={'Sign Up'} />
      <ScrollView
        // ref={listViewRef}
        // onScroll={event => setScrollY(event.nativeEvent.contentOffset.y)}
        keyboardShouldPersistTaps="always"
        contentContainerStyle={{paddingBottom: 50}}
        style={styles.mainView}>
        <MyText
          text="Let's get started"
          fontFamily="bold"
          fontSize={32}
          textColor="theme_green"
        />
        <MyText
          text="Enter your details to create account"
          fontSize={18}
          marginBottom={20}
        />
        {filePath == '' ? (
          // <TouchableOpacity onPress={chooseFile} style={styles.uploadProfile}>
          //   <MyText
          //     text={
          //       filePath == '' ? 'Upload Profile Picture' : filePath.fileName
          //     }
          //     style={{
          //       width: '85%',
          //     }}
          //   />
          //   <MyIcon.AntDesign
          //     name="upload"
          //     size={20}
          //     color={Colors.THEME_GREEN}
          //   />
          // </TouchableOpacity>
          <View style={styles.imageViewStyle}>
            <Image
              resizeMode="contain"
              borderRadius={100}
              source={require('assets/images/profile_pic.png')}
              style={{
                height: '100%',
                width: '100%',
              }}
            />
            <TouchableOpacity
              onPress={() => {
                setShowImageSourceModal(true);
              }}
              style={styles.addButtonStyle}>
              <MyIcon.AntDesign
                name="plus"
                color={Colors.THEME_GREEN}
                size={24}
              />
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.imageViewStyle}>
            <Image
              resizeMode="cover"
              borderRadius={100}
              source={{uri: filePath.uri}}
              style={{height: '100%', width: '100%'}}
            />
            <TouchableOpacity
              onPress={() => setFilePath('')}
              style={styles.deleteButtonStyle}>
              <MyIcon.MaterialIcons
                name="delete"
                color={Colors.RED}
                size={24}
              />
            </TouchableOpacity>
          </View>
        )}
        <MyTextInput
          inputRef={firstNameRef}
          Title="First Name"
          placeholder={'Enter First Name'}
          onChangeText={text => setFirstName(text)}
          onSubmitEditing={() => lastNameRef.current.focus()}
        />
        <MyTextInput
          inputRef={lastNameRef}
          Title="Last Name"
          placeholder={'Enter Last Name'}
          onChangeText={text => setLastName(text)}
          onSubmitEditing={() => emailAddressRef.current.focus()}
        />
        <MyTextInput
          inputRef={emailAddressRef}
          Title="Email Address"
          placeholder={'Enter Email Address'}
          onChangeText={text => setEmailAddress(text)}
          keyboardType="email-address"
          onSubmitEditing={() => phoneNumberRef.current.focus()}
        />
        <TextInputWithFlag
          inputRef={phoneNumberRef}
          value={phoneNumber}
          Flag={selectedCountry.flag}
          CountryCode={selectedCountry.dial_code}
          onPress={() => setShow(true)}
          placeholder="Enter Phone Number"
          maxLength={10}
          keyboardType="number-pad"
          onChangeText={text => setPhoneNumber(text)}
          onSubmitEditing={() => Keyboard.dismiss()}
        />
        {/* <TouchableOpacity onPress={chooseFile} style={styles.uploadProfile}>
          <MyText
            text={filePath == '' ? 'Upload Profile Picture' : filePath.fileName}
            fontFamily="medium"
            textColor="lite_grey"
            style={{
              width: '85%',
            }}
          />
          <MyIcon.AntDesign
            name="upload"
            size={20}
            color={Colors.THEME_GREEN}
          />
        </TouchableOpacity> */}
        {/* <MyText text="Gender" fontFamily="bold" />
        <View style={styles.genderView}>
          {Constant.Gender.map((item, index) => {
            return (
              <TouchableOpacity
                onPress={() => setGender(index + 1)}
                key={item.id.toString()}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <MyIcon.Ionicons
                  name={
                    gender == index + 1 ? 'radio-button-on' : 'radio-button-off'
                  }
                  size={24}
                />
                <MyText text={item.name} />
              </TouchableOpacity>
            );
          })}
        </View> */}
        <MyText text="Birthdate" fontFamily="bold" />
        <DateSelector
          Title={
            moment(date).format('YYYY-MM-DD') ==
            moment(new Date()).format('YYYY-MM-DD')
              ? 'Select Date'
              : // : moment(date).format('MMMM Do YYYY')
                moment(date).format('DD-MM-YYYY')
          }
          onPress={() => setOpen(true)}
          dateViewStyle={{borderWidth:0}}
        />
        <MyTextInput
          inputRef={socialSecurityNumberRef}
          placeholder="Enter Social Security Number"
          onChangeText={text => setSocialSecurityNumber(text)}
          keyboardType="number-pad"
          maxLength={9}
          onSubmitEditing={() => addressRef.current.focus()}
        />
        {/* <TouchableOpacity
          onPress={requestLocationPermission}
          style={{
            backgroundColor: Colors.WHITE,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            shadowColor: '#000',
            shadowOffset: {width: 0, height: 2},
            shadowOpacity: 0.1,
            elevation: 2,
            padding: 10,
            borderRadius: 10,
          }}>
          <MyIcon.FontAwesome5 name="map-marker-alt" size={20} />
          <MyText
            text="Add Location"
            marginHorizontal={10}
            fontFamily="bold"
          />
        </TouchableOpacity> */}
        {/* <MyTextInput
          inputRef={addressRef}
          value={address}
          // placeholder="Enter Address"
          editable={false}
          multiline={true}
          onChangeText={text => setAddress(text)}
          onSubmitEditing={() => passwordRef.current.focus()}
        /> */}
        <GooglePlacesAutocomplete
          placeholder="Add Location"
          textInputProps={{
            placeholderTextColor: '#c9c9c9',
            // placeholderTextColor: Colors.BLACK,
            returnKeyType: 'search',
            // onFocus: () => setShowPlacesList(true),
            // onBlur: () => setShowPlacesList(false),
            multiline: true,
            // onTouchStart: ()=>{downButtonHandler()}
            height: 55,
          }}
          enablePoweredByContainer={false}
          listViewDisplayed={'auto'}
          styles={styles.searchbar}
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true
            // setShowPlacesList(false)
            setLatLng({
              lat: details.geometry.location.lat,
              lng: details.geometry.location.lng,
            });
            setAddress(data?.description);
          }}
          GooglePlacesDetailsQuery={{
            fields: 'geometry',
          }}
          fetchDetails={true}
          query={{
            key: GOOGLE_MAPS_APIKEY,
            language: 'en',
          }}
        />
        <MyTextInput
          placeholder="Enter Your Service Area Range(in miles)"
          keyboardType="number-pad"
          onChangeText={text => setServiceRange(text)}
        />
        <MyPasswordInput
          inputRef={passwordRef}
          Title="Password"
          placeholder="Enter Password"
          onChangeText={text => setPassword(text)}
          showPassword
          secureTextEntry={true}
          ShowIcon={
            <MyIcon.FontAwesome5
              name="eye"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          HideIcon={
            <MyIcon.FontAwesome5
              name="eye-slash"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          onSubmitEditing={() => confirmPasswordRef.current.focus()}
        />
        <MyPasswordInput
          inputRef={confirmPasswordRef}
          Title="Confirm Password"
          placeholder="Enter Confirm Password"
          onChangeText={text => setConfirmPassword(text)}
          textInputWidth="90%"
          showPassword
          secureTextEntry={true}
          ShowIcon={
            <MyIcon.FontAwesome5
              name="eye"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          HideIcon={
            <MyIcon.FontAwesome5
              name="eye-slash"
              size={24}
              style={{
                transform: [
                  {
                    scaleX: -1,
                  },
                ],
              }}
              color={Colors.LITE_GREY}
            />
          }
          onSubmitEditing={() => Keyboard.dismiss()}
        />
        <TermOfUseCheck
          value={isAcceptedTC}
          setValue={setIsAcceptedTC}
          onPress={() => gotoWebPage('terms-condition', 'Terms And Conditions')}
        />
        <View style={{height: 20}} />
        <MyButton Title="Sign Up" onPress={ServSignUp} />
        <TouchableOpacity onPress={gotoSignIn} style={styles.bottomLineStyle}>
          <MyText text="Already have an account? " fontFamily="bold" />
          <MyText text="Sign In" fontFamily="bold" textColor="theme_green" />
        </TouchableOpacity>
        <View style={styles.termsAndPPView}>
          <MyText text="By signing up, you agree to our" />
          <TouchableOpacity
            onPress={() =>
              gotoWebPage('terms-condition', 'Terms And Conditions')
            }>
            <MyText
              text=" Terms and Conditions "
              textColor="theme_green"
              isUnderLine
            />
          </TouchableOpacity>
          <MyText text="and " />
          <TouchableOpacity
            onPress={() => gotoWebPage('privacy-policy', 'Privacy Policy')}>
            <MyText text="Privacy Policy" textColor="theme_green" isUnderLine />
          </TouchableOpacity>
        </View>
      </ScrollView>
      <CustomLoader showLoader={showLoader} />
      <DatePicker
        modal
        mode="date"
        open={open}
        date={date}
        onConfirm={date => {
          if (
            moment(date).format('YYYY-MM-DD') >=
            moment(new Date()).format('YYYY-MM-DD')
          ) {
            setOpen(false);
            // dispatch(showToast({text: 'Please select valid date of birth'}));
            Toast.show('Please select valid date of birth', Toast.SHORT)
            // Alert.alert('', 'Please select valid date of birth');
          } else if (
            // moment(new Date()).format('YYYY') - moment(date).format('YYYY') <
            moment().diff(date, 'years', true) < 18
          ) {
            setOpen(false);
            // dispatch(showToast({text: 'Your age must be at least 18 years'}));
            Toast.show('Your age must be at least 18 years', Toast.SHORT)
            // Alert.alert('', 'Your age must be at least 18 years');
          } else {
            setOpen(false);
            setDate(date);
          }
        }}
        onCancel={() => {
          setOpen(false);
        }}
      />
      <SelectAddress
        visible={showSelectAddress}
        setVisibility={setShowSelectAddress}
        Addresses={allCurrentLocations}
        setValue={setAddress}
        setLatLng={setLatLng}
      />
      <CountryPicker
        show={show}
        disableBackdrop={false}
        style={styles.countrySilderStyle}
        // when picker button press you will get the country object with dial code
        pickerButtonOnPress={item => {
          // console.warn('item', item);
          // setCountryCode(item.dial_code);
          setSelectedCountry(item);
          setShow(false);
        }}
        placeholderTextColor={'#c9c9c9'}
        color={Colors.BLACK}
        onBackdropPress={() => setShow(false)}
      />
      <SelectImageSource
        visible={showImageSourceModal}
        setVisibility={setShowImageSourceModal}
        openLibrary={openLibrary}
        openCamera={checkCameraPermission}
      />
      <SearchLocation
        visible={showLocationModal}
        setVisibility={setShowLocationModal}
        setAddress={setAddress}
        setLatLng={setLatLng}
      />
    </View>
  );
};
export default ServiceSignUp;
