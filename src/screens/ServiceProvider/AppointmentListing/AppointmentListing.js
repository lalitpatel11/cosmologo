//react components
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  PermissionsAndroid,
  TouchableOpacity,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import AppointmentCard from 'components/AppointmentCard/AppointmentCard';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyText from 'components/MyText/MyText';
import NoInternet from 'components/NoInternet/NoInternet';
//globals
import {ScreenNames, Service, Colors, MyIcon, Constant} from 'global/Index';
//styles
import {styles} from './AppointmentListingStyle';
//redux
import {useSelector, useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import MyHeader from 'components/MyHeader/MyHeader';
//import : third parties
import RNFetchBlob from 'rn-fetch-blob';
import {Dropdown} from 'react-native-element-dropdown';
import moment from 'moment';
import AppointmentCalender from 'modals/AppointmentCalender/AppointmentCalender';
import BufferTime from 'modals/BufferTime/BufferTime';
import Toast from 'react-native-simple-toast';
import NetInfo, {useNetInfo} from "@react-native-community/netinfo";

//data
const data = [
  {label: 'All', value: 'all'},
  {label: 'Today', value: 'today'},
  {label: '5 days', value: '5'},
  {label: '10 days', value: '10'},
  {label: '20 days', value: '20'},
  {label: '30 days', value: '30'},
];
const AppointmentListing = ({navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  const dispatch = useDispatch();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [showLoader2, setShowLoader2] = useState(false);
  const [appointmentData, setAppointmentData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [isFocus, setIsFocus] = useState(false);
  const [numDays, setNumDays] = useState(null);
  const [showCalendarModal, setShowCalendarModal] = useState(false);
  const [showBufferTimeModal, setShowBufferTimeModal] = useState(false);
  const [selectedAptId, setSelectedAptId] = useState(null);
  const [startDay, setStartDay] = useState(null);
  const [endDay, setEndDay] = useState(null);
  const [markedDates, setMarkedDates] = useState({});
  const [temporarySelectedDates, setTemporarySelectedDates] = useState([]);
  const [bufferTimes, setBufferTimes] = useState([]);
  const [selectedTimeId, setSelectedTimeId] = useState('');
  const [selectedTime, setSelectedTime] = useState('');
  const [isOnline, setIsOnline] = useState(true);
  //function : navigation function
  const gotoAppointmentDetail = (appointment_id, status, service_time) =>
    navigation.navigate(ScreenNames.SERVICE_APPOINTMENT_DETAILS, {
      appointment_id,
      status,
      service_time,
    });
  //function : service function
  const getOngoingAppnt = async () => {
    setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('service_provider_id', userInfo.id);
      // myData.append('is_verified', Constant.ApprovalStatus.PENDING);
      // myData.append('is_approved', Constant.ApprovalStatus.PENDING);
      myData.append('filter', 'new');
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SERVICE_PROVIDER_ONGOING_APPOINTMENT,
        myData,
      );
      if (resp.data.status) {
        // console.warn('resp---->', resp.data);
        //Note: is_verified = 2(cancel),1(approved),0(not action taken)
        console.log('setAppointmentData', resp.data.data);
        // const updatedData = resp.data.data?.filter(el=>el.is_approved == 0 && el.status === 'Processing' && el.is_verified == 0)
        const updatedData2 = resp.data.data?.map(el => {
          const newTime = el?.date_time.replace('PM', '');
          const newTime2 = moment(
            moment(newTime).format('dddd,D MMMM YYYY,kk:mm'),
          ).format('ddd, D MMM YYYY, h:mm A');
          return {...el, date_time: newTime2};
        });
        setAppointmentData(updatedData2);
        setFilteredData(updatedData2);
      }
    } catch (error) {
      console.log('error in getOngoingAppnt', error);
    }
    setShowLoader(false);
  };
  const getBufferTimes = async () => {
    setShowLoader2(true);
    try {
      const resp = await Service.postApiWithToken(
        userToken,
        Service.ADD_BUFFER_TIME,
        {}
      );
      if (resp.data.status) {
        // console.warn('resp---->', resp.data);
        //Note: is_verified = 2(cancel),1(approved),0(not action taken)
        console.log('getBufferTimes', resp.data.data);
        // const updatedData = resp.data.data?.filter(el=>el.is_approved == 0 && el.status === 'Processing' && el.is_verified == 0)
        setBufferTimes(resp.data.data?.map(el=>el?.time))
        console.log('setBufferTimes', resp.data.data?.map(el=>el.time));
      }
    } catch (error) {
      console.log('error in getBufferTimes', error);
    }
    setShowLoader2(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getOngoingAppnt();
      getBufferTimes();
    });
    return unsubscribe;
  }, [navigation]);

  //function : get invoice url
  const getInvoiceUrl = async (appointmentId, bookingId) => {
    try {
      const endPoint = `${Service.DOWNLOAD_INVOICE}?appointment_id=${appointmentId}`;
      const res = await Service.postApiWithToken(userToken, endPoint, {});
      console.log('getInvoiceUrl res', res.data);
      if (res.data.status) {
        checkPermission(res.data.url, appointmentId, bookingId);
      }
    } catch (error) {
      console.log('error', error);
    }
  };
  //function : imp function
  const checkPermission = async (url, appointmentId, bookingId) => {
    if (Platform.OS === 'ios') {
      downloadFile(url, appointmentId, bookingId);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadFile(url, appointmentId, bookingId);
          console.log('Storage Permission Granted.');
        } else {
          Toast.show('Storage Permission Not Granted', Toast.LONG);
          // Alert.alert('Error', 'Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('ERROR' + err);
      }
    }
  };
  //function : service function
  const downloadFile = async (url, appointmentId, bookingId) => {
    let pdfUrl = url;
    let DownloadDir =
      Platform.OS == 'ios'
        ? RNFetchBlob.fs.dirs.DocumentDir
        : RNFetchBlob.fs.dirs.DownloadDir;
    const {dirs} = RNFetchBlob.fs;
    const dirToSave =
      Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
    const configfb = {
      fileCache: true,
      useDownloadManager: true,
      notification: true,
      mediaScannable: true,
      title: 'Cosmologo',
      path: `${dirToSave}.pdf`,
    };
    const configOptions = Platform.select({
      ios: {
        fileCache: configfb.fileCache,
        title: configfb.title,
        path: configfb.path,
        appendExt: 'pdf',
      },
      android: configfb,
    });
    Platform.OS == 'android'
      ? RNFetchBlob.config({
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path: `${DownloadDir}/.pdf`,
            description: 'Cosmologo',
            title: `${bookingId}invoice.pdf`,
            mime: 'application/pdf',
            mediaScannable: true,
          },
        })
          .fetch('GET', `${pdfUrl}`)
          .catch(error => {
            console.warn(error.message);
          })
      : RNFetchBlob.config(configOptions)
          .fetch('GET', `${pdfUrl}`, {})
          .then(res => {
            if (Platform.OS === 'ios') {
              RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
              RNFetchBlob.ios.previewDocument(configfb.path);
            }
            console.log('The file saved to ', res);
          })
          .catch(e => {
            console.log('The file saved to ERROR', e.message);
          });
  };

  const acceptAppointment = async (addBufferTime) => {
    setShowLoader(true);
    try {
      const requiredData = new FormData();
      requiredData.append('appointment_id', selectedAptId);
      requiredData.append('varify_id', 1);
      if(addBufferTime){
        requiredData.append('time', selectedTime);
      }else{
        requiredData.append('time', 0);
        // requiredData.append('time', 0);
      }
      console.log('requiredData', requiredData);
      // if(selectedAptId !== ''){
      //   requiredData.append('time', selectedTimeId);
      // }
      const resp = await Service.postApiWithToken(
        userToken,
        Service.VERIFY_APPOINTMENT,
        requiredData,
      );
      console.log('acceptAppointment resp', resp);
      if (resp.data.status) {
        // dispatch(showToast({text:'Appointment accepted', duration:2000}))
        Toast.show('Appointment accepted', Toast.SHORT);
        setFilteredData(filteredData.filter(el => el.id !== selectedAptId));
        setAppointmentData(
          appointmentData.filter(el => el.id !== selectedAptId),
        );
        // getOngoingAppnt()
        console.log('acceptAppointment', resp.data);
        console.warn('resp------>', resp.data);
      }else{
        Toast.show(resp.data.message, Toast.SHORT);
      }
    } catch (error) {
      console.log('error in acceptAppointment', error);
    }
    setShowLoader(false);
    setShowBufferTimeModal(false)
  };
  const rejectAppointment = async appointmentId => {
    setShowLoader(true);
    try {
      const requiredData = new FormData();
      requiredData.append('appointment_id', appointmentId);
      requiredData.append('varify_id', 2);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.VERIFY_APPOINTMENT,
        requiredData,
      );
      if (resp.data.status) {
        // dispatch(showToast({text:'Appointment cancelled', duration:2000}))
        Toast.show('Appointment Rejected', Toast.SHORT);
        setFilteredData(filteredData.filter(el => el.id !== appointmentId));
        setAppointmentData(
          appointmentData.filter(el => el.id !== appointmentId),
        );
        // getOngoingAppnt()
        console.log('rejectAppointment', resp.data);
        console.warn('resp------>', resp.data);
      }
    } catch (error) {
      console.log('error in rejectAppointment', error);
    }
    setShowLoader(false);
  };

  const changeNumDays = item => {
    // ! tell sanjay is 24 hrs date_time... then don't add am/pm
    setNumDays(item.value);
    if (item.value === 'all') {
      clearSelection();
      setFilteredData(appointmentData);
      return;
    }
    const filteredByDays = appointmentData.filter(el => {
      const newTime = el?.date_time.replace('PM', '');
      // console.log('newTime', newTime);
      // console.log('today', moment(moment(newTime).format('YYYY-MM-DD')).isSame(moment().format('YYYY-MM-DD')))
      // console.log('between', newTime, moment(moment(newTime)).isBetween(moment().format('YYYY-MM-DD'), moment(moment().format('YYYY-MM-DD')).add(10, 'days')))
      if (
        item.value === 'today' &&
        moment(moment(newTime).format('YYYY-MM-DD')).isSame(
          moment().format('YYYY-MM-DD'),
        )
      ) {
        return true;
      }
      if (
        item.value === '5' &&
        moment(moment(newTime)).isBetween(
          moment().format('YYYY-MM-DD'),
          moment(moment().format('YYYY-MM-DD')).add(6, 'days'),
        )
      ) {
        return true;
      }
      if (
        item.value === '10' &&
        moment(moment(newTime)).isBetween(
          moment().format('YYYY-MM-DD'),
          moment(moment().format('YYYY-MM-DD')).add(11, 'days'),
        )
      ) {
        return true;
      }
      if (
        item.value === '20' &&
        moment(moment(newTime)).isBetween(
          moment().format('YYYY-MM-DD'),
          moment(moment().format('YYYY-MM-DD')).add(21, 'days'),
        )
      ) {
        return true;
      }
      if (
        item.value === '30' &&
        moment(moment(newTime)).isBetween(
          moment().format('YYYY-MM-DD'),
          moment(moment().format('YYYY-MM-DD')).add(31, 'days'),
        )
      ) {
        return true;
      }
      return false;
    });
    clearSelection();
    setFilteredData(filteredByDays);
  };
  const OKPressed = () => {
    if (temporarySelectedDates?.length === 0) {
      // dispatch(showToast({text:'Please select a date', duration:2000}))
      setShowCalendarModal(false);
      return;
    }
    const filteredByDays = appointmentData.filter(el => {
      const newTime = el?.date_time.replace('PM', '');
      if (
        temporarySelectedDates?.length === 1 &&
        moment(moment(newTime).format('YYYY-MM-DD')).isSame(
          moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
        )
      ) {
        return true;
      } else if (
        temporarySelectedDates?.length >= 2 &&
        moment(moment(newTime)).isBetween(
          moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
          moment(
            moment(
              temporarySelectedDates[temporarySelectedDates?.length - 1],
            ).format('YYYY-MM-DD'),
          ).add(1, 'days'),
        )
      ) {
        return true;
      }
      return false;
    });
    setFilteredData(filteredByDays);
    setNumDays(null);
    setShowCalendarModal(false);
  };
  const clearSelection = () => {
    setTemporarySelectedDates([]);
    setMarkedDates({});
    setStartDay(null);
    setEndDay(null);
    setFilteredData(appointmentData);
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="New Appointments" />
      <ScrollView
        contentContainerStyle={{paddingBottom: '10%'}}
        style={styles.mainView}>
        <View
          style={{
            marginBottom: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Dropdown
            style={[
              styles.dropdown,
              isFocus && {borderColor: Colors.THEME_GREEN},
            ]}
            data={data}
            containerStyle={{backgroundColor: Colors.BG_GREEN}}
            activeColor={Colors.LITE_GREEN}
            maxHeight={300}
            labelField="label"
            valueField="value"
            placeholder={!isFocus ? 'Select Days' : 'Select Days'}
            value={numDays}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
            onChange={item => {
              changeNumDays(item);
              setIsFocus(false);
            }}
            iconStyle={{width: 40, height: 40}}
            iconColor={Colors.BLACK}
            placeholderStyle={{marginLeft: 10}}
            selectedTextStyle={{marginLeft: 10}}
          />
          <TouchableOpacity
            onPress={() => {
              setShowCalendarModal(true);
            }}
            style={{
              backgroundColor: Colors.BG_GREEN,
              borderRadius: 10,
              padding: 10,
              flex: 0.1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {temporarySelectedDates?.length === 0 ? (
              <MyIcon.AntDesign
                name="calendar"
                size={24}
                color={Colors.THEME_GREEN}
              />
            ) : (
              <MyIcon.FontAwesome5
                name="calendar-check"
                size={24}
                color={Colors.THEME_GREEN}
              />
            )}
          </TouchableOpacity>
        </View>
        {/* <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: Colors.WHITE,
            marginTop: 10,
            padding: 10,
            borderRadius: 10,
            marginTop:20
          }}>
          <MyText text="New Appointments Received" fontFamily="bold" />
          <TouchableOpacity
            style={{
              backgroundColor: Colors.BG_GREEN,
              borderRadius: 30,
              padding: 5,
            }}>
            <MyIcon.AntDesign name="info" size={15} color={'#294527'} />
          </TouchableOpacity>
        </View> */}
        <MyText
            text={`${filteredData.length} ${
              filteredData.length === 1 || filteredData.length === 0
                ? 'Appointment found'
                : 'Appointments found'
            }`}
            marginVertical={20}
            fontFamily="medium"
          />
        {filteredData.length > 0 ? (
          filteredData.map((item, index) => {
            return (
              <AppointmentCard
                key={index.toString()}
                BookingID={item.booking_id}
                SubCategory={item.subcategory}
                ConcernArea={item.concern_area_title}
                Product={item.product}
                ImageUrl={item.concern_area_image}
                Amount={parseFloat(item.amount).toFixed(2)}
                isDoctor={false}
                isVerified={item.is_verified}
                AppointmentTime={item.date_time}
                UserName={item.user_name}
                userImageUrl={item.user_profile_image}
                onPress={() =>
                  gotoAppointmentDetail(
                    item.id,
                    item.status,
                    `${item.service_time} ${item.service_time_unit}`,
                  )
                }
                downloadInvoice={() => getInvoiceUrl(item.id, item.booking_id)}
                acceptAppointment={() => {setSelectedAptId(item.id); setShowBufferTimeModal(true);}}
                rejectAppointment={() => rejectAppointment(item.id)}
                Role={'Service Provider'}
                showButtons={true}
                showStatus={true}
                generate={true}
                status={item.status}
                user_address={item.user_address}
                showAppointmentDuration={true}
                service_time={`${item.service_time} ${item.service_time_unit}`}
              />
            );
          })
        ) : (
          <MyText
            text={'No Appointments found'}
            fontSize={16}
            fontFamily="medium"
            // textColor="white"
            textAlign="center"
            marginVertical={10}
          />
        )}
      </ScrollView>
      <AppointmentCalender
        visible={showCalendarModal}
        setVisibility={setShowCalendarModal}
        startDay={startDay}
        setStartDay={setStartDay}
        endDay={endDay}
        setEndDay={setEndDay}
        temporarySelectedDates={temporarySelectedDates}
        setTemporarySelectedDates={setTemporarySelectedDates}
        markedDates={markedDates}
        setMarkedDates={setMarkedDates}
        OKPressed={OKPressed}
        clearSelection={clearSelection}
      />
      <BufferTime 
        visible={showBufferTimeModal} 
        setVisibility={setShowBufferTimeModal} 
        // times={['15 mins','30 mins','45 mins','60 mins','75 mins',]}
        times={bufferTimes}
        selectedTime={selectedTime} 
        setSelectedTime={setSelectedTime} 
        acceptAppointment={acceptAppointment}
        />
      <CustomLoader showLoader={showLoader || showLoader2} />
    </View>
  );
};

export default AppointmentListing;
