//react components
import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyButton from 'components/MyButton/MyButton';
import ServiceProfile from 'components/ServiceCard/ServiceProfile';
import ImageSelector from 'components/ServiceCard/ImageSelector';
//globals
import {Colors, Images, MyIcon} from 'global/Index';
//styles
import {styles} from './EditServiceStyle';

const EditService = () => {
  return (
    <View style={styles.container}>
      <MyHeader Title="Edit Service" />
      <View style={styles.mainView}>
        <ServiceProfile />
        <ImageSelector />
        <MyText text="Procedure Descrition" fontSize={16} fontFamily="bold" />
        <MyTextInput
          placeholder="Lorem ipsum kjsfdu kgfds kldjfg kldfhg kljdfhg ilfdg lgf gjdfjbgljkdg gebgiug gbrijgberiug er irfdklgrg rkhoiwefgb f kusdfyg uksfy jsadfrgsf ksghdiar yuierwt lonvci saiot afhuk aewrb jxcbv"
          multiline={true}
        />
      </View>
      <View style={styles.bottomSection}>
        <View style={styles.flexRowView}>
          <MyButton
            Title="Cancel"
            width="48%"
            borderColor={Colors.THEME_GREEN}
          />
          <MyButton Title="Save Details" width="48%" />
        </View>
      </View>
    </View>
  );
};

export default EditService;
