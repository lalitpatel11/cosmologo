import {Colors, Constant} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  AppIcon: {
    width: Constant.width / 1.5,
    height: Constant.width / 2,
    alignSelf: 'center',
  },
  subCategoryView: {
    // flexDirection: 'row',
    // flexWrap: 'wrap',
  },
  bottomSection: {
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: '5%',
    // position: 'absolute',
    // bottom: 20,
    // right: 0,
    // left: 0,
  },
});
