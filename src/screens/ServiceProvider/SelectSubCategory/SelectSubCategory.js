//react components
import React, {useState, useEffect} from 'react';
import {View, ScrollView, Image, TouchableOpacity, Alert, FlatList} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//modals
import UnselectService from 'modals/UnselectService/UnselectService'; 
//global
import {
  Colors,
  Constant,
  Images,
  MyIcon,
  ScreenNames,
  Service,
} from 'global/Index';
//styles
import {styles} from './SelectSubCategoryStyle';
//redux
import {useDispatch, useSelector} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {useNetInfo} from "@react-native-community/netinfo";
// third parties
import Toast from 'react-native-simple-toast';

const SelectSubCategory = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const selectedCategory = route.params.selectedCategory;
  const categoryiesData = route.params.categoryiesData;
  const userId = route.params.userId;
  //variables
  const dispatch = useDispatch();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [subCategoriesData, setSubCategoriesData] = useState([]);
  const [subcatByCat, setSubcatByCat] = useState([]);
  const [selectedSubCategory, setSelectedSubCategory] = useState([]);
  const [openUnselectServiceModal, setOpenUnselectServiceModal] = useState(false);
  const [unselectMessage, setUnselectMessage] = useState('');
  const [text1, setText1] = useState('');
  const [values, setValues] = useState([]);
  const [text2, setText2] = useState('');
  const [selected, setSelected] = useState([]);
  //function : navigation function

  const gotoServiceability = () =>
    navigation.navigate(ScreenNames.AREA_OF_SERVICEABILITY, {
      selectedSubCategory,
      userId,
    });
  const gotoSelectProduct = () => {
    // setOpenUnselectServiceModal(false)
    navigation.navigate(ScreenNames.SERVICE_SELECT_PRODUCT, {
      selectedSubCategory,
      selectedCategory,
      subCategoriesData: subCategoriesData,
      userId,
    });
  }
  //function : imp function
  const selectItem = item => {
    const index = selectedSubCategory.findIndex(e => e == item.id);
    if (index > -1) {
      const filterArray = selectedSubCategory.filter(e => e != item.id);
      setSelectedSubCategory(filterArray);
      const temp = [] 
      subcatByCat.map((el, index)=>{
        let count = 0
        el?.map(el2=>{
          if(filterArray.includes(el2.id)){
            count++
          }
        })
        temp[index] = count
      })
      setSelected(temp) 
    } else {
      setSelectedSubCategory([...selectedSubCategory, item.id]);
      const temp = []
      subcatByCat.map((el, index)=>{
        let count = 0
        el?.map(el2=>{
          if([...selectedSubCategory, item.id].includes(el2.id)){
            count++
          }
        })
        temp[index] = count
      })
      setSelected(temp)
    }
  };
  const selectSubCtgry = () => {
    const notSelectedIndexes = []
    for(let i = 0; i < selected.length; i++){
      // console.log('selected[i]', selected[i]);
      if(selected[i] == 0){
        notSelectedIndexes.push(i)
        // categoryiesData?.find(el=>el.id == subcatByCat[0][0].service_category_id)?.title
      }
    }
    const unselectedCategoryIds = []
    subcatByCat.map((sb, index)=>{
      if(notSelectedIndexes.includes(index)){
        unselectedCategoryIds.push(sb[0]?.service_category_id)
      }
    })
    // console.log('unselectedCategoryIds', unselectedCategoryIds);
    const unselectedCategories = []
    unselectedCategoryIds.map(el=>{
      const catName = categoryiesData?.find(el2=>el2?.id == el)?.title
      unselectedCategories.push(catName)
    })
    console.log('unselectedCategories', unselectedCategories);
    if(selected.map(el=>el).reduce((a,b)=>a+b, 0) === 0){
      Toast.show('Please select at least one sub category from any one of the categories', Toast.LONG)
      return
    }
    // if(unselectedCategories?.length > 0){
    //   setText1('Please select at least one sub category for')
    //   setValues(unselectedCategories)
    //   setText2('Else above categories will be removed')
    //   setOpenUnselectServiceModal(true)
    //   return
    // }
    // unselectedCategories?.length > 0 && Toast.show(`Please select at least one sub category for ${unselectedCategories.join(', ')}`, Toast.SHORT)  
    if (selectedSubCategory.length > 0) {
      console.warn(selectedSubCategory);
      // gotoServiceability();
      gotoSelectProduct();
    } else {
      // dispatch(showToast({text: 'Please select sub category'}));
      Toast.show('Please select sub category', Toast.SHORT)
      // Alert.alert('', 'Please select sub category');
    }
  };
  //function : service function
  const getSubCategories = async () => {
    setShowLoader(true);
    try {
      const subCategoryData = new FormData();
      // subCategoryData.append(`service_category_id`, selectedCategory);
      selectedCategory.map((e, i) => {
      subCategoryData.append(`service_category_id[${i}]`, e);
      });
      const resp = await Service.postApi(
        Service.SERVICE_SUB_CATEGORY,
        subCategoryData,
      );
      if (resp.data.status) {
        let serIndex = 0
        const data = resp.data.data?.map(el=>{
          serIndex++
          return {...el, serIndex: String(serIndex) }
        })
        // console.log('sub data', data);
        const unique = [...new Set(data.map(item => item.service_category_id))]
        // console.log('unique', unique);
        const allSubCats = []
        unique.map((un, index)=>{
          const subCat = []
          data.map(el=>{
            if(el.service_category_id === un){
              // final[un].push(el)
              // console.log('final[index]', final[index]);
              subCat.push(el)
            }
          })
          var obj = {};
          obj[un] = subCat;
          // allSubCats.push(obj);
          allSubCats.push(subCat);
          // console.log('allSubCats', allSubCats);
        })
        setSubcatByCat(allSubCats)
        setSelected(new Array(allSubCats?.length).fill(0))
        // console.log('new Array(allSubCats?.length).fill(0)', new Array(allSubCats?.length).fill(0));
        setSubCategoriesData(data);
        console.log('setSubCategoriesData', data);
        // console.warn(data);
      }
    } catch (error) {
      console.log('error in getSubCategories', error);
    }
    setShowLoader(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  useEffect(()=>{
    console.log('categoryiesData', categoryiesData);
  },[])
  //useEffet
  useEffect(() => {
    getSubCategories();
    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title={`Sub Category`} />
      <ScrollView
        contentContainerStyle={{paddingBottom: 50}}
        style={styles.mainView}>
        {/* <MyText
          text={`Choose Sub-Category`}
          fontFamily="bold"
          textAlign="center"
          marginBottom={20}
        />
        <Image
          resizeMode="contain"
          source={Images.AppIcon}
          style={styles.AppIcon}
        /> */}
        <View style={styles.subCategoryView}>
          {subcatByCat?.map((subcat, index)=>{
              return (
                <View>
                <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center',marginBottom:10, marginTop:20}}>
                  <View style={{flexDirection:'row', alignItems:'center', width:'60%'}}>
                  <MyText
                    text={`${categoryiesData?.find(el=>el.id == subcat[0].service_category_id)?.title}`}
                    // text={`category 1`}
                    fontFamily="medium"
                    numberOfLines={2}
                    fontSize={18}
                    textColor={'#3E5869'}
                    // width={'80%'}
                    marginLeft={10}
                  />
                  <MyText
                    text={`(${selected[index]})`}
                    // text={`category 1`}
                    fontFamily="medium"
                    textColor="theme_green"
                    numberOfLines={2}
                    fontSize={18}
                    // width={'80%'}
                    marginLeft={5}
                  />
                  </View>
                  {subcat?.length > 2 ?
                  <View style={{flexDirection:'row', alignItems:'center', alignSelf:'flex-end', marginRight:10}}>
                    <MyText
                      text={'More'}
                      // text={`category 1`}
                      fontFamily="medium"
                      textColor="theme_green"
                      numberOfLines={2}
                      fontSize={14}
                      // width={'80%'}
                      marginLeft={5}
                    />
                    <Image source={require('assets/images/caret-right.png')} resizeMode='cover' style={{height:25, width:25}}/>
                  </View>
                  :null}
                </View>
                <FlatList
                  // showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  data={subcat}
                  // keyExtractor={(_, index) => index.toString()}
                  keyExtractor={item => item.serIndex}
                  renderItem={({item, index}) => (
                    <TouchableOpacity
                      // key={item.id.toString()}
                      key={item.serIndex}
                      onPress={() => selectItem(item)}
                      style={{
                        width: Constant.width / 2 - 40,
                        height: Constant.width / 2 - 50,
                        margin: 5,
                        backgroundColor: Colors.WHITE,
                        borderRadius: 20,
                        borderWidth:
                          selectedSubCategory.indexOf(item.id) > -1 ? 2 : 0,
                        borderColor: Colors.THEME_GREEN,
                        alignItems:'center',
                        // justifyContent:'center',
                        shadowColor: '#459743',
                        shadowOffset: {width: 0, height: 5},
                        shadowOpacity: 0.8,
                        shadowRadius: 15,
                        elevation: 5,
                      }}>
                      <Image
                        // resizeMode="contain"
                        source={{uri: item.background_image}}
                        style={{
                          width: 70,
                          height: 70,
                          borderRadius: 35,
                          marginTop:15
                        }}
                      />
                        <MyText
                          text={item.title}
                          fontFamily="medium"
                          textColor='#3E5869'
                          numberOfLines={2}
                          fontSize={14}
                          marginTop={20}
                          textAlign='center'
                        />
                        {selectedSubCategory.indexOf(item.id) > -1 ? 
                        <Image source={require('assets/images/check-circle-service.png')} resizeMode='cover' style={{height:25, width:25,position:'absolute', right:10, top:10}}/>
                        :null}
                    </TouchableOpacity>
                  )}
                />
                </View>
              )
          })}
          {/* {subCategoriesData.map((item, index) => {
            return (
              <TouchableOpacity
                key={item.id.toString()}
                onPress={() => selectItem(item)}
                style={{
                  width: Constant.width / 2 - 40,
                  height: 200,
                  margin: 10,
                  backgroundColor: Colors.WHITE,
                  borderRadius: 20,
                  borderWidth:
                    selectedSubCategory.indexOf(item.id) > -1 ? 2 : 0,
                  borderColor: Colors.THEME_GREEN,
                }}>
                <Image
                  // resizeMode="contain"
                  source={{uri: item.background_image}}
                  style={{
                    width: '100%',
                    height: '70%',
                    borderRadius: 20,
                  }}
                />
                <View
                  style={{
                    position: 'absolute',
                    bottom: 10,
                    left: 5,
                    right: 5,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    marginTop: 10,
                  }}>
                  <MyText
                    text={item.title}
                    fontFamily="medium"
                    textColor="theme_green"
                    numberOfLines={2}
                    fontSize={16}
                    width={'80%'}
                  />
                  <MyIcon.Feather
                    name="arrow-right-circle"
                    size={24}
                    color={Colors.THEME_GREEN}
                  />
                </View>
              </TouchableOpacity>
            );
          })} */}
        </View>
      </ScrollView>
      <View style={styles.bottomSection}>
        <MyButton Title="Next" onPress={selectSubCtgry} />
        {/* <TouchableOpacity>
          <MyText
            text="Add More Category"
            textColor="theme_green"
            textAlign="center"
            marginBottom={10}
          />
        </TouchableOpacity> */}
      </View>
      <UnselectService
        visible={openUnselectServiceModal}
        setVisibility={setOpenUnselectServiceModal}
        text1={text1}
        values={values}
        text2={text2}
        next={gotoSelectProduct}
        // deleteSlot={() => deleteSlot(selectedItem?.serviceUnavailableId)}
      />
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default SelectSubCategory;
