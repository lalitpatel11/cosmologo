//react components
import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  ScrollView,
  PermissionsAndroid,
  TouchableOpacity,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import AppointmentCard from 'components/AppointmentCard/AppointmentCard';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyText from 'components/MyText/MyText';
import Toast from 'react-native-simple-toast';
//globals
import {ScreenNames, Service, Colors, MyIcon, Constant} from 'global/Index';
//styles
import {styles} from './MyAppointmentStyle';
//redux
import {useSelector, useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import MyHeader from 'components/MyHeader/MyHeader';
//import : third parties
import RNFetchBlob from 'rn-fetch-blob';
import {Dropdown} from 'react-native-element-dropdown';
import moment from 'moment';
import {useNetInfo} from "@react-native-community/netinfo";
//import : modals
import AppointmentCalender from 'modals/AppointmentCalender/AppointmentCalender';
import VerifyBooking from 'modals/VerifyBooking/VerifyBooking';
import VerifyApptOtp from 'modals/VerifyApptOtp/VerifyApptOtp';
import ConfirmBooking from 'modals/ConfirmBooking/ConfirmBooking';

//data
const data = [
  {label: 'All', value: 'All'}, //show verified, completed and cancelled appointments
  {label: 'Cancelled', value: 'Cancelled'},
  {label: 'Completed', value: 'Completed'},
];
//Note: only show cancelled (cancel in new appointments) and completed (complete in accepted appointments) and verified appointments on this screen, and allow filtering by status
// additioanlly, for cancelled appointments, show refund button next to cancelled text
const MyAppointment = ({navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  const dispatch = useDispatch();
  //ref
  const firstCodeRef = useRef();
  const secondCodeRef = useRef();
  const thirdCodeRef = useRef();
  const forthCodeRef = useRef();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [appointmentData, setAppointmentData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [isFocus, setIsFocus] = useState(false);
  const [appointmentStatus, setAppointmentStatus] = useState(null);
  const [showCalendarModal, setShowCalendarModal] = useState(false);
  const [startDay, setStartDay] = useState(null);
  const [endDay, setEndDay] = useState(null);
  const [markedDates, setMarkedDates] = useState({});
  const [temporarySelectedDates, setTemporarySelectedDates] = useState([]);
  const [showVerifyBooking, setShowVerifyBooking] = useState(false);
  const [selectedBookingId, setSelectedBookingId] = useState('');
  const [generateOtp, setGenerateOtp] = useState(false);
  const [showConfirmBooking, setShowConfirmBooking] = useState(false);
  const [firstCode, setFirstCode] = useState('');
  const [secondCode, setSecondCode] = useState('');
  const [thirdCode, setThirdCode] = useState('');
  const [forthCode, setForthCode] = useState('');
  //function : navigation function
  const gotoAppointmentDetail = (
    appointment_id,
    status,
    service_time,
    approvedStatus,
  ) =>
    navigation.navigate(ScreenNames.SERVICE_APPOINTMENT_DETAILS, {
      appointment_id,
      status,
      service_time,
      approvedStatus,
      disableManageStatus: 'yes',
    });
  //function : service function
  const getOngoingAppnt = async () => {
    setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('service_provider_id', userInfo.id);
      myData.append('filter', 'all');
      // myData.append('is_verified', Constant.ApprovalStatus.ACCEPTED);
      // myData.append('status_id', [4]);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SERVICE_PROVIDER_ONGOING_APPOINTMENT,
        myData,
      );
      console.log('trying to get appointments', resp.data.data);
      if (resp.data.status) {
        // console.warn('resp---->', resp.data);
        //Note: is_verified = 2(cancel),1(approved),0(not verified)
        console.log('myapt setAppointmentData', resp.data.data);
        //show all these: is_verified: cancelled (0), is_verified: verified, status: 'Completed'
        // const updatedData = resp.data.data?.filter(el=>el.is_approved == '3' || el.is_approved == '4' || el.status === 'Completed')
        // const updatedData2 = updatedData.filter(el=>el.status !== 'Processing')
        const updatedData3 = resp.data.data?.map(el => {
          const newTime = el?.date_time.replace('PM', '');
          const newTime2 = moment(
            moment(newTime).format('dddd,D MMMM YYYY,kk:mm'),
          ).format('ddd, D MMM YYYY, h:mm A');
          return {...el, date_time: newTime2};
        });
        console.log('updatedData3', updatedData3);
        setAppointmentData([...updatedData3]);
        setFilteredData([...updatedData3]);
      }
    } catch (error) {
      console.log('error in getOngoingAppnt', error);
    }
    setShowLoader(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getOngoingAppnt();
    });
    return unsubscribe;
  }, [navigation]);

  //function : get invoice url
  const getInvoiceUrl = async (appointmentId, bookingId) => {
    try {
      const endPoint = `${Service.DOWNLOAD_INVOICE}?appointment_id=${appointmentId}`;
      const res = await Service.postApiWithToken(userToken, endPoint, {});
      console.log('getInvoiceUrl res', res.data);
      if (res.data.status) {
        checkPermission(res.data.url, appointmentId, bookingId);
      }
    } catch (error) {
      console.log('error', error);
    }
  };
  //function : imp function
  const checkPermission = async (url, appointmentId, bookingId) => {
    if (Platform.OS === 'ios') {
      downloadFile(url, appointmentId, bookingId);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadFile(url, appointmentId, bookingId);
          console.log('Storage Permission Granted.');
        } else {
          Toast.show('Storage Permission Not Granted', Toast.LONG);
          // Alert.alert('Error', 'Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('ERROR' + err);
      }
    }
  };
  //function : service function
  const downloadFile = async (url, appointmentId, bookingId) => {
    let pdfUrl = url;
    let DownloadDir =
      Platform.OS == 'ios'
        ? RNFetchBlob.fs.dirs.DocumentDir
        : RNFetchBlob.fs.dirs.DownloadDir;
    const {dirs} = RNFetchBlob.fs;
    const dirToSave =
      Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
    const configfb = {
      fileCache: true,
      useDownloadManager: true,
      notification: true,
      mediaScannable: true,
      title: 'Cosmologo',
      path: `${dirToSave}.pdf`,
    };
    const configOptions = Platform.select({
      ios: {
        fileCache: configfb.fileCache,
        title: configfb.title,
        path: configfb.path,
        appendExt: 'pdf',
      },
      android: configfb,
    });
    Platform.OS == 'android'
      ? RNFetchBlob.config({
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path: `${DownloadDir}/${bookingId}.pdf`,
            description: 'Cosmologo',
            title: `${bookingId}invoice.pdf`,
            mime: 'application/pdf',
            mediaScannable: true,
          },
        })
          .fetch('GET', `${pdfUrl}`)
          .catch(error => {
            console.warn(error.message);
          })
      : RNFetchBlob.config(configOptions)
          .fetch('GET', `${pdfUrl}`, {})
          .then(res => {
            if (Platform.OS === 'ios') {
              RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
              RNFetchBlob.ios.previewDocument(configfb.path);
            }
            console.log('The file saved to ', res);
          })
          .catch(e => {
            console.log('The file saved to ERROR', e.message);
          });
  };

  const changeAppointmentStatus = item => {
    // ! tell sanjay if 24 hrs date_time... then don't add am/pm
    setAppointmentStatus(item.value);
    if (item.value === 'All') {
      clearSelection();
      setFilteredData(appointmentData);
      return;
    } else if (item.value === 'Completed') {
      console.log('appointmentData', appointmentData);
      clearSelection();
      setFilteredData(appointmentData.filter(el => el.status === 'Completed'));
    } else if (item.value === 'Cancelled') {
      console.log('appointmentData', appointmentData);
      clearSelection();
      setFilteredData(
        appointmentData.filter(
          el => el.is_approved == 3 || el.is_approved == 4,
        ),
      );
    }
  };
  const OKPressed = () => {
    if (temporarySelectedDates?.length === 0) {
      // dispatch(showToast({text:'Please select a date', duration:2000}))
      setShowCalendarModal(false);
      return;
    }
    const filteredByDays = appointmentData.filter(el => {
      const newTime = el?.date_time.replace('PM', '');
      if (
        temporarySelectedDates?.length === 1 &&
        moment(moment(newTime).format('YYYY-MM-DD')).isSame(
          moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
        )
      ) {
        return true;
      } else if (
        temporarySelectedDates?.length >= 2 &&
        moment(moment(newTime)).isBetween(
          moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
          moment(
            moment(
              temporarySelectedDates[temporarySelectedDates?.length - 1],
            ).format('YYYY-MM-DD'),
          ).add(1, 'days'),
        )
      ) {
        return true;
      }
      return false;
    });
    setFilteredData(filteredByDays);
    setAppointmentStatus(null);
    setShowCalendarModal(false);
  };
  const clearSelection = () => {
    setTemporarySelectedDates([]);
    setMarkedDates({});
    setStartDay(null);
    setEndDay(null);
    setFilteredData(appointmentData);
  };
  const clearValues = () => {
    setFirstCode('');
    setSecondCode('');
    setThirdCode('');
    setForthCode('');
  };

  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Appointment" />
      <ScrollView
        contentContainerStyle={{paddingBottom: '10%'}}
        style={styles.mainView}>
        <View
          style={{
            marginBottom: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Dropdown
            style={[
              styles.dropdown,
              isFocus && {borderColor: Colors.THEME_GREEN},
            ]}
            data={data}
            containerStyle={{backgroundColor: Colors.BG_GREEN}}
            activeColor={Colors.LITE_GREEN}
            maxHeight={300}
            labelField="label"
            valueField="value"
            placeholder={
              !isFocus
                ? 'Select Appointment Status'
                : 'Select Appointment Status'
            }
            value={appointmentStatus}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
            onChange={item => {
              changeAppointmentStatus(item);
              setIsFocus(false);
            }}
            iconStyle={{width: 40, height: 40}}
            iconColor={Colors.BLACK}
            placeholderStyle={{marginLeft: 10}}
            selectedTextStyle={{marginLeft: 10}}
          />
          <TouchableOpacity
            onPress={() => {
              setShowCalendarModal(true);
            }}
            style={{
              backgroundColor: Colors.BG_GREEN,
              borderRadius: 10,
              padding: 10,
              flex: 0.1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {temporarySelectedDates?.length === 0 ? (
              <MyIcon.AntDesign
                name="calendar"
                size={24}
                color={Colors.THEME_GREEN}
              />
            ) : (
              <MyIcon.FontAwesome5
                name="calendar-check"
                size={24}
                color={Colors.THEME_GREEN}
              />
            )}
          </TouchableOpacity>
        </View>
        {/* <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: Colors.WHITE,
            marginTop: 10,
            padding: 10,
            borderRadius: 10,
            marginTop:20
          }}>
          <MyText text="New Appointments Received" fontFamily="bold" />
          <TouchableOpacity
            style={{
              backgroundColor: Colors.BG_GREEN,
              borderRadius: 30,
              padding: 5,
            }}>
            <MyIcon.AntDesign name="info" size={15} color={'#294527'} />
          </TouchableOpacity>
        </View> */}
        <MyText
            text={`${filteredData.length} ${
              filteredData.length === 1 || filteredData.length === 0
                ? 'Appointment found'
                : 'Appointments found'
            }`}
            marginVertical={20}
            fontFamily="medium"
          />
        {filteredData.length > 0 ? (
          filteredData.map((item, index) => {
            return (
              <AppointmentCard
                key={index.toString()}
                BookingID={item.booking_id}
                SubCategory={item.subcategory}
                ConcernArea={item.concern_area_title}
                Product={item.product}
                ImageUrl={item.concern_area_image}
                Amount={parseFloat(item.amount).toFixed(2)}
                isDoctor={false}
                isVerified={item.is_verified}
                AppointmentTime={item.date_time}
                UserName={item.user_name}
                userImageUrl={item.user_profile_image}
                onPress={() =>
                  gotoAppointmentDetail(
                    item.id,
                    item.status,
                    `${item.service_time} ${item.service_time_unit}`,
                    item.is_approved == 3 || item.is_approved == 4
                      ? 'Cancelled'
                      : item.status,
                  )
                }
                downloadInvoice={() => getInvoiceUrl(item.id, item.booking_id)}
                Role={'Service Provider'}
                // status={item.status}
                status={
                  item.is_approved == 3 || item.is_approved == 4
                    ? 'Cancelled'
                    : item.status
                }
                user_address={item.user_address}
                showAppointmentDuration={true}
                service_time={`${item.service_time} ${item.service_time_unit}`}
              />
            );
          })
        ) : (
          <MyText
            text={'No Appointments found'}
            fontSize={16}
            fontFamily="medium"
            // textColor="white"
            textAlign="center"
            marginVertical={10}
          />
        )}
      </ScrollView>
      <VerifyBooking
        visible={showVerifyBooking}
        setVisibility={setShowVerifyBooking}
        generateOtpPress={() => {
          setShowVerifyBooking(false);
          setGenerateOtp(true);
        }}
        validateBookingPress={() => {
          setShowVerifyBooking(false);
          setShowConfirmBooking(true);
        }}
        selectedBookingId={selectedBookingId}
      />
      <ConfirmBooking
        visible={showConfirmBooking}
        setVisibility={setShowConfirmBooking}
      />
      <VerifyApptOtp
        visible={generateOtp}
        setVisibility={setGenerateOtp}
        validateBookingPress={() => {
          setGenerateOtp(false);
          setShowConfirmBooking(true);
        }}
        firstCode={firstCode}
        secondCode={secondCode}
        thirdCode={thirdCode}
        forthCode={forthCode}
        setFirstCode={setFirstCode}
        setSecondCode={setSecondCode}
        setThirdCode={setThirdCode}
        setForthCode={setForthCode}
        firstCodeRef={firstCodeRef}
        secondCodeRef={secondCodeRef}
        thirdCodeRef={thirdCodeRef}
        forthCodeRef={forthCodeRef}
      />
      <AppointmentCalender
        visible={showCalendarModal}
        setVisibility={setShowCalendarModal}
        startDay={startDay}
        setStartDay={setStartDay}
        endDay={endDay}
        setEndDay={setEndDay}
        temporarySelectedDates={temporarySelectedDates}
        setTemporarySelectedDates={setTemporarySelectedDates}
        markedDates={markedDates}
        setMarkedDates={setMarkedDates}
        OKPressed={OKPressed}
        clearSelection={clearSelection}
      />
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default MyAppointment;
