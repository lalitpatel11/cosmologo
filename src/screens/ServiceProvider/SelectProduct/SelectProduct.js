//react components
import React, {useState, useEffect} from 'react';
import {View, Image, ScrollView, TouchableOpacity, Alert, FlatList} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyText from 'components/MyText/MyText';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
//globals
import {
  Colors,
  Constant,
  Images,
  MyIcon,
  ScreenNames,
  Service,
} from 'global/Index';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from "@react-native-community/netinfo";
//modals
import UnselectService from 'modals/UnselectService/UnselectService'; 
//styles
import {styles} from './SelectProductStyle';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import MyButton from 'components/MyButton/MyButton';

const SelectProduct = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const selectedSubCategory = route.params.selectedSubCategory;
  const subCategoriesData = route.params.subCategoriesData;
  const selectedCategory = route.params.selectedCategory;
  console.warn('selectedSubCategory', selectedSubCategory);
  const userId = route.params.userId;
  //states
  const [productList, setProductList] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState([]);
  const [productBySubcat, setProductBySubcat] = useState([]);
  const [selected, setSelected] = useState([]);
  const [openUnselectServiceModal, setOpenUnselectServiceModal] = useState(false);
  const [unselectMessage, setUnselectMessage] = useState('');
  const [text1, setText1] = useState('');
  const [values, setValues] = useState([]);
  const [text2, setText2] = useState('');
  //states : modal states
  const [showLoader, setShowLoader] = useState(false);
  //function : imp function
  const selectItem = item => {
    const index = selectedProduct.findIndex(e => e == item.id);
    if (index > -1) {
      const filterArray = selectedProduct.filter(e => e != item.id);
      setSelectedProduct(filterArray);
      const temp = [] 
      productBySubcat.map((el, index)=>{
        let count = 0
        el?.map(el2=>{
          if(filterArray.includes(el2.id)){
            count++
          }
        })
        temp[index] = count
      })
      setSelected(temp)
    } else {
      setSelectedProduct([...selectedProduct, item.id]);
      const temp = []
      productBySubcat.map((el, index)=>{
        let count = 0
        el?.map(el2=>{
          if([...selectedProduct, item.id].includes(el2.id)){
            count++
          }
        })
        temp[index] = count
      })
      setSelected(temp)
    }
  };
  //function : navigation function
  const gotoSelectAreaOfConcern = () => {
    // setOpenUnselectServiceModal(false)
    navigation.navigate(ScreenNames.SERVICE_SELECT_AREAOFCONCERN, {
      selectedProduct,
      selectedSubCategory,
      selectedCategory,
      productList, 
      userId,
    });
  }
  //function : service function
  const getProduct = async () => {
    setShowLoader(true);
    try {
      const getproductData = new FormData();
      selectedSubCategory.map((e, i) => {
        getproductData.append(`sub_category_id[${i}]`, e);
      });
      // getproductData.append('sub_category_id', selectedSubCategory.id);
      const resp = await Service.postApi(Service.PRODUCT_LIST, getproductData);
      if (resp.data.status) {
        let serIndex = 0
        const data = resp.data.data.product_list?.map(el=>{
          serIndex++
          return {...el, serIndex: String(serIndex) }
        })
        const unique = [...new Set(data?.map(item => item.subcategory_id))]
        // console.log('unique', unique);
        const allProducts = []
        unique.map((un, index)=>{
          const products = []
          data?.map(el=>{
            if(el.subcategory_id === un){
              // final[un].push(el)
              // console.log('final[index]', final[index]);
              products.push(el)
            }
          })
          var obj = {};
          obj[un] = products;
          // allProducts.push(obj);
          allProducts.push(products);
          // console.log('allProducts', allProducts);
        })
        setProductBySubcat(allProducts)
        // console.log('setProductBySubcat', allProducts);
        setSelected(new Array(allProducts?.length).fill(0))
        // console.log('new Array(allProducts?.length).fill(0)', new Array(allProducts?.length).fill(0));
        setProductList(data);
        console.log('setProductList', data);
        // console.warn(resp.data);
      }
    } catch (error) {
      console.log('error in getProduct', error);
    }
    setShowLoader(false);
  };
  const selectProduct = () => {
    const notSelectedIndexes = []
    for(let i = 0; i < selected.length; i++){
      // console.log('selected[i]', selected[i]);
      if(selected[i] == 0){
        notSelectedIndexes.push(i)
        // categoryiesData?.find(el=>el.id == subcatByCat[0][0].service_category_id)?.title
      }
    }
    const unselectedSubcategoriesIds = []
    productBySubcat.map((sb, index)=>{
      if(notSelectedIndexes.includes(index)){
        unselectedSubcategoriesIds.push(sb[0]?.subcategory_id)
      }
    })
    // console.log('unselectedCategoryIds', unselectedSubcategoriesIds);
    const unselectedsubcategories = []
    unselectedSubcategoriesIds.map(el=>{
      const prodName = subCategoriesData?.find(el2=>el2?.id == el)?.title
      unselectedsubcategories.push(prodName)
    })
    console.log('unselectedsubcategories', unselectedsubcategories);
    if(selected.map(el=>el).reduce((a,b)=>a+b, 0) === 0){
      Toast.show('Please select at least one product', Toast.SHORT)
      return
    }
    // if(unselectedsubcategories?.length > 0){
    //   setText1('Please select at least one product for')
    //   setValues(unselectedsubcategories)
    //   setText2('Else above subcategories will be removed')
    //   setOpenUnselectServiceModal(true)  
    //   return
    // }
    if (Object.keys(selectedProduct).length > 0) {
      gotoSelectAreaOfConcern();
    } else {
      Toast.show('Please select Product / Treatment', Toast.LONG);
      // Alert.alert('', `Please select Product / Treatment`);
    }
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getProduct();
    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader title="Products / Treatments" />
      <ScrollView
        contentContainerStyle={{paddingBottom: '20%'}}
        style={styles.mainView}>
        {/* <MyText
          text={`Choose  Product / Treatment`}
          fontFamily="bold"
          textAlign="center"
          marginBottom={20}
        />
        <Image
          resizeMode="contain"
          source={Images.AppIcon}
          style={styles.AppIcon}
        /> */}
        <View style={styles.productView}>
        {productBySubcat?.map((subcat, index)=>{
              return (
                <View>
                <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center',marginBottom:10, marginTop:20}}>
                <View style={{flexDirection:'row', alignItems:'center', width:'60%'}}>
                <MyText
                  text={`${subCategoriesData?.find(el=>el.id == subcat[0].subcategory_id)?.title}`}
                  // text={`category 1`}
                  fontFamily="bold"
                  numberOfLines={2}
                  fontSize={18}
                  // width={'80%'}
                  marginLeft={10}
                  textColor='#3E5869'
                />
                <MyText
                  text={`(${selected[index]})`}
                  // text={`category 1`}
                  fontFamily="medium"
                  textColor="theme_green"
                  numberOfLines={2}
                  fontSize={18}
                  // width={'80%'}
                  marginLeft={5}
                />
                </View>
                {subcat?.length > 2 ?
                  <View style={{flexDirection:'row', alignItems:'center', alignSelf:'flex-end', marginRight:10}}>
                    <MyText
                      text={'More'}
                      // text={`category 1`}
                      fontFamily="medium"
                      textColor="theme_green"
                      numberOfLines={2}
                      fontSize={14}
                      // width={'80%'}
                      marginLeft={5}
                    />
                    <Image source={require('assets/images/caret-right.png')} resizeMode='cover' style={{height:25, width:25}}/>
                  </View>
                  :null}
                  </View>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  data={subcat}
                  // keyExtractor={(_, index) => index.toString()}
                  keyExtractor={item => item.serIndex}
                  renderItem={({item, index}) => (
                    <TouchableOpacity
                key={item.serIndex}
                onPress={() => selectItem(item)}
                style={{
                  width: Constant.width / 2 - 40,
                  height: Constant.width / 2 - 50,
                  // height: 200,
                  alignItems:'center',
                  margin: 10,
                  backgroundColor: Colors.WHITE,
                  borderRadius: 20,
                  borderWidth: selectedProduct.indexOf(item.id) > -1 ? 2 : 0,
                  borderColor: Colors.THEME_GREEN,
                  shadowColor: '#459743',
                  shadowOffset: {width: 0, height: 5},
                  shadowOpacity: 0.8,
                  shadowRadius: 15,
                  elevation: 5,
                }}>
                <Image
                  source={{uri: item.image}}
                  style={{
                    width: 70,
                    height: 70,
                    borderRadius: 35,
                    marginTop:15
                  }}
                />
                  <MyText
                    text={item.title}
                    fontFamily="medium"
                    textColor='#3E5869'
                    fontSize={14}
                    // width={'80%'}
                    textAlign='center'
                    numberOfLines={2}
                    marginTop={20}
                  />
                  {selectedProduct.indexOf(item.id) > -1 ? 
                        <Image source={require('assets/images/check-circle-service.png')} resizeMode='cover' style={{height:25, width:25,position:'absolute', right:10, top:10}}/>
                        :null}
              </TouchableOpacity>
                  )}
                  />
          
            </View>  
            );
          })}
        </View>
      </ScrollView>
      <View
        style={{
          backgroundColor: Colors.WHITE,
          padding: 10,
          paddingHorizontal: 20,
        }}>
        <MyButton Title="Next" onPress={selectProduct} />
      </View>
      <CustomLoader showLoader={showLoader} />
      <UnselectService
        visible={openUnselectServiceModal}
        setVisibility={setOpenUnselectServiceModal}
        text1={text1}
        values={values}
        text2={text2}
        next={gotoSelectAreaOfConcern}
        // deleteSlot={() => deleteSlot(selectedItem?.serviceUnavailableId)}
      />
    </View>
  );
};

export default SelectProduct;
