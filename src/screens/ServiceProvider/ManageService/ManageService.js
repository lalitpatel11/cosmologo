//react components
import React, {useState, useEffect} from 'react';
import {View, Text, FlatList, Alert, ScrollView} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import ServiceCard from 'components/ServiceCard/ServiceCard';
import TitleViewAll from 'components/TitleViewAll/TitleViewAll';
import MyText from 'components/MyText/MyText';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//third parties
import {useNetInfo} from "@react-native-community/netinfo"; 
//global
import {Constant, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ManageServiceStyle';
//redux
import {useSelector} from 'react-redux';
//third parties
import Toast from 'react-native-simple-toast';

const ManageService = ({navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [Data, setData] = useState({});
  //function : navigation function
  const gotoSelectService = () =>
    navigation.replace(ScreenNames.SERVICE_SELECT_SERVICE, {
      userId: userInfo.id,
    });
  const gotoEditService = () => navigation.navigate(ScreenNames.SERVICE_EDIT);
  const gotoEditSubCategory = () =>
    navigation.navigate(ScreenNames.SERVICE_EDIT_SUBCATEGORY);
  //function : imp function
  const askDeleteService = item => {
    Alert.alert(
      'Are you sure you want to delete this service?',
      'Please note that deleting this service will delete its respective sub category as well.',
      [
        {
          text: 'YES',
          onPress: () => deleteCategory(item),
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
        },
      ],
    );
  };
  const askDeleteSubCategory = item => {
    Alert.alert('Are you sure you want to delete this sub category?', '', [
      {
        text: 'YES',
        onPress: () => deleteSubCategory(item),
      },
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
      },
    ]);
  };
  const askDeleteProduct = item => {
    Alert.alert('Are you sure you want to delete this Product?', '', [
      {
        text: 'YES',
        onPress: () => deleteProduct(item),
      },
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
      },
    ]);
  };
  const askDeleteAreaOfConcern = item => {
    Alert.alert('Are you sure you want to delete this Area of concern?', '', [
      {
        text: 'YES',
        onPress: () => deleteAreaOfConcern(item),
      },
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
      },
    ]);
  };

  //function : service function
  const getSelectedServices = async () => {
    setShowLoader(true);
    try {
      const requiredData = new FormData();
      requiredData.append('service_provider_id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SELECTED_SERVICES,
        requiredData,
      );
      if (resp.data.status) {
        console.log('get services', resp.data.data);
        let serIndex = 0
        const data = {}
        data.category_data = resp.data.data?.category_data?.map(el=>{
          serIndex++
          return {...el, serIndex: String(serIndex) }
        })
        data.subcategory_data = resp.data.data?.subcategory_data?.map(el=>{
          serIndex++
          return {...el, serIndex: String(serIndex) }
        })
        data.product_data = resp.data.data?.product_data?.map(el=>{
          serIndex++
          return {...el, serIndex: String(serIndex) }
        })
        data.concern_area_data = resp.data.data?.concern_area_data?.map(el=>{
          serIndex++
          return {...el, serIndex: String(serIndex) }
        })
        setData({...data});
        console.log('setData', data);
        console.warn('resp------>', resp.data);
      }
    } catch (error) {
      console.log('error in getSelectedServices', error);
    }
    setShowLoader(false);
  };
  const deleteCategory = async item => {
    setShowLoader(true);
    try {
      const deleteCategoryData = new FormData();
      deleteCategoryData.append('service_provider_id', userInfo.id);
      deleteCategoryData.append('category_id', item.id);
      console.log('deleteCategoryData', deleteCategoryData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.DELETE_SELECTED_CATEGORY,
        deleteCategoryData,
      );
      console.log('deleteCategoryData resp', resp);
      if (resp.data.status) {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT)
        getSelectedServices();
      }
    } catch (error) {
      console.log('error in deleteCategory', error);
    }
    setShowLoader(false);
  };
  const deleteSubCategory = async item => {
    setShowLoader(true);
    try {
      const deleteSubCtgryData = new FormData();
      deleteSubCtgryData.append('service_provider_id', userInfo.id);
      deleteSubCtgryData.append('subcategory_id', item.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.DELETE_SELECTED_SUBCATEGORY,
        deleteSubCtgryData,
      );
      if (resp.data.status) {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT)
        getSelectedServices();
      }
    } catch (error) {
      console.log('error in deleteSubCategory', error);
    }
    setShowLoader(false);
  };
  const deleteProduct = async item => {
    setShowLoader(true);
    try {
      const formData = new FormData();
      formData.append('product_id', item.id);
      formData.append('service_provider_id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.DELETE_SELECTED_PRODUCT,
        formData,
      );
      if (resp.data.status) {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT)
        getSelectedServices();
      }
    } catch (error) {
      console.log('error in deleteProduct', error);
    }
    setShowLoader(false);
  };
  const deleteAreaOfConcern = async item => {
    setShowLoader(true);
    try {
      const formData = new FormData();
      formData.append('area_id', item.id);
      formData.append('service_provider_id', userInfo.id);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.DELETE_SELECTED_AREAOFCONCERN,
        formData,
      );
      if (resp.data.status) {
        // Alert.alert('', resp.data.message);
        Toast.show(resp.data.message, Toast.SHORT)
        getSelectedServices();
      }
    } catch (error) {
      console.log('error in deleteProduct', error);
    }
    setShowLoader(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getSelectedServices();
    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Manage Service" />
      <ScrollView
        contentContainerStyle={{paddingBottom: '20%'}}
        style={styles.mainView}>
        <TitleViewAll
          Title="Your Selected Services"
          isPlusButton
          onPress={gotoSelectService}
        />
        <MyText
          text="Category"
          fontSize={16}
          fontFamily="bold"
          marginTop={10}
          marginBottom={20}
        />
        <FlatList
          horizontal
          data={Data.category_data}
          renderItem={({item, index}) => {
            return (
              <ServiceCard
                hasDeleteButton
                imageUrl={item.image}
                Title={item.title}
                deletePress={() => askDeleteService(item)}
                height={200}
                width={Constant.width / 1.3}
              />
            );
          }}
          keyExtractor={item => item.serIndex}
          showsHorizontalScrollIndicator={false}
        />
        <MyText
          text="Subcategory"
          fontSize={16}
          fontFamily="bold"
          marginVertical={20}
        />
        <FlatList
          horizontal
          data={Data.subcategory_data}
          renderItem={({item, index}) => {
            return (
              <ServiceCard
                hasDeleteButton
                imageUrl={item.image}
                Title={item.title}
                height={200}
                width={Constant.width / 1.3}
                deletePress={() => askDeleteSubCategory(item)}
              />
            );
          }}
          keyExtractor={item => item.serIndex}
          showsHorizontalScrollIndicator={false}
        />
        <MyText
          text="Products / Treatments"
          fontSize={16}
          fontFamily="bold"
          marginVertical={20}
        />
        <FlatList
          horizontal
          data={Data.product_data}
          renderItem={({item, index}) => {
            return (
              <ServiceCard
                hasDeleteButton
                imageUrl={item.image}
                Title={item.title}
                height={200}
                width={Constant.width / 1.3}
                deletePress={() => askDeleteProduct(item)}
              />
            );
          }}
          keyExtractor={item => item.serIndex}
          showsHorizontalScrollIndicator={false}
        />
        <MyText
          text="Area Of Concern"
          fontSize={16}
          fontFamily="bold"
          marginVertical={20}
        />
        <FlatList
          horizontal
          data={Data.concern_area_data}
          renderItem={({item, index}) => {
            return (
              <ServiceCard
                hasDeleteButton
                imageUrl={item.image}
                Title={item.title}
                height={200}
                width={Constant.width / 1.3}
                deletePress={() => askDeleteAreaOfConcern(item)}
              />
            );
          }}
          keyExtractor={item => item.serIndex}
          showsHorizontalScrollIndicator={false}
        />
      </ScrollView>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default ManageService;
