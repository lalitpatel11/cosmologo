import {StyleSheet, Dimensions} from 'react-native';
import {Colors, Fonts} from 'global/Index';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  birthDateView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  genderView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  imageViewStyle: {
    height: 150,
    width: 150,
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    // borderWidth: 2,
    // borderColor: Colors.THEME_GREEN,
    marginVertical: 10,
    borderRadius: 150 / 2,
  },
  docImageStyle: {
    height: 100,
    width: 100,
    alignSelf: 'center',
    backgroundColor: Colors.WHITE,
    // borderWidth: 2,
    // borderColor: Colors.THEME_GREEN,
    marginVertical: 10,
    // borderRadius: 100 / 2,
  },
  docAddButtonStyle: {
    // position: 'absolute',
    backgroundColor: Colors.WHITE,
    borderRadius: 25,
    padding: 5,
    height:40,
    width:40,
    justifyContent:'center',
    alignItems:'center'
  },
  addButtonStyle: {
    position: 'absolute',
    backgroundColor: Colors.WHITE,
    borderRadius: 100,
    padding: 5,
    right: 5,
    bottom: 5,
  },
  deleteButtonStyle: {
    position: 'absolute',
    backgroundColor: Colors.WHITE,
    borderRadius: 100,
    padding: 5,
    right: 5,
    top: 5,
  },
  searchbar: {
    description: {
      fontWeight: 'bold',
    },
    predefinedPlacesDescription: {
      color: '#1faadb',
    },
    textInputContainer: {
      backgroundColor: 'rgba(0,0,0,0)',
      // top: 50,
      // width: width - 10,
      borderWidth: 0,
      marginTop:5,
    },
    textInput: {
      marginLeft: 0,
      marginRight: 0,
      height: 100,
      color: Colors.BLACK,
      fontSize: 14,
      borderWidth: 0,
      elevation: 2,
      fontFamily: Fonts.BOLD,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
    },
    listView: {
      // backgroundColor: 'rgba(192,192,192,0.9)',
      // top: 23,
    },
  },
  deleteButtonContainer:{
    flexDirection:'row',
    // backgroundColor: Colors.WHITE,
    borderRadius: 10,
    // height:34,
    // width:34,
    // justifyContent:'space-between',
    alignItems:'center',
    paddingVertical:5,
    paddingHorizontal:10,
    borderWidth:1,
    borderColor: Colors.THEME_GREEN
  },
  pdfContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25,
  },
});
