//react components
import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  ScrollView,
  PermissionsAndroid,
  TouchableOpacity,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import AppointmentCard from 'components/AppointmentCard/AppointmentCard';
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyText from 'components/MyText/MyText';
import NoInternet from 'components/NoInternet/NoInternet';
//globals
import {ScreenNames, Service, Colors, MyIcon, Constant} from 'global/Index';
//styles
import {styles} from './VerifiedAppointmentListingStyle';
//redux
import {useSelector, useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import MyHeader from 'components/MyHeader/MyHeader';
//import : third parties
import Toast from 'react-native-simple-toast';
import RNFetchBlob from 'rn-fetch-blob';
import {Dropdown} from 'react-native-element-dropdown';
import moment from 'moment';
import NetInfo, {useNetInfo} from "@react-native-community/netinfo";
//import : modals
import AppointmentCalender from 'modals/AppointmentCalender/AppointmentCalender';
import VerifyBooking from 'modals/VerifyBooking/VerifyBooking';
import VerifyApptOtp from 'modals/VerifyApptOtp/VerifyApptOtp';
import ConfirmBooking from 'modals/ConfirmBooking/ConfirmBooking';
import CancelAppointment from 'modals/CancelAppointment/CancelAppointment';

//data
const data = [
  {label: 'All', value: 'all'},
  {label: 'Today', value: 'today'},
  {label: '5 days', value: '5'},
  {label: '10 days', value: '10'},
  {label: '20 days', value: '20'},
  {label: '30 days', value: '30'},
];
// on clicking verify button, if api call successful, replace verify button with verified status
const AppointmentListing = ({navigation}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : redux variables
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  const dispatch = useDispatch();
  //ref
  const firstCodeRef = useRef();
  const secondCodeRef = useRef();
  const thirdCodeRef = useRef();
  const forthCodeRef = useRef();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [appointmentData, setAppointmentData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [isFocus, setIsFocus] = useState(false);
  const [numDays, setNumDays] = useState(null);
  const [showCalendarModal, setShowCalendarModal] = useState(false);
  const [startDay, setStartDay] = useState(null);
  const [endDay, setEndDay] = useState(null);
  const [markedDates, setMarkedDates] = useState({});
  const [temporarySelectedDates, setTemporarySelectedDates] = useState([]);
  const [showVerifyBooking, setShowVerifyBooking] = useState(false);
  const [selectedBookingId, setSelectedBookingId] = useState('');
  const [selectedAppointmentId, setSelectedAppointmentId] = useState('');
  const [selectedAppointment, setSelectedAppointment] = useState({});
  const [generateOtp, setGenerateOtp] = useState(false);
  const [showConfirmBooking, setShowConfirmBooking] = useState(false);
  const [firstCode, setFirstCode] = useState('');
  const [secondCode, setSecondCode] = useState('');
  const [thirdCode, setThirdCode] = useState('');
  const [forthCode, setForthCode] = useState('');
  const [message, setMessage] = useState('');
  const [showCancelAppt, setShowCancelAppt] = useState(false);
  const [isOnline, setIsOnline] = useState(true);
  //function : navigation function
  const gotoAppointmentDetail = (
    appointment_id,
    isVerified,
    service_time,
    status = '',
  ) => {
    console.log('gotoAppointmentDetail service_time', service_time);
    status
      ? navigation.navigate(ScreenNames.SERVICE_APPOINTMENT_DETAILS, {
          appointment_id,
          status,
          isVerified,
          service_time,
        })
      : navigation.navigate(ScreenNames.SERVICE_APPOINTMENT_DETAILS, {
          appointment_id,
          isVerified,
          service_time,
        });
  };
  //function : service function
  const getOngoingAppnt = async () => {
    setShowLoader(true);
    try {
      // dont show cancelled by user and cancelled by service provider
      const myData = new FormData();
      myData.append('service_provider_id', userInfo.id);
      myData.append('filter', 'verify');
      // myData.append('is_approved', [1]);
      // remove cancelled by service provider (3), completed (4), and cancelled by user (5)
      // myData.append('status_id', [1,2,6]);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.SERVICE_PROVIDER_ONGOING_APPOINTMENT,
        myData,
      );
      if (resp.data.status) {
        // console.warn('resp---->', resp.data);
        console.log('setAppointmentData', resp.data.data);
        // remove completed appointments
        // const updatedData = resp.data.data?.filter(el=>(el.status_id == 1 || el.status_id == 2 || el.status_id == 6) && (el.status == 'Processing') && el.is_approved == 1)
        // console.log('updatedData', updatedData);
        const updatedData2 = resp.data.data?.map(el => {
          const newTime = el?.date_time.replace('PM', '');
          const newTime2 = moment(
            moment(newTime).format('dddd,D MMMM YYYY,kk:mm'),
          ).format('ddd, D MMM YYYY, h:mm A');
          return {...el, date_time: newTime2};
        });
        setAppointmentData(updatedData2);
        setFilteredData(updatedData2);
      }
    } catch (error) {
      console.log('error in getOngoingAppnt', error);
    }
    setShowLoader(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getOngoingAppnt();
    });
    return unsubscribe;
  }, [navigation]);
  //useEffect
  // useEffect(() => {
  //   if(!generateOtp){
  //     setMessage('')
  //   }
  // }, [generateOtp]);

  //function : get invoice url
  const getInvoiceUrl = async (appointmentId, bookingId) => {
    try {
      const endPoint = `${Service.DOWNLOAD_INVOICE}?appointment_id=${appointmentId}`;
      const res = await Service.postApiWithToken(userToken, endPoint, {});
      console.log('getInvoiceUrl res', res.data);
      if (res.data.status) {
        checkPermission(res.data.url, appointmentId, bookingId);
      }
    } catch (error) {
      console.log('error', error);
    }
  };
  //function : imp function
  const checkPermission = async (url, appointmentId, bookingId) => {
    if (Platform.OS === 'ios') {
      downloadFile(url, appointmentId, bookingId);
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Storage Permission Required',
            message:
              'Application needs access to your storage to download File',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          downloadFile(url, appointmentId, bookingId);
          console.log('Storage Permission Granted.');
        } else {
          // dispatch(showToast({text: 'Storage Permission Not Granted'}));
          Toast.show('Storage Permission Not Granted', Toast.SHORT)
          // Alert.alert('Error', 'Storage Permission Not Granted');
        }
      } catch (err) {
        // To handle permission related exception
        console.log('ERROR' + err);
      }
    }
  };
  //function : service function
  const downloadFile = async (url, appointmentId, bookingId) => {
    let pdfUrl = url;
    let DownloadDir =
      Platform.OS == 'ios'
        ? RNFetchBlob.fs.dirs.DocumentDir
        : RNFetchBlob.fs.dirs.DownloadDir;
    const {dirs} = RNFetchBlob.fs;
    const dirToSave =
      Platform.OS == 'ios' ? dirs.DocumentDir : dirs.DownloadDir;
    const configfb = {
      fileCache: true,
      useDownloadManager: true,
      notification: true,
      mediaScannable: true,
      title: 'Cosmologo',
      path: `${dirToSave}.pdf`,
    };
    const configOptions = Platform.select({
      ios: {
        fileCache: configfb.fileCache,
        title: configfb.title,
        path: configfb.path,
        appendExt: 'pdf',
      },
      android: configfb,
    });
    Platform.OS == 'android'
      ? RNFetchBlob.config({
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path: `${DownloadDir}/${bookingId}.pdf`,
            description: 'Cosmologo',
            title: `${bookingId}invoice.pdf`,
            mime: 'application/pdf',
            mediaScannable: true,
          },
        })
          .fetch('GET', `${pdfUrl}`)
          .catch(error => {
            console.warn(error.message);
          })
      : RNFetchBlob.config(configOptions)
          .fetch('GET', `${pdfUrl}`, {})
          .then(res => {
            if (Platform.OS === 'ios') {
              RNFetchBlob.fs.writeFile(configfb.path, res.data, 'base64');
              RNFetchBlob.ios.previewDocument(configfb.path);
            }
            console.log('The file saved to ', res);
          })
          .catch(e => {
            console.log('The file saved to ERROR', e.message);
          });
  };

  const acceptAppointment = async appointmentId => {
    try {
      const requiredData = new FormData();
      requiredData.append('appointment_id', appointmentId);
      requiredData.append('varify_id', 1);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.VERIFY_APPOINTMENT,
        requiredData,
      );
      if (resp.data.status) {
        getOngoingAppnt();
        console.log('acceptAppointment', resp.data);
        console.warn('resp------>', resp.data);
      }
    } catch (error) {
      console.log('error in getSelectedServices', error);
    }
  };
  const cancelAppointment = async (
    appointmentId,
    appointmentDate,
    bookingId,
    isCancelled,
  ) => {
    // if(isVerified == 1){
    //   Toast.show('Verified appointment cannot be cancelled', Toast.SHORT)
    //   return
    // }
    if (isCancelled == 4) {
      Toast.show('Appointment already cancelled by User', Toast.SHORT);
      return;
    }
    if (
      moment(moment(appointmentDate).format('YYYY-MM-DD')).isBefore(
        moment().format('YYYY-MM-DD'),
      )
    ) {
      Toast.show('Appointment date cannot be in past', Toast.SHORT);
      return;
    }
    setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('appointment_id', appointmentId);
      myData.append('roletype', 'S');
      myData.append('booking_id', bookingId);
      console.log('myData', myData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.CANCEL_APPOINTMENT_USER_SERVICE,
        myData,
      );
      console.log('sp cancel resp', resp);
      if (resp.data.status) {
        Toast.show(resp.data.message, Toast.SHORT);
        // setFilteredData(filteredData.filter(el => el.id !== appointmentId))
        // setAppointmentData(AppointmentData.filter(el => el.id !== appointmentId))
        getOngoingAppnt();
        console.log('cancelAppointment', resp.data);
        console.warn('resp------>', resp.data);
      }
    } catch (error) {
      console.log('error in cancelAppointment', error);
    }
    setShowLoader(false);
    setShowCancelAppt(false);
  };

  const changeNumDays = item => {
    // ! tell sanjay is 24 hrs date_time... then don't add am/pm
    setNumDays(item.value);
    if (item.value === 'all') {
      clearSelection();
      setFilteredData(appointmentData);
      return;
    }
    const filteredByDays = appointmentData.filter(el => {
      const newTime = el?.date_time.replace('PM', '');
      console.log(
        'between',
        newTime,
        moment(moment(newTime)).isBetween(
          moment().format('YYYY-MM-DD'),
          moment(moment().format('YYYY-MM-DD')).add(10, 'days'),
        ),
      );
      if (
        item.value === 'Today' &&
        moment(moment(newTime).format('YYYY-MM-DD')).isSame(
          moment().format('YYYY-MM-DD'),
        )
      ) {
        return true;
      }
      if (
        item.value === '5' &&
        moment(moment(newTime)).isBetween(
          moment().format('YYYY-MM-DD'),
          moment(moment().format('YYYY-MM-DD')).add(6, 'days'),
        )
      ) {
        return true;
      }
      if (
        item.value === '10' &&
        moment(moment(newTime)).isBetween(
          moment().format('YYYY-MM-DD'),
          moment(moment().format('YYYY-MM-DD')).add(11, 'days'),
        )
      ) {
        return true;
      }
      if (
        item.value === '20' &&
        moment(moment(newTime)).isBetween(
          moment().format('YYYY-MM-DD'),
          moment(moment().format('YYYY-MM-DD')).add(21, 'days'),
        )
      ) {
        return true;
      }
      if (
        item.value === '30' &&
        moment(moment(newTime)).isBetween(
          moment().format('YYYY-MM-DD'),
          moment(moment().format('YYYY-MM-DD')).add(31, 'days'),
        )
      ) {
        return true;
      }
      return false;
    });
    clearSelection();
    setFilteredData(filteredByDays);
  };
  const OKPressed = () => {
    if (temporarySelectedDates?.length === 0) {
      // dispatch(showToast({text:'Please select a date', duration:2000}))
      setShowCalendarModal(false);
      return;
    }
    const filteredByDays = appointmentData.filter(el => {
      const newTime = el?.date_time.replace('PM', '');
      if (
        temporarySelectedDates?.length === 1 &&
        moment(moment(newTime).format('YYYY-MM-DD')).isSame(
          moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
        )
      ) {
        return true;
      } else if (
        temporarySelectedDates?.length >= 2 &&
        moment(moment(newTime)).isBetween(
          moment(temporarySelectedDates[0]).format('YYYY-MM-DD'),
          moment(
            moment(
              temporarySelectedDates[temporarySelectedDates?.length - 1],
            ).format('YYYY-MM-DD'),
          ).add(1, 'days'),
        )
      ) {
        return true;
      }
      return false;
    });
    setFilteredData(filteredByDays);
    setNumDays(null);
    setShowCalendarModal(false);
  };
  const clearSelection = () => {
    setTemporarySelectedDates([]);
    setMarkedDates({});
    setStartDay(null);
    setEndDay(null);
    setFilteredData(appointmentData);
  };
  const clearValues = () => {
    setFirstCode('');
    setSecondCode('');
    setThirdCode('');
    setForthCode('');
  };
  const verifyBooking = item => {
    if (Number(item.is_verified) === Constant.VerificationStatus.VERIFIED) {
      // dispatch(
      //   showToast({text: 'Appointment already approved', duration: 2000}),
      // );
      Toast.show('Appointment already approved', Toast.SHORT)
      return;
    }
    // item.booking_id, item.id, item.is_verified
    setSelectedAppointment(item);
    setSelectedBookingId(item.booking_id);
    setSelectedAppointmentId(item.id);
    clearValues();
    setMessage('');
    // setShowVerifyBooking(true)
    setGenerateOtp(true);
  };
  const verifyAppointment = async () => {
    setShowLoader(true);
    try {
      const myData = new FormData();
      myData.append('appointment_id', selectedAppointmentId);
      myData.append('booking_id', selectedBookingId);
      myData.append('otp', `${firstCode}${secondCode}${thirdCode}${forthCode}`);
      console.log('myData', myData);
      const resp = await Service.postApiWithToken(
        userToken,
        Service.VERIFY_APPOINTMENT_OTP,
        myData,
      );
      console.log('verifyAppointment resp', resp);
      if (resp.data.status) {
        console.log('verifyAppointment response', resp.data.data);
        console.log('verifyAppointment message', resp.data.message);
        setMessage('');
        setGenerateOtp(false);
        setShowConfirmBooking(true);
        getOngoingAppnt();
      } else {
        setMessage(resp.data.message);
        clearValues();
      }
    } catch (error) {
      console.log('error in verifyAppointment', error);
      setGenerateOtp(false);
      // dispatch(
      //   showToast({text: 'Appointment cannot be verified', duration: 2000}),
      // );
      Toast.show('Appointment cannot be verified', Toast.SHORT)
    }
    setShowLoader(false);
  };
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Accepted Appointments" />
      <ScrollView
        contentContainerStyle={{paddingBottom: '10%'}}
        style={styles.mainView}>
        <View
          style={{
            marginBottom: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Dropdown
            style={[
              styles.dropdown,
              isFocus && {borderColor: Colors.THEME_GREEN},
            ]}
            data={data}
            containerStyle={{backgroundColor: Colors.BG_GREEN}}
            activeColor={Colors.LITE_GREEN}
            maxHeight={300}
            labelField="label"
            valueField="value"
            placeholder={!isFocus ? 'Select Days' : 'Select Days'}
            value={numDays}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
            onChange={item => {
              changeNumDays(item);
              setIsFocus(false);
            }}
            iconStyle={{width: 40, height: 40}}
            iconColor={Colors.BLACK}
            placeholderStyle={{marginLeft: 10}}
            selectedTextStyle={{marginLeft: 10}}
          />
          <TouchableOpacity
            onPress={() => {
              setShowCalendarModal(true);
            }}
            style={{
              backgroundColor: Colors.BG_GREEN,
              borderRadius: 10,
              padding: 10,
              flex: 0.1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {temporarySelectedDates?.length === 0 ? (
              <MyIcon.AntDesign
                name="calendar"
                size={24}
                color={Colors.THEME_GREEN}
              />
            ) : (
              <MyIcon.FontAwesome5
                name="calendar-check"
                size={24}
                color={Colors.THEME_GREEN}
              />
            )}
          </TouchableOpacity>
        </View>
        {/* <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: Colors.WHITE,
            marginTop: 10,
            padding: 10,
            borderRadius: 10,
            marginTop:20
          }}>
          <MyText text="New Appointments Received" fontFamily="bold" />
          <TouchableOpacity
            style={{
              backgroundColor: Colors.BG_GREEN,
              borderRadius: 30,
              padding: 5,
            }}>
            <MyIcon.AntDesign name="info" size={15} color={'#294527'} />
          </TouchableOpacity>
        </View> */}
        <MyText
            text={`${filteredData.length} ${
              filteredData.length === 1 || filteredData.length === 0
                ? 'Appointment found'
                : 'Appointments found'
            }`}
            marginVertical={20}
            fontFamily="medium"
          />
        {filteredData.length > 0 ? (
          filteredData.map((item, index) => {
            return (
              <AppointmentCard
                key={index.toString()}
                BookingID={item.booking_id}
                SubCategory={item.subcategory}
                ConcernArea={item.concern_area_title}
                Product={item.product}
                ImageUrl={item.concern_area_image}
                Amount={parseFloat(item.amount).toFixed(2)}
                isDoctor={false}
                isVerified={item.is_verified}
                AppointmentTime={item.date_time}
                UserName={item.user_name}
                userImageUrl={item.user_profile_image}
                onPress={() =>
                  item.is_verified == 1
                    ? gotoAppointmentDetail(
                        item.id,
                        item.is_verified,
                        `${item.service_time} ${item.service_time_unit}`,
                      )
                    : gotoAppointmentDetail(
                        item.id,
                        item.is_verified,
                        `${item.service_time} ${item.service_time_unit}`,
                        item.status,
                      )
                }
                downloadInvoice={() => getInvoiceUrl(item.id, item.booking_id)}
                acceptAppointment={() => acceptAppointment(item.id)}
                SPcancelAppointment={() => {
                  setSelectedAppointment(item);
                  setShowCancelAppt(true);
                }}
                Role={'Service Provider'}
                showVerifyButton={true}
                // showButtons={true}
                verifyAction={() => verifyBooking(item)}
                approvedStatus={item.is_approved}
                status={
                  item.status === 'Processing' && item.is_verified == 1
                    ? 'Verified'
                    : item.status
                }
                user_address={item.user_address}
                showAppointmentDuration={true}
                service_time={`${item.service_time} ${item.service_time_unit}`}
              />
            );
          })
        ) : (
          <MyText
            text={'No Appointments found'}
            fontSize={16}
            fontFamily="medium"
            // textColor="white"
            textAlign="center"
            marginVertical={10}
          />
        )}
      </ScrollView>
      <VerifyBooking
        visible={showVerifyBooking}
        setVisibility={setShowVerifyBooking}
        generateOtpPress={() => {
          setShowVerifyBooking(false);
          setGenerateOtp(true);
        }}
        validateBookingPress={() => {
          setShowVerifyBooking(false);
          // setShowConfirmBooking(true);
          setGenerateOtp(true);
        }}
        selectedBookingId={selectedBookingId}
      />
      <ConfirmBooking
        visible={showConfirmBooking}
        setVisibility={setShowConfirmBooking}
        selectedBookingId={selectedBookingId}
        gotoAppointmentDetail={() =>
          gotoAppointmentDetail(
            selectedAppointment.id,
            '1',
            `${selectedAppointment.service_time} ${selectedAppointment.service_time_unit}`,
          )
        }
      />
      <VerifyApptOtp
        visible={generateOtp}
        setVisibility={setGenerateOtp}
        validateBookingPress={() => {
          verifyAppointment();
        }}
        firstCode={firstCode}
        secondCode={secondCode}
        thirdCode={thirdCode}
        forthCode={forthCode}
        setFirstCode={setFirstCode}
        setSecondCode={setSecondCode}
        setThirdCode={setThirdCode}
        setForthCode={setForthCode}
        firstCodeRef={firstCodeRef}
        secondCodeRef={secondCodeRef}
        thirdCodeRef={thirdCodeRef}
        forthCodeRef={forthCodeRef}
        message={message}
        setMessage={setMessage}
      />
      <AppointmentCalender
        visible={showCalendarModal}
        setVisibility={setShowCalendarModal}
        startDay={startDay}
        setStartDay={setStartDay}
        endDay={endDay}
        setEndDay={setEndDay}
        temporarySelectedDates={temporarySelectedDates}
        setTemporarySelectedDates={setTemporarySelectedDates}
        markedDates={markedDates}
        setMarkedDates={setMarkedDates}
        OKPressed={OKPressed}
        clearSelection={clearSelection}
      />
      <CustomLoader showLoader={showLoader} />
      <CancelAppointment
        visible={showCancelAppt}
        setVisibility={setShowCancelAppt}
        onPress={() =>
          cancelAppointment(
            selectedAppointment.id,
            selectedAppointment.date_time,
            selectedAppointment.booking_id,
            selectedAppointment.cancelled,
          )
        }
      />
    </View>
  );
};

export default AppointmentListing;
