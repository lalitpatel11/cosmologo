//react components
import React, {useEffect, useState} from 'react';
import {View, ScrollView, TouchableOpacity, Alert} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import MyText from 'components/MyText/MyText';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import MyTextInput from 'components/MyTextInput/MyTextInput';
import MyButton from 'components/MyButton/MyButton';
//third parties
import DatePicker from 'react-native-date-picker';
import {Dropdown} from 'react-native-element-dropdown';
import {Calendar} from 'react-native-calendars';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import {useNetInfo} from "@react-native-community/netinfo";
//globals
import {Colors, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './ManageAvailabilityStyle';
//redux
import {useDispatch, useSelector} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';

const ManageAvailability = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const AgendaList = route.params.AgendaList;
  //data
  const data = [
    {label: 'Unavailable', value: 'U'},
    {label: 'Available', value: 'A'},
  ];
  //variables
  //variables : redux variables
  const dispatch = useDispatch();
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [eventName, setEventName] = useState('');
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [_markedDates, set_markedDates] = useState({});
  const [date, setDate] = useState(new Date());
  const [openStartTime, setOpenStartTime] = useState(false);
  const [openEndTime, setOpenEndTime] = useState(false);
  const [showLoader, setShowLoader] = useState(false);
  const [isFocus, setIsFocus] = useState(false);
  const [value, setValue] = useState(null);
  const [selectedDates, setSelectedDates] = useState([]);

  const [startDay, setStartDay] = useState(null);
  const [endDay, setEndDay] = useState(null);
  const [unavailableDays, setUnavailableDays] = useState(
    AgendaList.filter(el => el.type === 'U')
      .map(el => el.date)
      .reduce(
        (o, key) => ({
          ...o,
          [key]: {disabled: true, disableTouchEvent: true, color: Colors.RED},
        }),
        {},
      ),
  );
  const [availableDays, setAvailableDays] = useState(
    AgendaList.filter(el => el.type === 'A')
      .map(el => el.date)
      .reduce(
        (o, key) => ({
          ...o,
          [key]: {
            disabled: true,
            disableTouchEvent: true,
            color: Colors.THEME_BLUE,
          },
        }),
        {},
      ),
  );
  const [originalMarkedDates, setOriginalMarkedDates] = useState({
    ...unavailableDays,
    ...availableDays,
  });
  const [markedDates, setMarkedDates] = useState({
    ...unavailableDays,
    ...availableDays,
  });

  //function : navigation function
  const gotoServiceAvailability = () =>
    navigation.navigate(ScreenNames.SERVICE_HOME);
  //function : imp function
  const Validation = () => {
    if (eventName == '') {
      Toast.show('Please enter event name', Toast.LONG);
      // Alert.alert('', 'Please enter event name');
    } else if (selectedDates.length < 0) {
      Toast.show('Please select date', Toast.LONG);
      // Alert.alert('', 'Please select date');
    } else if (value == 'A' && startTime == '') {
      Toast.show('Please select start time', Toast.LONG);
      // Alert.alert('', 'Please select start time');
    } else if (value == 'A' && endTime == '') {
      Toast.show('Please select end time', Toast.LONG);
      // Alert.alert('', 'Please select end time');
    } else return true;
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //function : service function
  const saveCalendar = async () => {
    if (Validation()) {
      setShowLoader(true);
      try {
        const postData = {
          service_provider_id: userInfo.id,
          type: value,
          event_name: eventName,
          date: selectedDates,
          // slot: {
          //   start_time:
          //     startTime == '' ? null : moment(startTime).format('H:MM:ss'),
          //   end_time: endTime == '' ? null : moment(endTime).format('H:MM:ss'),
          // },
          slot: {
            start_time:
              startTime == '' ? null : moment(startTime).format('H:mm:ss'),
            end_time: endTime == '' ? null : moment(endTime).format('H:mm:ss'),
          },
        };
        console.warn('post', postData);
        console.log('post', postData);
        const resp = await Service.postJsonApiWithToken(
          userToken,
          Service.ADD_SERVICE_UNAVAILABLE,
          postData,
        );
        console.log('save calender resp', resp);
        // console.warn('resp', resp.data);
        if (resp.data.status) {
          // dispatch(showToast({text: resp.data.message}));
          Toast.show(resp.data.message, Toast.SHORT);
          // Alert.alert('', resp.data.message);
          gotoServiceAvailability();
        } else {
          // dispatch(showToast({text: resp.data.message}));
          Toast.show(resp.data.message, Toast.SHORT);
        }
      } catch (error) {
        console.log('error in saveCalendar', error);
      }
      setShowLoader(false);
    }
  };
  const clearSelection = () => {
    setSelectedDates([]);
    setMarkedDates({...originalMarkedDates});
    setStartDay(null);
    setEndDay(null);
  };
  console.warn(userInfo.is_approved);
  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Manage Calendar" />
      <ScrollView
        contentContainerStyle={{paddingBottom: '20%'}}
        style={styles.mainView}>
        <MyText text="Event Name" fontFamily="medium" />
        <MyTextInput
          placeholder={'Enter event name'}
          onChangeText={text => setEventName(text)}
        />
        <View style={styles.middleView}>
          <MyText
            text="Availability Status"
            fontSize={16}
            fontFamily="medium"
            marginBottom={5}
          />
          {/* {userInfo.is_approved == 1 ? ( */}
          <Dropdown
            style={[
              styles.dropdown,
              isFocus && {borderColor: Colors.THEME_GREEN},
            ]}
            data={data}
            containerStyle={{backgroundColor: Colors.BG_GREEN}}
            activeColor={Colors.LITE_GREEN}
            maxHeight={300}
            labelField="label"
            valueField="value"
            placeholder={!isFocus ? 'Select Status' : 'Select Status'}
            value={value}
            onFocus={() => setIsFocus(true)}
            onBlur={() => setIsFocus(false)}
            onChange={item => {
              setValue(item.value);
              setIsFocus(false);
            }}
          />
          {/* ) : ( */}
          {/* <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginVertical: 10,
                padding: 10,
                borderRadius: 10,
                borderWidth: 0.5,
              }}>
              <MyText text="Select Status" fontFamily="medium" fontSize={16} />
              <MyIcon.AntDesign name="down" size={24} />
            </View> */}
          {/* )} */}
        </View>
        <MyText text="Select Date" fontFamily="medium" />
        <Calendar
          style={{
            marginVertical: 10,
            borderRadius: 10,
          }}
          onDayPress={day => {
            if (
              moment(day.dateString).format('YYYY-MM-DD') <
              moment(new Date()).format('YYYY-MM-DD')
            ) {
              // dispatch(
              //   showToast({
              //     text: `The Date must be Bigger or Equal to today's date`,
              //     duration: 500,
              //   }),
              // );
              Toast.show(
                `The Date must be Bigger or Equal to today's date`,
                Toast.SHORT,
              );
            } else if (startDay && !endDay) {
              const selectedDates = [];
              const date = {};
              for (
                const d = moment(startDay);
                d.isSameOrBefore(day.dateString);
                d.add(1, 'days')
              ) {
                date[d.format('YYYY-MM-DD')] = {
                  marked: true,
                  color: Colors.LITE_GREEN,
                  textColor: 'white',
                };
                selectedDates.push(d.format('YYYY-MM-DD'));
                if (d.format('YYYY-MM-DD') === startDay)
                  date[d.format('YYYY-MM-DD')].startingDay = true;
                if (d.format('YYYY-MM-DD') === day.dateString)
                  date[d.format('YYYY-MM-DD')].endingDay = true;
              }
              setSelectedDates(selectedDates);
              setMarkedDates({...markedDates, ...date});
              // console.log('first case', {...markedDates, ...date})
              setEndDay(day.dateString);
            } else {
              setStartDay(day.dateString);
              setEndDay(null);
              setSelectedDates([day.dateString]);
              setMarkedDates({
                ...markedDates,
                [day.dateString]: {
                  marked: true,
                  color: Colors.THEME_GREEN,
                  textColor: 'white',
                  startingDay: true,
                  endingDay: true,
                },
              });
            }
          }}
          // monthFormat={'yyyy MMM'}
          // hideDayNames={false}
          markingType={'period'}
          markedDates={markedDates}
          theme={{
            selectedDayBackgroundColor: Colors.THEME_BLUE,
            selectedDayTextColor: 'white',
            monthTextColor: Colors.THEME_GREEN,
            dayTextColor: 'black',
            textMonthFontSize: 16,
            textDayHeaderFontSize: 14,
            arrowColor: Colors.THEME_GREEN,
            dotColor: 'black',
          }}
          minDate={moment().format('YYYY-MM-DD')}
        />
        <MyButton
            Title="Clear Date Selection"
            width="100%"
            backgroundColor={Colors.RED}
            onPress={clearSelection}
        />
        {value == 'A' ? (
          <>
            <MyText text="Select Start Time" fontFamily="medium" />
            <TouchableOpacity
              onPress={() => {
                if (selectedDates?.length === 0) {
                  Toast.show('Please select date', Toast.SHORT);
                } else {
                  setOpenStartTime(true);
                }
              }}
              style={{
                marginVertical: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
                shadowColor: '#000',
                shadowOffset: {width: 0, height: 2},
                shadowOpacity: 0.1,
                backgroundColor: Colors.WHITE,
                shadowRadius: 15,
                elevation: 2,
                padding: 10,
                borderRadius: 10,
              }}>
              <MyText
                text={
                  startTime
                    ? moment(startTime).format('h:mm:ss A')
                    : 'Select Start Time'
                }
              />
              <MyIcon.AntDesign
                name="calendar"
                size={24}
                color={Colors.THEME_GREEN}
              />
            </TouchableOpacity>
            <MyText text="Select End Time" fontFamily="medium" />
            <TouchableOpacity
              onPress={() => setOpenEndTime(true)}
              style={{
                marginVertical: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
                shadowColor: '#000',
                shadowOffset: {width: 0, height: 2},
                shadowOpacity: 0.1,
                backgroundColor: Colors.WHITE,
                shadowRadius: 15,
                elevation: 2,
                padding: 10,
                borderRadius: 10,
              }}>
              <MyText
                text={
                  endTime
                    ? moment(endTime).format('h:mm:ss A')
                    : 'Select End Time'
                }
              />
              <MyIcon.AntDesign
                name="calendar"
                size={24}
                color={Colors.THEME_GREEN}
              />
            </TouchableOpacity>
          </>
        ) : null}

        <MyButton Title="Save" onPress={saveCalendar} />
      </ScrollView>
      <DatePicker
        modal
        // mode="date"
        mode="time"
        open={openStartTime}
        // minimumDate={new Date()}
        date={date}
        onConfirm={time => {
          setOpenStartTime(false);
          console.log('selectedDates[0]', selectedDates[0]);
          // if selected date is today's date and current time is in past (don't check for seconds)
          // if(moment(moment().format('YYYY-MM-DD')).isSame(selectedDates[0]) && (moment().format('H:mm') > moment(time).format('H:mm'))){
          const isTodaysDate = moment(moment().format('YYYY-MM-DD')).isSame(
            selectedDates[0],
          )
            ? true
            : false;
          const timeNow = moment();
          const selectedTime = moment(time);
          const isTimePast = moment(selectedTime).isSameOrBefore(timeNow)
            ? true
            : false;
          if (isTodaysDate && isTimePast) {
            // dispatch(showToast({text: `Time must be Bigger or Equal to current time for today's date`, duration: 3000}));
            Toast.show('Past time not allowed', Toast.SHORT);
          } else if (
            endTime &&
            moment(new Date(time).setSeconds(0, 0)) >=
              moment(new Date(endTime).setSeconds(0, 0))
          ) {
            // dispatch(showToast({text: 'End Time must be greater than Start Time',duration: 3000}));
            Toast.show('Start Time must be smaller than End Time', Toast.SHORT);
          } else {
            setStartTime(time);
          }
        }}
        onCancel={() => {
          setOpenStartTime(false);
        }}
      />
      <DatePicker
        modal
        // mode="date"
        mode="time"
        open={openEndTime}
        date={date}
        onConfirm={time => {
          setOpenEndTime(false);
          // if end time is before than start time
          const isTodaysDate = moment(moment().format('YYYY-MM-DD')).isSame(
            selectedDates[0],
          )
            ? true
            : false;
          const timeNow = moment();
          const selectedTime = moment(time);
          const isTimePast = moment(selectedTime).isSameOrBefore(timeNow)
            ? true
            : false;
          if (isTodaysDate && isTimePast) {
            // dispatch(showToast({text: `Time must be Bigger or Equal to current time for today's date`, duration: 3000}));
            Toast.show('Past time not allowed', Toast.SHORT);
          } else if (
            moment(new Date(startTime).setSeconds(0, 0)) >=
            moment(new Date(time).setSeconds(0, 0))
          ) {
            // dispatch(showToast({text: 'End Time must be greater than Start Time',duration: 3000}));
            Toast.show('End Time must be greater than Start Time', Toast.SHORT);
          } else {
            setEndTime(time);
          }
        }}
        onCancel={() => {
          setOpenEndTime(false);
        }}
      />
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default ManageAvailability;
