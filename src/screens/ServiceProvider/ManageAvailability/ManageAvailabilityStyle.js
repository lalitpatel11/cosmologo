import {Colors, Constant} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  calendarStyle: {
    padding: 5,
    borderRadius: 10,
    marginVertical: 10,
  },
  middleView: {
    backgroundColor: Colors.WHITE,
    padding: 10,
    borderRadius: 10,
    marginVertical: 5,
  },
  dropdown: {
    height: 50,
    borderColor: Colors.LITE_GREY,
    borderWidth: 0.5,
    borderRadius: 8,
    paddingHorizontal: 8,
  },
  slotView: {
    flexDirection: 'row',
    // justifyContent: 'space-evenly',
    flexWrap: 'wrap',
  },
  slotItemStyle: {
    width: Constant.width / 3 - 30,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 2,
    margin: 5,
  },
});
