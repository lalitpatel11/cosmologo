//react components
import React, {useState, useEffect} from 'react';
import {View, ScrollView} from 'react-native';
//custom components
import MyHeader from 'components/MyHeader/MyHeader';
import AppointmentCard from 'components/AppointmentCard/AppointmentCard';
import DateSelector from 'components/DateSelector/DateSelector';
//styles
import {styles} from './CompleteAppointListingStyle';
//redux
import {connect} from 'react-redux';
import {Service} from 'global/Index';

const CompleteAppointListing = ({userToken}) => {
  //states
  const [completedAppointData, setCompletedAppointData] = useState([]);
  //function : service function
  const getCompletedAppointList = async () => {
    try {
      const resp = await Service.postApiWithToken(
        userToken,
        Service.COMPLETED_APPOINTMENT_LIST,
        {},
      );
      if (resp.data.status) {
        setCompletedAppointData(resp.data.data);
      }
    } catch (error) {
      console.log('error in getCompletedAppointList', error);
    }
  };
  //useEffect
  useEffect(() => {
    getCompletedAppointList();
    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <MyHeader Title="Appointments" />
      <ScrollView style={styles.mainView}>
        <DateSelector />
        {completedAppointData.length > 0 ? <AppointmentCard /> : null}

        <AppointmentCard />
      </ScrollView>
    </View>
  );
};
const mapStateToProps = state => ({
  userToken: state.user.userToken,
});
export default connect(mapStateToProps, null)(CompleteAppointListing);
