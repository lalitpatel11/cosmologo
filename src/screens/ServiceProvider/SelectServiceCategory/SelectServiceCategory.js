//react components
import React, {useState, useEffect} from 'react';
import {View, ScrollView, TouchableOpacity, Image, Alert} from 'react-native';
import {CommonActions} from '@react-navigation/native';
//custom components
import SimpleHeader from 'components/SimpleHeader/SimpleHeader';
import MyText from 'components/MyText/MyText';
import MyButton from 'components/MyButton/MyButton';
import CustomLoader from 'components/CustomLoader/CustomLoader';
//global
import {
  Colors,
  Constant,
  Images,
  MyIcon,
  ScreenNames,
  Service,
} from 'global/Index';
//styles
import {styles} from './SelectServiceCategoryStyle';
//redux
import {useDispatch, useSelector} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {useNetInfo} from "@react-native-community/netinfo";
// third parties
import Toast from 'react-native-simple-toast';

const SelectServiceCategory = ({navigation, route}) => {
  const {isInternetReachable} = useNetInfo();
  //variables : route variables
  const userId = route.params.userId;
  const dispatch = useDispatch();
  //variables
  //variables : redux variables
  const userToken = useSelector(state => state.user.userToken);
  //states
  const [showLoader, setshowLoader] = useState(false);
  const [services, setServices] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  //function : navigation function
  const gotoSubCategory = () =>
    navigation.navigate(ScreenNames.SERVICE_SUB_CATEGORY, {
      selectedCategory,
      categoryiesData: services,
      userId,
    });
  //function : imp function
  const selectItem = item => {
    const index = selectedCategory.findIndex(e => e == item.id);
    if (index > -1) {
      const filterArray = selectedCategory.filter(e => e != item.id);
      setSelectedCategory(filterArray);
    } else {
      setSelectedCategory([...selectedCategory, item.id]);
    }
  };
  const selectSrvcCtgry = () => {
    if (selectedCategory.length > 0) {
      gotoSubCategory();
    } else {
      // dispatch(showToast({text: 'Please select service category'}));
      Toast.show('Please select service category', Toast.SHORT)
      // Alert.alert('', 'Please select service category');
    }
  };
  //function : service function
  const getServiceCategoriesList = async () => {
    setshowLoader(true);
    try {
      const resp = await Service.getApi(Service.GET_SERVICE_CATEGORY);
      if (resp.data.status) {
        setServices(resp.data.data);
        console.log('setcategories',resp.data.data);
        // console.warn(resp.data.data);
      }
    } catch (error) {
      console.log('error in getServiceCategoriesList', error);
    }
    setshowLoader(false);
  };
  const resetIndexGoToNoConnection = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.NO_CONNECTION}],
  });
  useEffect(() => {
    //only for checking when wifi or data is on
    // if (isConnected === null) return;
    // if (!isConnected) {
    //   navigation?.navigate(ScreenNames.NETWORK_ERROR);
    // } else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
    //actual checking of internet reachability
    console.log('isInternetReachable', isInternetReachable);
    if (isInternetReachable === undefined || isInternetReachable === null)
      return;
    if (!isInternetReachable) {
      // navigation.navigate(ScreenNames.NO_CONNECTION);
      navigation.dispatch(resetIndexGoToNoConnection);
    } 
    // else {
    //   if (navigation?.canGoBack()) {
    //     navigation?.goBack();
    //   }
    // }
  }, [isInternetReachable]);
  //useEffect
  useEffect(() => {
    getServiceCategoriesList();
    return () => {};
  }, []);

  //UI
  return (
    <View style={styles.container}>
      <SimpleHeader
        title="Choose the service category"
        backButton={userToken ? true : false}
      />
      <ScrollView
        contentContainerStyle={{paddingBottom: 50}}
        style={styles.mainView}>
        {/* <Image
          resizeMode="contain"
          source={Images.AppIcon}
          style={styles.applogoStyle}
        /> */}
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}>
          {services.map((item, index) => {
            return (
              <TouchableOpacity
                key={item.id.toString()}
                onPress={() => selectItem(item)}
                style={{
                  width: Constant.width / 2 - 40,
                  height: Constant.width / 2 - 50,
                  // height: 200,
                  margin: 10,
                  backgroundColor: Colors.WHITE,
                  borderRadius: 20,
                  borderWidth: selectedCategory.indexOf(item.id) > -1 ? 2 : 0,
                  borderColor: Colors.THEME_GREEN,
                  alignItems:'center',
                  // justifyContent:'center',
                  shadowColor: '#459743',
                  shadowOffset: {width: 0, height: 5},
                  shadowOpacity: 0.8,
                  shadowRadius: 15,
                  elevation: 5,
                }}>
                <Image
                  source={{uri: item.image}}
                  style={{
                    width: 70,
                    height: 70,
                    borderRadius: 35,
                    marginTop:15
                  }}
                />
                <MyText
                  text={item.title}
                  fontFamily="medium"
                  textColor='#3E5869'
                  textAlign='center'
                  numberOfLines={2}
                  fontSize={14}
                  width={'80%'}
                  marginTop={10}
                />
                {selectedCategory.indexOf(item.id) > -1 ? 
                <Image source={require('assets/images/check-circle-service.png')} resizeMode='cover' style={{height:25, width:25,position:'absolute', right:10, top:10}}/>
                :null}
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
      <View style={{flex: 1, justifyContent: 'flex-end', padding: 20}}>
        <MyButton Title="Next" onPress={selectSrvcCtgry} />
      </View>
      <CustomLoader showLoader={showLoader} />
    </View>
  );
};

export default SelectServiceCategory;
