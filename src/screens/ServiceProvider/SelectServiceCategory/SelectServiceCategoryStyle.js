import {Constant} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    padding: 20,
  },
  applogoStyle: {
    width: Constant.width / 1.5,
    height: Constant.width / 2,
    alignSelf: 'center',
  },
});
