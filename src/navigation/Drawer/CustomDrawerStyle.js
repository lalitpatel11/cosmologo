import {Colors} from 'global/Index';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  themeColorBackground: {
    backgroundColor: Colors.THEME_GREEN,
    padding: 20,
    paddingBottom: 100,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
  },
  crossButton: {
    backgroundColor: Colors.WHITE,
    alignSelf: 'flex-end',
    padding: 5,
    borderRadius: 100,
  },
  profileDataView: {
    flexDirection: 'row',
  },
  imageStyle: {
    width: 100,
    height: 100,
    borderRadius: 10,
    backgroundColor:'white'
  },
  editProfileButton: {
    backgroundColor: Colors.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    paddingVertical: 7,
    borderRadius: 5,
  },
  mainView: {
    margin: 20,
    padding: 20,
    borderRadius: 10,
    marginTop: -70,
    backgroundColor: Colors.WHITE,
  },
  bottomSection: {
    flex: 1,
    padding: 20,
    justifyContent: 'flex-end',
  },
  freeConsultationView: {
    borderRadius: 20,
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  numberView: {
    marginLeft: 20,
  },
  logoutVersionView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    justifyContent: 'space-between',
  },
  logoutView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
