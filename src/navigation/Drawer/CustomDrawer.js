//react components
import React, {useState, useCallback, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  Alert,
  Linking
} from 'react-native';
import {CommonActions} from '@react-navigation/core';
import {useFocusEffect} from '@react-navigation/native';
//custom components
import DrawerItem from 'components/DrawerItem/DrawerItem';
import MyText from 'components/MyText/MyText';
import CustomLoader from 'components/CustomLoader/CustomLoader';
import CustomLoaderLogout from 'components/CustomLoader/CustomLoaderLogout';
//third parties
import AsyncStorage from '@react-native-async-storage/async-storage';
//global
import {Colors, Images, MyIcon, ScreenNames, Service} from 'global/Index';
//styles
import {styles} from './CustomDrawerStyle';
//import : modal
import CustomToast from 'modals/CustomToast/CustomToast';
import DeleteAccount from 'modals/DeleteAccount/DeleteAccount';
//redux
import {useDispatch, useSelector} from 'react-redux';
import {logOutUser} from 'src/reduxToolkit/reducer/user';
import {clearCart} from 'src/reduxToolkit/reducer/cart';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
import {setUser, setUserToken, setUserNotifications, setServiceProviderNotifications, setTotalPaymentAmount} from 'src/reduxToolkit/reducer/user';
import {useDrawerStatus} from '@react-navigation/drawer';
import Toast from 'react-native-simple-toast';
// Note: show about us, terms and conditions, cancellation policy, and dispute policty in webview (not <webview/> but actual outside app)
const CustomDrawer = ({navigation}) => {
  //variables
  const resetIndexGoToWelcome = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.WELCOME}],
  });
  const resetIndexGoToSplash = CommonActions.reset({
    index: 1,
    routes: [{name: ScreenNames.SPLASH}],
  });

  //variables : redux variables
  const userInfo = useSelector(state => state.user.userInfo);
  const userToken = useSelector(state => state.user.userToken);
  const dispatch = useDispatch();
  const isDrawerOpen = useDrawerStatus();
  //states
  const [showLoader, setShowLoader] = useState(false);
  const [showLoaderLogout, setShowLoaderLogout] = useState(false);
  const [openDeleteAccountModal, setOpenDeleteAccountModal] = useState(false);
  //function : navigation function
  useEffect(()=>{
    isDrawerOpen === 'open' && checkTokenExpired()
  },[isDrawerOpen])
  const checkTokenExpired = async () => {
    try {
      const resp = await Service.postApiWithToken(
        userToken,
        Service.CHECK_TOKEN_EXPIRY,
        {}
      );
      if (!resp?.data?.status) {
          Toast.show('Please login again', Toast.SHORT)
          closeDrawer();
          await AsyncStorage.clear();
          dispatch(logOutUser());
          navigation.dispatch(gotoWelcome)
      }
    } catch (error) {
      console.log('error in checkTokenExpired', error);
    }
  };
  const checkIfUserActive = async ()=>{
    // if service provider is activated, return
    if(userInfo?.is_active == 1){
      return
    }
    try {
      // console.log('userInfo', userInfo)
      const resp = await Service.getApiWithToken(
        userToken,
        Service.CHECK_USER_STATUS,
        );
      console.log('checkIfUserActive', userToken);
      console.log('checkIfUserActive', resp);
      if (resp.data.status === true) {
        const updatedUserInfo = {...userInfo, is_active:'1'}
        await AsyncStorage.setItem('userInfo', JSON.stringify(updatedUserInfo));
        dispatch(setUser(updatedUserInfo));
      }
    } catch (error) {
      console.log('error in checkUserStatus', error);
    }
  }
  const closeDrawer = async (getStatus = false) => {
    getStatus && checkIfUserActive()
    navigation.closeDrawer();
  }
  const gotoWebPage = async (endPoint, name) => {
    // setShowLoader(true)
    // try {
    //   const myData = new FormData();
    //   myData.append('name', param);
    //   const resp = await Service.postApi(
    //     Number(userInfo?.user_type) === 3 ? Service.SERVICE_LINKS : Service.USER_LINKS,
    //     myData
    //   );
    //   console.log(`${param} resp`, resp);
    //   if (resp.data.status === true) {
    //     Linking.openURL(resp.data.link)
    //   }
    // } catch (error) {
    //   console.log(`error in ${param}`, error);
    // }
    // setShowLoader(false)
    navigation.navigate(ScreenNames.SIDE_MENU_LINKS, {endPoint, name})
  }
  // const gotoAboutUs = () => navigation.navigate(ScreenNames.ABOUT_US);
  const gotoAboutUs = async () => {
    gotoWebPage('about-us', 'About Us')
  }
  const gotoEditProfile = () => {
    if(Number(userInfo?.user_type) === 3){
      navigation.navigate(ScreenNames.SERVICE_EDIT_PROFILE);
    }else if(Number(userInfo?.user_type) === 2){
      navigation.navigate(ScreenNames.USER_EDIT_PROFILE);
    }
  }
  // const gotoTermCondition = () =>
  //   navigation.navigate(ScreenNames.TERM_CONDITION);
  const gotoTermCondition = async () => {
    gotoWebPage('terms-condition', 'Terms And Conditions')
  }  
  const gotoContactUs = () => navigation.navigate(ScreenNames.CONTACT_US);
  // const gotoCancellationPolicy = () =>
  //   navigation.navigate(ScreenNames.CANCELLATION_POLICY);
  const gotoCancellationPolicy = async () => {
    gotoWebPage('cancellation-policy', 'Cancellation Policy')
  }  
  const gotoDisputePolicy = async () => {
    gotoWebPage('dispute-policy', 'Dispute Policy')
  }  
  const gotoWelcome = () =>
    CommonActions.reset({
      index: 1,
      routes: [{name: ScreenNames.WELCOME}],
    });
    // navigation.navigate({
    //   index: 0,
    //   routes: [{name: ScreenNames.WELCOME}],
    // });
  const gotoWelcome2 = () => {
    navigation.navigate(ScreenNames.WELCOME);
  }
  const gotoContactUsScreen = () => {
    navigation.navigate(ScreenNames.CONTACT_US_SCREEN);
  }
  const gotoMyBookings = () => navigation.navigate(ScreenNames.BOOKINGS)
  const gotoUserPayments = () => navigation.navigate(ScreenNames.USER_PAYMENTS)
  //function : service function
  const logoutDirectly = async () => {
    closeDrawer();
    gotoWelcome2();
    dispatch(logOutUser());
    // dispatch(clearCart());
    await AsyncStorage.clear();
  };
  const logout = async () => {
    setShowLoaderLogout(true);
    try {
      const resp = await Service.getApiWithToken(
        userToken,
        Service.USER_LOGOUT,
      );
      if (resp?.data?.status) {
        // don't need to show message after successfully logging out
        // dispatch(showToast({text: resp.data.message}));
        closeDrawer();
        // gotoWelcome();
        navigation.dispatch(gotoWelcome)
        dispatch(logOutUser());
        // dispatch(clearCart());
        await AsyncStorage.clear();
      }
    } catch (error) {
      console.log('error in logout', error);
    }
    setShowLoaderLogout(false);
  };
  const clearStorage = async (deleteAccountMessage) => {
    try {
      const isServiceProvider = Number(userInfo?.user_type) === 3
      await AsyncStorage.setItem('userToken', '');
      await AsyncStorage.setItem('userInfo', JSON.stringify({}));
      dispatch(setUserToken(''));
      dispatch(setUser({}));
      if(isServiceProvider){
        dispatch(setTotalPaymentAmount(0));
        dispatch(setServiceProviderNotifications([]));
      }else{
        dispatch(clearCart());
        dispatch(setUserNotifications([]));
      }
      Toast.show(deleteAccountMessage, Toast.SHORT)
      closeDrawer()
      setOpenDeleteAccountModal(false)
      setShowLoader(false)
      navigation.dispatch(resetIndexGoToWelcome)
    } catch (error) {
      console.log('error in clearning storage after deleteing account', error);
      setOpenDeleteAccountModal(false)
      setShowLoader(false)
    }
  }

  const onDeleteAccount = async () => {
    setShowLoader(true)
    try {
      const endPoint = Number(userInfo?.user_type) === 3 ? Service.SERVICE_DELETE_ACCOUNT : Service.USER_DELETE_ACCOUNT
      const resp = await Service.postApiWithToken(
        userToken, 
        endPoint, 
        {}
      );
      console.log('delete account resp', resp);
      if (resp?.data?.status) {
        // Toast.show(resp?.data?.message, Toast.SHORT)
        clearStorage(resp?.data?.message)
      } else {
        Toast.show(resp?.data?.message, Toast.LONG)
        setShowLoader(false)
        setOpenDeleteAccountModal(false)
      }
    } catch (error) {
      setShowLoader(false)
      setOpenDeleteAccountModal(false)
      console.log('error in deleteAccount', error);
    }
  }
  //UI
  return (
    <View style={styles.container}>
      <CustomToast />
      <ScrollView>
        <View style={styles.themeColorBackground}>
          <TouchableOpacity onPress={()=>closeDrawer(true)} style={styles.crossButton}>
            <MyIcon.Feather name="x" color={Colors.THEME_GREEN} size={24} />
          </TouchableOpacity>
          {Object.keys(userInfo).length > 0 ? (
            <View style={styles.profileDataView}>
              <Image
              source={(userInfo.profile_image == "" || userInfo.profile_image === `${Service.BASE_URL.replace('api/','')}public`) ? require('assets/images/profile_pic.png') : {uri: userInfo.profile_image}}
                style={styles.imageStyle}
              />
              <View style={{marginLeft: 10, justifyContent: 'space-between'}}>
                <MyText
                  text={`${userInfo.first_name} ${userInfo.last_name}`}
                  fontFamily="bold"
                  fontSize={20}
                  textColor="white"
                />
                <MyText text={userInfo.email} textColor="white" />
                <TouchableOpacity
                  onPress={gotoEditProfile}
                  style={styles.editProfileButton}>
                  <MyText text="Edit Profile" textColor="theme_green" />
                </TouchableOpacity>
              </View>
            </View>
          ) : null}
        </View>
        <View style={styles.mainView}>
          {Number(userInfo?.user_type) === 2 ?
          <DrawerItem
            Icon={Images.drawerItemIcon.bookingsIcon}
            Title="Bookings"
            // onPress={gotoContactUs}
            onPress={gotoMyBookings}
          />
          :null}
          <DrawerItem
            Icon={Images.drawerItemIcon.paymentsIcon}
            Title="Payments"
            // onPress={gotoContactUs}
            onPress={gotoUserPayments}
          />
          <DrawerItem
            Icon={Images.drawerItemIcon.infoIcon}
            Title="About Us"
            onPress={gotoAboutUs}
          />
          {/* <DrawerItem Title="Cart" onPress={gotoAboutUs} /> */}
          <DrawerItem
            Icon={Images.drawerItemIcon.termIcon}
            Title="Terms And Conditions"
            onPress={gotoTermCondition}
          />
          <DrawerItem
            Icon={Images.drawerItemIcon.cancelPolicyIcon}
            Title="Cancellation Policy"
            onPress={gotoCancellationPolicy}
          />
          <DrawerItem
            Icon={Images.drawerItemIcon.disputePolicyIcon}
            Title="Dispute Policy"
            onPress={gotoDisputePolicy}
          />
          <DrawerItem
            Icon={Images.drawerItemIcon.contactUsIcon}
            Title="Contact us"
            // onPress={gotoContactUs}
            onPress={gotoContactUsScreen}
          />
          <DrawerItem
            Icon={Images.drawerItemIcon.deleteAccountIcon}
            Title="Delete Account"
            onPress={()=>{setOpenDeleteAccountModal(true)}}
          />
        </View>
        <View style={styles.bottomSection}>
          <ImageBackground source={Images.drawerItemIcon.consultBg}>
            <View style={styles.freeConsultationView}>
              <Image
                resizeMode="contain"
                source={Images.drawerItemIcon.consultIcon}
                style={{height: 50, width: 50}}
              />
              <View style={styles.numberView}>
                <MyText text="Free consultation" fontFamily="medium" />
                <MyText
                  // text="888-888-8888"
                  text="+1 (212) 223-0716"
                  fontSize={20}
                  textColor="theme_green"
                  fontFamily="black"
                  marginTop={10}
                />
              </View>
            </View>
          </ImageBackground>

          <View style={styles.logoutVersionView}>
            <TouchableOpacity onPress={logout} style={styles.logoutView}>
              <MyIcon.AntDesign
                name="logout"
                size={20}
                color={Colors.THEME_GREEN}
              />
              <MyText text="Logout" marginHorizontal={10} />
            </TouchableOpacity>
            <MyText text="App Version - V2.0" textColor="lite_grey" />
          </View>
        </View>
      </ScrollView>
      <DeleteAccount
        visible={openDeleteAccountModal}
        setVisibility={setOpenDeleteAccountModal}
        onDeleteAccount={onDeleteAccount}
      />
      <CustomLoader showLoader={showLoader}/>
      <CustomLoaderLogout showLoader={showLoaderLogout}/>
    </View>
  );
};

export default CustomDrawer;
