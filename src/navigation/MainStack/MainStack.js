//react components
import React from 'react';
//stack
import {createStackNavigator} from '@react-navigation/stack';
//global
import {ScreenNames} from 'global/Index';
//screens
import Splash from 'screens/WelcomeSection/Splash/Splash';
import NoConnection from 'screens/UserSection/NoConnection/NoConnection';
import Welcome from 'screens/WelcomeSection/Welcome/Welcome';
import SignIn from 'screens/UserSection/SignIn/SignIn';
import SignUp from 'screens/UserSection/SignUp/SignUp';
import Home from 'screens/UserSection/Home/Home';
import ServiceHome from 'screens/ServiceProvider/Home/Home';
import UserBottomTab from 'navigation/BottomTab/UserBottomTab/UserBottomTab';
import ForgotPassword from 'screens/UserSection/ForgotPassword/ForgotPassword';
import OtpValidate from 'screens/UserSection/OtpValidate/OtpValidate';
import UserUpcomingAppointment from 'screens/UserSection/UserUpcomingAppointment/UserUpcomingAppointment';
import MobileNumberOtp from 'screens/UserSection/MobileNumberOtp/MobileNumberOtp';
import ChangePassword from 'screens/UserSection/ChangePassword/ChangePassword';
import EditProfile from 'screens/UserSection/EditProfile/EditProfile';
import ProcDetail from 'screens/UserSection/ProcDetail/ProcDetail';
import AddOn from 'screens/UserSection/AddOn/AddOn';
import ServiceBottomTab from 'navigation/BottomTab/ServiceBottomTab/ServiceBottomTab';
import TermCondition from 'screens/WelcomeSection/TermCondition/TermCondition';
import AboutUs from 'screens/WelcomeSection/AboutUs/AboutUs';
import SideMenuLinks from 'screens/WelcomeSection/SideMenuLinks/SideMenuLinks';
import Bookings from 'screens/UserSection/Bookings/Bookings';
import PaymentsMade from 'screens/UserSection/PaymentsMade/PaymentsMade';
import ContactUsScreen from 'screens/UserSection/ContactUs/ContactUs';
import ContactUs from 'screens/WelcomeSection/ContactUs/ContactUs';
import Notication from 'screens/WelcomeSection/Notication/Notication';
import ManageService from 'screens/ServiceProvider/ManageService/ManageService';
import EditService from 'screens/ServiceProvider/EditService/EditService';
import EditSubCategory from 'screens/ServiceProvider/EditSubCategory/EditSubCategory';
import ServiceAvailability from 'screens/ServiceProvider/ServiceAvailability/ServiceAvailability';
import PaymentListing from 'screens/ServiceProvider/PaymentListing/PaymentListing';
import CompleteAppointListing from 'screens/ServiceProvider/CompleteAppointListing/CompleteAppointListing';
import CancellationPolicy from 'screens/WelcomeSection/CancellationPolicy/CancellationPolicy';
import SelectDate from 'screens/UserSection/SelectDate/SelectDate';
import Payment from 'screens/UserSection/Payment/Payment';
import ServiceProviders from 'screens/UserSection/ServiceProviders/ServiceProviders';
import AppointmentDetail from 'screens/UserSection/AppointmentDetail/AppointmentDetail';
import ServiceSignIn from 'screens/ServiceProvider/ServiceSignIn/ServiceSignIn';
import ServiceSignUp from 'screens/ServiceProvider/ServiceSignUp/ServiceSignUp';
import SelectServiceCategory from 'screens/ServiceProvider/SelectServiceCategory/SelectServiceCategory';
import SelectSubCategory from 'screens/ServiceProvider/SelectSubCategory/SelectSubCategory';
import AreaOfServiceability from 'screens/ServiceProvider/AreaOfServiceability/AreaOfServiceability';
import UploadDocument from 'screens/ServiceProvider/UploadDocument/UploadDocument';
import SubCategoryService from 'screens/UserSection/SubCategoryService/SubCategoryService';
import AreaOfConcern from 'screens/UserSection/AreaOfConcern/AreaOfConcern';
import ServiceChangePassword from 'screens/ServiceProvider/ServiceChangePassword/ServiceChangePassword';
import ServiceEditProfile from 'screens/ServiceProvider/ServiceEditProfile/ServiceEditProfile';
import ProceduresList from 'screens/UserSection/ProceduresList/ProceduresList';
import Search from 'screens/UserSection/Search/Search';
import SelectProduct from 'screens/ServiceProvider/SelectProduct/SelectProduct';
import SelectAreaOfConcern from 'screens/ServiceProvider/SelectAreaOfConcern/SelectAreaOfConcern';
import AppointmentListing from 'screens/ServiceProvider/AppointmentListing/AppointmentListing';
import VerifiedAppointmentListing from 'screens/ServiceProvider/VerifiedAppointmentListing/VerifiedAppointmentListing';
import ServiceUpcomingAppointmentListing from 'screens/ServiceProvider/ServiceUpcomingAppointmentListing/ServiceUpcomingAppointmentListing';
import AppointmentDetails from 'screens/ServiceProvider/AppointmentDetails/AppointmentDetails';
import ManageAvailability from 'screens/ServiceProvider/ManageAvailability/ManageAvailability';
import TrackService from 'screens/UserSection/TrackService/TrackService';
import Cart from 'screens/UserSection/Cart/Cart';
import AddAvailableSlot from 'screens/ServiceProvider/AddAvailableSlot/AddAvailableSlot';
import ViewInvoice from 'screens/UserSection/ViewInvoice/ViewInvoice';

const MainStack = () => {
  //variables
  const Stack = createStackNavigator();
  const initialRouteName = ScreenNames.SPLASH;
  const screenOptions = {
    headerShown: false,
  };
  return (
    <Stack.Navigator
      screenOptions={screenOptions}
      initialRouteName={initialRouteName}>
      <Stack.Screen name={ScreenNames.SPLASH} component={Splash} />
      <Stack.Screen name={ScreenNames.WELCOME} component={Welcome} />
      <Stack.Screen name={ScreenNames.USER_SIGNIN} component={SignIn} />
      <Stack.Screen name={ScreenNames.USER_SIGNUP} component={SignUp} />
      <Stack.Screen
        name={ScreenNames.USER_BOTTOM_TAB}
        component={UserBottomTab}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_BOTTOM_TAB}
        component={ServiceBottomTab}
      />
      <Stack.Screen name={ScreenNames.USER_HOME} component={Home} />
      <Stack.Screen
        name={ScreenNames.USER_MOBILE_OTP}
        component={MobileNumberOtp}
      />
      <Stack.Screen
        name={ScreenNames.USER_OTP_VALIDATE}
        component={OtpValidate}
      />
      <Stack.Screen name={ScreenNames.NO_CONNECTION} component={NoConnection} />
      <Stack.Screen
        name={ScreenNames.USER_FORGOT_PASSWORD}
        component={ForgotPassword}
      />
      <Stack.Screen name={ScreenNames.CART} component={Cart} />
      <Stack.Screen
        name={ScreenNames.USER_CHANGE_PASSWORD}
        component={ChangePassword}
      />
      <Stack.Screen
        name={ScreenNames.USER_EDIT_PROFILE}
        component={EditProfile}
      />
      <Stack.Screen
        name={ScreenNames.USER_SUB_CATEGORY}
        component={SubCategoryService}
      />
      <Stack.Screen
        name={ScreenNames.USER_PROC_DETAIL}
        component={ProcDetail}
      />
      <Stack.Screen
        name={ScreenNames.USER_AREAOFCONCERN}
        component={AreaOfConcern}
      />
      <Stack.Screen
        name={ScreenNames.USER_TRACK_SERVICE}
        component={TrackService}
      />
      <Stack.Screen name={ScreenNames.USER_ADD_ON} component={AddOn} />
      <Stack.Screen
        name={ScreenNames.TERM_CONDITION}
        component={TermCondition}
      />
      <Stack.Screen name={ScreenNames.ABOUT_US} component={AboutUs} />
      <Stack.Screen name={ScreenNames.SIDE_MENU_LINKS} component={SideMenuLinks} />
      <Stack.Screen name={ScreenNames.BOOKINGS} component={Bookings} />
      <Stack.Screen name={ScreenNames.USER_PAYMENTS} component={PaymentsMade} />
      <Stack.Screen name={ScreenNames.CONTACT_US_SCREEN} component={ContactUsScreen} />
      <Stack.Screen name={ScreenNames.CONTACT_US} component={ContactUs} />
      <Stack.Screen name={ScreenNames.NOTIFICATION} component={Notication} />
      <Stack.Screen
        name={ScreenNames.SERVICE_MANAGE}
        component={ManageService}
      />
      <Stack.Screen name={ScreenNames.VIEW_INVOICE} component={ViewInvoice} />
      <Stack.Screen
        name={ScreenNames.SERVICE_AVAILABILITY}
        component={ServiceAvailability}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_HOME}
        component={ServiceHome}
      />
      <Stack.Screen name={ScreenNames.SERVICE_EDIT} component={EditService} />
      <Stack.Screen
        name={ScreenNames.SERVICE_EDIT_SUBCATEGORY}
        component={EditSubCategory}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_PAYMENT_LISTING}
        component={PaymentListing}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_CMPLETE_APPOINT_LISTING}
        component={CompleteAppointListing}
      />
      <Stack.Screen
        name={ScreenNames.CANCELLATION_POLICY}
        component={CancellationPolicy}
      />
      <Stack.Screen
        name={ScreenNames.USER_SELECT_DATE}
        component={SelectDate}
      />
      <Stack.Screen name={ScreenNames.USER_PAYMENT} component={Payment} />
      <Stack.Screen
        name={ScreenNames.USER_SERVICE_PROVIDERS}
        component={ServiceProviders}
      />
      <Stack.Screen
        name={ScreenNames.USER_APPOINTMENT_DETAILS}
        component={AppointmentDetail}
      />
      <Stack.Screen
        name={ScreenNames.USER_UPCOMING_APPOINTMENT_LIST}
        component={UserUpcomingAppointment}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_SIGN_IN}
        component={ServiceSignIn}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_SIGN_UP}
        component={ServiceSignUp}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_SELECT_SERVICE}
        component={SelectServiceCategory}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_SUB_CATEGORY}
        component={SelectSubCategory}
      />
      <Stack.Screen
        name={ScreenNames.AREA_OF_SERVICEABILITY}
        component={AreaOfServiceability}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_UPLOAD_DOCUMENTS}
        component={UploadDocument}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_CHANGE_PASSWORD}
        component={ServiceChangePassword}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_EDIT_PROFILE}
        component={ServiceEditProfile}
      />
      <Stack.Screen
        name={ScreenNames.PROCEDURES_LIST}
        component={ProceduresList}
      />
      <Stack.Screen name={ScreenNames.SEARCH} component={Search} />
      <Stack.Screen
        name={ScreenNames.SERVICE_SELECT_PRODUCT}
        component={SelectProduct}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_SELECT_AREAOFCONCERN}
        component={SelectAreaOfConcern}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_APPOINTMENT_LIST}
        component={AppointmentListing}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_VERIFIED_APPOINTMENT_LIST}
        component={VerifiedAppointmentListing}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_UPCOMING_APPOINTMENT_LIST}
        component={ServiceUpcomingAppointmentListing}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_APPOINTMENT_DETAILS}
        component={AppointmentDetails}
      />
      <Stack.Screen
        name={ScreenNames.SERVICE_MANAGE_AVAILABILITY}
        component={ManageAvailability}
      />
      <Stack.Screen
        name={ScreenNames.ADD_AVAILABLE_SLOT}
        component={AddAvailableSlot}
      />
    </Stack.Navigator>
  );
};

export default MainStack;
