//react components
import React from 'react';
import {View} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//navigation
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
//global
import {Colors, MyIcon, ScreenNames} from 'global/Index';
//styles
import {styles} from './ServiceBottomTabStyle';
//screen
import Home from 'screens/ServiceProvider/Home/Home';
import MyAppointment from 'screens/ServiceProvider/MyAppointment/MyAppointment';
import Profile from 'screens/ServiceProvider/Profile/Profile';
//import : redux
import {useSelector, useDispatch} from 'react-redux';
import {showToast} from 'src/reduxToolkit/reducer/customToast';
//third parties
import Toast from 'react-native-simple-toast';

const ServiceBottomTab = () => {
  //variables : redux variables
  const userInfo = useSelector(state => state.user.userInfo);
  const dispatch = useDispatch();
  //variables
  const Tab = createBottomTabNavigator();
  const screenOptions = {
    showLabel: false,
    headerShown: false,
    tabBarShowLabel: false,
    tabBarStyle: styles.navigatorStyle,
  };
  //UI
  return (
    <Tab.Navigator backBehavior="history" screenOptions={screenOptions}>
      <Tab.Screen
        name={ScreenNames.SERVICE_HOME}
        component={Home}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.tabStyle}>
              <MyIcon.Ionicons
                name={focused ? 'home' : 'home-outline'}
                color={focused ? Colors.THEME_GREEN : Colors.LITE_GREY}
                size={24}
              />
              {focused ? (
                <MyText
                  text="Home"
                  fontFamily="black"
                  fontSize={16}
                  textColor="theme_green"
                  marginHorizontal={5}
                />
              ) : null}
            </View>
          ),
        }}
      />
      <Tab.Screen
        name={ScreenNames.SERVICE_APPOINTMENT}
        component={MyAppointment}
        listeners={{
          tabPress: e => {
            // add your conditions here
            if(userInfo?.is_active == 0){
              // dispatch(showToast({text: 'Your account is not approved by administrator.', duration: 1000}));
              Toast.show('Your account is not approved by administrator.', Toast.SHORT);
              e.preventDefault(); // <-- this function blocks navigating to screen
              return
            }
          },
        }}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.tabStyle}>
              <MyIcon.Ionicons
                name={focused ? 'calendar' : 'calendar-outline'}
                color={focused ? Colors.THEME_GREEN : Colors.LITE_GREY}
                size={24}
              />
              {focused ? (
                <MyText
                  text="Appointments"
                  fontFamily="black"
                  fontSize={16}
                  textColor="theme_green"
                  marginHorizontal={5}
                />
              ) : null}
            </View>
          ),
          unmountOnBlur:true
        }}
      />
      <Tab.Screen
        name={ScreenNames.SERVICE_PROFILE}
        component={Profile}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.tabStyle}>
              <MyIcon.Ionicons
                name={focused ? 'person' : 'person-outline'}
                color={focused ? Colors.THEME_GREEN : Colors.LITE_GREY}
                size={24}
              />
              {focused ? (
                <MyText
                  text="Profile"
                  fontFamily="black"
                  fontSize={16}
                  textColor="theme_green"
                  marginHorizontal={5}
                />
              ) : null}
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default ServiceBottomTab;
