//react components
import React from 'react';
import {View, Image} from 'react-native';
//custom components
import MyText from 'components/MyText/MyText';
//Bottom Tab
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
//global
import {Colors, MyIcon, ScreenNames} from 'global/Index';
//styles
import {styles} from './UserBottomTabStyle';
//screens
import Home from 'screens/UserSection/Home/Home';
import Appointment from 'screens/UserSection/Appointment/Appointment';
import Profile from 'screens/UserSection/Profile/Profile';

const UserBottomTab = ({userToken}) => {
  //variables
  const Tab = createBottomTabNavigator();
  const screenOptions = {
    showLabel: false,
    headerShown: false,
    tabBarShowLabel: false,
    tabBarStyle: styles.navigatorStyle,
  };
  // backBehavior = order - return to previous tab (in the order they are shown in the tab bar)
  // backBehavior = history - return to last visited tab
  return (
    <Tab.Navigator backBehavior="history" screenOptions={screenOptions}>
      <Tab.Screen
        name={ScreenNames.USER_HOME}
        component={Home}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.tabStyle}>
              <MyIcon.Ionicons
                name={focused ? 'home' : 'home-outline'}
                color={focused ? Colors.THEME_GREEN : Colors.LITE_GREY}
                size={24}
              />
              {focused ? (
                <MyText
                  text="Home"
                  fontFamily="black"
                  fontSize={16}
                  textColor="theme_green"
                  marginHorizontal={5}
                />
              ) : null}
            </View>
          ),
        }}
      />
      <Tab.Screen
        name={ScreenNames.USER_APPOINTMENT}
        component={Appointment}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.tabStyle}>
              <MyIcon.Ionicons
                name={focused ? 'calendar' : 'calendar-outline'}
                color={focused ? Colors.THEME_GREEN : Colors.LITE_GREY}
                size={24}
              />
              {focused ? (
                <MyText
                  text="Appointments"
                  fontFamily="black"
                  fontSize={16}
                  textColor="theme_green"
                  marginHorizontal={5}
                />
              ) : null}
            </View>
          ),
          unmountOnBlur:true
        }}
      />
      <Tab.Screen
        name={ScreenNames.USER_PROFILE}
        component={Profile}
        options={{
          tabBarIcon: ({focused}) => (
            <View style={styles.tabStyle}>
              <MyIcon.Ionicons
                name={focused ? 'person' : 'person-outline'}
                color={focused ? Colors.THEME_GREEN : Colors.LITE_GREY}
                size={24}
              />
              {focused ? (
                <MyText
                  text="Profile"
                  fontFamily="black"
                  fontSize={16}
                  textColor="theme_green"
                  marginHorizontal={5}
                />
              ) : null}
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default UserBottomTab;
