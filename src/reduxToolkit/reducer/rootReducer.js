import {combineReducers} from 'redux';

// import cart from './cart';
import user from './user';
import cart from './cart';
import customToast from './customToast';
export default combineReducers({
  user: user,
  cart: cart,
  customToast: customToast,
});
