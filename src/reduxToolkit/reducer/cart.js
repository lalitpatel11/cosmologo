import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  cartItems: {},
};
const cart = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    setToCart(state, {payload}) {
      return {
        ...state,
        cartItems: payload,
      };
    },
    addToCart(state, {payload}) {
      return {
        ...state,
        cartItems: payload,
      };
    },
    removeFromCart(state, {payload}) {
      return {
        cartItems: {},
      };
    },
    clearCart(state, {payload}) {
      return {
        cartItems: {},
      };
    },
  },
});

export const {setToCart, addToCart, removeFromCart, clearCart} = cart.actions;
const cartReducer = cart.reducer;

export default cartReducer;
