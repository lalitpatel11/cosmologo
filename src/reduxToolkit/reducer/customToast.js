import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  visible: false,
  text: '',
  duration: 2000,
};
const customToast = createSlice({
  name: 'customToast',
  initialState,
  reducers: {
    showToast(state, {payload}) {
      return {
        ...state,
        visible: true,
        text: payload.text,
        duration: payload.duration ? payload.duration : 2500,
      };
    },
    hideToast(state, {payload}) {
      return {
        ...state,
        visible: false,
        text: '',
      };
    },
  },
});

export const {showToast, hideToast} = customToast.actions;
const customToastReducer = customToast.reducer;

export default customToastReducer;
