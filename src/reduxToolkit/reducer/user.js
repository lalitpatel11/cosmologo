import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  userInfo: {},
  userToken: '',
  totalPaymentAmount: '',
  userNotifications: [],
  serviceProviderNotifications: []
};
const user = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUserToken(state, {payload}) {
      return {
        ...state,
        userToken: payload,
      };
    },
    setUser(state, {payload}) {
      return {
        ...state,
        userInfo: payload,
      };
    },
    setTotalPaymentAmount(state, {payload}) {
      return {
        ...state,
        totalPaymentAmount: payload,
      };
    },
    setUserNotifications(state, {payload}) {
      return {
        ...state,
        userNotifications: payload,
      };
    },
    setServiceProviderNotifications(state, {payload}) {
      return {
        ...state,
        serviceProviderNotifications: payload,
      };
    },
    logOutUser(state, {payload}) {
      return {
        userInfo: {},
        userToken: '',
      };
    },
  },
});

export const {setUserToken, setUser, setTotalPaymentAmount, setUserNotifications, setServiceProviderNotifications, logOutUser} = user.actions;
const userReducer = user.reducer;

export default userReducer;
// const user = (state = initialState, action) => {
//   const {type, payload} = action;
//   switch (type) {
//     case UserActionType.SET_USER:
//       return {
//         ...state,
//         userInfo: payload,
//       };
//     case UserActionType.SET_USER_TOKEN:
//       return {
//         ...state,
//         userToken: payload,
//       };
//     case UserActionType.LOGOUT_USER:
//       return {
//         userInfo: {},
//         shippingAddress: {},
//         billingAddress: {},
//         userToken: '',
//       };
//     case UserActionType.SET_SHIPPING_ADDRESS:
//       return {
//         ...state,
//         shippingAddress: payload,
//       };
//     case UserActionType.SET_BILLING_ADDRESS:
//       return {
//         ...state,
//         billingAddress: payload,
//       };
//     default:
//       return state;
//   }
// };

// export default user;
